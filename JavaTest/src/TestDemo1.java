/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-17
 * Time:17:04
 */
class Test{
//    private static int a = 10;
//    public static int b;
    // 类中成员变量处于堆上,即使不进行初始化,堆也会帮助进行初始化为默认值;
    static String a1;
    static int b1;

    void print() {
        String t1;
        int t2;
        // 类中局部变量,不论是引用类型还是基础类型,在定义的时候都需要进行初始化;
        // 局部变量处于栈上,不能够不进行初始化;
//        System.out.println(t1);
//        System.out.println(t2);
    }
}
public class TestDemo1 {
    static String a2;
    static int b2;
    public static void main(String[] args) {
//        //private类中访问权限;
//        System.out.println(Test.a);
//        //public全局访问权限;
//        System.out.println(Test.b);
        String a3;
        int b3;
        //类外的静态成员变量,还是处于堆上,堆帮助进行初始化为默认值;
        System.out.println(Test.a1);
        System.out.println(Test.b1);
        // 类中成员变量,处于堆上,有默认初始化;
        System.out.println(a2);
        System.out.println(b2);
        // 类中方法里面的局部变量,处于栈中,没有默认初始化;
//        System.out.println(a3);
//        System.out.println(b3);
    }
}
