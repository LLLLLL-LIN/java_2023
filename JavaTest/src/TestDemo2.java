/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-17
 * Time:17:54
 */
// 静态代码块>构造代码块>局部代码块(方法内部,限定变量的作用范围以及生命周期,尽快释放资源,从而提升内存的利用率);
class Test2 {
    static int a;
    static String b;
    // 静态代码块的执行是在类加载到JVM的时候就执行了,并且只执行一次;
    // 但是不是说只有第一次 new 的对象才会有这个初始化,而是说相当于在定义的时候就显示的进行了初始化;
    // 后续的 new 对象的变量的值还是静态代码块对类初始化的值;虽然静态的变量和对象不属于实例;
    static {
        a = 1;
        b = "abc";
    }
}

class Test3 {
    int t1;
    String t2;
    Test3() {
        t1 = 2;
        t2 = "b";
    }

    {
        t1 = 1;
        t2 = "a";
    }

    void print() {
        // 即使构造代码块在构造函数之后,但是成员变量的值还是发生了改变;
        // 构造代码块的执行顺序在构造函数之前;
        System.out.println(t1);
        System.out.println(t2);
    }
}

public class TestDemo2 {
    public static void main(String[] args) {
//        Test2 test1 = new Test2();
//        Test2 test2 = new Test2();
//        System.out.println(test1.a);
//        System.out.println(test1.b);
//        System.out.println(test2.a);
//        System.out.println(test2.b);
        Test3 test3 = new Test3();
        test3.print();
    }
}
