package controller;

import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-15
 * Time:下午5:10
 */
@WebServlet("/blogDelete")
public class BlogDeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        //检测登录状态;
        if(session == null) {
            resp.setContentType("text/html; charset = utf8");
            resp.getWriter().write("未登录状态,无法删除!");
            return;
        }
        User user = (User)session.getAttribute("user");
        if(user == null) {
            resp.setContentType("text/html; charset = utf8");
            resp.getWriter().write("未登录状态,无法删除!");
            return;
        }
        //获取要删除的blodId;
        String blogId = req.getParameter("blogId");
        if(blogId == null || "".equals(blogId)) {
            resp.setContentType("text/html; charset = utf8");
            resp.getWriter().write("blog参数不对!");
            return;
        }
        //获取要删除的博客信息;
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(Integer.parseInt(blogId));
        if(blog == null) {
            resp.setContentType("text/html; charset = utf8");
            resp.getWriter().write("你要删除的博客不存在!");
            return;
        }
        //检验用户是否是被删除的博客的作者;
        if(user.getUserId() != blog.getUserId()) {
            resp.setContentType("text/html; charset = utf8");
            resp.getWriter().write("不是博客作者,无法删除!");
            return;
        }
        blogDao.delete(Integer.parseInt(blogId));
        resp.sendRedirect("blog_list.html");
    }
}
