import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-08
 * Time:14:05
 */
public class TestDemo2 {
    public static void main(String[] args) {
        String s = "abcdef";
        charAtReverse(s);
        reverse(s);
    }

    public static void reverse(String str) {
        StringBuilder s = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            stack.push(str.charAt(i));
        }
        for (int i = 0; i < str.length(); i++) {
            s.append(stack.pop());
        }
        System.out.println(s);
    }

    public static void charAtReverse(String s) {
        int length = s.length();
        String reverse = " ";
        for (int i = 0; i < length; i++) {
            reverse = s.charAt(i) + reverse;//字符串中获取单个字符的字符的放法
        }
        System.out.println(reverse);
    }
}
