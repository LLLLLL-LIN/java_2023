/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-10
 * Time:12:30
 */
public class TestDemo5 {
    static String type;
    static String a;
    static String b;
//    public TestDemo5() {
//        type = "null";
//    }

    public TestDemo5(int val) {
        type = "int";
    }

    public TestDemo5(String str) {
        type = "String";
    }

    // 构成重载;构造函数中可以通过this()调用其他的构造函数,根据参数的类型和个数;构成重载;
    public TestDemo5(int x, int y) {
        a = "int a";
        b  = "int b";
    }

    public TestDemo5(String a, String b) {
       a = "String a";
       b = "String b";
    }

    public static void main(String[] args) {
        // 这里的 new TestDemo5() ;()中的参数类型,参数个数,就决定了调用类中的哪个构造参数,不是new之后类中按顺序调用哪个构造函数;
        new TestDemo5(12);
        print();
    }
    static void print() {
        // 调用哪个构造参数,就更改对应的变量的值;
        System.out.println(a);
        System.out.println(b);
        System.out.println(type);
    }
}
