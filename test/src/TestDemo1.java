/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-10
 * Time:11:55
 */

public class TestDemo1 {
    int a = 1;
    public TestDemo1() {
        // this能在构造方法中调用其他的的构造函数,根据参数的类型和个数;
        this(12);
        System.out.println("没有参数");
    }

    public TestDemo1(int val) {
        this("abc");
        System.out.println("int参数");
    }

    public TestDemo1(String str) {
        this(12, 13);
        System.out.println("String参数");
    }

    // 构成重载;构造函数中可以通过this()调用其他的构造函数,根据参数的类型和个数;构成重载;
    public TestDemo1(int a, int b) {
        this("String a" , "String b");
        System.out.println("int a = "+ a);
        System.out.println("两个int参数");
    }

    // 这个之所以不执行是因为,没人调用,无参构造函数是 new 的时候调用的,
    // 前面的其他的是构造函数中 this()调用的,这个没人调用就不执行;
    public TestDemo1(String a, String b) {
        System.out.println("String b = " + b);
        System.out.println("两个String参数");
    }

    public static void main(String[] args) {
        new TestDemo1();
//         //main方法也是能通过类名.main调用,这里栈溢出了;
//        String[] strings = new String[3];
//        Test1.main(strings);
    }
}
