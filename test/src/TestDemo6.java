/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-10
 * Time:12:30
 */
public class TestDemo6 {
   String type;
   // 静态变量是存在方法区中;
   static int a;
    public TestDemo6() {
//        this("String");
//        type = "null";
    }

    public TestDemo6(String val) {
        this('c');
        type = val;
    }

    public TestDemo6(char a) {
        type = "char";
    }
    public static void main(String[] args) {
        TestDemo6 testDemo6 = new TestDemo6();
        // 构造函数的调用,type一直在改变;
        System.out.println(testDemo6.type);
        System.out.println(TestDemo6.a);
    }
}
