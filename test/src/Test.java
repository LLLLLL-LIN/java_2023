/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-08
 * Time:10:15
 */
class ListNode {
    int val;
    ListNode next;
    public ListNode(int val){
        this.val = val;
    }
}
public class Test {
    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        ListNode head = listNode1;
        System.out.println("反转之前");
        display(head);
        reverse(head);
        System.out.println("反转之后");
        display(listNode5);
    }
    public static void reverse(ListNode head) {
        if(head == null) {
            return;
        }
        ListNode prev = null;
        ListNode cur = head;
        ListNode curNext =  null;
        while(cur != null) {
//            cur.next = prev;
//            cur = curNext;
//            prev = cur;
//            curNext = cur.next;
            curNext = cur.next;
            // 第一步已经将 head 节点的 next 置为空了, 后续不需要单独对 head.next 置空;
            cur.next = prev;
            prev = cur;
            cur = curNext;
        }
    }
    public static void display(ListNode head) {
        if(head == null) {
            return;
        }
        ListNode cur = head;
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        System.out.println();
    }
}
