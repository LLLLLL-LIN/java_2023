import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-09
 * Time:10:34
 */
class A{
    static int val;
}

public class TestDemo3 {
    static int a;
    static String s1;
    static Integer i1;
    static int[] array1 = new int[3];
    public static void main(String[] args) {
//        String s = 10 + "abcdef";
//        String s2 = "字符串";
//        // 不管是字符串在前还是在后,只要是 + 操作中有字符串,那就是进行字符串的拼接操作,不会报错;
//        System.out.println(10 + "abcdef");
//        System.out.println(s);
//        // js 字符长度是属性,这里是方法,但是单位都是字符个数;
//        System.out.println(s.length());
//        System.out.println(s2.length());
//        int b;
//        String c;
//        Character a;
//         //在Java中 未初始化的变量定义的时候不会报错,但是打印的时候就会报, 不管是引用类型还是还是基础类型或者是包装类型;
//        System.out.println(b);
//        System.out.println(c);
//        System.out.println(a);
//        // 创建数组的几个方法;
//        // 1. 动态初始化;
//        int[] array = new int[4];
//        // 2. 静态初始化;
//        int[] array2 = new int[] {1,2,3,4};
//        // 2.1 静态初始化的另一种格式,不用写数组大小,给定数组中元素的时候就知道了;
//        int[] array3 = {1,2,3,4,};
//        ArrayList<Integer> arrayList = new ArrayList<>(5);
//        arrayList.add(1);
//        arrayList.add(2);
//        arrayList.add(3);
//        arrayList.add(4);
//        arrayList.add(5);
//        System.out.println(arrayList.toString());
//        // 外部类的属性未初始化,和类中方法属性即是成员属性未初始化不一样;后者打印会出错;
//        int B;
//        System.out.println(B);
//        System.out.println(A.val);
//        System.out.println(a);
        // 存储在堆中的元素会自动初始化为默认值,而局部变量是存储在栈中,没有默认初始化,这时候使用就会报错;
        // 所有类型都是同理;引用类型,基础类型,
        String s2;
        Integer i2;
        int[] array2 = new int[3];
//        System.out.println(s1);
//        System.out.println(s2 + "adc");
        System.out.println(i1);
//        System.out.println(i2);
        // 数组这有点特殊,成员变量的数组,局部变量的数组都能未初始化的时候使用,默认初始化为0;
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
}
