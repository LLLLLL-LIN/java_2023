package listnode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-29
 * Time:下午4:42
 */
public class RunList {
    public static void main(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.createList();
//        System.out.println("===========打印节点========");
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("========链表长度=======");
//        System.out.println(myLinkedList.size());
//        System.out.println("===========清空链表,计算链表长度===========");
//        myLinkedList.clear();
//        System.out.println(myLinkedList.size());
//        System.out.println("============查找链表是否包含关键字===========");
//        System.out.println(myLinkedList.contains(12));
//        System.out.println("============头插法===========");
//        myLinkedList.addFirst(10);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("============尾插法===========");
//        myLinkedList.addLast(67);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("=============在index位置插入节点=============");
//        myLinkedList.addIndex(2, 20);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("================删除第一次出现的key==================");
//        myLinkedList.remove(20);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("===================删除所有的key节点=====================");
//        myLinkedList.addIndex(4, 12);
//        myLinkedList.display();
//        System.out.println();
//        myLinkedList.removeAllKey(12);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("==================反转链表================");
//        myLinkedList.reverse();
//        System.out.println();
//        myLinkedList.display();
//        myLinkedList.addIndex(3, 22);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("============中间节点=============");
//        System.out.println(myLinkedList.midNode().val);
//        System.out.println("=============找到第 k 个节点==============");
//        System.out.println(myLinkedList.findKthToTail(2).val);
//        System.out.println("============根据 X 值分割链表===========");
//        myLinkedList.addIndex(3,25);
//        myLinkedList.addIndex(5,29);
//        myLinkedList.addIndex(1,55);
//        System.out.println("开始的链表!");
//        myLinkedList.display();
//        System.out.println();
//        System.out.println("按 X 值分割的链表!");
//        ListNode newHead = myLinkedList.partition(33);
//        myLinkedList.display2(newHead);
//        System.out.println("==========判断是否是回文链表===========");
//        myLinkedList.addIndex(2,21);
//        myLinkedList.display();
//        System.out.println();
//        System.out.println(myLinkedList.chkPalindrome());
//        System.out.println("================判断链表是否有环===============");
//        System.out.println(myLinkedList.hasCycle());
//        System.out.println("创建环");
//        myLinkedList.createLoop();
//        System.out.println(myLinkedList.hasCycle());
        System.out.println("===========找到链表的环入口==============");
        System.out.println("链表是否有环");
        System.out.println(myLinkedList.hasCycle());
        System.out.println("创建环");
        myLinkedList.createLoop();
        System.out.println("创建环后是否有环,打印节点的值");
        System.out.println("节点地址: "+ myLinkedList.detectCycle().toString());
        System.out.println(myLinkedList.detectCycle().val);
    }
}
