package listnode;

// 单向不带头链表的实现;
class ListNode {
    ListNode next;
    int val;

    public ListNode(int val) {
        this.val = val;
    }
}

public class MyLinkedList {
    // 成员变量,实例化对象的时候就出现了;
    ListNode head;

    // 创建链表;
    public void createList() {
        ListNode listNode1 = new ListNode(12);
        ListNode listNode2 = new ListNode(23);
        ListNode listNode3 = new ListNode(34);
        ListNode listNode4 = new ListNode(23);
        ListNode listNode5 = new ListNode(12);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        this.head = listNode1;
    }


    // 头插法
    public void addFirst(int data) {
        ListNode newHead = new ListNode(data);
        if (head == null) {
            head = newHead;
            return;
        }
        newHead.next = this.head;
        this.head = newHead;
    }

    // 尾插法
    public void addLast(int data) {
        ListNode newLast = new ListNode(data);
        if (head == null) {
            head = newLast;
        }
        // 非空,在链表末尾插入节点(new List Node(data));
        ListNode cur = this.head;
        while (cur.next != null) {
            cur = cur.next;
        }
        cur.next = newLast;
    }

    // 找到 index 位置的前驱节点,并返回;
    public ListNode findNode(int index) {
        ListNode cur = this.head;
        while (index - 1 > 0) {
            cur = cur.next;
            index--;
        }
        return cur;
    }

    // 任意位置插入,第一个数据节点为0号下标;
    public boolean addIndex(int index, int data) {
        if (index < 0 || index > size()) {
            System.out.println("pos位置不合理!");
            return false;
        }
        if (index == 0) {
            // 头插;
            ListNode newHead = new ListNode(data);
            newHead.next = head;
            this.head = newHead;
            return true;
        }
        if (index == size()) {
            // 尾插;
            ListNode newLast = new ListNode(data);
            ListNode cur = this.head;
            while (cur.next != null) {
                cur = cur.next;
            }
            cur.next = newLast;
            return true;
        }
        ListNode prevNode = findNode(index);
        ListNode addNode = new ListNode(data);
        addNode.next = prevNode.next;
        prevNode.next = addNode;
        return true;
    }

    // 查找是否包含关键字key是否在单链表当中
    public boolean contains(int key) {
        if (head == null) {
            System.out.println("链表为空!");
            return false;
        }
        ListNode cur = this.head;
        while (cur != null) {
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    // 删除第一次出现关键字为key的节点
    public void remove(int key) {
        if (head == null) {
            System.out.println("链表为空! 无法删除!");
            return;
        }
        if (head.val == key) {
            ListNode cur = this.head.next;
            head.next = null;
            head = cur;
            return;
        }
        ListNode cur = this.head;
        //定义一个计数器, 找到要删除的节点位置,调用前面的 findNode 方法找到节点的前驱;
        int count = 0;
        while (cur != null) {
            count++;
            if (cur.val == key) {
                // 找节点下标, 调用方法, 找到前驱位置;
                //这里的 count 注意一下因为在这里是 cur 还没到 cur.next 就已经 +1 了 ,
                // 所以在传参的时候注意 -1, 否则返回的节点还是需要删除的节点, 不是前驱节点;
                ListNode prevNode = findNode(count - 1);
                // 使前驱节点的 next 为要删除节点的后驱节点;
                prevNode.next = cur.next;
                return;
            }
            cur = cur.next;
        }
        System.out.println("没有要删除的key节点!");
    }

    // 删除所有值为key的节点
    public void removeAllKey(int key) {
        if (head == null) {
            return;
        }
        ListNode prev = this.head;
        ListNode cur = prev.next;
        while (cur != null) {
            if (cur.val == key) {
                prev.next = cur.next;
                cur = cur.next;
            } else {
                prev = cur;
                cur = cur.next;
            }
        }
        if (head.val == key) {
            ListNode headNext = this.head.next;
            head.next = null;
            head = headNext;
        }
    }

    // 得到单链表的长度
    public int size() {
        ListNode cur = this.head;
        int count = 0;
        while (cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    // 打印链表;
    public void display() {
        ListNode cur = this.head;
        if (head == null) {
            return;
        }
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
    }

    // 清空链表;节点的引用置为空;  最简单的是 this.head = null;
    public void clear() {
        ListNode cur = this.head;
        ListNode curNext;
        while (cur != null) {
            curNext = cur.next;
            cur.next = null;
            cur = curNext;
        }
        head = null;
//        public void clear() {
////        ListNode cur = this.head;
//        ListNode curNext;
//        while (head != null) {
//            curNext = head.next;
//            head.next = null;
//            head = curNext;
//        }
    }

    //反转链表
    public void reverse() {
        if (head == null) {
            System.out.println("链表为空!");
            return;
        }
        ListNode prev = null;
        ListNode cur = this.head;
        ListNode curNext;
        // curNext 记录 cur.next 在 cur 反转指向前面的节点之后, cur = curNext ,重新循环指向前一个节点;
        while (cur != null) {
            curNext = cur.next;
            // 第一步已经将 head 节点的 next 置为空了, 后续不需要单独对 head.next 置空;
            cur.next = prev;
            prev = cur;
            cur = curNext;
        }
        // head 节点引用原节点的最后一个节点;
//        this.head = prev;
        display2(prev);
    }

    // 根据传入的节点打印;
    public void display2(ListNode listNode) {
        if (listNode == null) {
            System.out.println("传入的节点为空!");
        }
        while (listNode != null) {
            System.out.print(listNode.val + " ");
            listNode = listNode.next;
        }
    }

    // 返回链表的中间节点;
    public ListNode midNode() {
        if (head == null) {
            return null;
        }
        ListNode fastCur = this.head;
        ListNode slowCur = this.head;
        // fastCur 的路程是 slowCur 的两倍;
        //所以当 fastCur 到 null 的时候, slowCur 就为中间路程, 为中间节点;
        while (fastCur != null && fastCur.next != null) {
            fastCur = fastCur.next.next;
            slowCur = slowCur.next;
        }
        return slowCur;
    }

    // 找到给定的倒数第 k 个节点;
    public ListNode findKthToTail(int k) {
        if (k <= 0 || head == null) {
            return null;
        }
        ListNode fastCur = this.head;
        ListNode slowCur = this.head;
        while (k - 1 > 0) {
            if (fastCur == null) {
                return null;
            }
            fastCur = fastCur.next;
            k--;
        }
        while (fastCur.next != null) {
            fastCur = fastCur.next;
            slowCur = slowCur.next;
        }
        return slowCur;
    }

    // 合并两个有序链表;
    public ListNode mergeTwoLists(ListNode headA, ListNode headB) {
        // 这里的其实不用, 后面的 if 的条件能处理, 即使两个都为空, tmp 拼接 null ,返回的也是null;
//        if (headA == null && headB == null) {
//            System.out.println("链表为空!");
//            return null;
//        }
        // 这个放到后面判定就行;
//        if (headA == null) {
//            return headB;
//        }
//        if (headB == null) {
//            return headA;
//        }
        ListNode newHead = new ListNode(-1);
        ListNode tmp = newHead;
        // 哪个链表的头节点大, tmp 的 next 就为谁;
        while (headA != null && headB != null) {
            if (headA.val < headB.val) {
                tmp.next = headA;
                headA = headA.next;
                tmp = tmp.next;
            } else {
                tmp.next = headB;
                headB = headB.next;
                tmp = tmp.next;
            }
        }
        // headA 为空,将 headB 全部拼接;
        if (headA == null) {
            tmp.next = headB;
        }
        // headB 为空,将 headA 全部拼接;
        if (headB == null) {
            tmp.next = headA;
        }
        return newHead.next;
    }

    // 根据 X 值,分割链表;
    public ListNode partition(int x) {
        ListNode bs = null;
        ListNode be = null;
        ListNode as = null;
        ListNode ae = null;
        ListNode cur = this.head;
        while (cur != null) {
            // 小于 X 的节点放到 bs 链表;
            if (cur.val < x) {
                if (bs == null) {
                    bs = cur;
                    be = cur;
                } else {
                    be.next = cur;
                    be = be.next;
                }
            } else {
                //大于 X 的值放到 as 链表;
                if (as == null) {
                    as = cur;
                    ae = cur;
                } else {
                    ae.next = cur;
                    ae = ae.next;
                }
            }
            cur = cur.next;
        }
        // bs 链表为空,直接返回 as 链表;
        if (bs == null) {
            return as;
        }
        // 如果 as 链表不为空, as 链表的最后一个节点 ae 的 next 要置为空;
        if (as != null) {
            ae.next = null;
        }
        // 将两个链表连起来;
        be.next = as;
        // 返回合并之后链表的头节点;
        return bs;
    }

    // 删除有序列表的重复数值节点;
    public ListNode deleteDuplication() {
        ListNode cur = head;
        ListNode newHead = new ListNode(-1);
        ListNode tmp = newHead;
        while (cur != null) {
            if (cur.next != null && cur.val != cur.next.val) {
                while (cur.next != null && cur.val != cur.next.val) {
                    cur = cur.next;
                }
                // 这里是 cur.val 和 cur.next.val 不一样,所以还得 .next 走到数值不一样的节点,进行新一轮的判定 cur.val 和 cur.next.val;
                cur = cur.next;
            } else {
                // cur.val 和 cur.next.val 的值不一样,则让前驱节点的 next 指向该节点;
                tmp.next = cur;
                tmp = tmp.next;
                cur = cur.next;
            }
        }
        // 最后一个节点的 next 置为空;
        tmp.next = null;
        return newHead.next;
    }

    // 判断是否是回文链表;
    public boolean chkPalindrome() {
        if (head == null) {
            return true;
        }
        // 快慢指针,找到中间节点;
        ListNode fast = this.head;
        ListNode slow = this.head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode cur = slow.next;
        ListNode curNext = null;
        // 根据中间节点,进行链表的反转;
        while (cur != null) {
            curNext = cur.next;
            cur.next = slow;
            slow = cur;
            cur = curNext;
        }
        ListNode newHead = this.head;
        // 进行回文判断;
        while (newHead != slow) {
            // 如果 slow.val != newHead.val, 直接return false;
            if (newHead.val != slow.val) {
                return false;
            }
            if (newHead.next == slow) {
                return true;
            }
            newHead = newHead.next;
            slow = slow.next;
        }
        return true;
    }

    // 判断链表是否相交;
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        // 定义 pl, ps,来遍历链表, lenA, lenB 分别记录两个链表的长度;
        ListNode pl = headA;
        ListNode ps = headB;
        int lenA = 0;
        int lenB = 0;
        while (pl != null) {
            lenA++;
            pl = pl.next;
        }
        pl = headA;
        while(ps != null) {
            lenB++;
            ps = ps.next;
        }
        ps = headB;
        int len = lenA - lenB;
        if(len < 0) {
            ps = headA;
            pl = headB;
            len = lenB - lenA;
        }
        // pl 总是指向最长的那个链表, pl 走差值步;
        while(len > 0) {
            pl = pl.next;
            len--;
        }
        // 走完差值步之后,两个链表一起走,看遍历到 null 之前是否有相同的节点,即相交节点;
        while(pl != ps) {
             //如果遍历到 null 就直接返回;
            if(pl == null || ps == null) {
                return null;
            }
            pl = pl.next;
            ps = ps.next;
        }
        // 返回相交节点,如果没相交节点,这里就返回 null;
        return pl;
    }

    // 判断链表是否有环;
    public boolean hasCycle() {
        if(head == null ) {
            return false;
        }
        // 操场跑步套圈问题;有速度差值,只要链表有环,就一定会出现套圈相遇;
        ListNode fast = this.head;
        ListNode slow = this.head;
        while(fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            // 相遇返回 true;
            if(fast == slow) {
                return true;
            }
        }
        // 链表没有环,fast 为 null, 返回 false;
        return false;
    }

    // 创建链表的环;
    public void createLoop() {
        if (head == null) {
            return;
        }
        ListNode cur = this.head;
        while(cur.next != null) {
            // 遍历找到最后一个节点;
            cur = cur.next;
        }
        // 让最后一个节点的 next 执行链表的的第三个节点;
        cur.next = this.head.next.next;
    }

    // 找到链表的入环点;
    public ListNode detectCycle() {
        if(head == null) {
            return null;
        }
        ListNode fast = head;
        ListNode slow = head;
        // 找到相遇节点,相遇节点到环入口节点和 head 节点到环入口节点是一样的距离;
        while(fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            if(fast == slow) {
                break;
            }
        }
        if(fast == null || fast.next == null) {
            return null;
        }
        fast = head;
        // 相遇节点和 head 节点一起遍历,相遇的节点就是环入口节点;
        while(fast != slow) {
            fast = fast.next;
            slow = slow.next;
        }
        return fast;
    }
}


//    //找到 pos-1 位置的Node节点,用来在pos位置添加节点;
//    public ListNode findNode(int pos) {
//        ListNode cur = this.head;
//        while( pos != 0) {
//            cur = cur.next;
//            pos--;
//        }
//        return cur;
//    }
//
//    // 在 pos 位置新增元素
//    public void add(int pos, int data) {
//        if (pos < 0 || pos > size()) {
//            System.out.println("pos位置不合理!");
//            return;
//        }
//        if (pos == 0) {
//            //头插;
//            ListNode newHead = new ListNode(data);
//            newHead.next = head;
//            this.head = newHead;
//            return;
//        }
//        if (pos == size()) {
//            //尾插;
//            ListNode newLast = new ListNode(data);
//            ListNode cur = this.head;
//            while (cur.next != null) {
//                cur = cur.next;
//            }
//            cur.next = newLast;
//            return;
//        }
//        ListNode prevNode = findNode(pos);
//        ListNode addNode = new ListNode(data);
//        addNode.next = prevNode.next;
//        prevNode.next = addNode;
//    }
//
//    // 判定是否包含某个元素
//    public boolean contains(int toFind) {
//        if(head == null) {
//            System.out.println("链表为空!");
//            return false;
//        }
//        ListNode cur =  this.head;
//        while(cur != null) {
//            if(cur.val == toFind){
//                return true;
//            }
//            cur = cur.next;
//        }
//        return false;
//    }
//
//    // 查找某个元素对应的位置
//    public int search(int toFind) {
//        if(head == null) {
//            System.out.println("链表为空!");
//            return -1;
//        }
//        int count = 0;
//        ListNode cur =  this.head;
//        while(cur != null) {
//            if(cur.val == toFind){
//                return count;
//            }
//            count++;
//            cur = cur.next;
//        }
//        return -1;
//    }
//
//    // 获取 pos 位置的元素
//    public int getPos(int pos) {
//        if(head == null) {
//
//        }
//    }
//
//    // 给 pos 位置的元素设为 value
//    public void setPos(int pos, int value) {
//    }
//
//    //删除第一次出现的关键字key
//    public void remove(int toRemove) {
//    }
//
//    // 获取顺序表长度
//    public int size() {
//        int count = 0;
//        ListNode cur = this.head;
//        while (cur != null) {
//            count++;
//            cur = cur.next;
//        }
//        return count;
//    }
//
//    // 清空顺序表
//    public void clear() {
//        ListNode cur = this.head;
//        ListNode curNext;
//        while(cur != null) {
//            curNext = cur.next;
//            cur = null;
//            cur = curNext;
//        }
//    }
