import java.util.Map;
import java.util.Random;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-25
 * Time:上午9:51
 */

//定义一个数组，随机生成值，使用HashMap输出每个值和对应的重复次数；
public class HashMap {
    public static void main(String[] args) {
        int[] array = new int[20];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
        }
        Map<Integer, Integer> map = func1(array);
        System.out.println(map);
    }
    public static Map<Integer, Integer> func1(int[] array) {
        Map<Integer, Integer> hashMap = new java.util.HashMap<>();
        for (int i = 0; i < array.length; i++) {
            if(hashMap.containsKey(array[i])) {
                int val = hashMap.get(array[i]);
                hashMap.put(array[i],val + 1 );
            }else{
                hashMap.put(array[i], 1 );
            }
        }
        return hashMap;
    }
}
