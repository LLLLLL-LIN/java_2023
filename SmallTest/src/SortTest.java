/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIiaolin
 * Date:2023-03-24
 * Time:下午3:12
 * <p>
 * 排序复习；
 * <p>
 * 排序复习；
 * <p>
 * 排序复习；
 * <p>
 * 排序复习；
 * <p>
 * 排序复习；
 */
/**
 * 排序复习；
 */


import java.util.Arrays;


/**
 * 时间复杂度：O(N^1.3 - 1.5);
 * 空间复杂度：O(1);
 * 不稳定
 */
//实现希尔排序对数组进行排序，打印排序之后的数组；
public class SortTest {
    static void shellSort(int[] array, int gap) {
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (; j >= 0; j = j - gap) {
                if (array[j] > tmp) {
                    array[j + gap] = array[j];
                } else {
                    break;
                }
            }
            array[j + gap] = tmp;
        }
    }

    //确定gap；
    static void gapNum1(int[] array) {
        int gap = array.length;
        while (gap > 1) {
            shellSort(array, gap);
            gap = gap / 2;
        }
        shellSort(array, 1);
    }

    static void gapNum2(int[] array) {
        int gap = 5;
        while (gap > 0) {
            shellSort(array, gap);
            gap = gap - 2;
        }
    }

    /**
     * 时间复杂度：O(N^2);
     * 空间复杂度：O(1);
     * 稳定；
     */
//实现插入排序，方法内打印；
    static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            int tmp = array[i];
            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    /**
     * 时间复杂度：O(N^2);
     * 空间复杂度：O(1);
     * 稳定性：不稳定的排序；
     */
    //实现选择排序，对数组进行排序；(未优化！)
    static void selectSort1(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int j = i + 1;
            for (; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int tmp = array[j];
                    array[j] = array[i];
                    array[i] = tmp;
                }
            }
        }
    }

    //优化排序算法；
    static void selectSort2(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            int j = i + 1;
            for (; j < array.length; j++) {
                if (array[min] > array[j]) {
                    min = j;
                }
            }
            if (min != i) {
                int tmp = array[i];
                array[i] = array[min];
                array[min] = tmp;
            }
        }
    }

    /**
     * 冒泡排序
     * 时间复杂度：O(N^2),不管是在最好还是最坏的情况下都是一样的时间复杂度；
     * 空间复杂度：O(1);
     * 稳定性：稳定的；
     * @param
     */
    //冒泡排序不优化；
    static void bubbleSort1(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    /**
     * 时间复杂度：O(N^2);
     * 有序的情况先：O(N);
     * 空间复杂度：O(1);
     * 稳定性：稳定；
     * @param array
     */
    //优化的冒泡排序；
    static void bubbleSort2(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            boolean flag = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                    flag = true;
                }
            }
            //未进入置换的循环证明数组已经是有序的了，直接跳出循环；
            if (!flag) {
                break;
            }
        }
    }

    //不使用循环实现冒泡排序, 使用递归实现;
    public static void bubbleSort3(int[] array, int startPos, int endPos, boolean flag) {
        if (array[startPos] > array[startPos + 1]) {
            int tmp = array[startPos];
            array[startPos] = array[startPos + 1];
            array[startPos + 1] = tmp;
            flag = false;
        }
        //每次都能使数组最后一位是最大值;
        if (startPos < endPos) {
            //这里的 endPos 不需要 -1 因为这里的是递归进行相邻位指的比较, 最后一位也需要进行比较, 得到这次比较的最大值, 交换到最后;
            bubbleSort3(array, startPos + 1, endPos, flag);
        } else {
            if (endPos > 0 && !flag) {
                //这里的是当开始比较的位置到了结束位置之后, 进行新一轮的递归比较, 这时, 数组最后一位已经是最大值, 所以比较的范围往前一位 endPos-1 ;
                bubbleSort3(array, 0, endPos - 1, true);
            }
        }
    }


    public static void main(String[] args) {
        System.out.println("插入排序！");
        int[] array = {13, 53, 34, 2, 66, 87, 21, 93, 13, 52, 67, 32, 78, 30, 51};
//        insertSort(array);
//        System.out.println(Arrays.toString(array));
        System.out.println("希尔排序！");
//        gapNum1(array);
//        gapNum2(array);
//        System.out.println(Arrays.toString(array));
        System.out.println("选择排序!");
//        selectSort1(array);
//        selectSort2(array);
//        System.out.println(Arrays.toString(array));
        System.out.println("冒泡排序！");
//        bubbleSort1(array);
//        bubbleSort2(array);
//        System.out.println(Arrays.toString(array));
        System.out.println("不使用循环实现冒泡排序, 递归实现!");
        boolean flag = true;
        bubbleSort3(array, 0, array.length - 2, flag);
        System.out.println(Arrays.toString(array));
    }
}
