import javax.naming.InsufficientResourcesException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2023-03-25
 * Time:上午10:08
 */
public class HashSet {
    //对数组去重；
    public static Set<Integer> func2(int[] array) {
        Set<Integer> hashSet = new java.util.HashSet<>();
        for (int i = 0; i < array.length; i++) {
            hashSet.add(array[i]);
        }
        return hashSet;
    }


    //返回数组中第一个重复的数字；
    public static int func1(int[] array) {
        Set<Integer> hashSet = new java.util.HashSet<>();
        int i = 0;
        for (; i < array.length; i++) {
            if(!hashSet.add(array[i]))
                break;
        }
        return array[i];
    }
    public static void main(String[] args) {
        int[] array = {1,3,2,3,5,1};
        int a = func1(array);
        System.out.println(a);
        Set<Integer> hashSet = func2(array);
        System.out.println(hashSet);
    }

}
