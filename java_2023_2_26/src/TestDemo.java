import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2023-02-26
 * Time:下午10:18
 */



/**
 * 力扣：771. 宝石与石头
 */
class Solution1 {
    public int numJewelsInStones(String jewels, String stones) {
        HashSet<Character> set = new HashSet<>();
        for(Character c : jewels.toCharArray()) {
            set.add(c);
        }
        int count = 0;
        for(Character ch : stones.toCharArray()) {
            if(set.contains(ch)){
                count++;
            }
        }
        return count;
    }
}

/**
 * 力扣：138. 复制带随机指针的链表
 */
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
class Solution2 {
    /**
     *
     * @param head
     * @return
     */
    public Node copyRandomList(Node head) {
        Map<Node, Node> map = new HashMap<>();
        Node cur = head;
        while(cur != null) {
            Node node = new Node(cur.val) ;
            map.put(cur, node);
            cur = cur.next;
        }
        cur = head;
        while(cur != null) {
            map.get(cur).next = map.get(cur.next);
            map.get(cur).random = map.get(cur.random);
            cur = cur.next;
        }
        return map.get(head);
    }
}

/**
 * 力扣：136. 只出现一次的数字
 */
class Solution3 {
    public int singleNumber(int[] nums) {
        HashSet<Integer> set = new HashSet<>();
        for(int x: nums) {
            if(set.contains(x)) {
                set.remove(x);
            }else{
                set.add(x);
            }
        }
        for(int i = 0; i < nums.length; i++) {
            if(set.contains(nums[i])) {
                return nums[i];
            }
        }
        return -1;
    }
}

public class TestDemo {
    public static void main(String[] args) {

    }
}
