package com.example.demo.service;

import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-08
 * Time:15:48
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public Userinfo getUserByid(Integer id) {
        return userMapper.getUserById(id);
    }

    public List<Userinfo> getAll() {
        return userMapper.getAll();
    }

    public Integer add(Userinfo userinfo) {
        return userMapper.add(userinfo);
    }


}
