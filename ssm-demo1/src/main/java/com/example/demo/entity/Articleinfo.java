package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-10
 * Time:21:45
 */
@Data
public class Articleinfo {
    private int id;
    private String title;
    private String context;
    private String createtime;
    private String updatetime;
    private int uid;
    private int rcount;
    private int state;
}
