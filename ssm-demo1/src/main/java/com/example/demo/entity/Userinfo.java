package com.example.demo.entity;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.xml.stream.Location;
import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-08
 * Time:15:18
 */
@Data
public class Userinfo {
    private int id;
    private String username;
    private String password;
    private String photo;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int state;
}
