package com.example.demo.vo;

import com.example.demo.entity.Articleinfo;
import lombok.Data;
import lombok.ToString;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-10
 * Time:21:48
 */
@Data
@ToString(callSuper = true)
public class ArticleinfoVO extends Articleinfo {
    private String username;
}
