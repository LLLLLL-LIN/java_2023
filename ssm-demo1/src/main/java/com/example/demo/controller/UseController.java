package com.example.demo.controller;

import com.example.demo.entity.Userinfo;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-08
 * Time:15:46
 */
@RestController
@RequestMapping("/userinfo")
@Slf4j
public class UseController {
    @Autowired
    private UserService userService;

    @RequestMapping("/getuserbyid")
    public Userinfo getUserById(Integer id) {
        if(id == null) {
            return null;
        }
        System.out.println(userService.getUserByid(id));
        log.error("查询结束");
        return userService.getUserByid(id);
    }

    @RequestMapping("/getall")
    public List<Userinfo> getAll() {
        return userService.getAll();
    }

    @RequestMapping("/add")
    public Integer add(Userinfo userinfo) {
        return userService.add(userinfo);
    }
}
