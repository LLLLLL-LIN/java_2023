package com.example.demo.mapper;

import com.example.demo.vo.ArticleinfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-10
 * Time:21:50
 */
@Mapper
public interface ArticleMapper {


    ArticleinfoVO getById(@Param("id") Integer id);

}
