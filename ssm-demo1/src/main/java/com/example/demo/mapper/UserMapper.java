package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-08
 * Time:15:34
 */
@Mapper
public interface UserMapper {

    Userinfo getUserById(@Param("id") Integer id);

    List<Userinfo> getAll();

    int add(Userinfo userinfo);

    int addId(Userinfo userinfo);

    int upUsername(Userinfo userinfo);

    int delById(@Param("id") Integer id);

    List<Userinfo> getDesc(@Param("desc") String desc);

    List<Userinfo> getByName(@Param("username") String username, @Param("password") String password);
}
