package com.example.demo.mapper;

import com.example.demo.vo.ArticleinfoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:
 * Date:-09-10
 * Time:22:17
 */
@SpringBootTest
@Transactional
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    void getById() {
        ArticleinfoVO articleinfoVO = articleMapper.getById(11);
        System.out.println(articleinfoVO);
    }
}