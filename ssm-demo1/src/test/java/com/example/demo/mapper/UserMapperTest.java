package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Param;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:
 * Date:-09-09
 * Time:16:29
 */
@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Test
    void getUserById() {
        Userinfo userinfo = userMapper.getUserById(2);
        Assertions.assertEquals("admin",userinfo.getUsername());
    }

    @Test
    void getAll() {
        List<Userinfo> list = userMapper.getAll();
        System.out.println(list.size());
        Assertions.assertEquals(2,list.size());
    }

    @Test
    void add() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("zhangsan");
        userinfo.setPassword("123");
        userinfo.setCreatetime(LocalDateTime.now());
        userinfo.setUpdatetime(LocalDateTime.now());
        Integer result = userMapper.add(userinfo);
        System.out.println(result);
        Assertions.assertEquals(1,result);
    }

    @Test
    void addId() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("lisi");
        userinfo.setPassword("123");
        userinfo.setCreatetime(LocalDateTime.now());
        userinfo.setUpdatetime(LocalDateTime.now());
        Integer result = userMapper.addId(userinfo);
        System.out.println(result);
        int id = userinfo.getId();
        System.out.println(id);
        Assertions.assertEquals(1,result);
    }

    @Test
    void upUsername() {
        Userinfo userinfo = new Userinfo();
        userinfo.setId(4);
        userinfo.setUsername("wangwu");
        userinfo.setPassword("222");
        userinfo.setCreatetime(LocalDateTime.now());
        userinfo.setUpdatetime(LocalDateTime.now());
        Integer result = userMapper.upUsername(userinfo);
        System.out.println(result);
    }

    @Test
    @Transactional
    void delById() {
        int result = userMapper.delById(11);
        Assertions.assertEquals(1,result);
        Assertions.assertEquals(1,result);

    }

    @Test
    void getDesc() {
        List<Userinfo> list = userMapper.getDesc("desc");
        System.out.println(list);
    }

    @Test
    void getByName() {
//        Userinfo userinfo = userMapper.getByName("小赵");
//        System.out.println(userinfo);
        List<Userinfo> list = userMapper.getByName("admin","'' or 1 = 1");
    }
}