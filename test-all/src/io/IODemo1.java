package io;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-02
 * Time:22:18
 */
public class IODemo1 {
    public static void main(String[] args) throws IOException {
        File file = new File("d:/IOTest/text.txt");
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.getParentFile());
        System.out.println(file.getAbsoluteFile());
        System.out.println(file.createNewFile());
        System.out.println(file.delete());
    }
}

