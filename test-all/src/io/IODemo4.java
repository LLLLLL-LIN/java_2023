package io;

import java.io.File;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2023-11-02
 * Time:22:57
 */
public class IODemo4 {
    public static void main(String[] args) {
        File file = new File("./aaa/bbb/ccc");
//        File file = new File("d:/IOTest/aaa");
//        System.out.println(file.mkdir());
        System.out.println(file.mkdirs());
        System.out.println(file.delete());
        
    }
}
