package io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-02
 * Time:22:39
 */
public class IODemo3 {
    public static void main(String[] args) throws IOException {
        File file = new File("d:/IOTest/io");
        System.out.println(file.mkdir());
        File file1 = new File("d:/IOTest/io/iotext.txt");
        System.out.println(file1.createNewFile());
        System.out.println(file1.exists());
        System.out.println(file1.isFile());
        System.out.println(file1.isDirectory());
    }
}
