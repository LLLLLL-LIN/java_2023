package io;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-02
 * Time:23:38
 */
public class IODemo5 {
    public static void main(String[] args) throws IOException {
        File file = new File("./aaa");
        File dest = new File("./111");
        file.renameTo(dest);
    }
}
