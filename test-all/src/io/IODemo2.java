package io;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-02
 * Time:22:38
 */
public class IODemo2 {
    public static void main(String[] args) throws IOException {
        File file = new File("./main");
//        File file = new File("./iotest/io.txt");
//        file.createNewFile();
        System.out.println(file.mkdir());
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.getParentFile());
        System.out.println(file.getPath());
//        file.delete();
    }
}
