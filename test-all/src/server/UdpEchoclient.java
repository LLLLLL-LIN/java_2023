package server;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-10-30
 * Time:15:28
 */

// Udp 服务器;
public class UdpEchoclient {
    // 使用 Datagramsocket 的对象来接收和发送数据;
    private DatagramSocket socket = null;
    private String serverIp = null;
    private int serverPort = 0;

    public UdpEchoclient(String serverIp, int serverPort) throws SocketException {
        socket = new DatagramSocket();
        this.serverIp = serverIp;
        this.serverPort = serverPort;
    }

    public void start() throws IOException {
        System.out.println("Udp客户端启动!");
        // 读取控制台;
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.print("> 请输入: ");
            String request = scanner.next();
            if(request.equals("exit") || request.equals("quit")) {
                break;
            }
            // 构造发送的数据,格式采用 DatagramPacket;
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
            InetAddress.getByName(serverIp),serverPort);
            // 发送请求;
            socket.send(requestPacket);
            // 构造缓冲区接收数据,缓冲区的格式也为 DatagramPacket;
            DatagramPacket responsePacket = new DatagramPacket(new byte[4096], 4096);
            // socket 拿到缓冲区的空壳,调用 receive 方法装在空壳;
            socket.receive(responsePacket);
            // 转化缓冲区的数据类型;
            String response = new String(responsePacket.getData(),0, responsePacket.getLength());
            System.out.println(response);
        }

    }

    public static void main(String[] args) throws IOException {
        UdpEchoclient client = new UdpEchoclient("127.0.0.1", 9090 );
        client.start();
    }

}
