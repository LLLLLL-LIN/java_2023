package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-10-30
 * Time:15:03
 */


// UDP 客户端
public class UdpEchoServer {
    // 需要 DatagramSocket 的对象来接收和发送数据;
    private DatagramSocket socket = null;


    public UdpEchoServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    public void start() throws IOException {
        System.out.println("udp服务器启动!");
        while(true) {
            // 创建一个 DatagramPacket 的缓冲区还装载数据;
            DatagramPacket requestPacket =  new DatagramPacket(new byte[4096], 4096);
            // 空包给 receive, receive装载数据;
            socket.receive(requestPacket);
            // 转换数据类型为 String;
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());
            // 逻辑处理
            String response = process(request);
            // 构造一个返回的 DatagramPacket,重新转换成字节数据,还需要字节数组的长度而不是 String 数据的长度,最后还需要一个目的ip+端口号;
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(), response.getBytes().length,
                    requestPacket.getSocketAddress());
            // 将构造好的 DatagramPacket 返回;
            socket.send(responsePacket);
            System.out.printf("[%s: %d] req: %s; resp; %s\n",requestPacket.getAddress().toString(),
                    requestPacket.getPort(), request,response);
        }
    }

    // 逻辑处理的方法;
    // 这里不写业务了,直接返回;
    public String process(String  request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer server = new UdpEchoServer(9090);
        server.start();
    }
}
