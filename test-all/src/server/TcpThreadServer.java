package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-01
 * Time:12:54
 */


public class TcpThreadServer {
    private ServerSocket serverSocket = null;

    public TcpThreadServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动!");
        while (true) {
            Socket clientSocket = serverSocket.accept();
            Thread thread = new Thread(() -> {
                try {
                    processConnection(clientSocket);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
    }

    private void processConnection(Socket clientSocket) throws IOException {
        try (InputStream inputStream = clientSocket.getInputStream()) {
            try (OutputStream outputStream = clientSocket.getOutputStream()) {
                System.out.printf("[IP: %s, Port: %d]\n", clientSocket.getInetAddress(), clientSocket.getPort());
                Scanner scanner = new Scanner(inputStream);
                while (true) {
                    if (!scanner.hasNext()) {
                        System.out.printf("[%s,:%d] 客户端断开连接!\n",
                                clientSocket.getInetAddress().toString(),
                                clientSocket.getPort());
                    }
                    // 获取请求;
                    String request = scanner.next();
                    // 业务代码,计算响应;
                    String response = process(request);
                    // 构造响应;
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    // 发送响应;
                    printWriter.println(response);
                    // 刷新缓冲区;
                    printWriter.flush();
                    System.out.printf("[IP: %s, Port: %d]  请求: %s,响应: %s \n",
                            clientSocket.getInetAddress(), clientSocket.getPort(), request,response);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            clientSocket.close();
        }
    }

    // 业务逻辑,计算响应;
    public String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        TcpEchoSever tcpEchoSever = new TcpEchoSever(9090);
        tcpEchoSever.start();
    }
}
