package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-01
 * Time:10:17
 */

// Tcp 客户端;
public class TcpEchoClient {
    private Socket socket = null;

    public TcpEchoClient(String serverIP, int serverPort) throws IOException {
        socket = new Socket(serverIP,serverPort);
    }

    public void start() {
        System.out.println("和服务器连接成功!");
        Scanner scanner = new Scanner(System.in);
        try (OutputStream outputStream = socket.getOutputStream()){
           try(InputStream inputStream = socket.getInputStream()){
               while (true) {
                   System.out.print("->");
                   // 从控制台读取信息;
                   String request = scanner.next();
                   if(request.equals("quit") || request.equals("exit")) {
                       break;
                   }
                   // 构造请求;
                   PrintWriter printWriter = new PrintWriter(outputStream);
                   printWriter.println(request);
                   // 刷新缓冲区;
                   printWriter.flush();
                   // 读取响应
                   Scanner respScanner = new Scanner(inputStream);
                   // 解析响应;
                   String response = respScanner.next();
                   // 打印响应
                   System.out.printf("req: %s, resp: %s\n", request,response);
               }
           }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient tcpEchoClient = new TcpEchoClient("127.0.0.1", 9090);
        tcpEchoClient.start();
    }
}



