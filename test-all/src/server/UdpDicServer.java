package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-10-31
 * Time:14:08
 */

// Udp词典服务器;
public class UdpDicServer {
    private DatagramSocket socket = null;


    public UdpDicServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    public void start() throws IOException {
        System.out.println("Udp翻译服务器启动!");
        while(true) {
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096], 4096);
            socket.receive(requestPacket);
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());
            String response =  precess(request);
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),response.getBytes().length,
                    requestPacket.getSocketAddress());
            socket.send(responsePacket);
        }
    }

    public String precess(String response) {
        if(response.equals("book")) {
            return "书";
        }
        if(response.equals("red")) {
            return "红色";
        }
        if(response.equals("black")) {
            return "黑色";
        }
        if(response.equals("blue")) {
            return "蓝色";
        }
        return "字段不存在!";
    }

    public static void main(String[] args) throws IOException {
        UdpDicServer server = new UdpDicServer(9090);
        server.start();
    }
}
