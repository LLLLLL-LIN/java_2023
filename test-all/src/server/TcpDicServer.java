package server;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-01
 * Time:12:33
 */

// Tcp 词典服务器;
public class TcpDicServer extends TcpEchoSever{
    HashMap<String,String> hashMap = new HashMap<>();

    public TcpDicServer(int port) throws IOException {
        super(port);
        hashMap.put("red","红色");
        hashMap.put("blue","蓝色");
        hashMap.put("black","黑色");
        hashMap.put("color","颜色");
    }

    @Override
    public String process(String request) {
        return hashMap.getOrDefault(request,"尚未记载的词!");
    }

    public static void main(String[] args) throws IOException {
        TcpDicServer tcpDicServer = new TcpDicServer(9090);
        tcpDicServer.start();
    }
}
