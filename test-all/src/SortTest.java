import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-26
 * Time:15:01
 */
public class SortTest {
    public static void main(String[] args) {
        int[] array = {5, 1, 3, 2, 4, 6};
//        bubble(array);
        bubble2(array);
//        insert(array);
//        selsect(array);
        System.out.println(Arrays.toString(array));
    }

    //  冒泡排序
    public static void bubble(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    public static void bubble2(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]){
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j + 1] = tmp;
                }
            }
        }
    }


    // 直接插入排序
    public static void insert(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    // 这里需要进行 break 出去,因为我们需要比较的当前下标 j 的值是否大于需要换位置的那个值;
                    // 如果不进行 break 就会往前找,因为前面的数组都是有序的,这时候会将全部值都换成一样的;
                    break;
                }
            }
            // 这里的 j 永远指向的是 i 的上一位,我们需要挪位置的地方永远都是前一位的值给后一位,
            // 最后我们要把需要挪地方的值,也就是一开始的 tmp 放到空的位置上去;
            // j 永远都是 i -1 ,两种情况,1是 i=0 j落空了,2是 j 已经--了,但是 j-- 这个位置的值大于 tmp ,他就得拍到后面去.就是 j + 1;
            array[j + 1] = tmp;
        }
    }

    //选择排序
    public static void selsect(int[] array) {
        for (int i = 0; i < array.length; i++) {
            // 这里的 i 下标表示的就是当前要进行排序的位置;
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[index]) {
                    // 如果有人比之前的最小值还要小,重置 index ;
                    // index 下标永远指向最小值的下标;
                    index = j;
                }
                int tmp = array[i];
                array[i] = array[index];
                array[index] = tmp;
            }
        }
    }
}
