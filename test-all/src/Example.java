/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-22
 * Time:17:08
 */
public class Example {
    private static int staticValue = 1;
    private int instanceValue = 2;

    static {
        System.out.println("静态代码块执行，静态变量赋值前，staticValue = " + staticValue);
        staticValue = 3;
        System.out.println("静态代码块执行，静态变量赋值后，staticValue = " + staticValue);
    }


    {
        System.out.println("实例代码块执行，实例变量赋值前，instanceValue = " + instanceValue);
        instanceValue = 4;
        System.out.println("实例代码块执行，实例变量赋值后，instanceValue = " + instanceValue);
    }


    public void exampleMethod() {
        final int localValue = 5;

        class LocalInnerClass {
            public void print() {
                System.out.println("局部内部类执行，局部变量localValue = " + localValue);
            }
        }

        LocalInnerClass localInnerClass = new LocalInnerClass();
        localInnerClass.print();
    }


    static class StaticInnerClass {
        static {
            System.out.println("静态内部类静态代码块执行");
        }

        public StaticInnerClass() {
            System.out.println("静态内部类构造方法执行");
        }
    }


    public static void main(String[] args) {
        Example example = new Example();
        example.exampleMethod();

        System.out.println("开始创建静态内部类");
        StaticInnerClass staticInnerClass = new StaticInnerClass();
    }
}
