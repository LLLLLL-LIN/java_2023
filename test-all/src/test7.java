/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-17
 * Time:21:45
 */
public class test7 {
    public static void main(String[] args) {
        // lambda表达式创建Runnable接口的子类对象实现多线程;
//        Thread thread1 = new Thread(() ->
//                System.out.println("hello1")
//                );
//        thread1.start();

////        匿名内部类实现Runnable接口,创建实现类的对象作为参数;
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("这是Runnable的匿名内部类的子类对象!!");
//            }
//        };
//        Thread thread = new Thread(runnable);
//        thread.start();
//
//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("这是Runnable的匿名内部类的子类对象!!");
//            }
//        });

//        // 内部类实现
//        Thread thread2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("hello2");
//            }
//        });
//        thread2.start();
//
//        Thread thread3 = new Thread(){
//            @Override
//            public void run() {
//                System.out.println("hello3");
//            }
//        };
    }
}
