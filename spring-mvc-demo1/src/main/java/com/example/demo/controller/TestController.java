package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-06
 * Time:15:12
 */
@Controller
@ResponseBody
@RequestMapping("/test")
public class TestController {

//    @GetMapping(value = "/testdemo1")
//    @PostMapping("/testdemo1")
//    @RequestMapping(value="testdemo1",method = RequestMethod.POST)
    @RequestMapping(value="testdemo1",method = RequestMethod.GET)
    public String testDemo1(String name,int value){
        return "name  " + name + "  ||  value  " + value;
    }
}
