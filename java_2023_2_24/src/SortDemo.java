import netscape.security.UserTarget;

import java.lang.reflect.Array;
import java.nio.channels.SelectableChannel;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-02-24
 * Time:下午5:18
 */

/**
 * 熟练掌握排序算法；
 */
public class SortDemo {
    public static void main(String[] args) {
        int[] array = {12, 5, 9, 34, 6, 0, 33, 56, 89, 0, 7, 55, 77};
        selectSort1(array);
        System.out.println(Arrays.toString(array));
        selectSort2(array);
        System.out.println(Arrays.toString(array));
        bubbleSort1(array);
        System.out.println(Arrays.toString(array));
        bubbleSort2(array);
        System.out.println(Arrays.toString(array));
        insertSort(array);
        System.out.println(Arrays.toString(array));
        quickSort(array);
        System.out.println(Arrays.toString(array));
        countingSort(array);
        System.out.println(Arrays.toString(array));
        mergeSort(array);
        System.out.println(Arrays.toString(array));
        heapSort(array);
        System.out.println(Arrays.toString(array));
        shellSort(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * 快速排序的主函数；
     *
     * @param array
     */
    public static void quickSort(int[] array) {
        quick(array, 0, array.length - 1);
    }

    /**
     * 递归quick，实现排序；
     *
     * @param array
     * @param left
     * @param right
     */
    public static void quick(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int midValIndex = findMindValIndex(array, left, right);
        swap(array, left, midValIndex);
        int pivot = partition(array, left, right);

        quick(array, left, pivot - 1);
        quick(array, pivot + 1, right);
    }

    /**
     * 找到基准值；
     *
     * @param array
     * @param start
     * @param end
     * @return
     */
    public static int partition(int[] array, int start, int end) {
        int tmp = array[start];
        while (start < end) {
            while (start < end && array[end] >= tmp) {
                end--;
            }
            array[start] = array[end];
            while (start < end && array[start] < tmp) {
                start++;
            }
            array[end] = array[start];
        }
        array[start] = tmp;
        return start;
    }

    /**
     * 找到基准值；
     *
     * @param array
     * @param start
     * @param end
     * @return
     */
    public static int findMindValIndex(int[] array, int start, int end) {
        int mid = start + ((end - start) / 2);
        if (array[start] < array[end]) {
            if (array[start] > array[mid]) {
                return start;
            } else if (array[end] < array[mid]) {
                return end;
            } else {
                return mid;
            }
        } else {
            if (array[start] < array[mid]) {
                return start;
            } else if (array[end] > array[mid]) {
                return end;
            } else {
                return mid;
            }
        }
    }

    /**
     * 插入排序
     *
     * @param array
     */
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j = i - 1;
            int tmp = array[i];
            for (; j >= 0; j++) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    /**
     * 选择排序；
     *
     * @param array
     */
    public static void selectSort1(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    swap(array, i, j);
                }
            }
        }
    }

    /**
     * 选择排序优化；
     *
     * @param array
     */
    public static void selectSort2(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[index]) {
                    index = j;
                }
            }
            swap(array, i, index);
        }
    }

    /**
     * 交换函数；
     *
     * @param array
     * @param i
     * @param j
     */
    public static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    /**
     * 冒泡排序
     *
     * @param array
     */
    public static void bubbleSort1(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                }
            }
        }
    }

    /**
     * 冒泡排序优化；
     */
    public static void bubbleSort2(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean flg = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                    flg = true;
                }
            }
            if (flg == false) {
                break;
            }
        }
    }

    /**
     * 计数排序；
     *
     * @param array
     */
    public static void countingSort(int[] array) {
        int maxVal = 0;
        int minVal = 0;
        for (int i = 0; i < array.length; i++) {
            if (minVal > array[i]) {
                minVal = array[i];
            }
            if (maxVal < array[i]) {
                maxVal = array[i];
            }
        }
        int[] count = new int[maxVal - minVal + 1];
        for (int i = 0; i < array.length; i++) {
            int index = array[i];
            count[index - minVal]++;
        }
        int indexArray = 0;
        for (int i = 0; i < count.length; i++) {
            while (count[i] > 0) {
                array[indexArray] = i + minVal;
                count[i]--;
                indexArray++;
            }
        }
    }

    /**
     * 归并排序；
     *
     * @param array
     */
    public static void mergeSort(int[] array) {
        mergeSortInternal(array, 0, array.length - 1);
    }

    //递归实现分组排序；
    private static void mergeSortInternal(int[] array, int low, int high) {
        if (low >= high) {
            return;
        }
        int mid = (low + high) / 2;
        mergeSortInternal(array, low, mid);
        mergeSortInternal(array, mid + 1, high);
        merge(array, low, mid, high);
    }

    //实现数组的两个数组的有序组合；
    private static void merge(int[] array, int low, int mid, int high) {
        int s1 = low;
        int e1 = mid;
        int s2 = mid + 1;
        int e2 = high;
        int[] tmp = new int[high - low + 1];
        int k = 0;
        while (s1 <= e1 && s2 <= e2) {
            if (array[s1] <= array[s2]) {
                tmp[k++] = array[s1++];
            } else {
                tmp[k++] = array[s2++];
            }
        }
        while (s1 <= e1) {
            tmp[k++] = array[s1++];
        }
        while (s2 <= e2) {
            tmp[k++] = array[s2++];
        }
        for (int i = 0; i < k; i++) {
            array[i + low] = tmp[i];
        }
    }

    /**
     * 希尔排序
     *
     * @param array
     */
    public static void shellSort(int[] array) {
        int gap = array.length;
        while(gap > 1) {
            shell(array, gap);
            gap /= 2;
        }
        shell(array,1);
    }
    public static void shell(int[] array, int gap) {
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (; j >= 0; j -= gap) {
                if(array[j] > tmp) {
                    array[j + gap] = array[j];
                }else {
                    break;
                }
            }
            array[j + gap] = tmp;
        }
    }

    /**
     * 堆排序；
     * @param array
     */
    public static void heapSort(int[] array) {
        createHeap(array);
        int end = array.length - 1;
        while(end > 0) {
            swap(array, 0, end);//解析这一步！！！
            shiftDown(array, 0, end);
            end--;
        }
    }
    /**
     * 创建大根数；
     *
     * @param array
     */
    public static void createHeap(int[] array) {
        for (int parent = (array.length - 1 - 1) / 2; parent >= 0; parent--) {
            shiftDown(array, parent, array.length);
        }
    }
    /**
     * 进行堆排序；
     * @param array
     * @param parent
     * @param len
     */
    public static void shiftDown(int[] array, int parent, int len) {
        int child = 2 * parent + 1;
        while (child < len) {
            if (child + 1 < len && array[child] < array[child + 1]) {
                child++;
            }
            if (array[child] > array[parent]) {
                swap(array, parent, child);
                parent = child;
                child = 2 * parent + 1;//自上向下排序；
            } else {
                break;
            }
        }
    }


}
