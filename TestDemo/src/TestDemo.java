/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-20
 * Time:21:34
 */

@FunctionalInterface
interface NoParameterNoReturn {
    void test();
}
@FunctionalInterface
interface NoParameterReturn {
    int test();
}
public class TestDemo {
    public static void main(String[] args) {
        // lambda 表达式相当于将这个类的定义的同时重写了其中的方法,不考虑方法的具体实现,直接得到期望的结果;
        //
//        NoParameterNoReturn noParameterNoReturn = () -> {
//            System.out.println("hello");
//        };
        // 这里相当与前面 new 的是重写方法之后的类,这里的调用还是得依赖与实例对象的;
        // 不调用不执行;
//        noParameterNoReturn.test();
        int a = 1;
        int b = 2;
        NoParameterReturn noParameterReturn = () -> a > b ? a : b;
        System.out.println(noParameterReturn.test());
    }
}
