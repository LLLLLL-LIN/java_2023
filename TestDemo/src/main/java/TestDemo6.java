import com.sun.xml.internal.ws.addressing.ProblemAction;

import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-04
 * Time:16:32
 */
public class TestDemo6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        if(scanner.hasNextInt()){
//            int a = scanner.nextInt();
//            System.out.println(a);
//        }
//        while(scanner.hasNextInt()) {
//            System.out.println(scanner.next());
//        }
        // .next() 读的是第一个正式字符到第一个空格中间的值，
        // 也就是说当正式字符前面有空格的时候自动忽略，直到第一个正式字符出现的时候就开始读取数据；
//        System.out.println(scanner.next());
        // .nextLine() 读的是回车之前的所有字符，包括空格；
        // 第一个是空格也是能读取的，键盘上的能输入的都能够读取；
        System.out.println(scanner.nextLine());
    }

}
