/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-14
 * Time:16:30
 */
public class TestDemo13 {
    // 要么直接显示的初始化，要么提供构造方法进行赋值；
    private final int a1 = 1;

    // 构造方法进行初始化；
    private final int a2;
    public TestDemo13(int a2) {
        this.a2 = a2;
    }

    public static void main(String[] args) {
        // 这里的 b 不进行显式初始化也没有报错；
        final int b;
        TestDemo13 testDemo13 = new TestDemo13(2);
        System.out.println(testDemo13.a2);
    }
}
