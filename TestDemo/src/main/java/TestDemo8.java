import org.w3c.dom.ls.LSOutput;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-09
 * Time:16:40
 */
public class TestDemo8 {
    String  a = "外部类的成员变量";
    static String b = "外部类的静态变量";
    public static class Demo8 {
        static String i = "静态内部类的静态变量";
        // 静态内部类中允许有非静态属性，相当于静态方法的非静态属性；
        String j = "1";
        void demo8Show1() {
            // 静态内部类中的成员方法只能调用外部类的静态属性；
            // 这和方法是不是静态没关系，是因为方法是在静态内部类当中的；
//            System.out.println(a);
            System.out.println(b);
            System.out.println("->静态内部类的非静态方法");
            demo8Show2();
        }
        static void demo8Show2() {
            System.out.println("->静态内部类的静态方法");
        }
    }
    void outShow() {
        // 非静态方法能调用静态和非静态方法；
        System.out.println(a);
        System.out.println(b);
        // 静态内部类中的成员方法访问内部类中的属性；
        System.out.println(Demo8.i);
        // 访问内部类的实例变量/成员变量；
        Demo8 demo8 = new Demo8();
        System.out.println(demo8.j);
        // 调用静态内部类的静态方法；
        Demo8.demo8Show2();
        // 调用静态内部类的非静态方法；
        demo8.demo8Show1();
    }

    public static void main(String[] args) {
        TestDemo8 testDemo8 = new TestDemo8();
        TestDemo8.Demo8.demo8Show2();
//        TestDemo8.Demo8 demo8 = new Demo8();外部类中调用不需要外部类类名；
        Demo8 demo8 = new Demo8();
        demo8.demo8Show1();
    }
    // 这是外部类的成员方法调用内部类的属性；
    static void show() {
        // 非静态方法能调用静态和非静态方法；
//        System.out.println(a);
        System.out.println(b);
        // 外部类中的成员方法访问内部类中的属性；
        System.out.println(Demo8.i);
        // 访问内部类的实例变量/成员变量；
        Demo8 demo8 = new Demo8();
        System.out.println(demo8.j);
        // 调用静态内部类的静态方法；
        Demo8.demo8Show2();
        // 调用静态内部类的非静态方法；
        demo8.demo8Show1();
    }
}
class test2{
    public static void main(String[] args) {
        // 外部类之外的静态内部类的实例化还是依赖于外部类名
        TestDemo8.Demo8 demo8 = new TestDemo8.Demo8();
        //依赖静态内部类对象；
        demo8.demo8Show1();
        // 不依赖静态内部来对象的实例化，直接外部类类名·静态内部类就可以调用；
        TestDemo8.Demo8.demo8Show2();
    }
}
