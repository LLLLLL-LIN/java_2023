/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-26
 * Time:17:16
 */
// 返回字符中的第一个为重复的字符；
public class TestDemo3 {
    public static void main(String[] args) {
//        String str = "leetcode";
        String str = "aadccef";
        System.out.println(findIdex(str));
    }
    public static int findIdex(String str) {
        int[] array = new int[26];
        for (int i = 0; i < str.length(); i++) {
            array[str.charAt(i) - 'a']++;
        }
        for (int i = 0; i < str.length(); i++) {
            if(array[str.charAt(i) - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }
}
