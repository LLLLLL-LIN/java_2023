import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-29
 * Time:14:24
 */

// 数组的去重
public class TestDemo5 {
    public static void main(String[] args) {
        hashSet();
        hashmap();
    }

    public static void hashSet() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        // Hashset的key不允许重复，相当于自动去重了；
        Set<Integer> hashSet = new HashSet<>();
        for (int j : array) {
            boolean flag = hashSet.add(j);
            // 如果出现元素添加为 false 的时候就说明该元素为重复元素；
            if (!flag) {
                System.out.println(j);
            }
        }
        // 数组中的数据去重之后的打印；
        System.out.println(hashSet);
    }

    public static void hashmap() {
        int[] array = {1, 2, 3, 4, 2, 6, 7, 2, 9, 1};
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (int x : array) {
            if (hashMap.get(x) == null) {
                // hashMap 中没有这个元素，则添加这个元素，使 val = 1；
                hashMap.put(x, 1);
            } else {
                // hashMap 中有这个元素，则 val = val + 1；
                // 将新的 val 写回到 val；
                int val = hashMap.get(x);
                hashMap.put(x, val + 1);
            }
        }
        for (Map.Entry<Integer,Integer> entry : hashMap.entrySet()) {
            if(entry.getValue() > 1) {
                System.out.println("key : " + entry.getKey() + " val : " + entry.getValue());
            }
        }
    }
}
