package TeseDemo2;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-25
 * Time:16:35
 */
public class test {
    public static void main(String[] args) {
        BanarySearchTree test = new BanarySearchTree();
        test.insert(12);
        test.insert(21);
        test.insert(5);
        test.insert(18);
        test.insert(9);
        // 二叉搜索树的中序遍历是有序的；
        test.inorder(test.root);
//        System.out.println(test.root);
        System.out.println();
        // 查询结果是内部类，需要拿内部类接收；
        BanarySearchTree.TreeNode tofind = test.toFind(12);
        System.out.println(tofind.val);
    }
}
