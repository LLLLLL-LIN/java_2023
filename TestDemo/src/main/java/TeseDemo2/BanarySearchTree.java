package TeseDemo2;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-25
 * Time:16:05
 */
public class BanarySearchTree {
    static class TreeNode {
        public int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    TreeNode root = null;

    // 根据val值查找二叉树的节点；
    public TreeNode toFind(int val) {
        if (root == null) {
            return null;
        }
        TreeNode cur = root;
        while (cur != null) {
            if (val > cur.val) {
                cur = cur.right;
            } else if (val == cur.val) {
                return cur;
            } else {
                cur = cur.left;
            }
        }
        return null;
    }

    // 二叉树的插入；
    public void insert(int val) {
        if (root == null) {
            root = new TreeNode(val);
            return;
        }
        TreeNode cur = root;
        TreeNode p = null;
        while (cur != null) {
            if(val > cur.val) {
                p = cur;
                cur = cur.right;
            } else if (val < cur.val) {
                p = cur;
                cur = cur.left;
            }
        }
        if(val < p.val) {
            p.left = new TreeNode(val);
        } else {
           p.right = new TreeNode(val);
        }
    }

    // 二叉树的中序遍历
    public void inorder(TreeNode root) {
        if(root == null) {
            return;
        }
        // 这里传递的还是root的形参，不会改变root；
        inorder(root.left);
        System.out.print (root.val + "  ");
        inorder(root.right);
    }
}
