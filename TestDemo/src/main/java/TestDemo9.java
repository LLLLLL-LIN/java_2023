/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-10
 * Time:00:09
 */
public class TestDemo9 {
    static int a = 0;
    public static void main(String[] args) throws InterruptedException {
//        while(true) {
//            Thread t1 = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("t1");
//                }
//            }) ;
//            Thread t2 = new Thread(() -> System.out.println("t2"));
//            t1.start();
//            t2.start();
//        }
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 50000; i++) {
                    a++;
                }
            }
        }) ;
        Thread t4 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                a++;
            }
        });
        t3.start();
        t4.start();
        Thread.sleep(20);
        System.out.println(a);
    }
}
