package testdemo12;

class B {
    public static void TestB() {
        System.out.println("同类的类中静态方法");
    }
}

public class TestDemo12 {
    public static void TestC() {
        System.out.println("该类中静态方法");
    }

    public static void main(String[] args) {
        // 类外的类静态方法需要用到类名.方法名；
        A.TestA();
        // 同类中的其他类的静态方法也要用到类名.方法名；
        B.TestB();
        // 该类中的静态方法就可以直接使用类名；
        TestC();
        // main 方法所在的类可以实例化；
        TestDemo12 testDemo12 = new TestDemo12();
    }
}
