/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-09
 * Time:16:17
 */
public class Testdemo7 {
    class Demo {
        String i = "->成员内部类的成员变量";

        void show() {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        Testdemo7 testdemo7 = new Testdemo7();
        // 这里注意是外部类类名.内部类类名，后面的是外部类实例.new
        // 而不是 new 外部类类名.内部类类名，这点不是以 Demo 是属于外部类这样的逻辑来的；
        Testdemo7.Demo demo = testdemo7.new Demo();
        demo.show();
    }
}
