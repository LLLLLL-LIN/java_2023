/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-10
 * Time:00:21
 */

import com.sun.webkit.ThemeClient;

/**
 * 多线程的集中创建方法
 * 其实原理其一：都是实现一个类继承自Thread方法，重写其中的 run 方法，这样这个类 new 出来的实例化对象就是线程；
 * 其二：或者创建类实现 Runnable 接口，再去将这个类的实例化对象最为 new Thread() 的参数去创建线程；
 * 需要注意的是；这两种都能够使用匿名内部类的形式创建，相当于一个变式；
 * 其三：还有一种方式就是使用 lambda 表达式；
 */
class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("1.继承Thread类，重写其中的run方法");
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("2.实现 Runnable 接口来创建线程");
    }
}

public class TestDemo10 {
    public static void main(String[] args) {
        // 1.创建类继承自Thread，重写 run 方法；
        MyThread thread1 = new MyThread();

        // 2.匿名内部类创建 Thread 的子类对象；
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("3.匿名内部类创建 Thread 的子类对象");
            }
        });

        // 3.匿名内部类创建 runnable 接口的子类对象；
        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("4.匿名内部类创建 runnable 接口的子类对象");
            }
        });

        // 4.lambda 表达式创建 Runnable 的子类对象；
        Thread thread4 = new Thread(() -> System.out.println("5.lambda 表达式创建 Runnable 的子类对象"));

        // 5.创建类实现 Runnable 接口，实例化这个类的对象最为 new Thread（）的参数；
        MyRunnable myRunnable = new MyRunnable();
        Thread thread5 = new Thread(myRunnable);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
    }
}
