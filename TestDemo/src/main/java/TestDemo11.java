import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-11
 * Time:06:05
 */
public class TestDemo11 {
    public static void main(String[] args) {
        int[] array = {8, 3, 2, 9, 5, 4, 7, 6, 1};
//        boubbleSort(array);
//        selectSort(array);
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }

    // 冒泡排序；
    public static void boubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    // 选择排序；
    public static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int tmp = array[j];
                    array[j] = array[i];
                    array[i] = tmp;
                }
            }
        }
    }

    // 插入排序；
    public static void insertSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            int j = i -1;
            for (;  j >= 0 ; j--) {
                if (num < array[j]) {
                   array[j + 1] = array[j];
                }else{
                    break;
                }
            }
            array[j + 1] = num;
        }
    }

    // 快速排序；
}
