import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-22
 * Time:14:28
 */

class Demo {
    int id;
    String name;
    public Demo(int id, String name){
        this.id = id;
        this.name = name;
    }
}

public class TestDemo {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(list);
        Demo demo1 = new Demo(1,"name1");
        Demo demo2 = new Demo(2,"demo2");
        Demo demo3 = new Demo(3,"name2");
        List<Demo> demoList = new LinkedList<>();
        demoList.add(demo1);
        demoList.add(demo2);
        demoList.add(demo3);
        System.out.println(demoList);
        Stack<Demo> demoStack = new Stack<>();
        demoStack.add(demo1);
        demoStack.add(demo2);
        demoStack.add(demo3);
        System.out.println(demoStack);
        demoStack.push(demo1);
        System.out.println(demoStack);
        // 栈的add方法返回值是添加元素并返回一个boolean类型的值，
        // 栈的push方法返回值是添加元素并当前元素的值；
        System.out.println(demoStack.add(demo1));
        System.out.println(demoStack.push(demo1));
        System.out.println(demoStack.pop());
        // ArrayList和LinkedList的区别？
        List arrayList1 = new ArrayList();
        List LinkedList1 = new LinkedList();

    }

    //    public static void main(String[] args) {
//        Thread t1 = new Thread();
//        Consumer<String> consumer = new Consumer<String>(){
//            @Override
//            public void accept(String s) {
//
//            }
//        };
//        ArrayList<String> list = new ArrayList<>();
//        list.forEach(consumer);
//
//    }
}
