package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yyh.demo.Result;
import com.yyh.demo.model.ServerDao;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:15:55
 */

// 添加业务：
@WebServlet("/serverAdd")
public class ServerAddServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String serverName = req.getParameter("serverName");
        String serverPrice = req.getParameter("serverPrice");
        String serverTime = req.getParameter("serverTime");

        if (serverName == null || "".equals(serverName) || serverPrice == null || "".equals(serverPrice) || serverTime == null || "".equals(serverTime)) {
            // 请求的内容缺失, 肯定是登录失败!!
            resp.setContentType("text/html; charset=utf8");
            Result result = new Result(0, "请填写业务必要的参数!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        } else {
            ServerDao serverDao = new ServerDao();
            serverDao.serverInsert(serverName, serverTime, serverPrice);
            Result result = new Result(1, "业务添加成功!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        }
    }
}
