package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Indent;
import com.yyh.demo.model.IndentDao;
import com.yyh.demo.model.Worker;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:10:04
 */
@WebServlet("/indentFind")
public class IndentFindServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String indentId = req.getParameter("indentId");
        String indentServer = req.getParameter("indentServer");
        String indentWorker = req.getParameter("indentWorker");
        String indentPrice = req.getParameter("indentPrice");
        String consumerName = req.getParameter("consumerName");
        String indentTime = req.getParameter("indentTime");
        // 2. 和数据库中的内容进行比较
        IndentDao indentDao = new IndentDao();
        List<Indent> indentList = new ArrayList<>();
        indentList = indentDao.indentAllFind(indentId,indentServer,indentWorker,indentPrice,consumerName,indentTime);
//        WorkerDao workerDao = new WorkerDao();
//        Worker worker = workerDao.selectByName(workerName);
        String respJson = objectMapper.writeValueAsString(indentList);
        resp.getWriter().write(respJson);
    }
}
