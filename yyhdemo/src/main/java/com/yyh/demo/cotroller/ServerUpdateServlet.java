package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.ServerDao;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:16:07
 */

// 业务修改；
@WebServlet("/serverUpdate")
public class ServerUpdateServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String serverId = req.getParameter("serverId");
        String serverName = req.getParameter("serverName");
        String serverPrice = req.getParameter("serverPrice");
        String serverTime = req.getParameter("serverTime");

//        if (serverId == null || "".equals(serverId) ||serverName == null || "".equals(serverName)||serverPrice == null || "".equals(serverPrice)||serverTime == null || "".equals(serverTime)) {
//            // 请求的内容缺失, 肯定是登录失败!!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("请输入必要的参数！");
//            return;
//        }
        ServerDao serverDao = new ServerDao();
        serverDao.serverUpdate(serverId, serverName, serverTime, serverPrice);
        resp.getWriter().write("更新成功");
    }
}
