package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Server;
import com.yyh.demo.model.ServerDao;
import com.yyh.demo.model.Worker;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:15:46
 */

// 业务的页面渲染查询；
@WebServlet("/serverList")
public class ServerListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        List<Server> serverList = new ArrayList<>();
        ServerDao serverDao = new ServerDao();
        serverList = ServerDao.selectAllServer();
        // 把 workerList对象转成 JSON 格式.
        String respJson = objectMapper.writeValueAsString(serverList);
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(respJson);
    }
}
