package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:11:39
 */

// 工作人员的删除；
@WebServlet("/workerDel")
public class WorkerDelServlet extends HelloServlet{
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String workerId = req.getParameter("workerId");
        if (workerId == null || "".equals(workerId) ) {
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("该工作人员不存在");
            return;
        }
        // 2. 和数据库中的内容进行比较
        WorkerDao workerDao = new WorkerDao();
        workerDao.workerDel(workerId);
        resp.getWriter().write("删除成功！");
    }
}
