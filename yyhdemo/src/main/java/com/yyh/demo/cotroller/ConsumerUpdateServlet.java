package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.ComsumerDao;
import com.yyh.demo.model.WorkerDao;

import javax.print.attribute.standard.ColorSupported;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:16:37
 */
// 更新会员；
    @WebServlet("/consumerUpdate")
public class ConsumerUpdateServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String consumerId = req.getParameter("consumerId");
        String consumerName = req.getParameter("consumerName");
        String consumerSex = req.getParameter("consumerSex");
        String level = req.getParameter("level");

        String discount = req.getParameter("discount");
        String count = req.getParameter("count");
        String addTime = req.getParameter("addTime");

//        if (consumerId == null || "".equals(consumerId) ||consumerName == null || "".equals(consumerName)||consumerSex == null || "".equals(consumerSex)||level == null || "".equals(level)||discount == null || "".equals(discount)||addTime == null || "".equals(addTime)) {
//            // 请求的内容缺失, 肯定是登录失败!!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("请输入必要的参数！");
//            return;
//        }
        if(level.equals("VIP")){
            discount = "九折";
        } else if (level.equals("SVIP")){
            discount = "八折";
        } else if(level.equals("SSVIP")) {
            discount = "七折";
        }
        ComsumerDao consumerDao = new ComsumerDao();
        consumerDao.consumerUpdate(consumerId,consumerName,consumerSex,level, discount,count,addTime);
        resp.getWriter().write("更新成功");
    }
}
