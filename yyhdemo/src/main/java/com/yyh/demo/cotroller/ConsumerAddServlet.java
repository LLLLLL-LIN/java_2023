package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yyh.demo.Result;
import com.yyh.demo.model.ComsumerDao;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:16:26
 */

// 添加会员；
    @WebServlet("/consumerAdd")
public class ConsumerAddServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String consumerName = req.getParameter("consumerName");
        String consumerSex = req.getParameter("consumerSex");
        String level = req.getParameter("level");
        String discount = req.getParameter("discount");
        String count = req.getParameter("count");
        if(level.equals("VIP")){
            discount = "九折";
        } else if (level.equals("SVIP")){
            discount = "八折";
        } else if(level.equals("SSVIP")) {
            discount = "七折";
        }
        if (consumerName == null || "".equals(consumerName) ||consumerSex == null || "".equals(consumerSex)||level == null || "".equals(level)||discount == null || "".equals(discount)||count == null || "".equals(count)) {
            // 请求的内容缺失, 肯定是登录失败!!
            resp.setContentType("text/html; charset=utf8");
            Result result = new Result(0,"请填写添加会员必要的参数!",null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
            return;
        }
        ComsumerDao comsumerDao = new ComsumerDao();
        comsumerDao.consumerInsert(consumerName,consumerSex,level,discount,count);
        Result result = new Result(1,"会员添加成功!",null);
        String s = new Gson().toJson(result);
        resp.getWriter().append(s);
    }
}
