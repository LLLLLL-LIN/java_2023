package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Indent;
import com.yyh.demo.model.IndentDao;
import com.yyh.demo.model.Server;
import com.yyh.demo.model.ServerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:15:49
 */

// 查找业务；
    @WebServlet("/serverFind")
public class ServerFindServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String serverId = req.getParameter("serverId");
        String serverName = req.getParameter("serverName");
        String serverPrice = req.getParameter("serverPrice");
        String serverTime = req.getParameter("serverTime");

        // 2. 和数据库中的内容进行比较
        ServerDao serverDao = new ServerDao();
        List<Server> serverList = new ArrayList<>();
        serverList = serverDao.serverAllFind(serverId,serverName,serverPrice,serverTime);
//        WorkerDao workerDao = new WorkerDao();
//        Worker worker = workerDao.selectByName(workerName);
        String respJson = objectMapper.writeValueAsString(serverList);
        resp.getWriter().write(respJson);
    }
}
