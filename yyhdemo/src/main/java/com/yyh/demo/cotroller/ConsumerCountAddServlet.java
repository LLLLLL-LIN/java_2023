package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.ComsumerDao;
import com.yyh.demo.model.Indent;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-29
 * Time:12:24
 */
@WebServlet("/consumerCountAdd")
public class ConsumerCountAddServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String consumerId = req.getParameter("consumerId");
        Integer id = Integer.parseInt(consumerId);
        String count = req.getParameter("count");
        String addCount = req.getParameter("addCount");
        Double num = Double.valueOf(count) + Double.valueOf(addCount);

//        if (consumerId == null || "".equals(consumerId) ) {
//            // 请求的内容缺失, 肯定是登录失败!!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("该会员不存在");
//            return;
//        }
        // 2. 和数据库中的内容进行比较
        ComsumerDao consumerDao = new ComsumerDao();
        consumerDao.consumerCountUpdate(id,num);
        resp.getWriter().write("充值成功！");
    }
}
