package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Worker;
import com.yyh.demo.model.WorkerDao;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:04:31
 */

// 这是工作人员页面渲染的查找；
@WebServlet("/workerList")
public class WorkerListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        List<Worker> workerList = new ArrayList<>();
        WorkerDao workerDao = new WorkerDao();
        workerList = workerDao.selectAllWorker();
        // 把 workerList对象转成 JSON 格式.
        String respJson = objectMapper.writeValueAsString(workerList);
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(respJson);
    }
}
