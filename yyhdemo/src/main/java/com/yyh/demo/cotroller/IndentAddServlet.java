package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yyh.demo.Result;
import com.yyh.demo.model.ComsumerDao;
import com.yyh.demo.model.Consumer;
import com.yyh.demo.model.Indent;
import com.yyh.demo.model.IndentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:14:39
 */

@WebServlet("/indentAdd")
public class IndentAddServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String indentServer = req.getParameter("indentServer");
        String indentPrice = req.getParameter("indentPrice");
        String indentWorker = req.getParameter("indentWorker");
        String consumerName = req.getParameter("consumerName");
        System.out.println("identPrice " + indentPrice);
        ComsumerDao consumerDao = new ComsumerDao();
        Consumer consumer = consumerDao.selectByName(consumerName);
        if (consumer == null) {
            Result result = new Result(0, "不存在该会员!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
            return;
        }
        Integer id = consumer.getConsumerId();
        Double a = consumer.getCount();
        Double indentPrice2 = Double.valueOf(indentPrice);
        String discount = consumer.getDiscount();
        Double indentPrice3 = indentPrice2 * 1.0;
        if (discount.equals("七折")) {
            indentPrice3 = indentPrice2 * 0.7;
        } else if (discount.equals("八折")) {
            indentPrice3 = indentPrice2 * 0.8;
        } else if (discount.equals("七折")) {
            indentPrice3 = indentPrice2 * 0.7;
        }
        System.out.println("idnentPrice3 " + indentPrice3);
        if ((a - indentPrice3) < 0) {
            Result result = new Result(0, "账户余额不够!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        } else {
            consumerDao.consumerCountUpdate(id, a - indentPrice3);
        }
        if (indentServer == null || "".equals(indentServer) || indentPrice == null || "".equals(indentPrice) || indentWorker == null || "".equals(indentWorker) || consumerName == null || "".equals(consumerName)) {
            // 请求的内容缺失, 肯定是登录失败!!
            resp.setContentType("text/html; charset=utf8");
            Result result = new Result(0, "请填写添加订单的必要的参数!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        } else {
            IndentDao indentDao = new IndentDao();
            System.out.println(3);
            indentDao.indentInsert(indentServer, indentPrice3, indentWorker, consumerName);
            Result result = new Result(1, "订单添加成功!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        }
    }
}
