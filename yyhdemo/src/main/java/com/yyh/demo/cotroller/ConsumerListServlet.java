package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yyh.demo.Result;
import com.yyh.demo.model.*;
import sun.awt.geom.PathConsumer2D;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:16:13
 */

// 会员页面渲染查询；
@WebServlet("/consumerList")
public class ConsumerListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        ComsumerDao consumerDao = new ComsumerDao();
        List<Consumer> consumerList = consumerDao.selectAllConsumer();
        // 把 consumerList对象转成 JSON 格式.
        String respJson = objectMapper.writeValueAsString(consumerList);
        resp.getWriter().write(respJson);
    }
}
