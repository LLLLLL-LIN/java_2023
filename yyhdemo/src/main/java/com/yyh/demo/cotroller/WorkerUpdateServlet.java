package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:14:53
 */

// 修改工作人员信息；

@WebServlet("/workerUpdate")
public class WorkerUpdateServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String workerId = req.getParameter("workerId");
        String workerName = req.getParameter("workerName");
        String workerSex = req.getParameter("workerSex");
        String workerNum = req.getParameter("workerNum");
        String joinTime = req.getParameter("joinTime");
        String address = req.getParameter("address");

//        if (workerName == null || "".equals(workerName) ||workerSex == null || "".equals(workerSex)||workerNum == null || "".equals(workerNum)||joinTime == null || "".equals(joinTime)||address == null || "".equals(address)) {
//            // 请求的内容缺失, 肯定是登录失败!!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("请输入必要的参数！");
//            return;
//        }
        WorkerDao workerDao = new WorkerDao();
        workerDao.workerUpdate(workerId,workerName,workerSex,workerNum, joinTime,address);
        resp.getWriter().write("更新成功");
    }
}
