package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Worker;
import com.yyh.demo.model.WorkerDao;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:03:13
 */

// 这是工作人员的查找；
@WebServlet("/workerFind")
public class WorkerFindServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String workerId = req.getParameter("workerId");
        String workerName = req.getParameter("workerName");
        String workerSex = req.getParameter("workerSex");
        String workerNum = req.getParameter("workerNum");
        String joinTime = req.getParameter("joinTime");
        String address = req.getParameter("address");
        // 2. 和数据库中的内容进行比较
        WorkerDao workerDao = new WorkerDao();
        List<Worker> workerList = new ArrayList<>();
        System.out.println(workerList);
        workerList = workerDao.workerAllFind(workerId,workerName,workerSex,workerNum,joinTime,address);
//        WorkerDao workerDao = new WorkerDao();
//        Worker worker = workerDao.selectByName(workerName);
        String respJson = objectMapper.writeValueAsString(workerList);
        resp.getWriter().write(respJson);
//        if (worker == null || !worker.getWorkerName().equals(workerName)) {
//            // 用户没有查到或者密码不匹配, 也是登录失败!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("没有此工作人员!");
//            return;
//        }

//        System.out.println(worker.toString());
//        // 3. 如果比较通过, 就创建会话.
//        HttpSession session = req.getSession(true);
//        // 把刚才的用户信息, 存储到会话中.
//        session.setAttribute("user", user);
        // 4. 返回一个重定向报文, 跳转到博客列表页.
//        resp.sendRedirect("worker.html");
    }
}
