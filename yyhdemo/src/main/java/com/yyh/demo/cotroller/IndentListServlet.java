package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.Indent;
import com.yyh.demo.model.IndentDao;
import com.yyh.demo.model.Worker;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2023-05-22
 * Time:10:55
 */

// 订单页面渲染查询；
@WebServlet("/indentList")
public class IndentListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    // 这个方法用来获取到数据库中的工作人员列表.
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        List<Indent> indentList = new ArrayList<>();
        IndentDao indentDao = new IndentDao();
        indentList = indentDao.selectAllIndent();
        // 把 indentList 对象转成 JSON 格式.
        String respJson = objectMapper.writeValueAsString(indentList);
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(respJson);
    }
}
