package com.yyh.demo.cotroller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.yyh.demo.model.IndentDao;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:15:00
 */

// 更新订单信息；
@WebServlet("/indentUpdate")
public class IndentUpdateServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String indentId = req.getParameter("indentId");
        String indentServer = req.getParameter("indentServer");
        String indentPrice = req.getParameter("indentPrice");
        String indentWorker = req.getParameter("indentWorker");
        String consumerName = req.getParameter("consumerName");
        String indentTime = req.getParameter("indentTime");

//        if (indentServer == null || "".equals(indentServer) ||indentPrice == null || "".equals(indentPrice)||indentWorker == null || "".equals(indentWorker)||consumerName == null || "".equals(consumerName)||indentTime == null || "".equals(indentTime)) {
//            // 请求的内容缺失, 肯定是登录失败!!
//            resp.setContentType("text/html; charset=utf8");
//            resp.getWriter().write("请输入必要的参数！");
//            return;
//        }
        IndentDao indentDao = new IndentDao();
        indentDao.indentUpdate(indentId,indentServer,indentPrice,indentWorker, consumerName,indentTime);
        resp.getWriter().write("更新成功");
    }
}
