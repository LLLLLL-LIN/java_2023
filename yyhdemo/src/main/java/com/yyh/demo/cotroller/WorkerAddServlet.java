package com.yyh.demo.cotroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.yyh.demo.Result;
import com.yyh.demo.model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-22
 * Time:11:47
 */

// 工作人员的添加；
@WebServlet("/wokerAdd")
public class WorkerAddServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        // 1. 获取到请求中的参数
        String workerName = req.getParameter("workerName");
        String workerSex = req.getParameter("workerSex");
        String workerNum = req.getParameter("workerNum");
        String address = req.getParameter("address");
        if (workerName == null || "".equals(workerName) || workerSex == null || "".equals(workerSex) || workerNum == null ||
                address == null || "".equals(address)) {
            // 请求的内容缺失, 肯定是登录失败!!
            resp.setContentType("text/html; charset=utf8");
            Result result = new Result(0, "请填写添加工作人员必要的参数!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        } else if (workerNum.length() != 11) {
            resp.setContentType("text/html; charset=utf8");
            Result result = new Result(0, "请填写正确的电话号码!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        } else {
            WorkerDao workerDao = new WorkerDao();
            workerDao.workerInsert(workerName, workerSex, workerNum, address);
            Result result = new Result(1, "工作人员添加成功!", null);
            String s = new Gson().toJson(result);
            resp.getWriter().append(s);
        }
    }
}
