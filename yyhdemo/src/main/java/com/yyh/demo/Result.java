package com.yyh.demo;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:
 * Date:-05-26
 * Time:19:05
 */
public class Result {
    private int code;
    private String msg;
    private Object data;

    public Result(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
