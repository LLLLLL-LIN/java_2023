package com.yyh.demo.model;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:03:10
 */
public class WorkerDao {


    // 【查】查找员工；
    public List<Worker> workerAllFind(String workerId, String workerName, String workerSex,String workerNum,String joinTime,String address) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Worker> workerList = new ArrayList<>();
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from worker where 1=1";
            if(!"".equals(workerId)) {
                sql+= " and workerId = " + workerId;
            }

            if(!"".equals(workerName) && workerName != null) {
                sql+= " and workerName = " + "'" + workerName + "'";
            }

            if(!"".equals(workerSex) && workerSex != null) {
                sql+= " and workerSex = " + "'" + workerSex + "'";
            }

            if(!"".equals(workerNum) && workerNum != null) {
                sql+= " and workerNum = " + "'" + workerNum + "'";
            }

            if(!"".equals(joinTime) && joinTime != null) {
                sql+= " and joinTime like '%"+joinTime+"%'";
            }

            if(!"".equals(address)&& address != null) {
                sql +=" and address = " + "'" + address + "'";
            }
            sql += " order by workerId desc";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Worker worker = new Worker();
                worker.setWorkerId(resultSet.getInt("workerId"));
                worker.setWorkerName(resultSet.getString("workerName"));
                worker.setWorkerSex(resultSet.getString("workerSex"));
                worker.setWorkerNum(resultSet.getString("workerNum"));
                worker.setJoinTime(Timestamp.valueOf(resultSet.getString("joinTime")));
                worker.setAddress(resultSet.getString("address"));
                workerList.add(worker);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return workerList;
    }


    // 【查】 页面渲染的查，查找全部的工作人员，采用顺序表返回；
    public  List<Worker> selectAllWorker() {
        List<Worker> workerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from worker order by joinTime desc";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Worker worker = new Worker();
                worker.setWorkerId(resultSet.getInt("workerId"));
                worker.setWorkerName(resultSet.getString("workerName"));
                worker.setWorkerSex(resultSet.getString("workerSex"));
                worker.setWorkerNum(resultSet.getString("workerNum"));
                worker.setJoinTime(resultSet.getTimestamp("joinTime"));
                worker.setAddress(resultSet.getString("address"));
                workerList.add(worker);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return workerList;
    }


    // 【删】删除工作人员，根据工作人员id；
    public void workerDel(String workerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from worker where workerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【改】根据工作人员 workerId，修改相关信息。
    public void workerUpdate(String workerId, String workerName, String workerSex, String workerNum, String joinTime,String address) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE worker SET workerName = ?, workerSex = ?, workerNum = ?, joinTime = ? ,address = ? WHERE workerId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerName);
            statement.setString(2, workerSex);
            statement.setString(3, workerNum);
            statement.setString(4, joinTime);
            statement.setString(5, address);
            statement.setString(6, workerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【增】添加工作人员；
    public void workerInsert(String workerName, String workerSex, String workerNum, String address) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "insert into worker values(null,?,?,?,now(),?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerName);
            statement.setString(2, workerSex);
            statement.setString(3, workerNum);
//            statement.setString(4, joinTime);
            statement.setString(4,address);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 根据工作人员的名字查找信息。
    public Worker selectByName(String workerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from worker where workerName = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Worker worker = new Worker();
                worker.setWorkerId(resultSet.getInt("workerId"));
                worker.setWorkerName(resultSet.getString("workerName"));
                worker.setWorkerSex(resultSet.getString("workerSex"));
                worker.setWorkerNum(resultSet.getString("workerNum"));
                worker.setJoinTime(Timestamp.valueOf(resultSet.getString("joinTime")));
                return worker;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }
}
