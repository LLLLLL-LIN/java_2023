package com.yyh.demo.model;

import javax.net.ssl.SNIServerName;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class ServerDao {

    // 【查】查找业务；
    public List<Server> serverAllFind(String serverId,String serverName,String serverPrice,String serverTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Server> serverList = new ArrayList<>();
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from server where 1=1";
            if(!"".equals(serverId)) {
                sql+= " and serverId = " + serverId;
            }

            if(!"".equals(serverName) && serverName != null) {
                sql+= " and serverName = " + "'" + serverName + "'";
            }

            if(!"".equals(serverPrice)) {
                sql+= " and serverPrice = " + serverPrice;
            }

            if(!"".equals(serverTime) && serverTime != null) {
                sql+= " and serverTime like '%"+serverTime+"%'";
            }
            sql += " order by serverPrice desc";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Server server = new Server();
                server.setServerId(resultSet.getInt("serverId"));
                server.setServerName(resultSet.getString("serverName"));
                server.setServerPrice(resultSet.getInt("serverPrice"));
                server.setServerTime(resultSet.getString("serverTime"));
                serverList.add(server);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return serverList;
    }

    // 【查】页面渲染的查，查找全部的工作人员，采用顺序表返回。
    public static List<Server> selectAllServer() {
        List<Server> serverList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from server";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Server server = new Server();
                server.setServerId(resultSet.getInt("serverId"));
                server.setServerName(resultSet.getString("serverName"));
                server.setServerTime(resultSet.getString("serverTime"));
                server.setServerPrice(resultSet.getInt("serverPrice"));
                serverList.add(server);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return serverList;
    }

    // 【删】删除业务，根据业务的serverId；
    public void serverDel(String serverId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from server where serverId = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 【增】添加业务；
    public void serverInsert(String serverName, String serverTime, String serverPrice) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "insert into server values(null,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverName);
            statement.setString(2, serverPrice);
            statement.setString(3, serverTime);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 【改】根据工作人员 workerId，更改相关信息。
    public void serverUpdate(String serverId, String serverName, String serverTime, String serverPrice) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE server SET serverName = ?, serverTime = ?, serverPrice = ? WHERE serverId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverName);
            statement.setString(2, serverTime);
            statement.setString(3, serverPrice);
            statement.setString(4, serverId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 根据工作人员的名字查找信息。
    public Server selectByName(String serverName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from server where serverName = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Server server = new Server();
                server.setServerId(resultSet.getInt("serverId"));
                server.setServerName(resultSet.getString("serverName"));
                server.setServerTime(resultSet.getString("serverTime"));
                server.setServerPrice(resultSet.getInt("serverPrice"));
                return server;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

}
