package com.yyh.demo.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class ComsumerDao {


    // 【查】查找员工；
    public List<Consumer> consumerAllFind(String consumerId, String consumerName, String consumerSex, String level, String discount, String addTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Consumer> consumerList = new ArrayList<>();
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from consumer where 1=1 ";
            if (!"".equals(consumerId)) {
                sql += " and consumerId = " + consumerId;
            }

            if (!"".equals(consumerName) && consumerName != null) {
                sql += " and consumerName like '%"+consumerName+"%'";
            }

            if (!"".equals(consumerSex) && consumerSex != null) {
                sql += " and consumerSex = " + "'" + consumerSex + "'";
            }

            if (!"".equals(level) && level != null) {
                sql += " and level = " + "'" + level + "'";
            }

            if (!"".equals(discount) && discount != null) {
                sql += " and discount = " + "'" + discount + "'";
            }
            if (!"".equals(addTime) && addTime != null) {
                sql += " and addTime like '%"+ addTime +"%'";
            }
            sql += " order by addTime desc";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Consumer consumer = new Consumer();
                consumer.setConsumerId(resultSet.getInt("consumerId"));
                consumer.setConsumerName(resultSet.getString("consumerName"));
                consumer.setConsumerSex(resultSet.getString("consumerSex"));
                consumer.setLevel(resultSet.getString("level"));
                consumer.setAddTime(Timestamp.valueOf(resultSet.getString("addTime")));
                consumer.setDiscount(resultSet.getString("discount"));
                consumer.setCount(resultSet.getInt("count"));
                consumerList.add(consumer);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return consumerList;
    }


    // 【查】页面渲染的查，查找全部的会员，采用顺序表返回。
    public List<Consumer> selectAllConsumer() {
        List<Consumer> consumerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from consumer order by consumerId desc";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Consumer consumer = new Consumer();
                consumer.setConsumerId(resultSet.getInt("consumerId"));
                consumer.setConsumerName(resultSet.getString("consumerName"));
                consumer.setConsumerSex(resultSet.getString("consumerSex"));
                consumer.setAddTime(resultSet.getTimestamp("addTime"));
                consumer.setLevel(resultSet.getString("level"));
                consumer.setDiscount(resultSet.getString("discount"));
                consumer.setCount(resultSet.getDouble("count"));
                consumerList.add(consumer);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return consumerList;
    }


    // 【增】添加会员；
    public void consumerInsert(String consumerName, String consumerSex, String level, String discount,String count) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "insert into consumer values(null,?,?,?,?,?,now())";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerName);
            statement.setString(2, consumerSex);
            statement.setString(3, level);
            statement.setString(4, discount);
            statement.setString(5, count);
//            statement.setTimestamp(5, addTime);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【删】删除会员，根据会员id；
    public void consumerDel(String consumerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from consumer where consumerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【改】修改会员信息；
    public void consumerUpdate(String consumerId, String consumerName, String consumerSex, String level, String discount,String count, String addTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE consumer SET consumerName = ?, consumerSex = ?,level = ?, discount = ?, addTime = ? ,count = ? WHERE consumerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerName);
            statement.setString(2, consumerSex);
            statement.setString(3, level);
            statement.setString(4, discount);
            statement.setString(5, addTime);
            statement.setString(6, count);
            statement.setString(7, consumerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 【改】修改会员账户信息；
    public void consumerCountUpdate(Integer consumerId , Double count) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE consumer SET count = ? WHERE consumerId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setDouble(1, count);
            statement.setInt(2, consumerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

//    // 【改】减少会员账户信息；
//    public void consumerCountUpdate(String consumerId , Integer count) {
//        Connection connection = null;
//        PreparedStatement statement = null;
//        try {
//            connection = DBUtil.getConnection();
//            String sql = "UPDATE consumer SET count = ? WHERE consumerId = ?;";
//            statement = connection.prepareStatement(sql);
//            statement.setInt(1, count);
//            statement.setString(2, consumerId);
//            statement.executeUpdate();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        } finally {
//            DBUtil.close(connection, statement, null);
//        }
//    }

    // 根据会员的名字查找信息。
    public Consumer selectByName(String consumerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from consumer where consumerName = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Consumer consumer = new Consumer();
                consumer.setConsumerId(resultSet.getInt("consumerId"));
                consumer.setConsumerName(resultSet.getString("consumerName"));
                consumer.setAddTime(resultSet.getTimestamp("addTime"));
                consumer.setLevel(resultSet.getString("level"));
                consumer.setDiscount(resultSet.getString("discount"));
                consumer.setCount(resultSet.getInt("count"));
                return consumer;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }
}
