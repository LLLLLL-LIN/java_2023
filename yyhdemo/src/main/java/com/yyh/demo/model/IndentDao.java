package com.yyh.demo.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class IndentDao {

    // 【查】查找订单；
    public List<Indent> indentAllFind(String indentId,String indentServer,String indentWorker,String indentPrice,String consumreName,String indentTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Indent> indentList = new ArrayList<>();
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from indent where 1=1";
            if(!"".equals(indentId)) {
                sql+= " and indentId = " + indentId;
            }

            if(!"".equals(indentServer) && indentServer != null) {
                sql+= " and indentServer like '%"+ indentServer +"%'";
            }

            if(!"".equals(indentWorker) && indentWorker != null) {
                sql+= " and indentWorker like '%"+ indentWorker +"%'";
            }

            if(!"".equals(indentPrice)) {
                sql+= " and IndentPrice = " + indentPrice;
            }
            System.out.println(consumreName);
            if(!"".equals(consumreName) && consumreName != null) {
                sql+= " and consumerName like '%"+ consumreName +"%'";
            }

            if(!"".equals(indentTime) && indentTime != null) {
                sql +=" and indentTime like '%"+ indentTime +"%'";
            }
            sql += " order by indentId desc";
            System.out.println(sql);
            statement = connection.prepareStatement(sql);
//            statement.setString(1, workerName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            while (resultSet.next()) {
                Indent indent = new Indent();
                indent.setIndentId(resultSet.getInt("indentId"));
                indent.setIndentServer(resultSet.getString("indentServer"));
                indent.setIndentPrice(resultSet.getDouble("indentPrice"));
                indent.setIndentWorker(resultSet.getString("indentWorker"));
                indent.setConsumerName(resultSet.getString("consumerName"));
                indent.setIndentTime(Timestamp.valueOf(resultSet.getString("indentTime")));
                indentList.add(indent);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return indentList;
    }


    // 【查】页面渲染的查，查找全部的订单，采用顺序表返回。
    public List<Indent> selectAllIndent() {
        List<Indent> indentList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from indent order by indentTime desc";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Indent indent = new Indent();
                indent.setIndentId(resultSet.getInt("indentId"));
                indent.setIndentServer(resultSet.getString("indentServer"));
                indent.setIndentPrice(resultSet.getDouble("indentPrice"));
                indent.setIndentWorker(resultSet.getString("indentWorker"));
                indent.setConsumerName(resultSet.getString("consumerName"));
                indent.setIndentTime(resultSet.getTimestamp("indentTime"));
                indentList.add(indent);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return indentList;
    }


    // 【增】添加订单；
    public void indentInsert( String indentServer, Double indentPrice, String indentWorker, String consumerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "insert into indent values(null,?,?,?,?,now())";
            statement = connection.prepareStatement(sql);
            statement.setString(1, indentServer);
            statement.setDouble(2, indentPrice);
            statement.setString(3, indentWorker);
            statement.setString(4,consumerName);
//            statement.setString(5, indentTime);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【删】删除订单，根据 indentId；
    public void indentDel(String indentId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from indent where indentId = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, indentId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 【改】修改订单；
    public void indentUpdate(String indentId, String indentServer, String indentPrice, String indentWorker, String consumerName, String indentTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE indent SET indentServer = ?, indentPrice = ?,indentWorker = ?, consumerName = ?, indentTime = ? WHERE indentId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, indentServer);
            statement.setString(2, indentPrice);
            statement.setString(3, indentWorker);
            statement.setString(4, consumerName);
            statement.setString(5, indentTime);
            statement.setString(6, indentId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }


    // 根据indentId 去查找订单，采用顺序表返回。
    public List<Indent> selectOne(int indentId) {
        List<Indent> indentList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from indent where indentId = ?";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Indent indent = new Indent();
                indent.setIndentId(resultSet.getInt("indentId"));
                indent.setIndentServer(resultSet.getString("indentServer"));
                indent.setIndentPrice(resultSet.getDouble("indentPrice"));
                indent.setIndentWorker(resultSet.getString("indentWorker"));
                indent.setConsumerName(resultSet.getString("consumerName"));
                indent.setIndentTime(resultSet.getTimestamp("indentTime"));
                indentList.add(indent);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return indentList;
    }



}
