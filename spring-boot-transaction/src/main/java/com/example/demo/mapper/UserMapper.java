package com.example.demo.mapper;

import com.example.demo.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-13
 * Time:09:41
 */
@Mapper
public interface UserMapper {
    int addUser(UserInfo userInfo);
    int insert(UserInfo userInfo);
}
