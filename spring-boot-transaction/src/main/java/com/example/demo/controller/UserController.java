package com.example.demo.controller;

import com.example.demo.entity.UserInfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-13
 * Time:09:11
 */
@RestController
@RequestMapping("/trans")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private DataSourceTransactionManager transactionManager;
    @Autowired
    private TransactionDefinition transactionDefinition;

    /**
     * 编程式开启事务
     * @param userInfo
     * @return
     */
    @RequestMapping("/adduser")
    public Integer addUser(UserInfo userInfo) {
        if(userInfo == null || !StringUtils.hasLength(userInfo.getUsername())
                || !StringUtils.hasLength(userInfo.getPassword())) {
            return 0;
        }
        TransactionStatus transactionStatus = transactionManager.getTransaction(transactionDefinition);

        int result = userService.addUser(userInfo);
        System.out.println("添加" + result);
        transactionManager.commit(transactionStatus);
//        transactionManager.rollback(transactionStatus);
        return result;
    }

    /**
     * 通过注解的方式开始事务
     * @param userInfo
     * @return
     */
    @Transactional
    @RequestMapping("/insert")
    public Integer insert(UserInfo userInfo) {
        if(userInfo == null || !StringUtils.hasLength(userInfo.getUsername())
                || !StringUtils.hasLength(userInfo.getPassword())) {
            return 0;
        }
        int result = userService.addUser(userInfo);
        System.out.println("添加" + result);
        try {
            System.out.println(10/0);
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println(e.getMessage());
//              throw e;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return result;
    }
}
