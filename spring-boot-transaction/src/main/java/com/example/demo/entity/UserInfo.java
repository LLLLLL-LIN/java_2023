package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-13
 * Time:09:34
 */
@Data
public class UserInfo {
    private Integer id;
    private String username;
    private String password;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private Integer state;
}
