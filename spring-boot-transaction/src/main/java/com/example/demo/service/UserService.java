package com.example.demo.service;

import com.example.demo.entity.UserInfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-13
 * Time:09:45
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    public Integer addUser(UserInfo userInfo) {
        return userMapper.addUser(userInfo);
    }

    public Integer insert(UserInfo userInfo) {
        return userMapper.insert(userInfo);
    }
}
