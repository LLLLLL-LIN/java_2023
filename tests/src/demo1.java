/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-06-22
 * Time:12:07
 */

// 有的时候基础的东西越能体现你的实力，走得越远，目标好像越来越模糊了。
public class demo1 {
    public static void main(String[] args) {
        System.out.println(getSum(100));
        System.out.println(getSum1(100));
    }

    // 循环求 1-100的和。
    private static int getSum1(int n) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    // 递归求 1-100的和。
    public static int getSum(int a) {
        if (a > 1) {
            return getSum(a - 1) + a;
        }
        return 1;
    }
}
