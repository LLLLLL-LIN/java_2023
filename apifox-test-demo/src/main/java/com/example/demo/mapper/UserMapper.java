package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-06-18
 * Time:09:47
 * @author 李奥林
 * 用户类的Mapper层;
 */

/**
 * @Author : liaolin
 * @UserMapper : 管理User的数据访问;
 * @getUser : 根据uid获取User;
 */
@Mapper
public interface UserMapper {
    /**
     * 根据用户ID(uid)获取对应的用户信息。
     * @param uid 用户ID
     * @return 对应的用户对象，如果不存在则返回null
     */
    User getUser(Integer uid);

    /**
     * 用户注册 : 根据User的实例化对象插入数据;
     * @param user : 要注册的用户;
     * @return : 返回值为数据局操作影响的行数;
     */
    Integer insertUser(User user);
}
