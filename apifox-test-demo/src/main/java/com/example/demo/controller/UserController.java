package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-06-18
 * Time:09:37
 */

/**
 * @author 李奥林
 *  用户类的接口层;
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 根据uid查询单个用户;
     * @param uid : user的自增主键;
     * @return 统一的返回格式AjaxResult;
     */
    @RequestMapping("/get_user")
    public AjaxResult getUser(Integer uid) {
        User user = new User();
        // 对数据库访问到的结果赋值,将赋值后的user进行包装返回;
        user = userService.getUser(uid);
        return AjaxResult.success(200,user);
    }

    /**
     * 用户注册接口;
     * @param user : 用来接受前端的数据的,User的实例化对象;
     * @return : 返回统一类型的返回对象;
     */
    @RequestMapping("/reg")
    public AjaxResult reg(@RequestBody User user) {
        // 针对前端数据进行判空处理;
        if (user == null) {
            return AjaxResult.fail(-2,"前端参数为空!");
        }
        Integer effectNum = userService.insertUser(user);
        // 根据sql执行的返回结果来判断执行知否成功,执行结果为1或0;
        if (effectNum == 1) {
            return AjaxResult.success(200,null);
        }
        return AjaxResult.fail(-1,"sql执行失败");
    }

}
