package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-06-18
 * Time:09:42
 */

/**
 * @Author:liaolin;
 * 用户实体类;
 */
@Data
public class User {
    /**
     * 用户ID，主键。
     */
    private Long uid;

    /**
     * 用户名，用于用户登录。
     */
    private String username;

    /**
     * 用户密码，用于用户登录，需加密存储。
     */
    private String password;

    /**
     * 用户的电子邮件地址。
     */
    private String email;

    /**
     * 用户的电话号码。
     */
    private String phoneNumber;

    /**
     * 用户的显示名，用户在聊天平台上的昵称。
     */
    private String displayName;

    /**
     * 用户的状态信息或签名。
     */
    private String statusMessage;

    /**
     * 用户的最后登录时间。
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime lastLoginTime;

    /**
     * 用户账号创建时间。
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 用户账号更新时间。
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}
