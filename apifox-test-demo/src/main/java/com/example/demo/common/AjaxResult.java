package com.example.demo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一数据格式返回
 * code : 状态码;
 * message : 状态码描述信息;
 * data : 返回的数据;
 */
@Data
public class AjaxResult implements Serializable {
    private Integer code;
    private String message;
    private Object data;

    /**
     * 操作成功返回的结果
     * 状态码为 200;
     * 返回结果描述为空;
     */
    public static AjaxResult success(Object data) {
        AjaxResult result = new AjaxResult();
        result.setCode(200);
        result.setMessage("");
        result.setData(data);
        return result;
    }

    /**
     * 操作成功返回的结果;
     * 自定义状态码;
     * 返回结果描述为空;
     */
    public static AjaxResult success(int code, Object data) {
        AjaxResult result = new AjaxResult();
        result.setCode(code);
        result.setMessage("");
        result.setData(data);
        return result;
    }


    /**
     * 返回失败结果
     * 自定义状态码以及描述信息;
     * 数据为 null;
     */
    public static AjaxResult fail(int code, String message) {
        AjaxResult result = new AjaxResult();
        result.setCode(code);
        result.setMessage(message);
        result.setData(null);
        return result;
    }

}
