package com.example.demo.service;

import com.example.demo.common.AjaxResult;
import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-06-18
 * Time:10:17
 * @author 李奥林
 * User的service层;
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 根据uid获取用户的Service层;
     * @param uid 用户的主键id;
     * @return 返回单个User;
     */
    public User getUser(Integer uid) {
        return userMapper.getUser(uid);
    }

    /**
     * 用户注册 : 根据User的实例化对象进行数据库插入操作;
     * @param user : User的实例化对象;
     * @return : 返回值为sql操作影响行数;
     */
    public Integer insertUser(User user) {
        return userMapper.insertUser(user);
    }
}
