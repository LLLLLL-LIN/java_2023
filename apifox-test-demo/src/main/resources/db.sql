-- 创建聊天平台用户表
CREATE TABLE chat_user (
    uid BIGINT PRIMARY KEY auto_increment, -- 用户ID，作为主键
    username VARCHAR(255) NOT NULL unique, -- 用户名，用于用户登录，不允许为空
    password VARCHAR(255) NOT NULL, -- 用户密码，用于用户登录，不允许为空。在实际应用中应当加密存储
    email VARCHAR(255), -- 用户的电子邮件地址
    phone_number VARCHAR(20), -- 用户的电话号码
    display_name VARCHAR(255), -- 用户在聊天平台上的昵称
    status_message VARCHAR(255), -- 用户的状态信息或签名
    last_login_time DATETIME, -- 用户的最后登录时间，使用DATETIME类型存储
    create_time DATETIME, -- 用户账号创建时间，使用DATETIME类型存储
    update_time DATETIME -- 用户账号更新时间，使用DATETIME类型存储
);

--mysql> INSERT INTO chat_user (id, username, password, email, phone_number, display_name, status_message, last_login_time, create_time, update_time)
--    -> VALUES
--    -> (1, 'user1', 'password1', 'user1@example.com', '1234567890', 'User One', 'Hello, I am User One', '2024-06-18 10:00:00', '2024-06-01 08:00:00', '2024-06-18 10:00:00'),
--    -> (2, 'user2', 'password2', 'user2@example.com', '0987654321', 'User Two', 'Hey there! I am User Two', '2024-06-17 15:30:00', '2024-06-02 09:30:00', '2024-06-17 15:30:00'),
--    -> (3, 'user3', 'password3', 'user3@example.com', NULL, 'User Three', NULL, NULL, '2024-06-03 11:45:00', '2024-06-03 11:45:00'),
--    -> (4, 'user4', 'password4', NULL, '9876543210', 'User Four', 'Nice to meet you!', '2024-06-16 18:20:00', '2024-06-04 13:15:00', '2024-06-16 18:20:00'),
--    -> (5, 'user5', 'password5', 'user5@example.com', '1234543210', 'User Five', 'Enjoying the day!', '2024-06-15 12:00:00', '2024-06-05 14:00:00', '2024-06-15 12:00:00'),
--    -> (6, 'user6', 'password6', NULL, NULL, 'User Six', NULL, '2024-06-14 09:10:00', '2024-06-06 16:45:00', '2024-06-14 09:10:00'),
--    -> (7, 'user7', 'password7', 'user7@example.com', '9998887777', 'User Seven', 'Hello world!', '2024-06-13 20:30:00', '2024-06-07 20:30:00', '2024-06-13 20:30:00'),
--    -> (8, 'user8', 'password8', NULL, '1112223333', 'User Eight', 'Feeling good!', '2024-06-12 17:45:00', '2024-06-08 09:00:00', '2024-06-12 17:45:00'),
--    -> (9, 'user9', 'password9', 'user9@example.com', '5556667777', 'User Nine', 'Welcome!', '2024-06-11 14:20:00', '2024-06-09 11:30:00', '2024-06-11 14:20:00'),
--    -> (10, 'user10', 'password10', NULL, NULL, 'User Ten', 'Happy chatting!', '2024-06-10 10:00:00', '2024-06-10 10:00:00', '2024-06-10 10:00:00');
--Query OK, 10 rows affected (0.02 sec)
--Records: 10  Duplicates: 0  Warnings: 0
