function sendMessage() {
  var uid = document.getElementById('uidInput').value;
  fetch(`http://localhost:8080/user/get_user?uid=${uid}`)
    .then(response => response.json())
    .then(data => {
      if (data.code === 200) {
        displayUserInfo(data.data);
      } else {
        alert('获取用户信息失败：' + data.message);
      }
    })
    .catch(error => {
      console.error('Error fetching user data:', error);
      alert('获取用户信息失败，请检查网络连接。');
    });
}

function displayUserInfo(user) {
  var resultDiv = document.getElementById('result');
  resultDiv.innerHTML = `
    <p>UID: ${user.uid}</p>
    <p>用户名: ${user.username}</p>
    <p>密码: ${user.password}</p>
    <p>邮箱: ${user.email}</p>
    <p>电话号码: ${user.phoneNumber}</p>
    <p>昵称: ${user.displayName}</p>
    <p>状态信息: ${user.statusMessage}</p>
    <p>最后登录时间: ${user.lastLoginTime}</p>
    <p>账号创建时间: ${user.createTime}</p>
    <p>账号更新时间: ${user.updateTime}</p>
  `;
}

function handleKeyDown(event) {
  if (event.key === 'Enter') {
    sendMessage();
  }
}
