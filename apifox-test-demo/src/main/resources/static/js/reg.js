document.getElementById('registrationForm').addEventListener('submit', function(event) {
    event.preventDefault();
    registerUser();
});

document.getElementById('registrationForm').addEventListener('keyup', function(event) {
    if (event.key === 'Enter') {
        event.preventDefault();
        registerUser();
    }
});

function registerUser() {
    const form = document.getElementById('registrationForm');
    const currentTime = new Date().toISOString().slice(0, 19).replace('T', ' ');

    const userData = {
        username: form.username.value,
        password: form.password.value,
        email: form.email.value,
        phoneNumber: form.phoneNumber.value,
        displayName: form.displayName.value,
        statusMessage: form.statusMessage.value,
        lastLoginTime: currentTime,
        createTime: currentTime,
        updateTime: currentTime
    };

    fetch('/user/reg', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userData),
    })
    .then(response => response.json())
    .then(data => {
        if (data.code === 200) {
            alert('注册成功');
            form.reset();
        } else {
            alert('注册失败: ' + data.message);
        }
    })
    .catch(error => {
        console.error('注册请求出错:', error);
        alert('注册请求出错，请稍后重试');
    });
}
