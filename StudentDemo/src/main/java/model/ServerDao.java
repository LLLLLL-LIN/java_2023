package model;

import javax.net.ssl.SNIServerName;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class ServerDao {
    // 根据工作人员的名字查找信息。
    public Server selectByName(String serverName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from server where serverName = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Server server = new Server();
                server.setServertId(resultSet.getInt("serverId"));
                server.setServerName(resultSet.getString("serverName"));
                server.setServerTime(resultSet.getString("serverTime"));
                server.setServerPrice(resultSet.getInt("serverPrice"));
                return server;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    // 查找全部的工作人员，采用顺序表返回。
    public static List<Server> selectAll() {
        List<Server> serverList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from server";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Server server = new Server();
                server.setServertId(resultSet.getInt("serverId"));
                server.setServerName(resultSet.getString("serverName"));
                server.setServerTime(resultSet.getString("serverTime"));
                server.setServerPrice(resultSet.getInt("serverPrice"));
                serverList.add(server);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return serverList;
    }

    // 4. 从工作人员表中, 根据工作人员 id 删除博客.
    public void delete(int serverId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from server where serverId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, serverId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 根据工作人员 workerId，更改相关信息。
    public void update(int serverId, String serverName, String serverTime, int serverPrice) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE server SET serverName = ?, serverTime = ?, serverPrice = ? WHERE workerId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, serverName);
            statement.setString(2, serverTime);
            statement.setInt(3, serverPrice);
            statement.setInt(4, serverId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }
}
