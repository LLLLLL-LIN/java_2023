package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:32
 */
public class Consumer {
    private int consumerId = 0;
    private String consumerName;
    private String consumerSex;
    private String level;
    private String discount;
    private Timestamp addTime;

    public int getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getConsumerSex() {
        return consumerSex;
    }

    public void setConsumerSex(String consumerSex) {
        this.consumerSex = consumerSex;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

//    public Timestamp getAddTime() {
//        return addTime;
//    }

    public String getAddTime() {
        // 使用 SimpleDateFormat 来完成时间戳到格式化日期时间的转换.
        // 这个转换过程, 需要在构造方法中指定要转换的格式, 然后调用 format 来进行转换
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(addTime);
    }


    public void setAddTime(Timestamp addTime) {
        this.addTime = addTime;
    }
}
