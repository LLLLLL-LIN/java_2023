package model;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:02:39
 */
public class Server {
    private int servertId = 0;
    private String serverName;
    private String serverTime;
    private int serverPrice;

    public int getServertId() {
        return servertId;
    }

    public void setServertId(int servertId) {
        this.servertId = servertId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public int getServerPrice() {
        return serverPrice;
    }

    public void setServerPrice(int serverPrice) {
        this.serverPrice = serverPrice;
    }
}
