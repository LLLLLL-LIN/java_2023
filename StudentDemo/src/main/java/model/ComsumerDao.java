package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class ComsumerDao {

    // 根据会员的名字查找信息。
    public Consumer selectByName(String consumerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from consumer where consumer = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Consumer consumer = new Consumer();
                consumer.setConsumerId(resultSet.getInt("consumerId"));
                consumer.setConsumerName(resultSet.getString("consumerName"));
                consumer.setAddTime(resultSet.getTimestamp("addTime"));
                consumer.setLevel(resultSet.getString("level"));
                consumer.setDiscount(resultSet.getString("discount"));
                return consumer;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    // 查找全部的会员，采用顺序表返回。
    public static List<Consumer> selectAll() {
        List<Consumer> consumerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from consumer";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Consumer consumer = new Consumer();
                consumer.setConsumerId(resultSet.getInt("consumerId"));
                consumer.setConsumerName(resultSet.getString("consumerName"));
                consumer.setAddTime(resultSet.getTimestamp("addTime"));
                consumer.setLevel(resultSet.getString("level"));
                consumer.setDiscount(resultSet.getString("discount"));
                consumerList.add(consumer);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return consumerList;
    }

    // 4. 从会员表中, 根据会员 id 删除会员信息.
    public void delete(int consumerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from consumer where consumerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, consumerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 根据会员 consumerId，更改相关信息。
    public void update(int consumerId, String consumerName, String consumerSex, String level, String disconnt, Timestamp addTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE consumer SET conseumerName = ?, consumerSex = ?,lever = ?, discount = ?, addTime = ? WHERE consumerId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, consumerName);
            statement.setString(2, consumerSex);
            statement.setString(3, level);
            statement.setString(4, disconnt);
            statement.setTimestamp(5, addTime);
            statement.setInt(6, consumerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }
}
