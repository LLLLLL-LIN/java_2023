/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-02
 * Time:14:11
 */
class Animal2{
    static String name = "动物的名字";
    static String age = "动物的年龄";
    public Animal2(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public static void print(){
//        Animal2.print();
//        类中也是能直接类名.方法名来调用的,不只能直接方法名调用;
        System.out.println(name + age);
    }

    public void Animal2Func1() {
        System.out.println("Animal2的方法用来给子类使用super来调用");
    }
    String Animal2Str1 = "  Animal2的属性给子类使用super来调用";
}
class Dog2 extends Animal2{
    // 子类和父类有同名字段,优先使用自己的,相当于重写了;
    String name = "狗的名字";
    String age = "狗的年龄";
    // 未定义为 null; 如果子类没重写属性,name 和 age 是父类的值;
//    String name;
//    String age;
   public Dog2(String name, String age) {
       // 调用父类的构造方法;
       super(name, age);
       // 未写以下这两行,就只是帮父类构造了属性的常量;
       // 输出语句中, Dog 类的属性的定义还是一开始定义的;
//       this.name = name;
//       this.age = age;
   }
   public void Animal2Func1() {
       System.out.println("这是子类重写父类的方法");
   }
   public void func(){
       System.out.println("未使用super方法");
       Animal2Func1();
       // 子类可以使用 super 调用父类的方法;
       System.out.println("使用super方法");
       super.Animal2Func1();


       // 子类可以调用父类的变量;
       System.out.println(super.Animal2Str1);

       // 子类未重写,子类直接方法名就调用;
       System.out.println("子类未重写,直接变量名,方法名就可以调用到父类所有的属性和方法" + Animal2Str1);
   }
}
public class Demo3 {
    public static void main(String[] args) {
        // 实例化子类,调用了子类重写的构造方法, 先得 super 调用父类的构造方法, 再在同一构造方法中来写构造子类的的代码语句;
        // 不写子类的构造语句,子类的变量还是一开始定义的值;
        Dog2 dog2 = new Dog2("实例化狗的名字","实例化狗的年龄");
        System.out.println(dog2.name);
        System.out.println(dog2.age);
        // 父类的变量的值,也是在子类实例化调用两个参数的构造方法的时候更改了;
        // 不是父类定义的时候的值;
        // 静态方法,在类外直接类名.方法名;
        Animal2.print();

        //
        dog2.func();
    }
}
