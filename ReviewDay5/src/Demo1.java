import sun.nio.cs.ext.SJIS;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-02
 * Time:11:36
 */


class Animal {
    String name;
    int age;

//    public Animal(String name) {
//        this.name = name;
//    }
//
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println(this.name + " " + this.age);
    }
    public void print() {
        System.out.println(this.name + " " +this.age);
        // 子类重写父类的 eat() 方法之后,在父类中调用的是子类的 eat() 方法;
        eat();
        // 父类引用,引用子类对象,向上转型,这里的 this 是当前的对象的引用,这里的当前对象是子类;
        this.eat();
    }
    public void eat() {
        System.out.println("动物的吃方法!");
    }
}

// 创建 Dog 类继承自 Animal 类;
class Dog extends Animal {
    // 这里的相当于也是重写了
    String name = new String("gou");
    int age = 11;
    public Dog(String name, int age) {
        super(name,age);
        // 这里的 name 是子类重写之后的name,如果没有使用 super 调用的是子类对象的;
        System.out.println(super.name);
        // 使用 super 调用的就是子类自己的 name;
        // 为什么 main 方法中,new 对象的时候的传参没有影响到子类的属性值,是因为在构造函数当中,
        // 子类只是用 super 先帮助父类构造了,而没有帮助自己构造,如果加上自己的构造函数,就能根据传参改变自己的属性值;
        System.out.println(this.name);
//        this.name = name;
//        this.age = age;
        // 这里是子类自己的构造函数之后的属性值;
        System.out.println(this.name + "   " + this.age);
    }

    //重写父类的 eat 方法;
    public void eat() {
        System.out.println("狗类的吃方法!");
    }

    // 重写父类的 ptint 方法;
    public void ptint() {
        System.out.println(name);
        System.out.println(age);
        System.out.printf(name,age);
    }
}

public class Demo1{
    public static void main(String[] args) {
//        Animal animal = new Dog("狗", 12);
//        Animal animal = new Dog("dog",12);
//        animal.eat();// 运行时多态;
//        Animal animal = new Animal("动物类",1);
        Dog dog = new Dog("gou2",53);

//        animal.print();
    }
}
