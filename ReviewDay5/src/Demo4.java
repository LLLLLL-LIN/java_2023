import sun.security.timestamp.TSRequest;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-02
 * Time:17:09
 */
class Test {
    public final StringBuilder sb = new StringBuilder("abc");
    public String str = new String("String");
    public final char[] char1 = {'x', 'y', 'z'};
    ArrayList<Character> char2 = new ArrayList<>();

    final void add(String str) {
        sb.append(str);
        str += 123;
        char1[0] = 'a';
        char2.add('x');
        char2.add('y');
        char2.add('z');
    }
    // final 修饰引用类型的变量的时候,引用的对象再定义之后是不能够更改的,但是能修改对象里面的内容,
    // 这里用到 StringBuilder 而不用 String 是因为 String 类本身也是 final 修饰的,不能更改对象,String 的修改常量本身就是借助引用不同的对象;
    // 而 StringBuilder 能做到修改是因为,在这里 StringBuilder 一直指向的是一个对象,没有更改,只是更改了对象里面的值;
//    void change() {
//        StringBuilder sb2 = new StringBuilder("123");
    // sb final修饰;无法修改 sb 的指向;
//        sb = sb2;
//    }
}

public class Demo4 {
    public static void main(String[] args) {
        Test test = new Test();
        // final 修饰的引用类型变量,不能更改引用的对象,但是能修改引用的对象的内容;
        test.add("ef");
        System.out.println(test.sb);
        // String类是不能被修改的 被 final 修饰的变量
        System.out.println(test.str);
        //
        System.out.println(test.char1);
        //
        System.out.println(test.char2);
    }
}
