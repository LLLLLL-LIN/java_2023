/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-02
 * Time:13:20
 */
class Func {
    public static void func1() {
        System.out.println("func1!");
    }

    public static void func2() {
        System.out.println("func2!");
    }

    public void func3() {
        // static修饰的属性,方法,类中直接方法名调用和不添加 static修饰 一致;
        func2();
    }
}

public class Demo2 {
    public static void main(String[] args) {
        // static 修饰的成员属性(类属性),成员方法(类方法),类外还是得类名.方法名;
        // 而在类中就可以是直接方法名调用;
        Func.func1();
        // 不是static还是得利用对象来访问;
        Func func = new Func();
        func.func3();

    }
}
