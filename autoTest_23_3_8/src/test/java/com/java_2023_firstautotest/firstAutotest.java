package com.java_2023_firstautotest;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-08
 * Time:下午2:19
 */
public class firstAutotest {
    WebDriver driver = new EdgeDriver();

    public void quit() {
        driver.quit();
    }

    //窗口大小的设置;
    public void showWindowsTest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_pg");
        Thread.sleep(2000);
        driver.manage().window().minimize();
        Thread.sleep(2000);
        driver.manage().window().maximize();
        Thread.sleep(2000);
        driver.manage().window().setSize(new Dimension(600, 500));
        Thread.sleep(2000);
    }

    //navigate 导航;
    public void navigation1Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_12_dg");
        Thread.sleep(2000);
        String curwindows1 = driver.getWindowHandle();
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        String curwindows2 = driver.getWindowHandle();
        Set<String> allwindows = driver.getWindowHandles();
        for (String windows : allwindows) {
            if (windows != curwindows1) {
                driver.switchTo().window(windows);
            }
        }
        driver.findElement(By.xpath("//*[@id=\"ww\"]")).sendKeys("hello");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#s_btn_wr")).click();
        driver.navigate().back();
        Thread.sleep(2000);
        driver.navigate().forward();
        Thread.sleep(2000);
        driver.navigate().back();
        Thread.sleep(2000);
        driver.navigate().back();
        Thread.sleep(2000);
    }

    public void navigation2Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_12_dg");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        driver.navigate().back();
        Thread.sleep(2000);
        driver.navigate().forward();
        Thread.sleep(2000);
    }
}
