package com.webAutotest;

import org.checkerframework.checker.units.qual.A;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import sun.awt.windows.ThemeReader;
import sun.security.krb5.internal.KdcErrException;

import java.util.Set;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-08
 * Time:下午5:09
 */
public class autoTest {
    private WebDriver driver = new EdgeDriver();
//    private WebDriver driver = new ChromeDriver();
//    private ChromeDriver driver = new ChromeDriver();

    public void close() {
        driver.quit();
    }

    public void sendkey() throws InterruptedException {
        //1
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_pg");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#kw")).sendKeys("hello");
//        driver.findElement(By.cssSelector("kw")).sendKeys("selenium");
        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("#su")).click();
        driver.findElement(By.cssSelector("#kw")).clear();
        Thread.sleep(2000);

        //2
//        Thread.sleep(1000);
//        driver.get("https://www.baidu.com/?tn=15007414_12_dg");
//        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("#kw")).sendKeys("hello");
//        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("#su")).click();
//        Thread.sleep(2000);
    }

    //打印的是百度首页的信息,不是当前页面;
    public void printtest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_pg");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"s-top-left\"]/a[1]")).click();
        Thread.sleep(2000);
        String curUrl = driver.getCurrentUrl();
        String title = driver.getTitle();
        System.out.println("title  expect:百度新闻——海量中文咨询平台   actual:" + title);
        System.out.println("url    expect:https://news.baidu.com/   actual:" + curUrl);
    }

    //找到当前的WindouHandle 进行当前页面信息的打印;
    public void changewindowsTest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("https://www.baidu.com/?tn=15007414_12_dg");
        String curwindows1 = driver.getWindowHandle();
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        Thread.sleep(2000);
        String curwindows2 = driver.getWindowHandle();
        Set<String> allwindows = driver.getWindowHandles();
        for (String windows : allwindows) {
            if (windows != curwindows1) {
                driver.switchTo().window(windows);
            }
        }
        String curUrl = driver.getCurrentUrl();
        String title = driver.getTitle();
        System.out.println("title expect:百度新闻——海量中文资讯平台   actual:" + title);
        System.out.println("url   expect:https://news.baidu.com/  actual:" + curUrl);
    }

    //alter
    public void alter1Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/alert.html");
        driver.findElement(By.cssSelector("#tooltip")).click();
        Thread.sleep(2000);
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    //alert
    public void alter2Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/alert.html#");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"tooltip\"]")).click();
        Alert alert = driver.switchTo().alert();
        Thread.sleep(2000);
        alert.accept();
    }

    //comfirm1,accept;
    public void comfirm1Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/confirm.html");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("body > input[type=button]")).click();
        Alert alert = driver.switchTo().alert();
        Thread.sleep(2000);
        alert.accept();
        Thread.sleep(2000);
        System.out.println(driver.findElement(By.cssSelector("body")).getText());
    }

    //comfirm2,cancel;
    public void comfirm2Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/confirm.html");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("body > input[type=button]")).click();
        Alert alert = driver.switchTo().alert();
        Thread.sleep(2000);
        alert.dismiss();
        Thread.sleep(2000);
        System.out.println(driver.findElement(By.cssSelector("body")).getText());
    }

    //level_locate,鼠标键盘模拟;
    public void mounseAndKeyboard1Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/level_locate.html");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("body > div:nth-child(2) > div > div > a")).click();
        Thread.sleep(2000);
        WebElement element1 = driver.findElement(By.cssSelector("#dropdown1 > li:nth-child(1) > a"));
        WebElement element2 = driver.findElement(By.cssSelector("#dropdown1 > li:nth-child(2) > a"));
        WebElement element3 = driver.findElement(By.cssSelector("#dropdown1 > li:nth-child(3) > a"));
        WebElement element4 = driver.findElement(By.cssSelector("#dropdown1 > li:nth-child(5) > a"));
        Actions actions = new Actions(driver);
        actions.clickAndHold(element1).perform();
        Thread.sleep(500);
        actions.clickAndHold(element2).perform();
        Thread.sleep(500);
        actions.clickAndHold(element3).perform();
        Thread.sleep(500);
        actions.clickAndHold(element4).perform();
        Thread.sleep(2000);
        actions.click(element4).perform();
    }

    public void mounseAndKeyboard2Test() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/level_locate.html");
        Thread.sleep(2000);
        driver.findElement(By.cssSelector("body > div:nth-child(3) > div > div > a")).click();
        Thread.sleep(2000);
        WebElement element = driver.findElement(By.cssSelector("body > div:nth-child(3) > div > div > ul > li:nth-child(5) > a"));
        Actions actions = new Actions(driver);
        actions.clickAndHold(element).perform();
        Thread.sleep(2000);
        actions.click(element).perform();
    }

    //Prompt;弹窗输入框;
    public void sendKeyTest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/Prompt.html");
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/input")).click();
        Thread.sleep(2000);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("高薪!!!");
        Thread.sleep(2000);
        alert.accept();
        Thread.sleep(2000);

    }

    //select;
    public void selectTest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/select.html");
        Thread.sleep(2000);
//        driver.findElement(By.cssSelector("#ShippingMethod")).click();
        WebElement element = driver.findElement(By.cssSelector("#ShippingMethod"));
        Select select = new Select(element);
        Thread.sleep(2000);
        select.selectByIndex(2);
        Actions actions = new Actions((WebDriver) select);
//        select.selectByValue("10.69");
//        select.selectByVisibleText("UPS 2nd Day Air ==> $9.63");
        Thread.sleep(2000);
    }

    //upload;
    public void uploadTest() throws InterruptedException {
        Thread.sleep(1000);
        driver.get("file:///D:/%E6%AF%94%E7%89%B9/Java/5.Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95/Java%E8%BD%AF%E4%BB%B6%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81/selenium4html/upload.html");
        WebElement element = driver.findElement(By.cssSelector("body > div > div > input[type=file]"));
        Thread.sleep(2000);
        element.sendKeys("D:\\比特\\Java\\5.Java软件测试\\Java软件测试代码\\selenium4html\\upload.html");
        Thread.sleep(2000);
    }
}
