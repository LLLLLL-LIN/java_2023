// 注销;
function logout(){
    if(confirm("确认注销？")){
        jQuery.ajax({
            url:"/user/logout",
            type:"POST",
            data:{},
            success:function(result){
                if(result!=null && result.code==200){
                    location.href = "/login.html";
                }
            }
        });
    }
}


// 获取当前url参数的公共方法
function getUrlValue(key){
    var params = location.search;
    if(params.length>1){
        params = location.search.substring(1);
        var paramArr = params.split("&");
        for(var i=0;i<paramArr.length;i++){
            var kv = paramArr[i].split("=");
            if(kv[0]==key){
                // 要查询的参数
                return kv[1];
            }
        }
    }
    return "";
}


// 未读消息提示音方法;
function playNotificationSound() {
  var audio = document.getElementById("notificationSound");
  audio.play();
}


// 查找用户的设置情况;
function editSelect(callback) {
     jQuery.ajax({
        url:"/edit/editselect",
        type:"POST",
        data:{},
        success:function(result) {
            callback(result);
        }
    });
}


// 用一个全局变量来记录当前的好友申请数;
let initialUnagreeCount = localStorage.getItem('initialUnagreeCount') || 0;
function rstcount() {
    editSelect(function(result) {
        var isCloseFriendRequest = result.frequestnotice;
        jQuery.ajax({
            url:"/friendship/rstcount",
            type:"POST",
            data:{},
            success:function(result) {
                jQuery("#rstcount").text(result.data);
                // 如果有新的好友申请,就响提示音;
                // 同时用户对于好友申请的提示音是 true 的才行;
                if(isCloseFriendRequest && result.data > initialUnagreeCount) {
                    playNotificationSound();
                    localStorage.setItem('isCloseFriendRequest', isCloseFriendRequest);
                }
                // 未同意的好友申请数目更新;
                initialUnagreeCount = result.data;
                // 将 initialUnagreeCount 的值保存到本地存储
                localStorage.setItem('initialUnagreeCount', initialUnagreeCount);
            }
        });
    });
}


// 用一个全局变量来记录未读消息数;
let initialUnreadCount = localStorage.getItem('initialUnreadCount') || 0;
function chatnum() {
editSelect(function(result) {
    let isCloseMessageNotification = result.msgnotice;
       jQuery.ajax({
            url:"/friendship/chatnum",
            type:"POST",
            data:{},
            success:function(result) {
                jQuery("#chatnum").text(result.data);
                // 如果未读消息数比全局变量的大了,也就是新增了,就响提示音;
                // 同时用户对未读消息的设置也得是 true 才行;
                if(isCloseMessageNotification && result.data > initialUnreadCount) {
                    playNotificationSound();
                    localStorage.setItem('isCloseMessageNotification', isCloseMessageNotification);
                }
                // 未读消息数更新;
                initialUnreadCount = result.data;
                // 将 initialUnreadCount 的值保存到本地存储
                localStorage.setItem('initialUnreadCount', initialUnreadCount);
            }
        });
    });
}


<!--    设置模块-->
// 用户点开设置的渲染方法(用来调用);
function renderSettings(edit) {
    // 根据 edit 对象中的属性值来判断是否选中复选框
    var closeMessageNotification = document.getElementById('closeMessageNotification');
    var closeFriendRequest = document.getElementById('closeFriendRequest');

    // 根据 edit 对象中的属性值来判断是否选中复选框;
    // 渲染消息提示音复选框
    closeMessageNotification.checked = edit.data.msgnotice;

    // 渲染好友申请提示音复选框
    closeFriendRequest.checked = edit.data.frequestnotice;
}


// 这个函数用于打开设置弹窗，它通过获取设置弹窗的 DOM 元素并将其样式中的 display 属性设置为 block 来实现弹窗的显示;
function openSettingsModal() {
// 在打开时调用渲染方法，并传入回调函数;
    editSelect(function(result) {
        renderSettings(result); // 调用渲染方法并传入 result;
    });

    var modal = document.getElementById("settingsModal");
    modal.style.display = "block";
}


// 这个函数用于关闭设置弹窗，它通过获取设置弹窗的 DOM 元素并将其样式中的 display 属性设置为 none 来实现弹窗的隐藏;
function closeSettingsModal() {
    var modal = document.getElementById("settingsModal");
    modal.style.display = "none";
}


// 这个函数用于保存用户对设置的选择，您可以在这个函数中编写保存设置的逻辑，根据用户的选择进行相应的操作;
function saveSettings() {
    // 在这里进行保存设置的逻辑
    var msgnotice = document.getElementById('closeMessageNotification').checked;
    var frequestnotice = document.getElementById('closeFriendRequest').checked;
    jQuery.ajax({
        url:"/edit/editupdate",
        type:"POST",
        data:{"msgnotice":msgnotice, "frequestnotice":frequestnotice},
        success:function(result) {

        }
    });
    closeSettingsModal(); // 保存设置后关闭弹窗
}

<!--    // 定时发送请求;-->
    setInterval(function() {
        // 获取当前登录用户的好友申请数;
        rstcount();
        // 获取未读消息数;
        chatnum();
    }, 5000);
