package com.example.demo.controller;


import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.component.IsOnline;
import com.example.demo.entity.Edit;
import com.example.demo.entity.User;
import com.example.demo.service.EditService;
import com.example.demo.service.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:15:26
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private IsOnline isOnline;

    @Autowired
    private EditService editService;

    // 注册模块;
    @RequestMapping("/reg")
    public AjaxResult addUser(@RequestParam("netname") String netname,
                              @RequestParam("username") String username,
                              @RequestParam("password") String password,
                              @RequestParam(value = "avatar", required = false) MultipartFile avatar) throws IOException {

        // 在这里创建 user 对象并使用接收到的数据进行初始化
        User user = new User();
        user.setNetname(netname);
        user.setUsername(username);
        user.setPassword(password);

        // 当头像的这个参数不为空;
        if(avatar != null) {
            // 处理头像文件

//           // 获取保存头像文件的路径(本地能够正常使用,但是服务器上采用的是 jar 包,似乎访问不到;
//            String uploadDir = new ClassPathResource("static/avatar/").getFile().getAbsolutePath();
//            Path uploadPath = Paths.get(uploadDir);


            // 获取保存头像文件的路径(服务器上采取这个写法尝试)
            Resource uploadDirResource = new ClassPathResource("static/avatar/");
            File uploadDir = uploadDirResource.getFile();
            Path uploadPath = uploadDir.toPath();

            // 确保目录存在，如果不存在则创建
            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }

            // 生成唯一文件名并保存头像文件到指定路径
            // 生成时间戳作为文件名的一部分
            long timestamp = System.currentTimeMillis();
            // 生成随机数作为文件名的一部分
            String randomNum = String.valueOf(new Random().nextInt(1000));
            // 将时间戳、随机数和文件扩展名拼接起来，作为唯一的文件名
            String uniqueFileName = "avatar_" + timestamp + "_" + randomNum + ".jpg";
            // 在上传目录下创建文件路径
            Path filePath = uploadPath.resolve(uniqueFileName);
            // 将上传的头像文件保存到指定路径
            avatar.transferTo(filePath.toFile());

            // 保存头像文件的相对路径
            String avatarPathInResource = "/avatar/" + uniqueFileName; // 相对于 resources/static/avatar 的路径
            user.setAvatarUrl(avatarPathInResource);

        }  else {
            // avatar为空,把后续注册用到的 user 参数的 avatarUrl 设置为 null;
            user.setAvatarUrl(null);
        }

        // 加盐加密;
        user.setPassword(PasswordUtils.encrypt(user.getPassword()));
        // 同时新增用户设置表;
        Edit edit = new Edit();
        edit.setUsername(user.getUsername());
        editService.editInsert(edit);
        return AjaxResult.success(userService.addUser(user));
    }

    // 登录模块;
    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, User user) {
        // 根据 username 查找该登录用户;
        User user1 = userService.selectByuname(user);
        // 判断有没有该账号的用户;
        if (user1 == null) {
            return AjaxResult.fail(-1, "用户名或密码错误");
        }

        // 这个是封装的验证密码的方法,只需要数据库的加盐后的密码和接口参数的密码;
        // 数据库密码附带盐值,封装了取盐值然后加盐再匹配的方法;
        if (PasswordUtils.check(user.getPassword(), user1.getPassword())) {
            // 密码匹配正确,建立会话;
            HttpSession session = request.getSession();
            session.setAttribute(AppVariable.USER_SESSION_KEY, user1);
//            System.out.println("当前登录用户是: " + session.getAttribute(AppVariable.USER_SESSION_KEY));

            // 设置最近一次在线的时间;
            userService.checkTime(user1.getUid());
            // 主要是登录了就在线状态 status 设置为 1;
            userService.upStatus1(user1.getUid());
            // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
            isOnline.isOnline();

            return AjaxResult.success(200, "密码正确");
        }
        return AjaxResult.success(0, null);
    }

    // 根据 netname 查找 User;
    @RequestMapping("/fnname")
    public AjaxResult findBynname(HttpServletRequest request, String netname) {
        // 获取当前登录用户;
        User user1 = UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if (user1 == null) {
            return AjaxResult.fail(-6, "获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 更新当前用户的在线状态 status 为1;
        userService.upStatus1(user1.getUid());
        // 判断所有用户的最近一次的在线时间,超过五分钟,置为离线;
        isOnline.isOnline();

        if (netname == null) {
            return AjaxResult.fail(-5, "传输参数为空");
        }
        Integer u1id = user1.getUid();
        // 根据用户名查找用户,采用模糊匹配 like 采用链表;
        List<User> list = userService.selectBynname(netname, u1id);
        if (list.size() == 0) {
            return AjaxResult.success(-1, "没有匹配的用户!");
        }
        // 循环置空密码;
        for (User user : list) {
            user.setPassword("");
        }

        return AjaxResult.success(list);
    }

    // 根据 username 查找 User;
    @RequestMapping("/funame")
    public AjaxResult findByuname(HttpServletRequest request, String username) {
        // 获取当前登录用户;
        User user3 = UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if (user3 == null) {
            return AjaxResult.fail(-6, "获取当前登录用户失败!");
        }
        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user3.getUid());
        // 更新当前用户的在线状态 status 为1;
        userService.upStatus1(user3.getUid());
        // 判断所有用户的最近一次的在线时间,超过五分钟,置为离线;
        isOnline.isOnline();

        // 参数为空,直接返回;
        if (username == null) {
            return AjaxResult.fail(-5, "传输内容为空");
        }
        User user = new User();
        user.setUsername(username);
        // 查找匹配 username 参数的用户;
        User user1 = userService.selectByuname(user);
        // user1 判空;
        if (user1 == null || user1.getUid().equals(user3.getUid())) {
            return AjaxResult.success(-1, "没有该账号!");
        }
        user1.setPassword("");
        return AjaxResult.success(user1);
    }


    // 个人信息查询(当前登录用户);
    @RequestMapping("personal")
    public AjaxResult personalInformation(HttpServletRequest request) {
        User user = UserSessionUtils.getUser(request);
        if (user == null) {
            return AjaxResult.fail(-1, "获取当前登录用户失败!");
        }
        User user1 = userService.selectByuname(user);
        if (user1 == null) {
            return AjaxResult.fail(-2, "查询用户信息失败!");
        }
        return AjaxResult.success(user1);
    }

    // 修改个人信息模块;
    @RequestMapping("/modify")
    public AjaxResult modifyPersonal(HttpServletRequest request,
                              @RequestParam("netname") String netname,
                              @RequestParam("password") String password,
                              @RequestParam(value = "avatar", required = false) MultipartFile avatar) throws IOException {

        // 在这里创建 user 对象并使用接收到的数据进行初始化
        User user = new User();

        // 如果用户填写了昵称;
        if(netname != null) {
            user.setNetname(netname);
        }

        // 如果用户提交了要更换的头像,进行头像的更新操作;
        if(avatar != null) {
            // 处理头像文件

//            // 获取保存头像文件的路径(本地能够正常使用,但是服务器上采用的是 jar 包,似乎访问不到;
//            String uploadDir = new ClassPathResource("static/avatar/").getFile().getAbsolutePath();
//            Path uploadPath = Paths.get(uploadDir);

            // 使用相对路径来保存上传文件
            String uploadDir = new ClassPathResource("static/avatar/").getFile().getAbsolutePath();
            Path uploadPath = Paths.get(uploadDir);


//            // 获取保存头像文件的路径(服务器上采取这个写法尝试)
//            Resource uploadDirResource = new ClassPathResource("static/avatar/");
//            File uploadDir = uploadDirResource.getFile();
//            Path uploadPath = uploadDir.toPath();


            // 确保目录存在，如果不存在则创建
            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }

            // 生成唯一文件名并保存头像文件到指定路径
            // 生成时间戳作为文件名的一部分
            long timestamp = System.currentTimeMillis();
            // 生成随机数作为文件名的一部分
            String randomNum = String.valueOf(new Random().nextInt(1000));
            // 将时间戳、随机数和文件扩展名拼接起来，作为唯一的文件名
            String uniqueFileName = "avatar_" + timestamp + "_" + randomNum + ".jpg";
            // 在上传目录下创建文件路径
            Path filePath = uploadPath.resolve(uniqueFileName);
            // 将上传的头像文件保存到指定路径
            avatar.transferTo(filePath.toFile());

            // 保存头像文件的相对路径
            String avatarPathInResource = "/avatar/" + uniqueFileName; // 相对于 resources/static/avatar 的路径
            user.setAvatarUrl(avatarPathInResource);
        }

        // 用户如果填写了要修改的密码;
        if(password != null && password.length() > 0) {
            user.setPassword(password);
            // 加盐加密;
            user.setPassword(PasswordUtils.encrypt(user.getPassword()));
        }
        // 获取当前登录用户;
        User user1 =  UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if(user1 == null) {
            return AjaxResult.fail(-1,"获取当前登录用户失败!");
        }
        user.setUid(user1.getUid());

        return AjaxResult.success(userService.modifyPersonal(user));
    }

    // 注销;
    @RequestMapping("logout")
    public AjaxResult logout(HttpSession session) {
        // 注销,会话置空;
        session.removeAttribute(AppVariable.USER_SESSION_KEY);
        return AjaxResult.success(1);
    }

    @RequestMapping("check")
    public AjaxResult check(HttpSession session) {
        return AjaxResult.success(1);
    }
}
