package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Edit;
import com.example.demo.entity.User;
import com.example.demo.mapper.EditMapper;
import com.example.demo.service.EditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-28
 * Time:16:31
 */
@RequestMapping("/edit")
@RestController
public class EditController {
    @Autowired
    private EditService editService;


    // 新增用户设置;
    @RequestMapping("/editinsert")
    public AjaxResult editInsert(Edit edit) {
        return AjaxResult.success(editService.editInsert(edit));
    }


    // 新增用户设置;
    @RequestMapping("/editupdate")
    public AjaxResult editUpdate(HttpServletRequest request, Edit edit) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-1,"获取当前登录用户失败!");
        }
        edit.setUsername(user.getUsername());
        return AjaxResult.success(editService.editUpdate(edit));
    }


    // 新增用户设置;
    @RequestMapping("/editselect")
    public AjaxResult editSelect(HttpServletRequest request, Edit edit) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-1,"获取当前登录用户失败!");
        }
        edit.setUsername(user.getUsername());
        return AjaxResult.success(editService.editSelect(edit));
    }
}
