package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.component.IsOnline;
import com.example.demo.entity.Friendship;
import com.example.demo.entity.MsgCount;
import com.example.demo.entity.User;
import com.example.demo.entity.VO.UserVO;
import com.example.demo.service.FriendshipService;
import com.example.demo.service.MsgCountService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:00:32
 */
@RestController
@RequestMapping("/friendship")
public class FriendshipController {
    @Autowired
    private FriendshipService friendshipService;
    @Autowired
    private UserService userService;
    @Autowired
    private MsgCountService msgCountService;
    @Autowired
    private IsOnline isOnline;

    // 发送好友申请;
    @RequestMapping("/contact")
    public AjaxResult contact(HttpServletRequest request, String username) {
        User user = new User();
        user.setUsername(username);
        // 根据前端的 username 去查找要添加的用户;
        User user1 = userService.selectByuname(user);
        // 根据 session 来获取当前发起好友申请的用户;
        User user2 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前添加用户失败!");
        }
        if(user2 == null) {
            return AjaxResult.fail(-6,"获取当前用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user2.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user2.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        List<Friendship> friendshipList = friendshipService.findFriends(user2.getUid());
        for (Friendship friendship : friendshipList) {
            // 拿到非当前登录用户的 uid 去获取用户;
            User user3 = userService.selectByUid(friendship.getU1id().equals(user2.getUid())?
                    friendship.getU2id():friendship.getU1id());
            // 如果二者的 username 一样,说明你已经添加过这个用户了;
            if(user3.getUsername().equals(username)) {
                return AjaxResult.success(-2,"您已经添加过当前用户!");
            }
        }
        // 通过好友关系的 u1id 和 u2id 对未读关系表插入条例;
        Friendship friendship = new Friendship();
        friendship.setU1id(user2.getUid());
        friendship.setU2id(user1.getUid());
//        // 建立好友关系就有两张未读消息表,双方针对对方各自有一张;
//        msgCountService.countadd(user2.getUid(),user1.getUid());
//        msgCountService.countadd(user1.getUid(),user2.getUid());
        return AjaxResult.success(friendshipService.contact(friendship));
    }


    // 好友管理页面的获取好友列表的接口;
    @RequestMapping("/friends")
    public AjaxResult findFriends(HttpServletRequest request) {
        // 获取当前登录用户;
        User user = UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 根据当前登录用户的 uid 拉取好友列表;
        List<Friendship> flist = friendshipService.findFriends(user.getUid());
        if(flist == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        List<User> list = new ArrayList<>(1000);
        for (int i = 0; i < flist.size(); i++) {
            // 针对要查找的用户的 uid 三元运算,取非当前登录用户的 uid;
            // 然后用这些用户的 uid 去查找得到 User 对象;
            User user1 = userService.selectByUid(
                    flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            user1.setPassword("");
            list.add(user1);
        }
        return AjaxResult.success(list);
    }

    // 好友申请页面的获取申请列表的接口;
    @RequestMapping("/findrequest")
    public AjaxResult findRequest(HttpServletRequest request) {
        // 获取当前登录用户;
        User user = UserSessionUtils.getUser(request);
        // 当前用户的判空;
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 根据当前登录用户的 uid 拉取好友申请列表;
        // 其中 status 要为 1, 也就是发送申请,但没建立好友关系;
        // 因为 status 为 2 是已经成为了好友;
        List<Friendship> rstList = friendshipService.findRequest(user.getUid());
        if(rstList == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        // 根据好友关系表中的 u1id, 也就是那些想要和你建立好友关系的用户的 uid,来获取 user 的集合;
        List<User> list = new ArrayList<>(1000);
        for (int i = 0; i < rstList.size(); i++) {
            // 获取向你发送好友申请的 User ;
            User user1 = userService.selectByUid(rstList.get(i).getU1id());
            user1.setPassword("");
            list.add(user1);
        }
        return AjaxResult.success(list);
    }


    // 好友申请页面同意申请好友关系接口;
    @RequestMapping("/friendagree")
    public AjaxResult friendAgree(HttpServletRequest request, String username) {
        // 前端的 username 判空;
        if(username == null) {
            return AjaxResult.fail(-5,"传输的参数为空!");
        }
        // 获取当前登录用户;
        User user1 = UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user1.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 因为按照 username 查询用户只有一个 User 类型参数的方法;
        // 这里就 new 对象,set username;
        User user = new User();
        user.setUsername(username);
        // 获取到要同意好友申请的用户;
        User user2 = userService.selectByuname(user);
        if(user2 == null) {
            return AjaxResult.fail(-7,"获取用户失败");
        }
        // 建立好友关系就有两张未读消息表,双方针对对方各自有一张;
        msgCountService.countadd(user2.getUid(),user1.getUid());
        msgCountService.countadd(user1.getUid(),user2.getUid());
        // 点击同意申请之后, 在 friendship 中建立好友关系;
        return AjaxResult.success(friendshipService.friendAgree(user1.getUid(), user2.getUid()));
    }


    // 好友管理列表拒绝申请好友关系接口;
    @RequestMapping("/friendreject")
    public AjaxResult friendReject(HttpServletRequest request, String username) {
        // 前端的 username 判空;
        if(username == null) {
            return AjaxResult.fail(-5,"传输的参数为空!");
        }
        // 获取当前登录用户;
        User user1 = UserSessionUtils.getUser(request);
        // 当前登录用户判空;
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user1.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 因为按照 username 查询用户只有一个 User 类型参数的方法;
        // 这里就 new 对象,set username;
        User user = new User();
        user.setUsername(username);
        // 获取到要同意好友申请的用户;
        User user2 = userService.selectByuname(user);
        if(user2 == null) {
            return AjaxResult.fail(-7,"获取用户失败");
        }
        // 点击拒绝申请之后, 在 friendship 中建立好友关系;
        return AjaxResult.success(friendshipService.friendReject(user1.getUid(), user2.getUid()));
    }


    // 好友管理列表删除好友关系接口;
    @RequestMapping("/fdel")
    public AjaxResult fdel(HttpServletRequest request, String username) {
        if(username == null) {
            return AjaxResult.fail(-5,"传输的参数为空!");
        }
        User user1 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user1.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 因为按照 username 查询用户只有一个 User 类型参数的方法;
        // 这里就 new 对象,set username;
        User user = new User();
        user.setUsername(username);
        // 获取到要删除的好友用户;
        User user2 = userService.selectByuname(user);
        if(user2 == null) {
            return AjaxResult.fail(-7,"获取用户失败");
        }
        // 删除好友的同时也要删除未读消息的连接;
        msgCountService.delMscount(user1.getUid(), user2.getUid());
        msgCountService.delMscount(user2.getUid(), user1.getUid());
        // 删除 friendship 表中的好友关系;
        return AjaxResult.success(friendshipService.fdel(user1.getUid(), user2.getUid()));
    }


    // 我的聊天窗口获取好友列表的的接口;
    @RequestMapping("/myfriends")
    public AjaxResult myfriends(HttpServletRequest request) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 获取用户关系表,取 u1id 和 u2id 作为参数查询未读消息表的未读消息数目;
        List<Friendship> flist = friendshipService.findFriends(user.getUid());
        if(flist == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        List<UserVO> list = new ArrayList<>(1000);
        for (int i = 0; i < flist.size(); i++) {
            User user1 = userService.selectByUid(
                    // 这里数据设计稍微有点不合适,好友是双方的关系,所以无法确定我们的 uid 就是 u1id 还是 u2id;
                    // 这里进行判断,如果 u2id 是当前用户自己,就用 u1id;
                    flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());

            // 对查询好友用户结果尽心判空;
            if(user1 == null) {
                return AjaxResult.fail(-1,"没有该好友用户!");
            }

            // 这个 VO 对象新增了 msgcount 属性, 来记录未读消息数量;
            UserVO userVO = new UserVO();

            // 查找未读消息表;
            // 这里也需要拿出好友的 uid,但是好友关系是不确定 uid 的前后顺序的,所以三元运算得到非当前的登录用户的 id;
            MsgCount msgCount = msgCountService.findCount(user.getUid()
                    ,flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            if(msgCount == null) {
                return null;
            }
            userVO.setMsgcount(msgCount.getMsgcount());
            // 对象深克隆;
            user1.setPassword("");
            BeanUtils.copyProperties(user1, userVO);
            user1.setPassword("");
            list.add(userVO);
        }
        return AjaxResult.success(list);
    }


    // 未读消息数;
    @RequestMapping("/chatnum")
    public AjaxResult chatNum(HttpServletRequest request) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 获取用户关系表,取 u1id 和 u2id 作为参数查询未读消息表的未读消息数目;
        List<Friendship> flist = friendshipService.findFriends(user.getUid());
        if(flist == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        Integer chatNum = 0;
        for (int i = 0; i < flist.size(); i++) {
            User user1 = userService.selectByUid(
                    // 这里数据设计稍微有点不合适,好友是双方的关系,所以无法确定我们的 uid 就是 u1id 还是 u2id;
                    // 这里进行判断,如果 u2id 是当前用户自己,就用 u1id;
                    flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            // 这个 VO 对象新增了 msgcount 属性, 来记录未读消息数量;
            // 查找未读消息表;
            // 这里也需要拿出好友的 uid,但是好友关系是不确定 uid 的前后顺序的,所以三元运算得到非当前的登录用户的 id;
            MsgCount msgCount = msgCountService.findCount(user.getUid()
                    ,flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            if(msgCount == null) {
                return null;
            }
            chatNum  = chatNum + msgCount.getMsgcount();
        }
        return AjaxResult.success(chatNum);
    }


    // 检测当前用户的好友申请接口;
    @RequestMapping("/rstcount")
    public AjaxResult rstCount(HttpServletRequest request) {
        // 获取当前登录用户;
        User user = UserSessionUtils.getUser(request);
        // 当前用户判空,看是否获取失败;
        if(user == null) {
            return AjaxResult.fail(-1," 获取当前用户失败");
        }
        // 根据 friendship 中的 u2id 为当前登录用户的 uid 去获取好友申请数目;
        List<Friendship> rstList = friendshipService.findRequest(user.getUid());
        Integer rstcount = rstList.size();

        return AjaxResult.success(rstcount);
    }
}
