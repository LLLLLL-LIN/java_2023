//package com.example.demo.config;//package com.example.demo.config;
////
/////**
//// * Created with IntelliJ IDEA
//// * Description
//// * User:Liaolin
//// * Date:2023-11-08
//// * Time:17:32
//// */
////public class FileUploadConfig {
////}
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.multipart.commons.CommonsMultipartResolver;
//
//@Configuration
//public class FileUploadConfig {
//
//    @Bean
//    public CommonsMultipartResolver multipartResolver() {
//        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
//        resolver.setDefaultEncoding("UTF-8");
//        // 设置最大上传文件大小 (以字节为单位)，这里设置为10MB
//        resolver.setMaxUploadSize(10 * 1024 * 1024);
//        // 设置最大内存大小 (以字节为单位)，超过该大小将写入临时文件
//        resolver.setMaxInMemorySize(10240);
//        return resolver;
//    }
//}
