package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/css/**")
                .excludePathPatterns("/editor.md/**")
                .excludePathPatterns("/img/**")
                .excludePathPatterns("/js/**")
                .excludePathPatterns("/static/avatar/**")
                .excludePathPatterns("/login.html")
                .excludePathPatterns("/reg.html")
                .excludePathPatterns("/friendship/contact")
                .excludePathPatterns("/friendship/friends")
                .excludePathPatterns("/friendship/fdel")
                .excludePathPatterns("/friendship/myfriends")
                .excludePathPatterns("/friendship/rstcount")
                .excludePathPatterns("/friendship/findrequest")
                .excludePathPatterns("/friendship/friendagree")
                .excludePathPatterns("/friendship/chatnum")
                .excludePathPatterns("/friendship/friendreject")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/logout")
                .excludePathPatterns("/user/fnname")
                .excludePathPatterns("/user/funame")
                .excludePathPatterns("/message/findmsg")
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/user/personal")
                .excludePathPatterns("/user/modify")
                .excludePathPatterns("/edit/editselect")
                .excludePathPatterns("/edit/editinsert")
                .excludePathPatterns("/edit/editupdate")
                .excludePathPatterns("/static/ringing.mp3");
    }
}
