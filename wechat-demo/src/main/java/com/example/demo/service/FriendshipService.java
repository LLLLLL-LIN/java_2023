package com.example.demo.service;

import com.example.demo.entity.Friendship;
import com.example.demo.mapper.FriendshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:00:33
 */
@Service
public class FriendshipService {
    @Autowired
    private FriendshipMapper friendshipMapper;

    // 添加好友关系;
    public Integer contact(Friendship friendship) {
        return friendshipMapper.contact(friendship);
    }

    // 根据当前登录用户的 uid 作为 uqid 查找用户链表;
    public List<Friendship> findFriends(Integer u1id) {
        return friendshipMapper.findFriends(u1id);
    }

    // 根据 u1id 和 u2id 删除好友关系;
    public Integer fdel(Integer u1id, Integer u2id) {
        return friendshipMapper.fdel(u1id,u2id);
    }

    // 根据当前登录用户的 uid 来获取好友申请列表;
    public List<Friendship> findRequest(Integer u1id) {
        return friendshipMapper.findRequest(u1id);
    }

    // 根据当前用户的 u1id 和要同意的用户的 u2id 来建立好友关系;
    public Integer friendAgree(Integer u1id, Integer u2id) {
        return friendshipMapper.friendAgree(u1id, u2id);
    }

    // 更新双方最近聊天时间;
    public Integer upChatTime(Integer u1id, Integer u2id) {
        return friendshipMapper.upChatTime(u1id, u2id);
    }

    // 拒绝好友申请;
    public Integer friendReject(Integer u1id, Integer u2id) {
        return friendshipMapper.friendReject(u1id, u2id);
    }
}
