package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaoin
 * Date:2023-11-08
 * Time:15:29
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public Integer addUser(User user){
        return userMapper.userAdd(user);
    }

    // 根据 username 查找 User;
    public User selectByuname(User user) {
        return userMapper.selectByuname(user);
    }

    // 根据 netname 查找要添加的用户;
    public List<User> selectBynname(String netname, Integer u1id) {
        return userMapper.selectBynname(netname, u1id);
    }

    // 根据 uid 查找 User;
    public User selectByUid(Integer uid) {
        return userMapper.selectByUid(uid);
    }

    // 检查用户的最近一次的在线时间; 用来判断用户是否还在线;
    public Integer checkTime(Integer uid) {
        return userMapper.checkTime(uid);
    }

    // 查找所有用户;
    public List<User> selectAll() {
        return userMapper.selectAll();
    }

    // 根据 uid 找到用户,将状态码置为 0 ,也就是不在线;
    public Integer upStatus(Integer uid) {
        return userMapper.upStatus(uid);
    }

    // 根据 uid 找到用户,将状态码置为 1 ,也就是在线;
    public Integer upStatus1(Integer uid) {
        return userMapper.upStatus1(uid);
    }

    // 根据当前登录用户的 uid 进行个人信息的修改;
    public Integer modifyPersonal(User user) {
        return userMapper.modifyPersonal(user);
    }

}
