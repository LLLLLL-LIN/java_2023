package com.example.demo.service;

import com.example.demo.common.AjaxResult;
import com.example.demo.entity.Edit;
import com.example.demo.mapper.EditMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-28
 * Time:16:34
 */
@Service
public class EditService {
    @Autowired
    private EditMapper editMapper;


    // 新增用户设置;
    public Integer editInsert(Edit edit) {
        return editMapper.editInsert(edit);
    }

    // 更新用户设置;
    public Integer editUpdate(Edit edit) {
        return editMapper.editUpdate(edit);
    }

    // 根据 username 查询用户的设置;
    public Edit editSelect(Edit edit) {
        return editMapper.editSelect(edit);
    }
}

