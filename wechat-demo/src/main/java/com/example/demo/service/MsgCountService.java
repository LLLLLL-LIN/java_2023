package com.example.demo.service;

import com.example.demo.entity.MsgCount;
import com.example.demo.mapper.MsgCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:16:10
 */
@Service
public class MsgCountService {
    @Autowired
    private MsgCountMapper msgCountMapper;

    // 查找当前用户对好友的的未读消息数;
    public MsgCount findCount(Integer u1id,Integer u2id){
        return msgCountMapper.findCount(u1id,u2id);
    }

    // 一旦建立好友关系,双方也就要新增对向关系的未读消息映射;
    public Integer countadd(Integer u1id,Integer u2id){
        return msgCountMapper.countadd(u1id,u2id);
    }

    // 发送消息之后,对方对你的未读消息就 + 1;
    public int mcountAdd(Integer u1id, Integer u2id) {
        return msgCountMapper.mcountAdd(u1id, u2id);
    }

    // 点开了聊天窗口,就把当前登录用户对聊天用户的未读消息置为 0;
    public int zeromcount(Integer u1id, Integer u2id) {
        return msgCountMapper.zeromcount(u1id, u2id);
    }

    // 在删除好友之后,也要删除双方对向的未读消息一栏删掉;
    public int delMscount(Integer u1id, Integer u2id){
        return msgCountMapper.delMscount(u1id,u2id);
    }
}
