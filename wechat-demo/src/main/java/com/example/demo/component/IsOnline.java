package com.example.demo.component;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-11
 * Time:19:11
 */
@Component
public class IsOnline {
    @Autowired
    private UserService userService;

    public void isOnline() {
        List<User> list = userService.selectAll();
        LocalDateTime currentTime = LocalDateTime.now();

        for (User user : list) {
            LocalDateTime checkTime = user.getChecktime();
            Duration duration = Duration.between(checkTime, currentTime);
            // 和上一次记录在线时间是否超过了 5分钟,超过了将 status 状态置为0;
            if (duration.toMinutes() > 1) {
                user.setStatus(0);
                userService.upStatus(user.getUid());
            }
        }
    }
}
