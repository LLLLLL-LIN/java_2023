package com.example.demo.mapper;

import com.example.demo.entity.Edit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-28
 * Time:16:32
 */
@Mapper
public interface EditMapper {
    // 新增用户设置;
    Integer editInsert (Edit edit);

    // 更新用户设置;
    Integer editUpdate (Edit edit);

    // 根据 username 查询用户的设置;
    Edit editSelect(Edit edit);
}
