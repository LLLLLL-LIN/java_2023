package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:15:29
 */
@Mapper
public interface UserMapper {
    // 添加 User;
    Integer userAdd(User user);

    // 根据 username 查找 User;
    User selectByuname(User user);

    // 根据 netname 查找 User 去添加;
    List<User> selectBynname(@Param("netname") String netname, @Param("u1id") Integer u1id);

    // 根据 uid 查找 User;
    User selectByUid(@Param("uid") Integer uid);

    // 更新用户的最近在线时间;
    Integer checkTime(@Param("uid") Integer uid);

    // 查询所有的用户;
    List<User> selectAll();

    // 更新用户的状态码为 0 (离线);
    Integer upStatus(@Param("uid") Integer uid);

    // 更新用户的状态码为 1 (在线);
    Integer upStatus1(@Param("uid") Integer uid);

    // 根据 uid 修改个人信息;
    Integer modifyPersonal(User user);
}
