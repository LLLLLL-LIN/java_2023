package com.example.demo.mapper;

import com.example.demo.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:22:07
 */
@Mapper
public interface MessageMapper {

    // 聊天窗口获取上方的聊天记录;
    List<Message> findMessage(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    // 点击发送之后的新增消息;
    Integer msgAdd(Message message);



}
