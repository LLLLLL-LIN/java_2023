package com.example.demo.mapper;

import com.example.demo.entity.MsgCount;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.ManagedBean;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:16:07
 */
@Mapper
public interface MsgCountMapper {
    // 根据当前用户对要查询的用户,获取双方各的 uid 来查找未读消息数;
    MsgCount findCount(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);

    // 建立好友关系之后,获取双方的 uid 来建立双方的对向关系用来管理未读消息数;
    Integer countadd(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);

    // 发送消息之后,对方对你的未读消息就 + 1;
    Integer mcountAdd(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    // 如果当前登录用户点开了聊天窗口,就把 user1 对 user2 的未读消息数置为 0;
    Integer zeromcount(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    // 删除好友的未读消息连接;
    Integer delMscount(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);
}
