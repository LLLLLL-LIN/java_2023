package com.example.demo.entity.VO;

import com.example.demo.entity.User;
import lombok.Data;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:15:43
 */
@Data
public class UserVO extends User {
    private Integer msgcount; // 未读消息数;
    private Integer rstcount; // 好友申请数;
    private String avatarBase64; // 头像的编码;
}
