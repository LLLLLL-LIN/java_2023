package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:11:49
 */
@Data
public class Message {
    private Integer mid; // 消息ID，作为主键
    private Integer sender; // 发送者的ID
    private Integer receiver; // 接收者的ID
    private String content; // 消息内容
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime sendtime; // 消息发送时间
    private String status; // 消息状态，如已发送、已读等
}
