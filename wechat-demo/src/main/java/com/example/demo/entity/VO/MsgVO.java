package com.example.demo.entity.VO;

import com.example.demo.entity.Message;
import lombok.Data;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:22:26
 */
@Data
public class MsgVO extends Message {
    private String netname; // 呢称/用户名;
}
