package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:11:45
 */
@Data
public class Friendship {
    private Integer fid;
    private Integer u1id; // 当前登录用户id;
    private Integer u2id; // 要添加的好友的id;
    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime friendtime; // 成为朋友的时间;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime chattime; // 聊天的时间;'
}
