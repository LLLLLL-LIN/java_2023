package com.example.demo.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-01-28
 * Time:16:26
 */
@Data
public class Edit {
    private Integer eid;
    private String username; // 账号;
    private Boolean msgnotice; // 消息通知;
    private Boolean frequestnotice; // 消息通知;
}
