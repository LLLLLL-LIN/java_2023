import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-12-05
 * Time:12:10
 */
public class RedisDemo1 {
    public static void main(String[] args) throws InterruptedException {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        Jedis jedis = jedisPool.getResource();
//        try (Jedis jedis = jedisPool.getResource()){
//            String ping = jedis.ping();
//            System.out.println(ping);
//            Set<String> strings = new HashSet<>();
//            testDemo1(jedis);
//        testDemo2(jedis);
        testDemo3(jedis);

        }

    public static void testDemo1(Jedis jedis) {
        System.out.println("set and keys");
        jedis.set("key1", "111");
        jedis.set("key2", "222");
        jedis.set("key3", "333");
        jedis.set("key4", "555");
        Set<String> set = jedis.keys("*");
        System.out.println(set);
    }

    public static void testDemo2(Jedis jedis) throws InterruptedException {
        System.out.println("ex and ttl");
        jedis.flushAll();
        jedis.set("key123","123");
        jedis.expire("key123",10);
        Thread.sleep(3000);
        long keyttl = jedis.ttl("key123");
        System.out.println(keyttl);
    }

    public static void testDemo3(Jedis jedis) {
        System.out.println("type");
        jedis.flushAll();

        jedis.set("key1","1");
        jedis.lpush("key12","1","2","3");
        jedis.hset("key123","filed1","value1");
        jedis.sadd("key1234","zhangsan","lisi","wangwu");
        jedis.zadd("key12345",98,"zhangfei");

        String type1 = jedis.type("key1");
        String type2 = jedis.type("key12");
        String type3 = jedis.type("key123");
        String type4 = jedis.type("key1234");
        String type5 = jedis.type("key12345");

        System.out.println("type(string): " + type1);
        System.out.println("type(list): " + type2);
        System.out.println("type(hash): " + type3);
        System.out.println("type(set): " + type4);
        System.out.println("type(zset): " + type5);

    }
}
