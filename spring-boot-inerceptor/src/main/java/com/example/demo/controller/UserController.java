package com.example.demo.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-12
 * Time:14:49
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/getuser")
    public String getUser() {
        System.out.println("这是 getuser 方法!");
        return "这是 getuser 方法!";
    }

    @RequestMapping("/login")
    public String loing() {
        System.out.println("这是 login 方法!");
        return "这是 login 方法!";
    }

    @RequestMapping("/reg")
    public String reg() {
        System.out.println("这是 reg 方法!");
        return "这是 reg 方法!";
    }

    @RequestMapping("/post")
    public String postMethod() {
        System.out.println("post 方法!");
        return "post 方法!";
    }
}
