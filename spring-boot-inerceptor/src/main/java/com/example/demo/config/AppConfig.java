package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-12
 * Time:14:41
 */
@Configuration
public class AppConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/**/*.html");
//                .excludePathPatterns("/**/getuser");
    }

//    @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//        configurer.addPathPrefix("zhangsan", c -> true);
//    }
}


//  两种为访问路径添加前缀 /xxx 的方法,1:是配置文件中配置;  2:是使用configurePathMatch()来配置
// 方法1: server.servlet.context-path=/zhangsan
// 方法2: @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//        configurer.addPathPrefix("zhangsan", c -> true);
//    }
// 第一点:在没有对任何接口,页面进行拦截的情况下,有以下区别;方法1 在访问页面的时候需要在路径前面加上添加的前缀 /xxx,不然访问不到; 而方法2 则相反,不用加 /xxx的前缀也能访问到;
// 第二点:在 login接口放开,login.html页面放开的时候,方法1 进行页面重定向的时候,或者说是直接访问页面的时候,仍然要加上前缀 /xxx ,而方法二不用;
// 而方法二对页面进行放开的时候也要在前面加上添加的前缀 /xxx, 否则,无法正确放开该接口; 而方法1不用,方法1 不用加 /xxx 的前缀也能正确的放开接口,不至于在访问的时候被拦截;