package packge1;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-23
 * Time:10:58
 */

import java.lang.reflect.Array;

/**
 * 排序算法
 */
public class SortTest2 {

    // 冒泡排序;
    public void bubbleSort(int[] array) {
        for (int i = array.length - 1; i > 0 ; i--) {
            for (int j = 0; j < i ; j++) {
                if(array[j] > array[j+1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    // 选择排序;
    public void seleceSort(int[] array) {
        for (int i = array.length - 1; i > 0; i--) {
            int maxIndex = i;
            for (int j = 0; j < i ; j++) {
                if(array[j] > array[maxIndex]) {
                    maxIndex = j;
                }
            }
            int tmp = array[maxIndex];
            array[maxIndex] = array[i];
            array[i] = tmp;
        }
    }

    // 插入排序;
    public void insertSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0 ; j--) {
                if(array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    //快速排序;
    public void quickSort(int[] array, int left, int right) {
        if(left >= right) {
            return;
        }
        int index = point(array, left, right);
        quickSort(array, left, index - 1);
        quickSort(array, index + 1, right);
    }

    private int point(int[] array, int start, int end) {
        int tmp = array[start];
        while(start < end) {
            while(start < end && array[end] >= tmp) {
                end--;
            }
            array[start] = array[end];
            while(start < end && array[start] <= array[end]) {
                start++;
            }
            array[end] = array[start];
        }
        array[start] = tmp;
        return start;
    }
}
