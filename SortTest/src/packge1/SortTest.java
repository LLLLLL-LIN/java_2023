package packge1;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-04-01
 * Time:上午10:04
 */
public class SortTest {
    // 1.冒泡排序;
    public void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    // 2.直接插入排序
    public void insertSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[i] = tmp;
        }
    }

    // 3.选择排序;
    public void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    // 4.快速排序;
    public void quickSort(int[] array) {
        quick(array, 0, array.length - 1);
    }
    public void quick(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = partition(array, left, right);
        quick(array,left,pivot - 1);
        quick(array,pivot + 1, right);
    }
    private int partition(int[] array, int start, int end) {
        int tmp = array[start];
        while(start < end) {
            while(start < end && array[end] >= tmp) {
                end--;
            }
            array[start] = array[end];
            while(start < end && array[start] <= array[end]) {
                start++;
            }
            array[end] = array[start];
        }
        array[start] = tmp;
        return start;
    }
}
