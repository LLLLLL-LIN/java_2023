package leecode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:11:27
 */

import java.util.Stack;

/**
 * 20. 有效的括号;
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 每个右括号都有一个对应的相同类型的左括号;
 *
 * 示例 1：
 * 输入：s = "()"
 * 输出：true
 *
 * 示例 2：
 * 输入：s = "()[]{}"
 * 输出：true
 *
 * 示例 3：
 * 输入：s = "(]"
 * 输出：false
 */
public class Demo02 {
    class Solution {
        public boolean isValid(String s) {
            Stack<Character> stack = new Stack<>();
            for (int i = 0; i < s.length(); i++) {
                char x = s.charAt(i);
                if (x == '('  || x == '[' || x == '{') {
                    stack.push(x);
                } else {
                    if (stack.empty()) {
                        return false;
                    }
                    if (stack.peek() == '(' && x == ')' ){
                        stack.pop();
                    } else if (stack.peek() == '[' && x == ']' ){
                        stack.pop();
                    } else if (stack.peek() == '{' && x == '}' ){
                        stack.pop();
                    } else {
                        return false;
                    }
                }
            }
            if (stack.empty()) {
                return true;
            }
            return false;
        }
    }
}
