package leecode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-31
 * Time:16:11
 */

import java.util.Comparator;
import java.util.PriorityQueue;

/** leecode;
 * 设计一个算法，找出数组中最小的k个数。以任意顺序返回这k个数均可。
 *
 * 示例 1：
 * 输入： arr = [1,3,5,7,2,4,6,8], k = 4
 * 输出： [1,2,3,4]
 * 提示：
 *
 * 0 <= len(arr) <= 100000
 * 0 <= k <= min(100000, len(arr))
 */
public class Demo09 {
}
class MyComparable implements Comparator<Integer> {
    public MyComparable() {
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }
}
class Solution {
    public int[] smallestK(int[] arr, int k) {
        int[] array = new int[k];
        if (k == 0) {
            // 返回空的数组;
            return array;
        }
        PriorityQueue<Integer> maxPriorityQueue = new PriorityQueue<>(new MyComparable());
        for (int i = 0; i < k; i++) {
            maxPriorityQueue.offer(arr[i]);
        }
        for (int i = k; i < arr.length; i++) {
            int num = maxPriorityQueue.peek();
            if (arr[i] < num) {
                // 删除树顶元素;
                maxPriorityQueue.poll();
                // 新增元素;
                maxPriorityQueue.offer(arr[i]);
            }
        }
        for (int i = 0; i < k; i++) {
            array[i] = maxPriorityQueue.poll();
        }
        return array;
    }
}
