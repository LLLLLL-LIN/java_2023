package leecode;
import java.util.Stack;
/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:11:08
 */

/**
 * 150. 逆波兰表达式求值;
 * 给你一个字符串数组 tokens ，表示一个根据 逆波兰表示法 表示的算术表达式;
 * 请你计算该表达式。返回一个表示表达式值的整数;
 *
 * 示例 1：
 * 输入：tokens = ["2","1","+","3","*"]
 * 输出：9
 * 解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
 */
public class Demo01 {
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String x : tokens) {
            if (isNum(x)) {
                stack.push(Integer.parseInt(x));
            } else {
                int num2 = stack.pop();
                int num1 = stack.pop();
                switch(x) {
                    case "+":
                        stack.push(num1 + num2);
                        break;
                    case "-":
                        stack.push(num1 - num2);
                        break;
                    case "*":
                        stack.push(num1 * num2);
                        break;
                    case "/":
                        stack.push(num1 / num2);
                        break;
                }
            }
        }
        return stack.pop();
    }
    public boolean isNum(String x) {
        return !x.equals("+") && !x.equals("-") && !x.equals("*") && !x.equals("/");
    }
}
