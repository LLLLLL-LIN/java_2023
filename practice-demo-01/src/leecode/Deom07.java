package leecode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-25
 * Time:09:34
 */

import java.util.Stack;

/**
 * 232. 用栈实现队列
 * 请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：
 *
 * 实现 MyQueue 类：
 * void push(int x) 将元素 x 推到队列的末尾
 * int pop() 从队列的开头移除并返回元素
 * int peek() 返回队列开头的元素
 * boolean empty() 如果队列为空，返回 true ；否则，返回 false
 *
 * 示例 1：
 * 输入：
 * ["MyQueue", "push", "push", "peek", "pop", "empty"]
 * [[], [1], [2], [], [], []]
 * 输出：
 * [null, null, null, 1, 1, false]
 */
public class Deom07 {

}
class MyQueue {
    public Stack<Integer> stack1;
    public Stack<Integer> stack2;

    public MyQueue() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    public void push(int x) {
        stack1.push(x);
    }

    public int pop() {
        if (empty()) {
            return -1;
        }
        int val = 0;
        if (!stack2.isEmpty()) {
            val = stack2.pop();
        } else {
            while (!stack1.isEmpty()) {
                val = stack1.pop();
                stack2.push(val);
            }
            val = stack2.pop();
        }
        return val;
    }

    public int peek() {
        if (empty()) {
            return -1;
        }
        int val = -1;
        if (!stack2.isEmpty()) {
            val = stack2.peek();
        } else {
            while (!stack1.isEmpty()) {
                val = stack1.pop();
                stack2.push(val);
            }
        }
        return val;
    }

    public boolean empty() {
        return stack1.isEmpty() && stack2.isEmpty();
    }
}