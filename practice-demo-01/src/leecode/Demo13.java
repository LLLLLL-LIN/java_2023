package leecode;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-08
 * Time:16:58
 */

public class Demo13 {
}

/**leecode;
 * 692. 前K个高频单词
 * 给定一个单词列表 words 和一个整数 k ，返回前 k 个出现次数最多的单词。
 * 返回的答案应该按单词出现频率由高到低排序。如果不同的单词有相同出现频率， 按字典顺序 排序。
 *
 * 示例 1：
 * 输入: words = ["i", "love", "leetcode", "i", "love", "coding"], k = 2
 * 输出: ["i", "love"]
 * 解析: "i" 和 "love" 为出现次数最多的两个单词，均为2次。
 *     注意，按字母顺序 "i" 在 "love" 之前。
 *
 * 示例 2：
 * 输入: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
 * 输出: ["the", "is", "sunny", "day"]
 * 解析: "the", "is", "sunny" 和 "day" 是出现次数最多的四个单词，
 *     出现次数依次为 4, 3, 2 和 1 次。
 */
class Test {
    public List<String> topKFrequent(String[] words, int k) {
        List<String> list = new ArrayList<>();
        Map<String, Integer> hashMap = new HashMap<>();
        Queue<Map.Entry<String, Integer>> priorityQueue = new PriorityQueue<>(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
//                // 1:
//                return entry2.getValue().compareTo(entry1.getValue());

//                // 2:
//                if (entry1.getValue().equals(entry2.getValue())) {
//                    return entry1.getKey().compareTo(entry2.getKey());
//                } else {
//                    return entry2.getValue().compareTo(entry1.getValue());
//                }

                // 3: a aa aaa 类似这样的就会出错,所以这里不是比较的字符串首字符的排序,而是字符串的大小;
                // 而 String 类型的对象去调用compareTo底层就是比较两个字符串的大小;
                if (entry1.getValue().equals(entry2.getValue())) {
                    return (entry2.getKey().charAt(0)) - (entry1.getKey().charAt(0));
                } else {
                    return entry2.getValue().compareTo(entry1.getValue());
                }
            }
        });
        for (String str : words) {
            if (hashMap.containsKey(str)) {
                hashMap.put(str, hashMap.get(str) + 1);
            } else {
                hashMap.put(str, 1);
            }
        }
        Set<Map.Entry<String, Integer>> entrySet = hashMap.entrySet();
        for (Map.Entry<String, Integer> entSet : entrySet ) {
            priorityQueue.offer(entSet);
        }
        for (int i = 0; i < k; i++) {
            list.add(priorityQueue.poll().getKey());
        }
        return list;
    }
}
