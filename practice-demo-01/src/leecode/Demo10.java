package leecode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-08
 * Time:14:23
 */
public class Demo10 {
    // 找到字符串中第一个不重复的字符的下标;

    /**leecode;
     * 387. 字符串中的第一个唯一字符
     * 给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。
     * 示例 1：
     * 输入: s = "leetcode"
     * 输出: 0
     *
     * 示例 2:
     * 输入: s = "loveleetcode"
     * 输出: 2
     *
     * 示例 3:
     * 输入: s = "aabb"
     * 输出: -1
     */
    public int firstUniqChar(String s) {
        if(s.length() == 0) {
            return -1;
        }
        Map<Character, Integer> hashMap = new HashMap<>();
        for(int i = 0; i < s.length(); i++) {
            hashMap.put(s.charAt(i), hashMap.getOrDefault(s.charAt(i), 0) + 1);
        }
        for (int i = 0; i < s.length(); i++) {
            if (hashMap.get(s.charAt(i)) == 1) {
                return i;
            }
        }
        return -1;
    }
}
