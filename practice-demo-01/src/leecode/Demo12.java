package leecode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-08
 * Time:14:42
 */
public class Demo12 {
    /**leecode
     * 771. 宝石与石头;
     * 给你一个字符串 jewels 代表石头中宝石的类型，另有一个字符串 stones 代表你拥有的石头。
     * stones 中每个字符代表了一种你拥有的石头的类型，你想知道你拥有的石头中有多少是宝石。
     * 字母区分大小写，因此 "a" 和 "A" 是不同类型的石头。
     * 示例 1：
     * 输入：jewels = "aA", stones = "aAAbbbb"
     * 输出：3
     *
     * 示例 2：
     * 输入：jewels = "z", stones = "ZZ"
     * 输出：0
     */
    public static void main(String[] args) {
        int count = numJewelsInStones1("aA", "aAAbbbb");
        System.out.println(count);
    }
    public static int numJewelsInStones1(String jewels, String stones) {
        Set<Character> hashSet1 = new HashSet<>();
        int count = 0;
        for (int i = 0; i < jewels.length(); i++) {
            hashSet1.add(jewels.charAt(i));
        }
        for (int i = 0; i < stones.length(); i++) {
            if (hashSet1.contains(stones.charAt(i))) {
                count++;
            }
        }
        return count;
    }

    public static int numJewelsInStones2(String jewels, String stones) {
        int count = 0;
        for (int i = 0; i < stones.length(); i++) {
            if (jewels.contains(String.valueOf(stones.charAt(i)))) {
                count++;
            }
        }
        return count;
    }

    public int numJewelsInStones3(String jewels, String stones) {
        int[] tmp = new int[128];
        for (char c : stones.toCharArray()) {
            tmp[c]++;
        }
        int count = 0;
        for(char c : jewels.toCharArray()) {
            count += tmp[c];
        }
        return count;
    }
}
