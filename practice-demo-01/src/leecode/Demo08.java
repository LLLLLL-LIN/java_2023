package leecode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-28
 * Time:15:25
 */

/**leecode;
 * 105. 从前序与中序遍历序列构造二叉树;
 * 给定两个整数数组 preorder 和 inorder ，其中 preorder 是二叉树的先序遍历，
 * inorder 是同一棵树的中序遍历，请构造二叉树并返回其根节点。
 * 示例 1:
 * 输入: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
 * 输出: [3,9,20,null,null,15,7]
 */
public class Demo08 {
}
class SolutionForPreIndex {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }
    public int preindex;
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return buildTreeChild(preorder, inorder, 0, preorder.length - 1);
    }
    private int findIndex(int[] inorder, int begin, int end, int toFind) {
        if (inorder == null) {
            return -1;
        }
        for(int i = begin; i <= end; i++) {
            if (inorder[i] == toFind) {
                return i;
            }
        }
        return -1;
    }
    public TreeNode buildTreeChild(int[] preorder, int[] inorder, int begin, int end) {
        if (begin > end) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preindex]);
        int rootIndex = findIndex(inorder, begin, end, preorder[preindex]);
        if (rootIndex == -1) {
            return null;
        }
        preindex++;
        root.left = buildTreeChild(preorder, inorder, begin, rootIndex - 1);
        root.right = buildTreeChild(preorder, inorder, rootIndex + 1, end);
        return root;
    }
}

/**leecode;
 * 106. 从中序与后序遍历序列构造二叉树
 * 给定两个整数数组 inorder 和 postorder ，其中 inorder 是二叉树的中序遍历，
 * postorder 是同一棵树的后序遍历，请你构造并返回这颗 二叉树 。
 * 示例 1:
 * 输入：inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
 * 输出：[3,9,20,null,null,15,7]
 */
class SolutionForPostIndex {
    static class TreeNode {
        int val;
        SolutionForPostIndex.TreeNode left;
        SolutionForPostIndex.TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }
    public int postindex;
    public TreeNode buildTree2(int[] inorder, int[] postorder) {
        postindex = postorder.length - 1;
        return buildTreeChild2(postorder, inorder, 0, postorder.length - 1);
    }
    private int findIndex2(int[] inorder, int begin, int end, int toFind) {
        for(int i = begin; i <= end; i++) {
            if (inorder[i] == toFind) {
                return i;
            }
        }
        return -1;
    }
    public TreeNode buildTreeChild2(int[] postorder, int[] inorder, int begin, int end) {
        if (begin > end) {
            return null;
        }
        TreeNode root = new TreeNode(postorder[postindex]);
        int inorderIndex = findIndex2(inorder, begin, end, postorder[postindex]);
        if (inorderIndex == -1) {
            return null;
        }
        postindex--;
        root.right = buildTreeChild2(postorder, inorder, inorderIndex + 1, end);
        root.left = buildTreeChild2(postorder, inorder, begin, inorderIndex - 1);
        return root;
    }
}