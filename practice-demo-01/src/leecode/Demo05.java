package leecode;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:22:44
 */

import java.util.LinkedList;
import java.util.Queue;

/**
 * 225. 用队列实现栈;
 *
 * 请你仅使用两个队列实现一个后入先出（LIFO）的栈，并支持普通栈的全部四种操作（push、top、pop 和 empty）。
 */
public class Demo05 {
}


class MyStack {
    Queue<Integer> queue1;
    Queue<Integer> queue2;

    public MyStack() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    public void push(int x) {
        if (queue1.isEmpty() && queue2.isEmpty()) {
            queue1.offer(x);
            return;
        }
        if (!queue1.isEmpty()) {
            queue1.offer(x);
        } else {
            queue2.offer(x);
        }
    }

    public int pop() {
        if (queue1.isEmpty() && queue2.isEmpty()) {
            return -1;
        }
        int val = 0;
        if (!queue1.isEmpty()) {
            while (queue1.size() > 1) {
                val = queue1.poll();
                queue2.offer(val);
            }
            val = queue1.poll();
        } else {
            while (queue2.size() > 1) {
                val = queue2.poll();
                queue1.offer(val);
            }
            val = queue2.poll();
        }
        return val;
    }

    public int top() {
        if (queue1.isEmpty() && queue2.isEmpty()) {
            return -1;
        }
        int val = 0;
        if (!queue1.isEmpty()) {
            while (!queue1.isEmpty()) {
                val = queue1.poll();
                queue2.offer(val);
            }
        } else {
            while (!queue2.isEmpty()) {
                val = queue2.poll();
                queue1.offer(val);
            }
        }
        return val;
    }

    public boolean empty() {
        return queue1.isEmpty() && queue2.isEmpty();
    }
}