package newcoder;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-27
 * Time:10:16
 */

/** newcoder;
 * KY11 二叉树遍历;
 * 描述:
 * 编一个程序，读入用户输入的一串先序遍历字符串，根据此字符串建立一个二叉树（以指针方式存储）。
 * 例如如下的先序遍历字符串： ABC##DE#G##F### 其中“#”表示的是空格，空格字符代表空树。建立起此二叉树以后，再对二叉树进行中序遍历，输出遍历结果。
 * 输入描述：
 * 输入包括1行字符串，长度不超过100。
 * 输出描述：
 * 可能有多组测试数据，对于每组数据， 输出将输入字符串建立二叉树后中序遍历的序列，每个字符后面都有一个空格。 每个输出结果占一行。
 *
 * 示例 1:
 * 输入：abc##de#g##f###
 * 输出：c b e g d f a
 */
class TreeNode{
    char val;
    TreeNode left;
    TreeNode right;
    public TreeNode(char val) {
        this.val = val;
    }
}
// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Demo01 {
    public static int i;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 读取字符串;
        while (in.hasNextLine()) {
            String str = in.nextLine();
            TreeNode root = createTree(str);
            inorder(root);
        }
    }
    // 根据前序遍历字符串来构造二叉树;
    public static TreeNode createTree(String str) {
        TreeNode root = null;
        // 1.判断是否是 #,不同处理;
        if (str.charAt(i) == '#') {
            i++;
        }else {
            root = new TreeNode(str.charAt(i));
            i++;
            root.left = createTree(str);
            root.right = createTree(str);
        }
        return root;
    }
    // 前序遍历;
    public static void inorder(TreeNode root) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }
}
