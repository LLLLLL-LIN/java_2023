package newcoder.huawei_test;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:10:19
 */
public class HJ3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int[] array = new int[num];
        for (int i = 0; i < num; i++) {
            array[i] = in.nextInt();
        }
        Set<Integer> list = theOnlyRandom(array);
        for (Integer i : list) {
            System.out.println(i);
        }
    }
    public static Set<Integer> theOnlyRandom(int[] array) {
        Set<Integer> treeSet = new TreeSet<>();
        for (int i = 0; i < array.length; i++) {
            treeSet.add(array[i]);
        }
        return treeSet;
    }
}
