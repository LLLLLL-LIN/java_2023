package newcoder.huawei_test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:10:58
 */

/** newcoder;
 * HJ4 字符串分隔
 * 描述
 * •输入一个字符串，请按长度为8拆分每个输入字符串并进行输出；
 * •长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
 * 输入描述：
 * 连续输入字符串(每个字符串长度小于等于100)
 * 输出描述：
 * 依次输出所有分割后的长度为8的新字符串
 *
 * 示例1
 * 输入 ：abc
 * 输出 ：abc00000
 *
 */
public class HJ4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            StringBuilder input = new StringBuilder(scanner.nextLine());

            // 处理空字符串
            if (input.length() == 0) {
                continue;
            }

            // 计算需要补足的0的数量
            int paddingCount = 8 - input.length() % 8;
            // 补足0
            if (paddingCount != 8) {
                while (paddingCount > 0) {
                    input.append("0");
                    paddingCount--;
                }
            }

            // 输出长度为8的子字符串
            for (int i = 0; i < input.length(); i += 8) {
                System.out.println(input.substring(i, Math.min(i + 8, input.length())));
            }
        }
        scanner.close();
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while (true) {
            String str = in.nextLine();
            List<String> list = splitString(str);
            for(String s : list) {
                System.out.println(s);
            }
        }
    }
    public static List<String> splitString(String str) {
        List<String> list = new ArrayList<>();
        if (str.length() == 8) {
            list.add(str);
            return list;
        }
        int i = 0;
        for (; (i + 1) * 8 <= str.length(); i++) {
            String sbuStr = str.substring(i * 8, (i + 1) * 8);
            list.add(sbuStr);
        }
        if ((i + 1) * 8 - str.length() < 8) {
            String str1 = str.substring((i * 8), str.length());
            for (int j = (str.length() - (i * 8)); j < 8; j++) {
                str1 += "0";
            }
            list.add(str1);
        }
        return list;
    }
}
