package newcoder.huawei_test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:15:07
 */

import java.util.Scanner;

/** newcoder;
 * HJ11 数字颠倒
 * 描述
 * 输入一个整数，将这个整数以字符串的形式逆序输出
 * 程序不考虑负数的情况，若数字含有0，则逆序形式也含有0，如输入为100，则输出为001
 * 数据范围：
 * 0 ≤ n ≤ 2 ^ (30−1);
 * 输入描述 ：输入一个int整数
 * 输出描述 ：将这个整数以字符串的形式逆序输出
 * 示例1
 * 输入 ：1516000
 * 输出 ：0006151
 * 示例2
 * 输入 ： 0
 * 输出 ：0
 */
public class HJ11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int num = in.nextInt();
            String str = String.valueOf(num);
            for (int i = str.length() - 1; i >= 0; i--) {
                System.out.print(str.charAt(i));
            }
        }
    }
}
