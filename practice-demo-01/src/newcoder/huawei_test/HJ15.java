package newcoder.huawei_test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:16:40
 */

import java.util.Scanner;

/** newcoder;
 * HJ15
 */

/** newcoder;
 * HJ15 求int型正整数在内存中存储时1的个数
 * 描述
 * 输入一个 int 型的正整数，计算出该 int 型数据在内存中存储时 1 的个数。
 * 数据范围：保证在 32 位整型数字范围内
 * 输入描述 ：输入一个整数（int类型）
 *
 * 输出描述 ：这个数转换成2进制后，输出1的个数
 *
 * 示例1
 * 输入 ：5
 * 输出 ：2
 *
 * 示例2
 * 输入 ：0
 * 输出 ：0
 */
//public class HJ15 {
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        String str = "";
//        int countOne = 0;
//        // 注意 hasNext 和 hasNextLine 的区别
//        while (in.hasNextInt()) { // 注意 while 处理多个 case
//            int num = in.nextInt();
//            str = Integer.toBinaryString(num);
//        }
//        for (int i = 0; i < str.length(); i++) {
//            if (str.charAt(i) == '1') {
//                countOne++;
//            }
//        }
//        System.out.println(countOne);
//    }
//}

