package newcoder.huawei_test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:13:38
 */

/** newcoder;
 * HJ7 取近似值
 * 描述
 * 写出一个程序，接受一个正浮点数值，输出该数值的近似整数值。如果小数点后数值大于等于 0.5 ,向上取整；小于 0.5 ，则向下取整。
 * 数据范围：保证输入的数字在 32 位浮点数范围内
 * 输入描述 ：输入一个正浮点数值
 * 输出描述 ：输出该数值的近似整数值
 *
 * 示例1
 * 输入 ：5.5
 * 输出 ：6
 * 说明 ：0.5>=0.5，所以5.5需要向上取整为6
 *
 * 示例2
 * 输入 ：2.499
 * 输出 ：2
 * 说明 ：0.499<0.5，2.499向下取整为2
 */
public class HJ7 {
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            double num = sc.nextDouble();
            System.out.println(getNum(num));
        }
    }
    public static int getNum(double num) {
        return (int) Math.floor(num + 0.5);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            float num = in.nextFloat();
            System.out.println(getNum(num));
        }
    }

    public static int getNum(float num) {
        return (int)(num + 0.5);
    }
}
