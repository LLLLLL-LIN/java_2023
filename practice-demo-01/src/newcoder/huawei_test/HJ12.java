package newcoder.huawei_test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:15:11
 */

import java.util.Scanner;

/** newcoder;
 * HJ12 字符串反转
 * 描述
 * 接受一个只包含小写字母的字符串，然后输出该字符串反转后的字符串。（字符串长度不超过1000）
 * 输入描述 ：输入一行，为一个只包含小写字母的字符串。
 * 输出描述 ：输出该字符串反转后的字符串。
 * 示例1
 * 输入 ：abcd
 * 输出：dcba
 */
public class HJ12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            String str = in.next();
            for (int i = str.length() - 1; i >= 0; i--) {
                System.out.print(str.charAt(i));
            }
        }
    }
}
