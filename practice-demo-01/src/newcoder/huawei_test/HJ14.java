package newcoder.huawei_test;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:15:24
 */

/** newcoder;
 * 题目描述：
 * 给定n个字符串，请对n个字符串按照字典序排列。
 * 数据范围：
 * 1 ≤ n ≤ 1000
 * 1 ≤ 字符串长度 ≤ 100
 * 输入描述 ：输入第一行为一个正整数n(1 ≤ n ≤ 1000)，下面n行为n个字符串(字符串长度 ≤ 100)，字符串中只含有大小写字母。
 * 输出描述 ：数据输出n行，输出结果为按照字典序排列的字符串。
 *
 * 示例1：
 * 输入：
 * 9
 * cap
 * to
 * cat
 * card
 * two
 * too
 * up
 * boat
 * boot
 *
 * 输出 :
 * boat
 * boot
 * cap
 * card
 * cat
 * to
 * too
 * two
 * up
 *
 */
public class HJ14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String[] strs = {};
        while (in.hasNext()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            strs = new String[n];
            for (int i = 0; i < n; i++) {
                strs[i] = in.next();
            }
        }
        Arrays.sort(strs);
        for (int i = 0; i < strs.length; i++) {
            System.out.println(strs[i]);
        }
    }
}
