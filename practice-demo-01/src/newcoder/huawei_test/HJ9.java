package newcoder.huawei_test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:14:37
 */

/**
 * 描述 :
 * 输入一个 int 型整数，按照从右向左的阅读顺序，返回一个不含重复数字的新的整数。
 * 保证输入的整数最后一位不是 0 。
 * 数据范围 ：1 ≤ n ≤ 10 ^ 8
 * 输入描述 ：输入一个int型整数
 * 输出描述 ：按照从右向左的阅读顺序，返回一个不含重复数字的新的整数
 *
 * 示例1
 * 输入 ：9876673
 * 输出 ：37689
 */
public class HJ9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int a = in.nextInt();
            List<Character> list = new ArrayList<>();
            String str = String.valueOf(a);
            for (int i = str.length() - 1; i >= 0; i--) {
                if (!list.contains(str.charAt(i))) {
                    list.add(str.charAt(i));
                }
            }
            for (Character c : list) {
                System.out.print(c);
            }
        }
    }
}
