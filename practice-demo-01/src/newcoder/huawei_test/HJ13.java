package newcoder.huawei_test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:15:18
 */

import java.util.Scanner;
import java.util.Stack;

/**
 * HJ13 句子逆序
 * 描述
 * 将一个英文语句以单词为单位逆序排放。例如“I am a boy”，逆序排放后为“boy a am I”
 * 所有单词之间用一个空格隔开，语句中除了英文字母外，不再包含其他字符
 * 数据范围：输入的字符串长度满足
 * 1 ≤ n ≤ 1000
 * 注意本题有多组输入
 * 输入描述：
 * 输入一个英文语句，每个单词用空格隔开。保证输入只包含空格和字母。
 * 输出描述 ：得到逆序的句子
 *
 * 示例1
 * 输入 ：I am a boy
 * 输出 ：boy a am I
 *
 * 示例2
 * 输入 ：nowcoder
 * 输出 ：nowcoder
 */
public class HJ13 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        Stack<String> stack = new Stack<>();
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str = in.nextLine();
            String[] strs = str.split(" ");
            for (int i = 0; i < strs.length; i++) {
                stack.push(strs[i]);
            }
        }
        while (!stack.isEmpty()) {
            System.out.print(stack.pop());
            if (!stack.isEmpty()) {
                System.out.print(" ");
            }
        }
    }
}
