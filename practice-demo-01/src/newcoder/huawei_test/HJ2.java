package newcoder.huawei_test;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-09
 * Time:09:55
 */

/** newcoder;
 * HJ2 计算某字符出现次数;
 * 描述
 * 写出一个程序，接受一个由字母、数字和空格组成的字符串，和一个字符，然后输出输入字符串中该字符的出现次数。（不区分大小写字母）
 * 数据范围：
 * 1 ≤ n ≤ 1000
 * 1≤n≤1000
 * 输入描述：
 * 第一行输入一个由字母、数字和空格组成的字符串，第二行输入一个字符（保证该字符不为空格）。
 * 输出描述：
 * 输出输入字符串中含有该字符的个数。（不区分大小写字母）
 *
 * 实例 1:
 * 输入 : ABCabc   A
 * 输出 : 2
 */
public class HJ2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String check = in.nextLine();
        int count = charCount(str, check);
        System.out.println(count);
    }

    public static int charCount(String str, String check) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char c1 = str.charAt(i);
            if (String.valueOf(c1).equalsIgnoreCase(check)) {
                count++;
            }
        }
        return count;
    }
}
