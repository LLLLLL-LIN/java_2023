package com.example.demo.Controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisAccessor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-12-08
 * Time:09:33
 */
@RestController
@RequestMapping("/redis")
public class RedisDemo1 {
    @Autowired
    StringRedisTemplate redisTemplate;

    // redis 的 string 类型;
    @RequestMapping("/string")
    public String setTest() {

        redisTemplate.execute((RedisConnection redisconnection) -> {
            redisconnection.flushAll();
            return null;
        });

        redisTemplate.opsForValue().set("key1", "111");
        redisTemplate.opsForValue().set("key2", "222");
        redisTemplate.opsForValue().set("key3", "333");

        // 根据 key 获取 value;
        String resutlt = redisTemplate.opsForValue().get("key1");
        System.out.println("key1的value: " + resutlt);

        // 根据 key 的字符串的拼接;
        redisTemplate.opsForValue().append("key1","string");

        // 根据 key 的 value 自增1;
        Long incrValue = redisTemplate.opsForValue().increment("key2");
        System.out.println("incrValue: " + incrValue);

        // 根据 key 获取 value 中的 string 类型的数据长度;
        Long stringSize = redisTemplate.opsForValue().size("key3");
        System.out.println("stringSize: " + stringSize);

        // 根据 index 截取对应 key 中的字符串;
        String getValue = redisTemplate.opsForValue().get("key1",0,-1);
        System.out.println("getValue: " + getValue);

//        // 根据 key 获取 value 并删除这个 key-value; (注意版本);
//        String getAndDel = redisTemplate.opsForValue().getAndDelete("key3");
//        System.out.println("getAndDel " + getAndDel);

        // 根据 key 找到对应的 value 加上 给定的整数;
        Long incrBy = redisTemplate.opsForValue().increment("key2",100);
        System.out.println("incrBy: " + incrBy);

        return "string set and get append increment size delete incrBy ok!";
    }

    // redis 的 list;
    @RequestMapping("/list")
    public String listTest() {
        redisTemplate.execute((RedisConnection redisconnction) -> {
            redisconnction.flushAll();
            return null;
        });

        // list 的左加和右加;
        redisTemplate.opsForList().leftPush("key1","111");
        redisTemplate.opsForList().leftPush("key1","222");
        redisTemplate.opsForList().leftPush("key1","333");
        redisTemplate.opsForList().rightPush("key1","list-right");

        // 根据起始 index 获取 key 的 value;
        List<String> listResqult = redisTemplate.opsForList().range("key1",0,-1);
        System.out.println("listResult: " + listResqult);

        // 根据 index 获取 key1 的 value 的 index 位置的值;
        String indexValue = redisTemplate.opsForList().index("key1",2);
        System.out.println("indexValue: " + indexValue);

        // 根据 key 的左删和右删;
        System.out.println("左删: " +redisTemplate.opsForList().leftPop("key1"));
        System.out.println("右删: " + redisTemplate.opsForList().rightPop("key1"));

        // 根据 key 获取 value 的 size;
        Long listSize = redisTemplate.opsForList().size("key1");
        System.out.println("listSize: " + listSize);

        /**
         *  如果返回值大于 0，则表示成功删除了指定数量的元素。
         *  如果返回值等于 0，则表示没有找到与指定值相等的元素，没有进行删除操作。
         *  如果返回值小于 0，则表示发生了错误或异常情况。
         */
        // 根据 value 删除 key 中匹配的 value, 最多删除 count 个;
        // 找到 key1 中匹配 "222" 的元素,成功删除,返回值为1;
        Long remove1 = redisTemplate.opsForList().remove("key1",3,"222");
        System.out.println("remove1: " + remove1);
        // key 中找不到匹配的 value,返回值为0;
        Long remove2 = redisTemplate.opsForList().remove("key1",3,"666");
        System.out.println("remove2: " + remove2);
        // 没有 key2, 这里找不到 key 铜钥匙没有找到指定的元素,不会抛异常, 返回值是0;
        Long remove3 = redisTemplate.opsForList().remove("key2",3,"222");
        System.out.println("remove3: " + remove3);

        return "list push range pop index size remove ok";
    }


    // redis 的 set;
    @RequestMapping("/set")
    public String testSet() {
        redisTemplate.execute((RedisConnection redisconnection) ->{
            redisconnection.flushAll();
            return null;
        });
        redisTemplate.opsForSet().add("key1","111","222","333","set-value");

        // 添加 set 集合:
        Set<String> setResult = redisTemplate.opsForSet().members("key1");
        System.out.println("setResult: " + setResult);

        // 判断 key 中元素是否存在;
        Boolean isMember = redisTemplate.opsForSet().isMember("key1","111");
        System.out.println("isMember: " + isMember);

        // 删除指定的元素(匹配 value 值去删除);
        Long toRemove = redisTemplate.opsForSet().remove("key1","222");
        System.out.println("roRemove: " + toRemove);

        // 随机删除;
        String randomDel = redisTemplate.opsForSet().pop("key1");
        System.out.println("随机pop: " + randomDel);

        // set 的 size;
        Long setSize = redisTemplate.opsForSet().size("key1");
        System.out.println("setSize: " + setSize);

        return "set add isMember remove pop size ok!";
    }

    //  redis 的 zset;
    @RequestMapping("/zset")
    public String zsetTest() {
        redisTemplate.execute((RedisConnection redisconnection) -> {
            redisconnection.flushAll();
            return null;
        });

        // zset 的 add;
        redisTemplate.opsForZSet().add("key1","张飞",98.0);
        redisTemplate.opsForZSet().add("key1","关羽",98.5);
        redisTemplate.opsForZSet().add("key1","刘备",99.0);

        // 根据 key 获取 zset 集合;
        Set<String> zsetResult= redisTemplate.opsForZSet().range("key1",0,-1);
        System.out.println("zsetResult: " + zsetResult);

        // zset 的 匹配删除;
        Long remove = redisTemplate.opsForZSet().remove("key1","张飞");
        System.out.println("remove: " + remove);

        // 返回 key 的对应范围内的 value 和对应的 score;
        Set<ZSetOperations.TypedTuple<String>>  valueScores = redisTemplate.opsForZSet().rangeWithScores("key1",0,-1);
        System.out.println("value and score: " + valueScores);

        return "zset add range remove rangeWithscores ok!";
    }

    // redis 的 hash;
    @RequestMapping("/hash")
    public String hashTest() {
        redisTemplate.execute((RedisConnection rediesconnection) -> {
            rediesconnection.flushAll();
            return null;
        });

        // hash 的 put;
        redisTemplate.opsForHash().put("key1","f1","v1");
        redisTemplate.opsForHash().put("key1","f2","v2");
        redisTemplate.opsForHash().put("key1","f3","v3");

        // hash 的根据 key 以及 field 去获取 value;
        String value = (String) redisTemplate.opsForHash().get("key1","f1");
        System.out.println("value: " + value);

        // 根据 key 获取 hash 的 size;
        Long hashSize = redisTemplate.opsForHash().size("key1");
        System.out.println("hash的size: " + hashSize);

        // 根据 key 和 field 删除 hash 的 field 和 value;
        Long hashDel = redisTemplate.opsForHash().delete("key1","f3");
        System.out.println("hashDel: " + hashDel);

        // 根据 key 和 field 判断是否存在该 f-v;
        Boolean haskey = redisTemplate.opsForHash().hasKey("key1","key3");
        System.out.println("haskey: " + haskey);

//        // 根据 key 随机获取一个 field;(注意 redis 的版本);
//        String randomKey = (String) redisTemplate.opsForHash().randomKey("key1");
//        System.out.println("randomKey: " + randomKey);

        return "hash put get size delete hasKey ok!";
    }

    // 让 spring 调用关闭资源的时候,主动调用;
//    @PreDestroy
//    public void cleanUp() {
//        // 执行资源关闭操作
//        redisTemplate.getConnectionFactory().getConnection().close();
//    }
}
