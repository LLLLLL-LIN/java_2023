package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-09-02
 * Time:16:20
 */
@Controller
@ResponseBody
public class TestController {
    @Autowired
    private Student student;

//    @PostConstruct
    public void postConstruct() {
        System.out.println(student);
    }
    @RequestMapping("/sayHi")
    public String sayHi(String name) {
       if(name == null || name.equals("")){
           name = "张三";
       }
       return "hello " + name;
    }

    @Value("${mytest}")
    private String mytest;

    @Value("${mytest2}")
    private String mytest2;

    @RequestMapping("/get")
    public String getConf() {
        return "mytest" + mytest +
                " || mytest2" + mytest2;
    }
}
