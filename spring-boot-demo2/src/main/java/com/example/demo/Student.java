package com.example.demo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-09-03
 * Time:16:52
 */

// 这里很奇怪,在 yml 中的 key 为 Student ,而到这里用大写获取不到,小写就能获取到对象;
// 在 YAML 中，键（Key）是大小写敏感的。这意味着，当在 YAML 文件中定义一个键的时候，它的大小写会被保留，并且在解析过程中匹配属性名时也要区分大小写。
//然而，在 Java 对象映射中，Java 通常遵循驼峰命名规则，即属性名以小写字母开头，并且根据约定将首字母大写的 key 转换为对应的属性名。
// 因此，如果 YAML 文件中的键名首字母大写，并且在 Java 类中使用首字母大写的属性名来匹配该键名，会导致属性无法正确赋值
@Component
@ConfigurationProperties("student")
@Data
public class Student {
    private String name;
    private int age;
    private int num;
}
