/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-02-20
 * Time:下午9:22
 */
public class TestDemo {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        TreeNode root = binaryTree.createTree();
//        System.out.print("前序" + ":");
//        binaryTree.preOrder(root);
//        System.out.println();
//        System.out.print("中序" + ":");
//        binaryTree.inOrder(root);
//        System.out.println();
//        System.out.print("后序" + ":");
//        binaryTree.postOrder(root);
//        System.out.println();
//        System.out.println("节点个数：" + binaryTree.getSize1(root));
//        System.out.println("节点个数：" + binaryTree.getSize2(root));
//        System.out.println("叶子节点个数：" + binaryTree.getLeafSize1(root));
//        System.out.println("叶子节点个数：" + binaryTree.getLeafSize2(root));
//        System.out.print("第k层的结点数: ");
//        System.out.println(binaryTree.getKLevelSize(root, 3));
//        System.out.print("二叉树的深度/高度: ");
//        try {
//            System.out.println(binaryTree.getHeight(root));
//            System.out.println(binaryTree.find(root, 'A').val);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            System.out.println("！空指针异常！");
//        }
        System.out.println(binaryTree.isBalanced(root));
//        System.out.println(binaryTree.isSymmetric(root));
//        binaryTree.levelOrderTraversal(root);
//        System.out.println();
//        System.out.println(binaryTree.isCompleteTree(root));
//        System.out.println();
//        System.out.println(binaryTree.levelOrder(root));
    }
}
