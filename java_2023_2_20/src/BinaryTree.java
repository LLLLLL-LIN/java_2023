import jdk.nashorn.internal.runtime.regexp.joni.constants.NodeType;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-02-20
 * Time:下午9:23
 */
class TreeNode {
    public char val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(char val) {
        this.val = val;
    }

}

public class BinaryTree {

    //创建二叉树；
    public TreeNode createTree() {
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;
        return A;
    }


    //前序遍历；
    void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + " ");
        preOrder(root.left);
        preOrder(root.right);
    }


    //中序遍历；
    void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }


    //后序遍历；
    void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val + " ");
    }


    // 遍历思路-求结点个数
    public int size = 0;

    public int getSize1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        size++;
        getSize1(root.left);
        getSize1(root.right);
        return size;
    }


    // 子问题思路-求结点个数
    public int getSize2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return getSize2(root.left) + getSize2(root.right) + 1;
    }


    // 遍历思路-求叶子结点个数
    static int leafSize = 0;

    public int getLeafSize1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            leafSize++;
        }
        getLeafSize1(root.left);
        getLeafSize1(root.right);
        return leafSize;
    }


    // 子问题思路-求叶子结点个数
    public int getLeafSize2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        return getLeafSize2(root.left) + getLeafSize2(root.right);
    }


    // 子问题思路-求第 k 层结点个数
    int getKLevelSize(TreeNode root, int k) {
        if (root == null || k <= 0) {
            return 0;
        }
        if (k == 1) {
            return 1;
        }
        return getKLevelSize(root.left, k - 1) + getKLevelSize(root.right, k - 1);
    }


    // 获取二叉树的高度
    int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return getHeight(root.left) > getHeight(root.right) ? getHeight(root.left) + 1 : getHeight(root.right) + 1;
    }



    //查找二叉树当中是否含有值为val0元素；
    public TreeNode find(TreeNode root, char val)  {
        if(root == null) {
            return null;
        }
        if(root.val == val) {
            return root;
        }
        find(root.left,val);
        find(root.right,val);
        return null;
    }



    //判断是否是平衡二叉树；
    private int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int leftHigh = maxDepth(root.left);
        int rightHigh = maxDepth(root.right);
        return leftHigh > rightHigh ? leftHigh + 1 : rightHigh + 1;
    }
    public boolean isBalanced(TreeNode root) {
        if(root == null) {
            return true;
        }
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        return Math.abs(left - right) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }


    //判断是否是对称二叉树；
    private boolean isSymmetriChild(TreeNode leftTree, TreeNode rightTree) {
        if(leftTree == null && rightTree == null) {
            return true;
        }
        if(leftTree == null || rightTree == null) {
            return false;
        }
        if(leftTree.val != rightTree.val) {
            return false;
        }
        return isSymmetriChild(leftTree.left,  rightTree.right)&& isSymmetriChild(leftTree.right, rightTree.left);
    }
    public boolean isSymmetric(TreeNode root) {
        if(root == null) {
            return true;
        }
        return isSymmetriChild(root.left, root.right);
    }


    //给定root和subRoot两棵树，判断subRoot是否是root的子树；力扣：572. 另一棵树的子树；
    private boolean isSameTree1(TreeNode p, TreeNode q) {
        if(p == null && q != null || p != null && q == null) {
            return false;
        }
        if(p == null && q == null) {
            return true;
        }
        if(p.val != q.val) {
            return false;
        }
        return isSameTree1(p.left,q.left) && isSameTree1(p.right,q.right);
    }
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if(root == null || subRoot == null) {
            return false;
        }

        if(isSameTree1(root,subRoot)) {
            return true;
        }
        if(isSubtree(root.left,subRoot)) {
            return true;
        }
        if(isSubtree(root.right,subRoot)) {
            return true;
        }
        return false;
    }


    //判断给定的两个二叉树是否相等；力扣：100. 相同的树；
    public boolean isSameTree2(TreeNode p, TreeNode q) {
        if(p == null && q != null || p != null && q == null) {
            return false;
        }
        if(p == null && q == null) {
            return true;
        }
        if(p.val != q.val) {
            return false;
        }
        return isSameTree2(p.left,q.left) && isSameTree2(p.right,q.right);
    }


    //层序遍历；
   public void levelOrderTraversal(TreeNode root) {
       Queue<TreeNode> qu = new LinkedList<>();
        if(root == null) {
            return;
        }
        qu.offer(root);
        while(!qu.isEmpty()) {
            TreeNode cur = qu.poll();
            System.out.print(cur.val + " ");
            if(cur.left != null) {
                qu.offer(cur.left);
            }
            if(cur.right != null) {
                qu.offer(cur.right);
            }
        }
   }


   //二叉树的层序遍历，链表中链表的形式打印；力扣：102. 二叉树的层序遍历
   public List<List<Integer>> levelOrder(TreeNode root) {
       List<List<Integer>> ret = new ArrayList<>();
       if(root == null) {
           return ret;
       }
       Queue<TreeNode> queue = new LinkedList<>();
       queue.offer(root);
       while(!queue.isEmpty()) {
           int size = queue.size();
           List<Integer> list = new ArrayList<>();
           while(size != 0) {
               TreeNode cur = queue.poll();
               //list.add(cur.val);
               if(cur.left != null) {
                   queue.offer(cur.left);
               }
               if(cur.right != null) {
                   queue.offer(cur.right);
               }
               size--;
           }
           ret.add(list);
       }
       return ret;
   }


   //判断是否是完全二叉树；
   public boolean isCompleteTree(TreeNode root) {
        if(root == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()) {
            TreeNode cur = queue.poll();
         if(cur != null) {
             queue.offer(cur.left);
             queue.offer(cur.right);
         }else{
             break;
         }
        }
        while(!queue.isEmpty()) {
            TreeNode top = queue.peek();
            if(top != null) {
                return false;
            }
            queue.poll();
        }
        return true;
   }


   //力扣：236. 二叉树的最近公共祖先
   public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
       if(root == null) {
           return null;
       }
       if(p == root || q == root) {
           return root;
       }
       TreeNode leftT = lowestCommonAncestor(root.left, p , q);
       TreeNode rightT = lowestCommonAncestor(root.right, p , q);
       if(leftT != null && rightT != null) {
           return root;
       }else if(leftT != null) {
           return  leftT;
       }else {
           return rightT;
       }
   }
}
