import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2023-04-14
 * Time:17:30
 */

/**
 * 1.建立项目;
 * 2.创建目录;
 * 3.引入依赖;
 * 4.编写代码;
 * 5.打包代码;
 * 6.部署代码;
 * 7.浏览器访问;
 */
    @WebServlet("/servlet")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("在控制台打印 : HelloServlet");
        resp.getWriter().write("hello Servlet");
    }
}
