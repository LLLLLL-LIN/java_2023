package webautotest2;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-28
 * Time:13:04
 */
@Suite
//@SelectClasses({webautotest2.JUnitTest.class,webautotest1.AllTest.class})
@SelectPackages("webautotest2")
public class RunAllSuite {

}
