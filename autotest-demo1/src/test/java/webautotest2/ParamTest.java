package webautotest2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.io.File;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-28
 * Time:15:22
 */
public class ParamTest {
    // 注意:这里的 '非引用类型' 必须显示有值,不然直接报错,这里的没有默认值;
    // 单参数;
    @ParameterizedTest
    @ValueSource(strings = {"张三","李四","王五","赵六"})
    void nameParamsTest(String name) {
        System.out.println("name" + name);
    }
    @ParameterizedTest
    @ValueSource(ints = {19,18,20,23})
    void ageParamsTest(int age) {
        System.out.println("age" + age);
    }

    // 多参数;
    @ParameterizedTest
//    @CsvSource(value = {"张三,19","李四,18","王五,20","赵六,23"}
    // 这里可以采用换掉默认的分隔符;如果参数中有','之类的影响,可以用''包裹参数,如"(value = {"'张,三'-19"});
    @CsvSource(value = {"张三-19","李四-18","王五-20","赵六-23"}, delimiterString = "-")
    void paramsTest(String name, int age) {
        System.out.println("name: " + name + " age: " + age);
    }

    // csv文件形式的多参数;保持代码美观;
    @ParameterizedTest
    // 指定文件路径为当前项目下的 resource 中的 my.csv 文件
    @CsvFileSource(resources = "/my.csv")
    // 指定磁盘中的文件;
//    @CsvFileSource(files = "D:\\file\\other\\my.csv")
    void csvParamsTest(String name, int age) {
        System.out.println("name: " + name + " age: " + age);
    }

    //动态单参数
    @ParameterizedTest
    // @MethodSource 注解可以用于指定一个方法来提供参数化测试的参数。
    // 这个方法必须返回一个Stream，Iterable，Iterator或者参数数组。因此，@MethodSource 注解只能接收返回这些类型的方法作为参数。
    @MethodSource("dateDemo") // 这里的数据源的方法如果没写是哪个方法,会直接找同名的静态方法,也就是xParamesTest();
    void xParamsTest(String x) {
        System.out.println("name" + x);
    }
    // 传输参数的方法;这个方法返回的是 Stream 流对象;
    // 注意我们采用了静态方法 Stream.of, 所以这里方法要设置为静态;
    static Stream<String> dateDemo() {
        return Stream.of("张三","李四","王五","赵六");
    }

    // 动态多参数;
    @ParameterizedTest
    @MethodSource
    void xsParamsTest(String name, int age) {
        System.out.println("name: " + " age: " + age);
    }
    // 动态多参数就采用 Arguments;
    static Stream<Arguments> xsParamsTest() {
        return Stream.of(Arguments.arguments("张三",20),Arguments.arguments("李四",22));
    }
}
