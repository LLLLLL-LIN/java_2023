package webautotest1;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-20
 * Time:14:04
 */
public class RunAutoTest {
    public static void main(String[] args) throws InterruptedException, IOException {
        FirstAutoTest firstAutoTest = new FirstAutoTest();
//        firstAutoTest.check();
//        firstAutoTest.testDemo1();
//        firstAutoTest.testDemo2();
//        firstAutoTest.testDemo3();
//        firstAutoTest.testDemo4();
//        firstAutoTest.testDemo5();
//        firstAutoTest.windows();
//        firstAutoTest.windowSize();
//        firstAutoTest.scroll();
        firstAutoTest.navigate();
//        firstAutoTest.screenShow();
        firstAutoTest.driverOut();
    }
}
