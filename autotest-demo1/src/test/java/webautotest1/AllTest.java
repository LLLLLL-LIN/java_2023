package webautotest1;

import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-27
 * Time:15:33
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AllTest {
    static ChromeDriver driver = null;
//
//    // 跳转页面;
//    @Test
//    @BeforeAll
//    public static void startTest() {
//        // 创建一个Chrome浏览器实例
//        ChromeOptions chromeOptions = new ChromeOptions();
//        //解决 403 出错问题
//        chromeOptions.addArguments("--remote-allow-origins=*");
//        driver = new ChromeDriver(chromeOptions);
//        driver.get("https://www.baidu.com");
//    }
//
//    // 执行中间操作;
//    @Test
//    public void demo1Test() {
//        driver.findElement(By.cssSelector("#kw")).sendKeys("selenium");
//        driver.findElement(By.cssSelector("#su")).click();
//    }
//
//    // 释放 driver;
//    @Test
//    @AfterAll
//    public static void quitTest() {
//        driver.quit();
//    }

//    @Test
//    @BeforeEach
//    void say1Test() {
//        System.out.println("前");
//    }
//
//    @Test
//    void say2Test() {
//        System.out.println("中");
//    }
//
//    @Test
//    @AfterEach
//    void say3Test() {
//        System.out.println("后");
//    }

//    /**
//     * 断言;
//     * 判断文本;
//     */
//    @Test
//    void textTest() {
//        Assertions.assertEquals("文本","文本");
//    }
//
//    @Test
//    void text2Test() {
//        Assertions.assertNotEquals("文本","文本2");
//    }
//
//    @Test
//    void nullTest() {
//        String a = null;
//        Assertions.assertNull(a);
//    }
//
//    @Test
//    void notNUllTest() {
//        String a = "AAA";
//        Assertions.assertNotNull(a);
//    }

    /**
     * @Order
     */
    @Test
    @Order(1)
    void printTest() {
        ChromeOptions chromeOptions = new ChromeOptions();
        //解决 403 出错问题
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw"));
    }
    @Test
    @Order(2)
    void findTest() {
        driver.findElement(By.cssSelector("#su")).click();
    }
    @Test
    @Order(3)
    void quitTest() {
        driver.quit();
    }
}
