package webautotest1;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.chromium.ChromiumOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.Duration;
import java.util.Set;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-20
 * Time:14:03
 */
public class FirstAutoTest {

    // 浏览器驱动对象;
//    private static FirefoxDriver driver;
    private static ChromeDriver driver;

    // 浏览器驱动对象的赋值;
    public FirstAutoTest() {
//        driver = new FirefoxDriver();

        // 创建一个Chrome浏览器实例
        ChromeOptions chromeOptions = new ChromeOptions();
        //解决 403 出错问题
        chromeOptions.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(chromeOptions);
    }

    public void check() {
        driver.get("https://www.baidu.com");
    }

    // 简单的自动化测试脚本;
    public void testDemo1() throws InterruptedException {
        // 获取页面;
        driver.get("https://www.baidu.com");
        // 获取输入框并进行输入内容;
        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
        Thread.sleep(100);
        // 触发点击;
        driver.findElement(By.cssSelector("#su")).click();
        Thread.sleep(1000);
    }

    // 获取文本用 .getText();
    // 有些文字虽然是展示在页面上的,但是其本质不是 text,有的可能是 value, 这里就得用 .getAttribute("") 来获取了;
    public void testDemo2() {
        driver.get("https://www.baidu.com");
//        System.out.println(driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).getText());
        System.out.println(driver.findElement(By.cssSelector("#su")).getAttribute("value"));
    }

    // 对于按钮的功能的执行,我们既可以用 .click 进行点击,也可以用 submit 进行提交;
    public void testDemo3() {
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#su")).click();
        driver.findElement(By.cssSelector("#su")).submit();
    }

    // 程序的执行速度快于渲染的速度;
    public void testDemo4() throws InterruptedException {
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
        Thread.sleep(2000);
        System.out.println(driver.findElement(By.cssSelector("#\\31  > div > div > div > div > div > div.cos-row.row-text_Johh7.row_5y9Az > div > div.title-wrapper_XLSiK")).getText());
    }

    // 三种等待方式;
    public void testDemo5() throws InterruptedException {
//        driver.get("https://www.baidu.com");
//        driver.findElement(By.cssSelector("#kw")).sendKeys("selenium");
//        driver.findElement(By.cssSelector("#su")).click();
//        // 1.强制等待/静态等待;
//        Thread.sleep(2000);
//        System.out.println(driver.findElement(By.cssSelector("#\\33 001 > div > div > div > div:nth-child(1) > h3 > div > a > font")));        driver.get("https://www.baidu.com");

        // 2.隐式等待;
        // 等于是全局的设置,这样设置之后,全局的查找和操作都得等待;
//        driver.get("https://www.baidu.com");
//        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
//        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
//        driver.findElement(By.cssSelector("#su")).click();
////        System.out.println(driver.findElement(By.xpath("//*[@id=\"1\"]/div/div/div/div/div/div[2]/div/div[2]/a/div/p/span/span")).getText());
//        System.out.println(driver.findElement(By.cssSelector("#\\31  > div > div > div > div > div > div.cos-row.row-text_Johh7.row_5y9Az > div > div.title-wrapper_XLSiK > a > div > p > span > span")).getText());

        // 3.显示等待;
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("QQ");
        driver.findElement(By.cssSelector("#su")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.textToBe(By.cssSelector("#\\31  > div > div:nth-child(1) > h3 > a:nth-child(1)"), "QQ-新不止步,乐不设限"));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#\\31  > div > div:nth-child(1) > h3 > a:nth-child(1)")));
    }

    /**
     * 窗口的切换;
     * getWindowHandle();
     * switch.to.window();
     */
    public void windows() {
        driver.get("https://www.baidu.com");
        System.out.println(driver.getTitle());
        // 打印当前页面的句柄;
        System.out.println(driver.getWindowHandle());
        // 跳转到了新的页面;
        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        // 获取新页面的额标题,但是还是第一个页面;
        System.out.println(driver.getTitle());
        // 获取所有页面的句柄;
        Set<String> windowSet = driver.getWindowHandles();
        System.out.println(windowSet);
        // 获取页面的句柄,但是这个句柄是第一个页面的;
        String nowWindow = driver.getWindowHandle();
        for (String window : windowSet) {
            // 非第一个页面的句柄,就直接进行跳转;
            if(!nowWindow.equals(window)) {
                driver.switchTo().window(window);
            }
        }
        // 打印当前的页面 Title;
        System.out.println(driver.getTitle());
    }

    /**
     * 窗口大小;
     * 最大化:maximize();
     * 最小化:minimize();
     * 自定义大小:setSize();
     * @throws InterruptedException
     */
    public void windowSize() throws InterruptedException {
        driver.get("https://www.baidu.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);
        driver.manage().window().minimize();
        Thread.sleep(2000);
        driver.manage().window().setSize(new Dimension(1000,800));
        Thread.sleep(2000);
    }

    /**
     * 滚动条滑动;
     * executeScript();
     * 括号中采用 js 语句;
     * "window.scroll(0,document.body.scrollHeight";
     * @throws InterruptedException
     */
    public void scroll() throws InterruptedException {
        driver.get("https://news.baidu.com/");
        driver.executeScript("window.scroll(0,document.body.scrollHeight)");
        Thread.sleep(2000);
        driver.executeScript("window.scroll(0,document.body.scrollTop)");
        Thread.sleep(2000);
    }

    /**
     * 前进: forward();
     * 后退: back();
     * @throws InterruptedException
     */
    public void navigate() throws InterruptedException {
        // 多个页面的 forward 和 back 存在一定的问题;
//        driver.get("https://www.baidu.com");
//        driver.findElement(By.cssSelector("#s-top-left > a:nth-child(1)")).click();
        // 针对于单个页面的前进和后退操作是没有问题的;
        driver.get("https://tool.lu/");
        driver.findElement(By.cssSelector("#nav > div > div > ul > li:nth-child(1) > a")).click();
        driver.navigate().back();
        Thread.sleep(2000);
        driver.navigate().forward();
        Thread.sleep(2000);
    }

    public void screenShow() throws IOException {
        driver.get("https://www.baidu.com");
        driver.findElement(By.cssSelector("#kw")).sendKeys("selenium");
        driver.findElement(By.cssSelector("#su")).submit();
        File srcfile = driver.getScreenshotAs(OutputType.FILE);
        File filename = new File("src/test/autopic/sereenPic.png");
        FileUtils.copyFile(srcfile,filename);
        driver.findElement(By.cssSelector("#\\33 001 > div > div:nth-child(1) > div > div > h3 > div > a"));
    }

    // 释放 driver;
    public void driverOut() {
        driver.quit();
    }
}
