package file;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-10
 * Time:10:21
 */
public class IODemo1 {
    public static void main(String[] args) throws IOException {
        File file = new File("d:/LearnING/test.txt");
        File file1 = new File("d:/LearnING/toCreate.txt");
        File file2 = new File("d:/LearnING/testDemo");
        File file3 = new File("d:/LearnING/testDemo/testDemo1/testDemo2/testDemo3/testDemo4/testDemo5");
        File file4 = new File("d:/LearnING/testDemo4");
//        // 打印父目录的文件路径;
//        System.out.println("父目录: " + file.getParentFile());
//        // 打印该文件对象的纯文件名;
//        System.out.println("纯文件名: " + file.getName());
//        // 打印该文件对象的文件路径;
//        System.out.println("文件路径: " + file.getPath());
//        // 打印该文件对象的绝对路径;
//        System.out.println("文件的绝对路径: " + file.getAbsolutePath());
//        // 打印该文件对象被修饰过的绝对路径;
//        System.out.println("被修饰过的绝对路径: " + file.getCanonicalPath());
//        // 判断该文件对象是否存在;
//        System.out.println("该文件对象代表的文件是否存在: " + file.exists());
//        // 判断该文件对象是否是一个文件夹;
//        System.out.println("是否是文件夹: " + file.isDirectory());
//        // 判断该文件对象代表的是否是一个普通文件;
//        System.out.println("是否是普通文件: " + file.isFile());
//        // 根据File对象调用delete删除;
//        System.out.println("是否删除: " + file.delete());
//        // 创建一个新的文件(file1);
//        System.out.println("是否成功创建文件(file1): " + file1.createNewFile());
//        // 删除文件(file1);
//        System.out.println("是否成功删除文件(file1): " + file1.delete());
//        // 创建目录(file2); 单一目录就采用 mkdir() 就行;
//        System.out.println("是否成功创建空文件夹(file2): " + file2.mkdir());
//        // 删除目录(file2);
//        System.out.println("是否成功删除空文件夹(file2): " + file2.delete());
//        // 创建多级目录(file3); 多级目录得采用 mkdirs();
//        System.out.println("是否成功创建多级空文件夹(file3): " + file3.mkdirs());
//        // 打印该目录下的所有文件;
//        toPrintPath(file2); // 调用递归方法打印文件路径;
//        // 删除多级目录(file3); 因为是多级目录,所以这里的删除得采用递归删除;
//        toDeleteFile(file2);
//        // 重命名该文件;
//        System.out.println("是否成功更改文件名: " + file2.renameTo(file4));
        // 判断文件是否能读 canRead();
        File file5 = new File("d:/LearnING/testDemo.txt");
        // 在磁盘中创建文件;
        file5.createNewFile();
        System.out.println("文件是否能读: " + file5.canRead());
        // 判断文件是否可写 canWrite();
        System.out.println("文件是否能写: " + file5.canWrite());
    }


    // 递归打印多级文件夹;
    public static void toPrintPath(File fileToPrint) {
        if (fileToPrint.isDirectory()) {
            File[] files = fileToPrint.listFiles();
            if (files != null) {
                for (File file : files) {
                    System.out.println("文件路径: " + file.getPath());
                    if (file.isDirectory()) {
                        toPrintPath(file);
                    }
                }
            }
        }
    }


    // 递归删除多级文件夹;
    public static void toDeleteFile(File fileToDelete) {
        if (fileToDelete.isDirectory()) {
            File[] files = fileToDelete.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        toDeleteFile(file);
                    }
                    System.out.println("删除文件: " + file.getPath());
                    file.delete();
                }
            }
        }
    }
}
