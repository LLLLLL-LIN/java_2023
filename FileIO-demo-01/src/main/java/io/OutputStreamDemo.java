package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-12
 * Time:17:29
 */
// 输出流(字节流);
// OutputStream;
public class OutputStreamDemo {
    public static void main(String[] args) {
        // 逻辑代码执行完之后自动执行括号中所定义对象的close(),前提是这个对象的类实现了Closeable接口;
        try(OutputStream outputStream = new FileOutputStream("d:/LearnING/test.txt");) {
            // 定义要写入到文件中的字符串;
            String toWrite = "你好Java";
            // 将字符串转为字节数组;
            byte[] bytes = toWrite.getBytes();
            // 作为输入型参数;
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
