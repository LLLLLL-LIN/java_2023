package io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-12
 * Time:16:49
 */
// 输入流(字节流);
// InputStream;
public class InputStreamDemo {
    public static void main(String[] args) throws IOException {
        // 构造流对象;
        try(InputStream inputStream = new FileInputStream("d:/LearnING/test.txt");) {
            // 定义一个输入型byte数组;
            byte[] buffer = new byte[1024];
            // 用InputStream实例对象调用 read() 按字节来读取;
            int num = inputStream.read(buffer);
            System.out.println("num: " + num);
            // 循环打印;
            for (int i = 0; i < num; i++) {
                System.out.printf("%x\n", buffer[i]);
            }
            // 把读取到的字节数组,采用String的构造方法,进行转换;
            // offset表示从哪里开始读,num,表示读取几个字节,最后一个参数是编码方式;
            String str = new String(buffer, 0, num, StandardCharsets.UTF_8);
            // 打印构造的字符串;
            System.out.println(str);
        }
    }
}
