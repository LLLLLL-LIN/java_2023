package io;

import java.io.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-13
 * Time:14:04
 */
// PrintWriter;
public class PrintWriterDemo {
    public static void main(String[] args) {
        try (OutputStream outputStream = new FileOutputStream("d:/LearnING/test.txt");) {
            // 用PrintWriter的 write() 或者 print() / printf(),这些没有字节数组的限制;
            // 相当于原先是字节流操作,参数得是字节数组,现在转变成字符流操作了;
            PrintWriter printWriter = new PrintWriter(outputStream);
            // 调用方法写入字符串;
            // 这个其实是写入缓存区,不是直接写到硬盘中;
            printWriter.println("hello world!");
            // 采取flush()就可以直接把缓存区的数据直接写到硬盘中;
            printWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
