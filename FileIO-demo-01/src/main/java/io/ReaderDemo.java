package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-10
 * Time:17:18
 */
// 输入流(字符流);
// Reader;
public class ReaderDemo {
    public static void main(String[] args) throws IOException {
        Reader reader = new FileReader("d:/LearnING/reader.txt");
        // 1.调用无参版本;
//        readDemo1(reader);
        // 2.调用参数为一个数组的版本;
        readDemo2(reader);
        // 3.文件使用完了,记得close();
        reader.close();
    }

    // 单独一个数组作为输入型参数的 read();
    private static void readDemo2(Reader reader) throws IOException {
        // 定义一个字符数组来作为输入性参数;
        char[] charArray = new char[3];
        // 定义变量来存储read()的返回值;
        int num = 0;
        // 当返回值为-1的时候,说明读到了文件末尾了;
        while (num != -1) {
            num = reader.read(charArray);
            // 打印一下当前read()获取到的字符数;
            System.out.println("num: " + num);
            // 循环打印,这里不能采用foreach,,等于会把所有空值全部解码;
            for (int i = 0; i < num; i++) {
                System.out.print(charArray[i]);
            }
            System.out.println();
        }
    }

    // 无参的 read();
    public static void readDemo1(Reader reader) throws IOException {
        // 循环读取文件;
        // 因为 read() 这个方法一次读取的是一个字符;
        while (true) {
            int c = reader.read();
            // 如果返回值为-1了,则代表这个文件读取结束了,换句话说,到文件末尾了;
            if (c == -1) {
                break;
            }
            // 将得到的 unicode 字符集的码点进行转换,得到字符;
            char word = (char)c;
            System.out.print(word);
        }
    }
}
