package io;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-12
 * Time:18:09
 */
// Scanner代码学习;
public class ScannerTestDemo {
    public static void main(String[] args) {
        // 定义要读取的字符串;
        String str1 = "hello 123 world";
        String str2 = " hello 123 world";
        String str3 = " 123 hello world";
        String str4 = " 123hello world";
        String str5 = " hello123 world";
        Scanner scanner = new Scanner(str5);
//        System.out.println(scanner.next());
//        System.out.println(scanner.nextInt());
        // nextLine()会读取到下一个换行符之前所有的字符,包括空格等;
        System.out.println(scanner.nextLine());
        // 总结起来就是,不管是 next() 还是 nextInt(),
        // 如果要读取的字符串的第一个字符就是空格,他就会跳过空格,从第一个非空格(有值)开始读取;
        // 1.如果第一个值为整数,则都能读取成功;
        // 2.如果第一个值为字符串,则next()能够正常读取,nextInt()报错(InputMismatchException);
        // 3.如果第一个值为整数+字符串,next()能够耨读取,nextInt()报错(InputMismatchException);
        // 4.如果第一个值为字符串+整数,next()能够读取,nextInt()报错(InputMismatchException);
        // 5.nextLine() 则不太相同,nextLine()是读取到下一个换行符之前所有的字符,包括空格也会读取到,并且不会跳过第一个空格;
    }
}
