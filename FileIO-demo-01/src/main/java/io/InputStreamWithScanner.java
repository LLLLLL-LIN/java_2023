package io;

import java.io.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-12
 * Time:17:43
 */
// 用 Scanner 来读取流对象;
public class InputStreamWithScanner {
    public static void main(String[] args) {
        try(InputStream inputStream = new FileInputStream("d:/LearnING/test.txt")) {
            // 定义 Scanner, 构造方法中告知要读取的内容是什么,这里是文件的字节流对象;
            Scanner scanner = new Scanner(inputStream);
            // 进行读取;
            String str = scanner.next();
            // 打印读取到的文件内容;
            System.out.println(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
