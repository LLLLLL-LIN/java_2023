package io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-11
 * Time:16:34
 */
// 输出流(字符流);
// Writer
public class WriteDemo {
    public static void main(String[] args) {
        // 把实现了Closeable接口的类型的变量放在这个try的括号中定义,在执行完中间的代码之后,会自动调用变量的 close(),进行关闭;
        // 这个 appecd 为 true 的话,write()就是在原先文件内容的基础上进行拼接,不然就是覆盖原先的内容;
        try(Writer writer = new FileWriter("d:/LearnING/writer.txt",true)) {
            writer.write("这是一个新的文件IO操作");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
