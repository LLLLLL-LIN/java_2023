package io;

import java.io.File;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-14
 * Time:09:57
 */
public class testProjectDemo1 {
    public static void main(String[] args) {
        // 提示文本;
        System.out.println("输入要扫描的路径");
        Scanner scanner = new Scanner(System.in);
        String toCheckPath = scanner.next();
        System.out.println("请输入要删除的文件关键字");
        String keyToDel = scanner.next();
        // 检验该路径的文件夹路径是否存在;
        File toCheckFile = new File(toCheckPath);
        if (!toCheckFile.exists() || !toCheckFile.isDirectory()) {
            // 提示文本!
            System.out.println("输入的文件路径不对");
            return;
        }
        // 路径正确,该文件夹存在,则调用遍历方法,进行递归;
        toCheckAndDel(toCheckFile, keyToDel);
    }

    // 递归便利文件;
    private static void toCheckAndDel(File toCheckFile, String keyToDel) {
        // 遍历得到文件对象数组;
        File[] files = toCheckFile.listFiles();
        // 如果文件为空,表明 toCheckFile 这个文件夹中没有文件,则直接 return;
        if (files == null) {
            return;
        }
        for (File file1 : files) {
            System.out.println("当前扫描路径是: " + file1.getAbsolutePath());
            if (file1.isDirectory()) {
                toCheckAndDel(file1, keyToDel);
            } else {
                isDel(file1, keyToDel);
            }
        }
    }

    // 检验文件名和关键字是否匹配,进行删除;
    private static void isDel(File file1, String keyToDel) {
        // 打印一下当前的文件路径;
        System.out.println("当前文件是: " + file1.getPath());
        // 判断一下该文件是否要删除;
        if ((file1.getName()).contains(keyToDel)) {
            // 询问一下用户是否要删除该文件;
            System.out.println("是否要删除该文件: " + file1.getName() + " 请输入: y/n 来确认");
            Scanner scanner = new Scanner(System.in);
            String yn = scanner.next();
            if(yn.equals("Y") || yn.equals("y")) {
                System.out.println("文件删除结果: " + (file1.delete() ?"yes":"no" ));
            } else {
                System.out.println("取消删除操作!");
            }
        }
    }
}
