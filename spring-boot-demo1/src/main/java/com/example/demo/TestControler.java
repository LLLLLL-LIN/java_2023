package com.example.demo;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-16
 * Time:00:32
 */

@Controller
@ResponseBody
public class TestControler  {
    @RequestMapping("/hi")
    // 这里的整数类型要使用包装类而不是基础类，这样在前端没有传 v 的参数的时候不会报错（500），这样前端最多显示一个 null；
    // 这里的参数名称要和前端传输的时候的关键字一摸一样，不然找不到，顺序不影响。
    public String demo1(String name,Integer v) {
        if (!StringUtils.hasLength(name)){
            name = "张三";
        }
        return "你好 " + name + " v : " + v;
    }
}
