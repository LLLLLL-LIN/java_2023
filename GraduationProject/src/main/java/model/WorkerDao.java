package model;

import controller.WorkerListServlet;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:03:10
 */
public class WorkerDao {

    // 根据工作人员的名字查找信息。
    public Worker selectByName(String workerName) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from worker where workerName = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerName);
            resultSet = statement.executeQuery();
            // 此处 username 使用 unique 约束, 要么能查到一个, 要么一个都查不到.
            if (resultSet.next()) {
                Worker worker = new Worker();
                worker.setWorkerId(resultSet.getInt("workerId"));
                worker.setWorkerName(resultSet.getString("workerName"));
                worker.setSex(resultSet.getString("sex"));
                worker.setNum(resultSet.getString("num"));
                worker.setJoinTime(Timestamp.valueOf(resultSet.getString("joinTime")));
                return worker;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return null;
    }

    // 查找全部的工作人员，采用顺序表返回。
    public static List<Worker> selectAll() {
        List<Worker> workerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from worker";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Worker worker = new Worker();
                worker.setWorkerId(resultSet.getInt("workerId"));
                worker.setWorkerName(resultSet.getString("workerName"));
                worker.setSex(resultSet.getString("sex"));
                worker.setNum(resultSet.getString("num"));
                worker.setJoinTime(resultSet.getTimestamp("joinTime"));
                workerList.add(worker);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return workerList;
    }

    // 4. 从工作人员表中, 根据工作人员 id 删除博客.
    public void delete(int workerId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from blog where workerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, workerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 根据工作人员 workerId，更改相关信息。
    public void update(int workerId, String workerName, String sex, String num, Timestamp joinTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE worker SET workerName = ?, sex = ?, num = ?, joinTime = ? WHERE workerId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, workerName);
            statement.setString(2, sex);
            statement.setString(3, num);
            statement.setTimestamp(4, joinTime);
            statement.setInt(1, workerId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }
}
