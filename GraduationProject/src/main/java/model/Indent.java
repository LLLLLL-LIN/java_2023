package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:02:38
 */
public class Indent {
    private int indentId = 0;
    private String indentServer;
    private int indentPrice;
    private String indentWorker;
    private String consumerName;
    private Timestamp indentTime;

    public int getIndentId() {
        return indentId;
    }

    public void setIndentId(int indentId) {
        this.indentId = indentId;
    }

    public String getIndentServer() {
        return indentServer;
    }

    public void setIndentServer(String indentServer) {
        this.indentServer = indentServer;
    }

    public int getIndentPrice() {
        return indentPrice;
    }

    public void setIndentPrice(int indentPrice) {
        this.indentPrice = indentPrice;
    }

    public String getIndentWorker() {
        return indentWorker;
    }

    public void setIndentWorker(String indentWorker) {
        this.indentWorker = indentWorker;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

//    public Timestamp getIndentTime() {
//        return indentTime;
//    }

    public String getIndentTime() {
        // 使用 SimpleDateFormat 来完成时间戳到格式化日期时间的转换.
        // 这个转换过程, 需要在构造方法中指定要转换的格式, 然后调用 format 来进行转换
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(indentTime);
    }

    public void setIndentTime(Timestamp indentTime) {
        this.indentTime = indentTime;
    }
}
