package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:16:07
 */
public class IndentDao {

    // 根据indentId 去查找订单，采用顺序表返回。
    public List<Indent> selectOne(int indentId) {
        List<Indent> indentList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from indent where indentId = ?";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Indent indent = new Indent();
                indent.setIndentId(resultSet.getInt("indentId"));
                indent.setIndentServer(resultSet.getString("indentServer"));
                indent.setIndentPrice(resultSet.getInt("indentPrice"));
                indent.setIndentWorker(resultSet.getString("indentWorker"));
                indent.setConsumerName(resultSet.getString("consumerName"));
                indent.setIndentTime(resultSet.getTimestamp("indentTime"));
                indentList.add(indent);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return indentList;
    }

    // 查找全部的订单，采用顺序表返回。
    public List<Indent> selectAll() {
        List<Indent> indentList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from indent";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Indent indent = new Indent();
                indent.setIndentId(resultSet.getInt("indentId"));
                indent.setIndentServer(resultSet.getString("indentServer"));
                indent.setIndentPrice(resultSet.getInt("indentPrice"));
                indent.setIndentWorker(resultSet.getString("indentWorker"));
                indent.setConsumerName(resultSet.getString("consumerName"));
                indent.setIndentTime(resultSet.getTimestamp("indentTime"));
                indentList.add(indent);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, resultSet);
        }
        return indentList;
    }

    // 4. 从订单表中, 根据订单 id 删除订单信息.
    public void delete(int indentId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from indent where consumerId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, indentId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }

    // 根据订单id indentId，更改相关信息。
    public void update(int indentId, String indentServer, int indentPrice, String indentWorker, String consumerName, Timestamp indentTime) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "UPDATE indent SET indentServer = ?, indentPrice = ?,indentWorker = ?, consumerName = ?, indentTime = ? WHERE indentId = ?;";
            statement = connection.prepareStatement(sql);
            statement.setString(1, indentServer);
            statement.setInt(2, indentPrice);
            statement.setString(3, indentWorker);
            statement.setString(4, consumerName);
            statement.setTimestamp(5, indentTime);
            statement.setInt(6, indentId);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(connection, statement, null);
        }
    }
}
