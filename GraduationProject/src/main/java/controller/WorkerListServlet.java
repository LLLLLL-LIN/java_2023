package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Worker;
import model.WorkerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-05-21
 * Time:04:31
 */
@WebServlet("/workerList")
public class WorkerListServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    // 这个方法用来获取到数据库中的工作人员列表.
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");
        WorkerDao workerDao = new WorkerDao();
        List<Worker> workerList = WorkerDao.selectAll();
        // 把 workerList对象转成 JSON 格式.
        String respJson = objectMapper.writeValueAsString(workerList);
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(respJson);
    }
}
