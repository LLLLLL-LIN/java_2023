package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:上午12:29
 */

//未登录访问测试；
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UnLoginTest extends CommonDriver {
     WebDriver driver = getEdgeDriver();

    @Test
    @Order(1)
    //未登录访问测试；
    public void unLoginTest() {
        driver.get("http://127.0.0.1:8080/webProject/blog_list.html");
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String text = driver.findElement(By.cssSelector("body > div.login-container > form > div > h3")).getText();
        Assertions.assertEquals("登录", text);
    }

    @Test
    @Order(2)
    public void driverQuitTest() {
        driver.quit();
    }
}
