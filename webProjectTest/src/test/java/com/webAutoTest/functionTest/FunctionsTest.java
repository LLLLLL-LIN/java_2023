package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:上午9:14
 */

//测试博客网站的基本功能；也可以用不同的类，使用套件全部调用；
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FunctionsTest extends CommonDriver {
    static WebDriver driver = getEdgeDriver();

    @Test
    @BeforeAll
    //主页登录测试；
    static void loginTest() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
    }

    //滚动条测试；
    void scrollTest() {

    }
    @Test
    @Order(1)
    //编辑博客测试；
    void editTest() {
        String str1 = driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).getText();
        Assertions.assertEquals("写博客", str1);
        driver.findElement(By.xpath("/html/body/div[1]/a[2]")).click();
        String str2 = driver.findElement(By.xpath("//*[@id=\"title\"]")).getAttribute("placeholder");
        Assertions.assertEquals("在此处输入标题", str2);
        driver.findElement(By.xpath("//*[@id=\"title\"]")).sendKeys("文章标题！");
    }

    @Test
    @Order(2)
    //发布博客测试；
    void pulishTest() {
        driver.findElement(By.cssSelector("#submit")).click();
        String text1 = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[1]")).getText();
        Assertions.assertEquals("文章标题！", text1);
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String text2 = driver.findElement(By.cssSelector("body > div.container > div.left > div > h3")).getText();
        System.out.println(text2);
        Assertions.assertEquals("zhangsan", text2);
    }

    @Test
    @Order(3)
    //删除博客测试；
    public void deleteTest() {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(7)")).click();
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String text = driver.findElement(By.cssSelector("body > div.container > div.right > div > h3")).getText();
        Assertions.assertNotEquals("文章标题！", text);
    }


    @Test
    @Order(4)
    //退出登录测试；
    void exitTest() {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String text = driver.findElement(By.cssSelector("body > div.login-container > form > div > h3")).getText();
        System.out.println(text);
        Assertions.assertEquals("登录", text);
    }

    @Test
    @AfterAll
    static void driverQuit() {
        driver.quit();
    }
}
