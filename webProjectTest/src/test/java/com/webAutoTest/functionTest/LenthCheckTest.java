package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-18
 * Time:下午9:59
 */

/**
 * 边界值法设计测试用例；
 * 1.正文为49个字符；
 * 2.正文为51个字符；
 * 3.正文为50个字符；
 * 正文为20个字符；
 * 正文为70个字符；
 */
//测试博客列表文章摘要部分的展示字符个数；
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LenthCheckTest extends CommonDriver {
    static WebDriver driver = getEdgeDriver();

    static String str1 = "0123456789";
    static String str2 = "012345678";
    static String str3 = "01234567890";


    void editTest(String string) {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        driver.findElement(By.cssSelector("#title")).sendKeys("字符长度！");
//        driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre")).click();
        driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre > span")).clear();
    }

    @Test
    @Order(1)
    void write1Test() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        //写入49个字符
        String string = str1 + str1 + str1 + str1 + str2;
        editTest(string);
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.desc")).getText();
        Assertions.assertEquals("49", String.valueOf(text1.length()));
    }

    @Test
    @Order(2)
    public void write2Test() {
        //写入51个字符
        editTest(str1 + str1 + str1 + str1 + str3);
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.desc")).getText();
        Assertions.assertEquals("50", String.valueOf(text1.length()));
    }

    @Test
    @Order(3)
    public void write3Test() {
        //写入50个字符
        editTest(str1 + str1 + str1 + str1 + str2);
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.desc")).getText();
        Assertions.assertEquals("50", String.valueOf(text1.length()));
    }

    @Test
    @Order(4)
    public void write4Test() {
        //写入20个字符
        editTest(str1 + str1);
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.desc")).getText();
        Assertions.assertEquals("20", String.valueOf(text1.length()));
    }

    @Test
    @Order(5)
    public void write5Test() {
        //写入80个字符
        editTest(str1 + str1 + str1 + str1 + str1 + str1 + str1 + str1);
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.desc")).getText();
        Assertions.assertEquals("50", String.valueOf(text1.length()));
    }
}
