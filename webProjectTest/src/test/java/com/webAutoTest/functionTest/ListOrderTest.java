package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-22
 * Time:上午9:40
 */

//博客列表顺序是否按照时间顺序；
public class ListOrderTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    void listOrderTest() {
        //首页登录,进入博客列表页；
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        //查看博客列表是否按照顺序排列；
        //连续发布两篇文章，准备查看文章发布时间；
        int count = 0;
        while(count < 2) {
            driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
            driver.findElement(By.cssSelector("#title")).sendKeys("列表顺序测试" + count);
            driver.findElement(By.cssSelector("#submit")).click();
            count++;
        }
        //检验两篇文章的发布时间；
        String str1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.date")).getText();
        String str2 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(2) > div.date")).getText();
        String timeC1 = str1.substring(18, 19);
        String timeC2 = str1.substring(18, 19);
        int time1 = Integer.valueOf(timeC1);
        int time2 = Integer.valueOf(timeC1);
        Assertions.assertTrue(time1 >= time2);
        driver.quit();
    }
}
