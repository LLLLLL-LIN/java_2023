package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.devtools.v85.css.model.Value;

import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.TreeUI;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:下午12:38
 */

//测试登录用户名和密码的四种情况；
public class LoginTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    /**
     * 判定表法设计用例，用户登录账号密码的四种情况；
     * 用户名正确，密码正确；
     * 用户名正确，密码错误；
     * 用户名错误，密码正确；
     * 用户名错误，密码错误；
     */
    @Test
    void login1Test() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        String str = driver.findElement(By.cssSelector("body > div.container > div.left > div > h3")).getText();
        Assertions.assertEquals("zhangsan", str);
    }

    @ParameterizedTest
    @CsvSource(value = {"zhagnsan,1234", "zhangsan2,123", "zhangsan2,12"})
    void loginTest(String username, String password) throws InterruptedException {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys(username);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        String str = driver.findElement(By.cssSelector("body")).getText();
        Assertions.assertEquals("用户名错误或密码错误!", str);
    }
}
