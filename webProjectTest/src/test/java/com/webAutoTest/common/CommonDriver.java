package com.webAutoTest.common;


import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.time.Duration;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-18
 * Time:下午6:05
 */
//继承此类返回driver;
public class CommonDriver {
    //创建驱动对象并返回;
    private static EdgeDriver driver;
    public static EdgeDriver getEdgeDriver() {
        //        if(driver == null) {
//            driver = new EdgeDriver();
//            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
//        }
        driver = new EdgeDriver();
//        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        return driver;
    }


//火狐浏览器
    public static FirefoxDriver getFirefoxDriver() {

        return new FirefoxDriver();
    }

//IE浏览器
    public static InternetExplorerDriver getIEDriver() {
        return new InternetExplorerDriver();
    }
}
