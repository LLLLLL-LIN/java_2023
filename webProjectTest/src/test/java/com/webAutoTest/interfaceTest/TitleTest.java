package com.webAutoTest.interfaceTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-22
 * Time:上午9:12
 */

public class TitleTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    void titleTest() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        String title1 = driver.getTitle();
        Assertions.assertEquals("登录页面", title1);
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        String title2 = driver.getTitle();
        Assertions.assertEquals("博客列表", title2);
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String title3 = driver.getTitle();
        Assertions.assertEquals("博客详情页", title3);
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        String title4 = driver.getTitle();
        Assertions.assertEquals("博客编辑页", title4);
        driver.quit();
    }
}
