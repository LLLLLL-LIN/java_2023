package com.webAutoTest.interfaceTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:下午4:11
 */
public class PageSizeTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    public void pageSizeTest() throws InterruptedException {
        //主页页面大小测试；
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.manage().window().maximize();
        driver.manage().window().minimize();
        driver.manage().window().setSize(new Dimension(1200, 2000));
        //博客列表页页面大小测试；
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        driver.manage().window().maximize();
        driver.manage().window().minimize();
        driver.manage().window().setSize(new Dimension(1300, 2100));
        //博客详情页面大小测试；
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        driver.manage().window().maximize();
        driver.manage().window().minimize();
        driver.manage().window().setSize(new Dimension(1400, 2200));
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        //编辑博客界面大小测试；
        driver.manage().window().maximize();
        driver.manage().window().minimize();
        driver.manage().window().setSize(new Dimension(1500, 2300));
        driver.quit();
    }
}
