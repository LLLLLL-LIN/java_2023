package com.webAutoTest.SafeTest;

import com.webAutoTest.common.CommonDriver;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-03-19
 * Time:下午4:27
 */
public class TryDeleteTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    //非作者删除博客测试；
    void tryDeleteTest() {
        //进入主页；
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        //找到非登录者的文章进行删除测试；
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(2) > a")).click();
        String str1 = driver.findElement(By.cssSelector("body > div.container > div.left > div > h3")).getText();
        Assertions.assertNotEquals("zhangsan", str1);
        String str2 = driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).getText();
        Assertions.assertNotEquals("删除", str2);
        driver.quit();
    }
}
