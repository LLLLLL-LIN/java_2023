package demo12;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-02-26
 * Time:18:01
 */

/**
 * 模拟实现阻塞队列;
 */
class MyBlockQueen{
    // 定义队列的头尾;
    private volatile int head = 0;
    private volatile int tail = 0;

    // 定义队列的大小(size);
    private volatile int size = 0;

    // 定义数组;
    public static String[] myBlockQueen;
    // 构造方法,自定义数组/队列的大小;
    public MyBlockQueen(int queenSize) {
        myBlockQueen = new String[queenSize];
    }

    // 队列的添加;
    public void put(String toJoin) throws InterruptedException {
        // 对当前对象加锁,防止多个线程利用当前对象同一时间调用 put 和 take;
        synchronized (this) {
            while(size == myBlockQueen.length) {
                // 证明队列满了;
                // 1.普通队列,直接return;
                // return;

                // 2.阻塞队列的做法是,在这直接阻塞;
                this.wait();
            }
            // 队列没满,放置数据;
            myBlockQueen[tail] = toJoin;
            tail++;
            // tail 本来就是队尾的那个,但是tail的值还是队列的长度 - 1;
            // 自增之后就有可能等于队列的长度,因为是是从0开始的,数组下标只有 lenth - 1;
            if(tail == myBlockQueen.length) {
                tail = 0;
            }
            size++;
            // 列表为空进行wait之后,这里进行了put,就把take中的wait阻塞唤醒;
            this.notify();
        }
    }

    //  队列的获取/删除;
    public String take() throws InterruptedException {
        // 加锁/针对同一实例对象加锁,防止同时调用 put 和 take;
        synchronized (this) {
            while(size == 0) {
                // 证明队列为空了;
                // 1.普通队列,直接 return ;
                // return null;
                // 2.如果队列为空了,此时进行了take(),就需要进行阻塞;
                this.wait();
            }
            // 队列不为空,直接弹出 head 位置的元素;
            String delHead = myBlockQueen[head];
            myBlockQueen[head] = null;
            head++;
            // head如果是末尾的话,直接就让他到 0 开始;
            if (head == myBlockQueen.length) {
                head = 0;
            }
            // 出队列,队列大小 --;
            size--;
            // 唤醒线程;
            this.notify();
            // 返回 take 的值;
            return delHead;
        }
    }

    // 打印队列;
    public void printBlockQueen() {
        // 循环打印队列;
        for (String toPrint : myBlockQueen) {
            if(true) {
                System.out.print(toPrint + "  ");
            }
        }
        // 格式;
        System.out.println();
    }

}

public class LinkBlockQueen {
    public static void main(String[] args) throws InterruptedException {
        MyBlockQueen myQueen = new MyBlockQueen(6);
        myQueen.put("111");
        myQueen.put("222");
        myQueen.put("333");
        myQueen.put("444");
        myQueen.put("555");
        myQueen.put("666");
        myQueen.printBlockQueen();

        System.out.println("delete");
        System.out.print(myQueen.take() + "  ");
        System.out.print(myQueen.take() + "  ");
        System.out.print(myQueen.take() + "  ");
        System.out.print(myQueen.take() + "  ");
        System.out.print(myQueen.take() + "  ");
        System.out.print(myQueen.take() + "  ");

//        System.out.println("打印队列");
//        myQueen.printBlockQueen();
    }
}
