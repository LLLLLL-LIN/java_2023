package demo7;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-19
 * Time:19:29
 */

// 线程中断的判定位,采用 Thread 自身有的属性;
public class InterruptThread {
    public static void main(String[] args) {
        Thread t = new Thread(() ->{
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // 这里的线程被唤醒会报异常,他直接就自己处理异常,把标志位重新改回来,所以这里的线程打断之后还会继续执行;
                    e.printStackTrace();
                    // 所以这里采用打断之后,不让他继续修改标志位,采用直接 break;
                    // 同时也可以在标志位之前进行一些逻辑处理;
                    break;
                }
                System.out.println("线程工作中......");
            }
            System.out.println("线程结束工作!");
        });

        t.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("设置标志位为 ture,中断线程 t");
        t.interrupt();
    }
}
