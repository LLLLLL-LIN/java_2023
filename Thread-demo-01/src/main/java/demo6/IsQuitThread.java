package demo6;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-19
 * Time:11:03
 */

// 线程的中断/终止;
public class IsQuitThread {
//    private static Boolean isQuit = false;
    public static void main(String[] args) {
        // lambda表达式的变量捕获规则,也就是说 lambda 表达式中的 isQuit 变量其实是一个捕获上一层作用域的复制体,
        // 这就要求这个被复制的本体要么是显示就是 final 的,要么就是后续没有修改操作,看作是 final 的;
        boolean isQuit = false;

        Thread t = new Thread(() -> {
            while ( !isQuit) {
                System.out.println("线程执行中!......");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程工作结束!");
        });

        t.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("isQuit 被设置为 true,即将中断线程!");
//        isQuit = true;
    }
}
