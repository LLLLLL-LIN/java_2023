package demo17;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-03
 * Time:14:16
 */
// 采用CAS的方式对于同一变量进行自增操作;
public class CASdemo {
    // 定义一个变量用来自增;
    // 这个变量是 AtomicInteger 类型的,jvm对于系统提供的CAS的指令的封装;
    // 主要作用就是在特定场景下的对于某些数据的操作不需要进行加锁了,因为这个CAS的cpu指令就是原子的;
    // 括号的的 initialValue 参数指的是构造参数,用来构造这个类的全局变量 value,不是 oldVale!!!
    private static AtomicInteger count = new AtomicInteger(0);
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                // count++;
                count.getAndIncrement();
//                // ++count;
//                count.incrementAndGet();
//                // count--;
//                count.getAndDecrement();
//                // --count;
//                count.decrementAndGet();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                // count++;
                count.getAndIncrement();
//                // ++count;
//                count.incrementAndGet();
//                // count--;
//                count.getAndDecrement();
//                // --count;
//                count.decrementAndGet();
            }
        });

        // 启动线程;
        t1.start();
        t2.start();

        // 等待两个线程执行结束;
        t1.join();
        t2.join();

        // 这里的打印数据要采取get();
        System.out.println("count: " + count.get());
    }
}
