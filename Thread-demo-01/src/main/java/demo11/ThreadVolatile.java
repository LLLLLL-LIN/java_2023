package demo11;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-02-25
 * Time:21:12
 */
// 内存可见性问题
// (本质上是jvm对于大数量读取某个变量但是这个变量一直没有发生变化,jvm就自作主张进行优化,不读取内存中的值了,直接读取和该线程相关的寄存器中的值);
// 采用 volatile 解决;
public class ThreadVolatile {
    private static volatile int isQuit = 1;

    public static void main(String[] args) {
        System.out.println("进入循环...");
        Thread t1 = new Thread(() ->{
            while(isQuit == 1) {

            }
            System.out.println("t1线程退出...");
        });
        t1.start();

        Thread t2 = new Thread(() -> {
            System.out.println("请输入isQuit: ");
            while(true) {
                Scanner scanner = new Scanner(System.in);
                isQuit = scanner.nextInt();
            }
        });
        t2.start();
    }
}
