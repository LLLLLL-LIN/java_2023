package demo18;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-05
 * Time:17:45
 */
// 用Callable来定义任务,让线程来执行,但是这个Callable类型的任务还要包装一下,包装成FutureTask类型;
public class CallableThread {
    public static void main(String[] args) {
        // 用Callable的匿名内部类的形式来定义任务,这个任务是有返回值的,返回值的类型这里是Integer;
        Callable<Integer> myCallble = new Callable<Integer>() {
            int sum = 0;
            @Override
            public Integer call() throws Exception {
                for (int i = 1; i <= 100; i++) {
                    sum +=i;
                }
                return sum;
            }
        };

        // 定义线程来执行任务;
        // 1.这里的线程的构造方法中没有Callable类型参数的构造方法;
        // 所以采用FutureTask类型来包装Callable类型的任务;
        FutureTask myFutureTask = new FutureTask<>(myCallble);
        // 2.线程执行任务,这个任务的类型转变成FutureTask才行;
        Thread t = new Thread(myFutureTask);

        // 线程执行;
        t.start();

        // 打印;
        try {
            // 这里的打印需要时用.get()才能获取;
            // 这里的get(),在任务还没有返回他的返回值的时候就会阻塞等待;
            System.out.println(myFutureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
