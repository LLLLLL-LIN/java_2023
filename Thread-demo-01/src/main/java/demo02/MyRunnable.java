package demo02;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-18
 * Time:10:31
 */

// 2.实现函数式接口 Runnable ,new 实现类的实例,作为 new Thread() 的构造参数;
class MyRunnable implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello Runnable!");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
