package demo02;


public class RunnableThread {
    public static void main(String[] args) throws InterruptedException {
        Runnable runnable = new MyRunnable();
        Thread t = new Thread(runnable);
        t.start();
        while(true) {
            System.out.println("hello main!");
            Thread.sleep(3000);
        }
    }
}
