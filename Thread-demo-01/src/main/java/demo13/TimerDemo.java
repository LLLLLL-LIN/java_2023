package demo13;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-02-29
 * Time:14:25
 */
// 定时器(Timer);
public class TimerDemo {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(3000);
            }
        },3000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(2000);
            }
        },2000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(1000);
            }
        },1000);
        System.out.println("程序启动了!...");
    }
}
