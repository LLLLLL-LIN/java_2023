package demo9;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-02-25
 * Time:16:16
 */
// 线程状态;
public class ThreadState {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() ->{

        });
        System.out.println("线程状态:");
        // new;
        System.out.println(t.getState());
        t.start();
        t.join();
        // terminated;
        System.out.println(t.getState());
        //
        t.sleep(1000);
        System.out.println(t.getState());
    }
}
