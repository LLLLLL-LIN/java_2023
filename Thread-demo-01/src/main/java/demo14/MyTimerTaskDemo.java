package demo14;

import java.util.PriorityQueue;
import java.util.concurrent.ThreadFactory;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-02
 * Time:10:23
 */

// 用来描述任务,包括执行的是什么任务(Runnable)以及任务要在哪个时间执行(delay);
class MyTimerTask implements Comparable<MyTimerTask> {
    private Runnable runnable;
    private long time;

    // 自定义任务单的构造方法;
    public MyTimerTask(Runnable runnable, long delay) {
        this.runnable = runnable;
        // 不记录延迟多少秒执行,记录的是当前时间加上延迟时间作为执行时间;
        // 这样为了在优先级队列中存储之后,后续的peek就更加方便,直接和当前时间进行比较就行;
        this.time = System.currentTimeMillis() + delay;
    }

    // 重写比较方法;
    @Override
    public int compareTo(MyTimerTask o) {
        return (int) (this.time - o.time);
    }

    // time 的 get方法;
    public long getTime() {
        return time;
    }

    // runnable的get方法;

    public Runnable getRunnable() {
        return runnable;
    }
}

// 定时器(定时任务存储器);
// 用来存储/扫描/执行定时任务;
class MyTimer {
    // 优先级队列,用来存储任务;
    PriorityQueue<MyTimerTask> myTimerQueue = new PriorityQueue<>();
    // 锁对象,用来给 schedule() 和扫描线程的读取操作进行互斥锁;
    Object locker = new Object();

    // 添加任务;
    public void schedule(Runnable runnable, long delay) {
        // 实例对象调用这个方法,也就是主线程调用的时候,对写入方法进行加锁;
        synchronized (locker) {
            myTimerQueue.offer(new MyTimerTask(runnable, delay));
            // 新增了任务,唤醒阻塞的线程;
            locker.notify();
        }
    }

    // 扫描线程,一个就够了,因为一个实例对象就一个任务队列就够了;
    // 所以把这个扫描线程放在构造方法里面;
    public MyTimer() {
        Thread t1 = new Thread(() -> {
            // 循环扫描,比较当前时间和对首元素的time;
            while (true) {
                // 捕获线程进行wait()的打断异常;
                try {
                    // t1线程读取的时候进行加锁;
                    synchronized (locker) {
                        // 任务队列为空的情况下;
                        // 这里要采用while进行判断,防止线程不是被schedule()中的notify正常唤醒的(非正常唤醒,队列其实还是为空);
                        // 就需要进行再次判断队列是否为空,不为空,直接就跳出循环了;
                        while (myTimerQueue.isEmpty()) {
                            // 为空,进行wait();
                            locker.wait();
                        }
                        // 获取对首任务;
                        MyTimerTask myTimerTask = myTimerQueue.peek();
                        // 进行对队列的对手元素的判断;
                        if (System.currentTimeMillis() >= myTimerTask.getTime()) {
                            // 当前时间大于等于对首任务的 time,执行任务;
                            myTimerTask.getRunnable().run();
                            // 执行完任务,删除任务;
                            myTimerQueue.poll();
                        } else {
                            // 如果不写 else,等于就是这个线程一直循环;
                            // 直到对首元素的time到了,才会跳出循环,这个循环没有意义的,一直在忙等;
                            // 这里设置成等待时间差,中间的时间就让他阻塞,不要浪费cpu资源了;
                            locker.wait(myTimerTask.getTime() - System.currentTimeMillis());
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        t1.start();
    }

}

// 自定义实现定时任务;
public class MyTimerTaskDemo {
    public static void main(String[] args) {
        MyTimer myTimer = new MyTimer();
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println(2000);
            }
        },2000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println(3000);
            }
        },3000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println(1000);
            }
        },1000);
    }
}

