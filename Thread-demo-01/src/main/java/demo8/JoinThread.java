package demo8;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-19
 * Time:20:07
 */

// 线程等待;
public class JoinThread {
    public static void main(String[] args) {
        Thread t = new Thread(() ->{
            System.out.println("t线程开始工作!");
            for (int i = 0; i < 5; i++) {
                System.out.println("线程工作中 + " + i + "......");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("t线程结束工作!");
        });

        t.start();
        System.out.println("main线程开始等待t线程......");
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("t线程执行结束, main线程继续执行");
    }
}
