package demo01;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-18
 * Time:10:17
 */

// 1.继承自 Thread 类,重写其中的 run 方法, new 实现类的实例 t,通过 t.start() 实现多线程;
class MyThread extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello thread!");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
