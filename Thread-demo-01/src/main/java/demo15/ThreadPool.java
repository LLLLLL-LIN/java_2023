package demo15;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-02
 * Time:15:16
 */
public class ThreadPool {
    public static void main(String[] args) {
        // 1.能够缓存的线程池;
        // 能够对线程进行缓存的一个线程池;
        // 主要是能够动态地变化数目,任务多了,忙不过来,就多创建几个线程,
        // 任务执行完了,线程空出来了,也不会立即释放,先放在线程池中进行缓存,
        // 到了60秒之后,会移除,但是不会释放或者销毁,以防在线程不够的时候能够马上顶上;
        ExecutorService service1 = Executors.newCachedThreadPool();

        // 2.固定数量的线程池;
        // 这个工厂方法主要是用来创建固定数量线程的线程池;
        ExecutorService service2 = Executors.newFixedThreadPool(5);

        // 3.一个线程的线程池;
        // 该线程池只有一个线程;
        ExecutorService service3 = Executors.newSingleThreadExecutor();

        // 4.定时任务的线程池;
        // 用来执行一个可执行的task(任务),也就是一个可执行的Runnable,和timer.schedule()方法一样,但是参数不同;
//        ExecutorService service4 = Executors.newScheduledThreadPool(1);
        ScheduledExecutorService service4 = Executors.newScheduledThreadPool(1);

        service4.execute(() -> {
            System.out.println("code1!");
        });

        // 执行一个定时的任务,定时三秒后执行;
        service4.schedule(() -> System.out.println("code2!"),3, TimeUnit.SECONDS);

        //周期任务,延迟两秒之后执行,后续每隔三秒再执行;
        service4.scheduleAtFixedRate(() -> System.out.println("code3!"),2,3,TimeUnit.SECONDS);
    }
}
