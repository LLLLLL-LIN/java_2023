package demo16;

import java.util.ArrayDeque;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-02
 * Time:17:17
 */
// 自定义线程池;
public class MyThreadPool {

    // 定义阻塞队列,用来存储任务,让线程消费;
    private BlockingDeque<Runnable> blockingDeque = new LinkedBlockingDeque<>(1000);

    // 定义一个方法,用来提交任务到阻塞队列中;
    public void submit(Runnable runnable) throws InterruptedException {
        blockingDeque.put(runnable);
    }

    // 定义构造方法;
    // 根据 n 创建多个线程;
    public MyThreadPool(int n) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    // 让线程不断地去消费阻塞队列中的任务;
                    while (true) {
                        try {
                            // 从阻塞队列中取到任务(Runnable);
                            // 没有的话就会阻塞等待;
                            Runnable runnable = blockingDeque.take();
                            // 调用run方法;
                            runnable.run();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            // 启动线程;
            t.start();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThreadPool myThreadPool = new MyThreadPool(5);
        for (int i = 0; i < 1000; i++) {
            // 变量捕获规则,采用匿名内部类中定义i的复制体;
            int id = i;
            myThreadPool.submit(() -> {
                System.out.println(id);
            });
        }
    }
}
