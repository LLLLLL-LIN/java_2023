package demo20;

import java.util.concurrent.Semaphore;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-09
 * Time:12:06
 */
// 用信号量计数,来对线程可执行/可获取到的任务进行管理;
public class MySemaphoreDemo {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    // 申请资源,信号量-1,p操作;
                    semaphore.acquire();
                    // 等待 1s;
                    Thread.sleep(5000);
                    // 释放资源,信号量+1,v操作;
                    semaphore.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("信号量使用测试!");
            }
        };

        // 信号量只有三个,所以一次会打印三个;
        // 阻塞等待信号量的释放;
        for (int i = 0; i < 20; i++) {
            Thread t = new Thread(runnable);
            t.start();
        }
    }
}
