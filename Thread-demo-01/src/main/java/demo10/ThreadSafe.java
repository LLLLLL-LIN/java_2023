package demo10;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-02-25
 * Time:16:29
 */
// 线程安全实例;
public class ThreadSafe {
    private static int count;
    public static void main(String[] args) throws InterruptedException {

        // 随便定义一个对象,用来进行加锁的竞争;
        Object locker = new Object();

        // 创建两个线程,对同一个静态变量各自进行自增50000;
        Thread t1 = new Thread(() ->{
            for (int i = 0; i < 50000; i++) {
                // 加锁处理;
                synchronized (locker) {
                    count++;
                }
            }
        });
        Thread t2 = new Thread(() ->{
            for (int i = 0; i < 50000; i++) {
                // 加锁处理;
                synchronized (locker) {
                    count++;
                }
            }
        });
        //启动线程;
        t1.start();
        t2.start();

        //等待两个线程执行结束;
        t1.join();
        t2.join();

        // 打印查看总数是不是100000;
        System.out.println("count: " + count);
    }
}
