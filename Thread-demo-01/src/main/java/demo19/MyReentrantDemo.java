package demo19;

import demo16.MyThreadPool;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-06
 * Time:16:27
 */

/**
 * synchronized 的可重入锁机制;
 * 1首先得是同一个线程:
 *  1.1:只有同一个线程调用两个加锁的模块才会出现可重入的现象,但是不同的线程还是会出现锁竞争的情况;
 * 2.其次得是同一个锁对象;
 *  2.1:不管是加锁代码块,还是加锁方法,只要是同一锁对象就行;
 */
// sychronized对于方法的修饰,没有说明锁对象,默认是当前类的实例为锁对象;
public class MyReentrantDemo {
    public static void main(String[] args) {
        MyReentrantDemo myReentrantDemo = new MyReentrantDemo();
        // 用实例对象来调用方法;
        myReentrantDemo.method1();
        MyReentrantDemo myReentrantDemo1 = new MyReentrantDemo();
        myReentrantDemo1.method2();
    }

    // 方法1;
    public synchronized void method1() {
        System.out.println("执行方法1");
        // 调用方法2;
        System.out.println("方法1调用   方法2");
        method2();
    }

    // 方法2;
    // 很明显的发现,只要是同一实例调用的,方法2不会和方法1发生锁竞争现象;
    private synchronized void method2() {
        System.out.println("执行了方法2");
        System.out.println("方法2的打印!");
    }
}
