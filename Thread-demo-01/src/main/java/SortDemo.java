import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:LIaolin
 * Date:2024-02-29
 * Time:13:39
 */
public class SortDemo {
    public static void main(String[] args) {
        int[] array = {3,2,5,1,4};
        int[] array2 = bubbleSort(array);
        System.out.println(Arrays.toString(array2));
    }
    public static int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if(array[j] < array[j + 1] ) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        return array;
    }
}
