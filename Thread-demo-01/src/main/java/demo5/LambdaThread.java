package demo5;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-01-18
 * Time:11:31
 */
public class LambdaThread {
    public static void main(String[] args) {
        Thread t = new Thread(() ->{
            while (true) {
                System.out.println("Hello Thread!");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        while (true) {
            System.out.println("hello main!");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
