import com.bit.component.ArticleCotroller;
import com.bit.component.StudentTest1;
import com.bit.controller.StudentController;
import com.bit.controller.StudentController2;
import com.bit.model.Student;
import com.bit.service.StudentService;
import com.bit.service.StudentService2;
import com.bit.test.Test1;
import com.bit.test.Test2;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-18
 * Time:18:36
 */
public class App {
    //    // main 方法中不能使用由 @Autowired 属性注入的对象, 会直接报空指针异常;
//    @Autowired
//    public static StudentController studentController;
    public static void main(String[] args) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        BeanFactory context = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        User user = (User)context.getBean("user");
//        System.out.println(user.sayHi());
//        ApplicationContext context1 = new ClassPathXmlApplicationContext("spring-config.xml");
//        // 根据 xml 中存储的 bean 对象中的 id 获取 bean 对象;
//        ArticleCotroller articleCotroller = (ArticleCotroller) context1.getBean("articleCotroller");
//        // 根据 xml 中存储的 bean 对象的 id 和类的名称获取 bean 对象;
//        ArticleCotroller articleCotroller1 = context1.getBean("articleCotroller",ArticleCotroller.class);
//        // 根据 xml 中存储的 '类的名称.class' 获取 bean 对象;
//        ArticleCotroller articleCotroller2 = context1.getBean(ArticleCotroller.class);
//        System.out.println(articleCotroller2.sayHello());
//        System.out.println(articleCotroller.sayHello());
//        System.out.println(articleCotroller1.sayHello());
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        // Bean 进行重命名之后,使用方法名作为参数无法获取 bean 对象;只能用重命名之后的名称来获取;
//        // 这里的方法名是不用小写的,和类的名称有点不一样,类的 Bean 对象的获取参数需要进行名称首字母小写处理;
//        Student student1 = context.getBean("StudentBeans",Student.class);
//        // Bean 重命名能够拥有多个名称;
//        Student student2 = context.getBean("lisi3",Student.class);
//        System.out.println(student1);
//        System.out.println(student2);
//        ApplicationContext context3 = new ClassPathXmlApplicationContext("spring-config.xml");
//        StudentController studentController = context3.getBean("studentController",StudentController.class);
//        System.out.println(studentController.studentController());
        // 这里为什么使用 ApplicationContext 来获取对象报错了,说嵌套过量;
//        ApplicationContext context4 = new ClassPathXmlApplicationContext("spring-config.xml");
//        BeanFactory context4 = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        StudentController studentController = context4.getBean("studentController",StudentController.class);
//        System.out.println(studentController.studentController());
//        StudentController2 studentController2 = context4.getBean("studentController2",StudentController2.class);
//        // set 注入有被修改的风险;
////        StudentService2 studentService3 = new StudentService2(1, "李四", 23);
//        studentController2.setStudentService2(new StudentService2(1, "李四", 23));
//        studentController2.say();
//        Test1 test1 = new Test2();
//        // 运行时多态的父类引用子类对象,引用的子类对象的属性还是父类的,只有子类重写的方法才会被父类调用;
//        System.out.println(test1.name1);
//        System.out.println(test1.name2);
//        System.out.println("-----------");
//        // 子类重写的方法才能够被调用;
//        test1.print1();
//        test1.print2();
//         Test1 test11 = new Test2();
//       ((Test2) test11).print3();
//        ApplicationContext context5 = new ClassPathXmlApplicationContext("spring-config.xml");
//        BeanFactory context5 = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        StudentController2 studentController3 = context5.getBean("studentController2",StudentController2.class);
//        studentController3.say();
    }
}


