package com.bit.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-23
 * Time:20:45
 */
@Service
public class StudentService2 {
    private int id = 1;
    private String name = "张三";
    private int age = 20;

    @Override
    public String toString() {
        return "StudentService2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
//    public StudentService2(int id, String name, int age){
//        this.id = id;
//        this.name = name;
//        this.age = age;
//    }
    public void print() {
        System.out.println("这是StudentService2");
    }
}
