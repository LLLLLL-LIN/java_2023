package com.bit.controller;

import com.bit.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-23
 * Time:16:50
 */
@Controller
public class StudentController {
//    @Autowired
//
//    private StudentService studentService;

//    private final StudentService studentService;
//
//    public StudentController(StudentService studentService) {
//        this.studentService = studentService;
//    }

    private StudentService studentService;
    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public String studentController(){
        return studentService.hiService();
    }
}
