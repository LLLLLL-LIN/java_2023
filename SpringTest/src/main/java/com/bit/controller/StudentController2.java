package com.bit.controller;

import com.bit.service.StudentService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-23
 * Time:20:47
 */
@Controller
public class StudentController2 {
    private final StudentService2 studentService2;

//    @Autowired
//    public void setStudentService2(StudentService2 studentService2) {
//        this.studentService2 = studentService2;
//    }

    public StudentController2(StudentService2 studentService2) {
        this.studentService2 = studentService2;
    }

    public void say(){
        System.out.println(studentService2);
    }
}
