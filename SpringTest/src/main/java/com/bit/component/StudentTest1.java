package com.bit.component;

import com.bit.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-22
 * Time:17:14
 */
@Component
public class StudentTest1 {
//    @Bean(name = "zhangsan1")
    @Bean
    public Student StudentBeans(){
        Student stu = new Student();
        stu.setId(1);
        stu.setAge(18);
        stu.setName("张三");
        return stu;
    }
}
