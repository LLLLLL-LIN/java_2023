package com.bit.component;

import com.bit.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-22
 * Time:17:16
 */
@Component
public class StudentTest2  {
    @Bean(name = {"lisi1","lisi2","lisi3"})
    public Student studentBeans() {
        Student stu = new Student();
        stu.setId(2);
        stu.setAge(20);
        stu.setName("李四");
        return stu;
    }
}
