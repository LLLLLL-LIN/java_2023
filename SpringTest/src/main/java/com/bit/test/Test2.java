package com.bit.test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-24
 * Time:17:50
 */
public class Test2 extends Test1{
    public String name1 = "子类";
    public static String name2 = "这是子类的静态属性";
    public void print1() {
        System.out.println("这是子类的打印方法");
    }
    public static void print2(){
        System.out.println("这是子类的静态方法");
    }
    public void print3(){
        System.out.println("这是子类新增的方法");
    }
}
