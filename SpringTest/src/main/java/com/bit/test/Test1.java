package com.bit.test;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:-08-24
 * Time:17:49
 */
public class Test1 {
    public String name1 = "父类";
    public static String name2 = "这是父类的静态属性";
    public void print1() {
        System.out.println("这是父类的打印方法");
    }
    public static void print2() {
        System.out.println("这是父类的静态方法");
    }
}
