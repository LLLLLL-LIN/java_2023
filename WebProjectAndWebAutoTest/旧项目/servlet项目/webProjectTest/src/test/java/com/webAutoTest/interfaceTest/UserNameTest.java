package com.webAutoTest.interfaceTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:下午1:52
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserNameTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    public void userNameTest() {
        //测试博客列表页显示的username是否是登录者；
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.left > div > h3")).getText();
        Assertions.assertEquals("zhangsan", text1);
        //测试博客全文页面是否显示作者username；
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        driver.findElement(By.cssSelector("#title")).sendKeys("博客作者的username！");
        driver.findElement(By.cssSelector("#submit")).click();
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String text2 = driver.findElement(By.cssSelector("body > div.container > div.left > div > h3")).getText();
        Assertions.assertEquals("zhangsan", text2);
        driver.quit();
    }
}
