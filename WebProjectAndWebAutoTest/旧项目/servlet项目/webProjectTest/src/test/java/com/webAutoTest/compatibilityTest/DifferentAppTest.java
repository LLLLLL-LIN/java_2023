package com.webAutoTest.compatibilityTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:下午3:33
 */

//FireFox浏览器兼容性测试；
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DifferentAppTest extends CommonDriver {
    static WebDriver driver = getFirefoxDriver();
    @Test
    @BeforeAll
    static void loginTest() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
    }

    @Test
    @Order(2)
    //编辑博客；
    public void editTest() {
        Assertions.assertEquals("写博客", driver.findElement(By.cssSelector(".nav > a:nth-child(5)")).getText());
        driver.findElement(By.cssSelector(".nav > a:nth-child(5)")).click();
        driver.findElement(By.cssSelector("#title")).sendKeys("文章标题!");
        Assertions.assertEquals("# 在这里写下一篇博客", driver.findElement(By.cssSelector("#editor > div.CodeMirror.cm-s-default.CodeMirror-wrap > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > div > pre > span > span")).getText());
    }
    @Test
    @Order(3)
    //提交博客；
    public void pulishTest() {
        driver.findElement(By.cssSelector("#submit")).click();
        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.title")).getText();
        Assertions.assertEquals("文章标题!", text1);
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String text2 = driver.findElement(By.cssSelector("body > div.container > div.right > div > h3")).getText();
        System.out.println(text2);
        Assertions.assertEquals("文章标题!", text2);
    }
    @Test
    @Order(4)
    //删除博客；
    public void deleteTest() {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(7)")).click();
//        String text1 = driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > div.title")).getText();
        driver.findElement(By.cssSelector("body > div.container > div.right > div:nth-child(1) > a")).click();
        String text = driver.findElement(By.cssSelector("body > div.container > div.right > div > h3")).getText();
        Assertions.assertNotEquals("文章标题!", text);
    }
    @Test
    @Order(5)
    //退出登录
    public void exitTest() {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String text = driver.findElement(By.cssSelector("body > div.login-container > form > div > h3")).getText();
        System.out.println(text);
        Assertions.assertEquals("登录", text);
    }
    @Test
    @Order(6)
    public void driverQuit() {
        driver.quit();
    }
}
