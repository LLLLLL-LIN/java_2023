package com.webAutoTest.functionTest;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-18
 * Time:下午11:01
 */
//@SelectPackages("com.webAutoTest.functionTest")
@Suite
@SelectClasses({FunctionsTest.class, LenthCheckTest.class, ListOrderTest.class, LoginTest.class, RunAllSuitTest.class, TryPublishTest.class, UnLoginTest.class})
public class RunAllSuitTest {

}
