package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-19
 * Time:上午12:28
 */

//测试登录名是否支持中英文字符；
public class LoginNameTest extends CommonDriver {
    static WebDriver driver = getEdgeDriver();

    @BeforeAll
    public static void login() {
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
    }
    //用户名支持中英文字符测试；
    @ParameterizedTest
    @ValueSource(strings = {"张三","zhangsa", "李四", "lisi"})
    public void loginTest(String name) {
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
//        System.out.println(name);
    }

    @Test
    @AfterAll
    public static void driverQuitTest() {
        driver.quit();
    }
}
