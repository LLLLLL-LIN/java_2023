package com.webAutoTest.functionTest;

import com.webAutoTest.common.CommonDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-22
 * Time:上午9:31
 */

//测试未填写必要参数是否能够发布文章；
public class TryPublishTest extends CommonDriver {
    WebDriver driver = getEdgeDriver();

    @Test
    void tryPublishTest() {
        //首页登录；
        driver.get("http://127.0.0.1:8080/webProject/blog_login.html");
        driver.findElement(By.cssSelector("#username")).sendKeys("zhangsan");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        //进入编辑页；
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)")).click();
        //未填写必要参数，发布博客；
        driver.findElement(By.cssSelector("#submit")).click();
        //检查页面信息，是否发布成功；
        String text = driver.findElement(By.cssSelector("body")).getText();
        Assertions.assertEquals("提交博客失败,缺少必要的参数!", text);
        driver.quit();
    }
}
