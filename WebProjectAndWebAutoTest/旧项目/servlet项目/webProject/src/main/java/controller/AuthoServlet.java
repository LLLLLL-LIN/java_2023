package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;
import model.UserDao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-03-10
 * Time:下午4:37
 */
@WebServlet("/authorInfo")
public class AuthoServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json; charset=utf8");
        String param = req.getParameter("blogId");
        if (param == null || "".equals(param)) {
            resp.getWriter().write("{ \"ok\":false,\"reason\":\"参数缺失!\"}");
            return;
        }
        //根据blogId在数据库中查找,找到blog对象 由此找到作者信息;
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(Integer.parseInt(param));
        if (blog == null) {
            resp.getWriter().write("{\"ok\":false,\"reason\":\"您要查找的博客不在!\"}");
            return;
        }
        //由blog对象查找用户对象;
        UserDao userDao = new UserDao();
        User author = userDao.selectById(blog.getUserId());
        if (author == null) {
            resp.getWriter().write("{\"ok\":false,\"reason\":\"您要查找的用户不在!\"}");
            return;
        }

        //返回author给浏览器;
        //删除密码;
        author.setPassword("");
        resp.getWriter().write(objectMapper.writeValueAsString(author));
    }
}
