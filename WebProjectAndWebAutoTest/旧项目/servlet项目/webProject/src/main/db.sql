create database if not exists webProject;

use webProject;
--博客表
drop table if exists blog;
create table blog(
     blogId int primary key auto_increment,
     title varchar(1024),
     content mediumtext,
     userId int,
     postTime datetime
);
insert into blog values(null, '这是第一篇博客','从今天开始,我要认真学习Java',1,now());
insert into blog values(null, '这是第二篇博客','从今天开始,我要认真学习Java',1,now());
insert into blog values(null, '这是第三篇博客','从今天开始,我要认真学习Java',1,now());
insert into blog values(null, '这是第一篇博客','从今天开始,我要认真学习C++',2,now());
insert into blog values(null, '这是第二篇博客','从今天开始,我要认真学习C++',2,now());
insert into blog values(null, '这是第三篇博客','# 一级标题\n ### 三级标题\n > 这是引用内容',2,now());

-- 用户表
drop table if exists user;
create table user(
    userId int primary key auto_increment,
    username varchar(128) unique,
    password varchar(128)
);
insert into user values(null, 'zhangsan', '123');
insert into user values(null, 'lisi', '123');
