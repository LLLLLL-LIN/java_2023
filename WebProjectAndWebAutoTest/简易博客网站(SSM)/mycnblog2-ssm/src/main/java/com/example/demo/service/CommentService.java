package com.example.demo.service;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.CommentVO;
import com.example.demo.mapper.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:03:14 */
@Service
public class CommentService {
    @Autowired
    private CommentMapper commentMapper;


    // 添加评论,根据文章 id, coment 对象, 当前登录对象 userinfo;
    public int insertCom(Integer id, String comment, Integer uid){
        return commentMapper.insertCom(id,comment,uid );
    }

    // 查找评论,用作前端渲染;
    public List<Comment> findCom(Integer id){
        return commentMapper.findCom(id);
    }

    // 根据 comment 的cid 找到 userinfo;
    public Userinfo selectUser(Integer uid){
        return commentMapper.selectUser(uid);
    }

    public int comdel(Integer cid) {
        return commentMapper.comdel(cid);
    }

    public Comment selectCom(Integer cid){
        return commentMapper.selectCom(cid);
    }
}
