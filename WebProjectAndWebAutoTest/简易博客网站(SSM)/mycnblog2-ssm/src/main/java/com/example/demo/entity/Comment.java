package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:03:10 */
@Data
public class Comment implements Serializable {
    private Integer id;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createtime;
    private Integer userid;
    private Integer artid;
}
