package com.example.demo.mapper;

import com.example.demo.entity.Total;
import org.apache.ibatis.annotations.Mapper;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:27 */
@Mapper
public interface TotalMapper {
    Total getTotal();
    Integer setTotal();
}
