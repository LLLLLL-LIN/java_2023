package com.example.demo.entity;

import lombok.Data;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:13:00 */
@Data
public class Captcha {
    private Integer id;
    private String code;
}
