//package com.example.demo.controller;
//
//import com.example.demo.common.AjaxResult;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:01:10 */
//@RestController
//@RequestMapping("/capt")
//public class CaptchaController {
//
//
//    @RequestMapping("/captcha")
//    public AjaxResult generateCaptcha() throws Exception {
//        // 生成随机验证码
//        String code = RandomStringUtis.randomAlphanumeric(4);
//        // 生成UUID作为验证码的唯一标识
//        String id = UUID.randomUUID().toString();
//        // 将验证码存入Map中
//        captchaMap.put(id, code);
//
//        Captcha captcha = new Captcha();
//        captcha.setId(id);
//        captcha.setCode(code);
//
//        return captcha;
//    }
//}
