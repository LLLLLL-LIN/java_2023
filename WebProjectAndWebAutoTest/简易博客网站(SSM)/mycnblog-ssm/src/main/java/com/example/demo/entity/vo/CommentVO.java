package com.example.demo.entity.vo;

import com.example.demo.entity.Comment;
import lombok.Data;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:04:01 */
@Data
public class CommentVO extends Comment {
    private String netname;
}
