package com.example.demo.entity;

import lombok.Data;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-16 Time:01:07 */
@Data
public class UseridSalt {
    private Integer userid;
    private String newsalt;
}
