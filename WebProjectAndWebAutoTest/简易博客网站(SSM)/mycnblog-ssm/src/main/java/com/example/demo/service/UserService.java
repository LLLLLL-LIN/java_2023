package com.example.demo.service;

import com.example.demo.entity.UseridSalt;
import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.jws.soap.SOAPBinding;

@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    @Transactional
    public int reg(Userinfo userinfo) {
        userMapper.reg(userinfo);
        return userMapper.selectUserId();
    }

    public Userinfo getUserByName(String username) {
        return userMapper.getUserByName(username);
    }

    public Userinfo getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    // 修改密码昵称
    public int changeNum(Userinfo userinfo) {
        return userMapper.changeNum(userinfo);
    }

    public Userinfo getbyid(Integer id){
        return userMapper.getbyid(id);
    }

    // 修改登录验证次数;
    public int upcounts(Integer id) {
        return userMapper.upcounts(id);
    }

    // 设置过多次数登录就设置状态码为 0 ;
    public int upUserStata(Integer id) {
        return userMapper.upUserState(id);
    }

    // 冻结的时间 和 登录次数 和 冻结状态 都设置为初始状态;
    public int upFreezeState(Integer id) {
        return userMapper.upFreezeState(id);
    }

    // 更新 userinfo 的 counts 为 0;
    public int upZereoCounts(Integer id) {
        return userMapper.upZereoCounts(id);
    }

    // 产生一个新的盐值来作为重置密码的凭证
    public int upNewSalt(UseridSalt useridSalt) {
        return userMapper.upNewSalt(useridSalt);
    }

    // 重置密码
    public int setPword(Userinfo userinfo) {
        return userMapper.setPword(userinfo);
    }
}
