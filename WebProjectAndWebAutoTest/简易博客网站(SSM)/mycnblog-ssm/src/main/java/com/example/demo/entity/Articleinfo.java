package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Articleinfo {
    private Integer id;
    private String title;
    private String content;
    // 在 .yml 中全局设置不生效,针对于 LocalDateTime/LocalDate 得单独设置;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createtime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updatetime;
    private Integer uid;
    // 浏览数
    private Integer rcount;
    private Integer state;
}
