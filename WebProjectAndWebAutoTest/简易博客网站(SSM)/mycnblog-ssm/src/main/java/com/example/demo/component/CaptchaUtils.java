package com.example.demo.component;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;



/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:01:05 */
public class CaptchaUtils {

    // 验证码的字符集（不包含易混淆字符）
    private static final String CODES = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";

    // 生成指定长度的验证码
    public static String generateCaptcha(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(CODES.length());
            sb.append(CODES.charAt(index));
        }
        return sb.toString();
    }

    // 创建验证码图片，并返回验证码文本
    public static BufferedImage createCaptchaImage(int width, int height, String captchaText) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();

        // 填充背景色
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);

        // 绘制干扰线
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int x1 = random.nextInt(width);
            int y1 = random.nextInt(height);
            int x2 = random.nextInt(width);
            int y2 = random.nextInt(height);
            g.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            g.drawLine(x1, y1, x2, y2);
        }

        // 绘制验证码文本
        g.setFont(new Font("Arial", Font.PLAIN, 30));
        for (int i = 0; i < captchaText.length(); i++) {
            int x = i * 30 + 10;
            int y = random.nextInt(10) + 40;
            g.setColor(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            g.drawString(String.valueOf(captchaText.charAt(i)), x, y);
        }

        // 绘制边框
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, width - 1, height - 1);

        return image;
    }

    // 校验用户输入的验证码是否正确
    public static boolean validateCaptcha(String inputCaptcha, String textCaptcha) {
        return textCaptcha != null && textCaptcha.equalsIgnoreCase(inputCaptcha);
    }
}

