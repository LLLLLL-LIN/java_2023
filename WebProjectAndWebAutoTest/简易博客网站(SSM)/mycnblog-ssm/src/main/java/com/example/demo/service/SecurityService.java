package com.example.demo.service;

import com.example.demo.entity.Security;
import com.example.demo.mapper.SecurityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-14 Time:22:44 */
@Service
public class SecurityService {
    @Autowired
    private SecurityMapper securityMapper;


    public int insertSecu(Security security) {
        return securityMapper.insertSecu(security);
    }

    public Security getSecuByUserid(Integer id) {
        return securityMapper.getSecuByUserid(id);
    }
}
