package com.example.demo.service;

import com.example.demo.entity.Total;
import com.example.demo.mapper.TotalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:28 */
@Service
public class TotalService {
    @Resource
    private TotalMapper totalMapper;
    public Total getTotal() {
        return totalMapper.getTotal();
    }
    public int setTotal() {
        return totalMapper.setTotal();
    }
}
