package com.example.demo.component;

import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:01:05 */
@Component
public class CaptchaUtil {
    // 字符集
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    // 验证码宽度
    private static final int WIDTH = 120;

    // 验证码高度
    private static final int HEIGHT = 40;

    // 验证码字符数量
    private static final int CHAR_COUNT = 4;

    // 生成验证码图片
    public static void generateCaptcha(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 创建画布
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();

        // 填充背景色
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        // 生成随机验证码
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < CHAR_COUNT; i++) {
            int index = new Random().nextInt(CHARACTERS.length());
            char c = CHARACTERS.charAt(index);
            sb.append(c);
        }
        String captcha = sb.toString();

        // 将验证码存储到Session中
        HttpSession session = request.getSession();
        session.setAttribute("captcha", captcha);

        // 绘制验证码文字
        g.setFont(new Font("Arial", Font.BOLD, 20));
        for (int i = 0; i < CHAR_COUNT; i++) {
            g.setColor(new Color(new Random().nextInt(256), new Random().nextInt(256), new Random().nextInt(256)));
            g.drawString(String.valueOf(captcha.charAt(i)), 20 * i + 10, HEIGHT / 2 + 5);
        }

        // 添加干扰线
        for (int i = 0; i < 10; i++) {
            g.setColor(new Color(new Random().nextInt(256), new Random().nextInt(256), new Random().nextInt(256)));
            g.drawLine(new Random().nextInt(WIDTH), new Random().nextInt(HEIGHT),
                    new Random().nextInt(WIDTH), new Random().nextInt(HEIGHT));
        }

        // 输出验证码图片
        response.setContentType("image/png");
        OutputStream os = response.getOutputStream();
        ImageIO.write(image, "png", os);
        os.flush();
        os.close();
    }

    // 验证用户输入的验证码是否正确
    public static boolean validateCaptcha(HttpServletRequest request, String userInputCaptcha) {
        HttpSession session = request.getSession();
        String captcha = (String) session.getAttribute("captcha");
        return captcha != null && captcha.equalsIgnoreCase(userInputCaptcha);
    }
}
