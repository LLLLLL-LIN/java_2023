//package com.example.demo.component;
//
//import com.example.demo.entity.Articleinfo;
//import com.example.demo.service.ArticleService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.util.List;
//
///** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:14:09 */
//@Component
//public class ScheduledPublishTask {
//
//    @Autowired
//    private ArticleService articleService;
//
//    @Scheduled(fixedDelay = 1000) // 每隔一秒触发一次
//    public void checkPublishTime() {
//        LocalDateTime currentDateTime = LocalDateTime.now();
//        List<Articleinfo> articlesToPublish = articleService.getArticlesToPublish(currentDateTime);
//
//        for (Articleinfo articleinfo : articlesToPublish) {
//            LocalDateTime publishTime = articleinfo.getPublishTime(); // 获取文章的发布时间
//            if (publishTime.isEqual(currentDateTime) || publishTime.isBefore(currentDateTime)) {
//                // 根据文章内容发布到博客网站上，这里可以使用你自己的发布方法或者调用第三方发布接口等方式
//                // ...
//                articleService.updatePublishStatus(articleinfo.setPublish()); // 修改文章发布状态
//            }
//        }
//    }
//}
