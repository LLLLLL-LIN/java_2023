package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
/**
*Created with IntelliJ IDEA
*Description
*User:
*Date:-09-16
*Time:23:37
*/
@SpringBootTest
class ArticleMapperTest {
    @Autowired
    private ArticleMapper articleMapper;
    @Test
    void getDetail() {
        Articleinfo articleinfo = articleMapper.getDetail(6);
        System.out.println(articleinfo.getContent());
      }
}