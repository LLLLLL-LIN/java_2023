package com.example.demo.mapper;

import com.example.demo.entity.Captcha;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-14 Time:19:44 */
@Mapper
public interface CaptchaMapper {
    // 添加验证码;
    int insertCapt(Captcha captcha);

    // 获得新添加的验证码的 id;
    int selectCaptId();

    Captcha selectCaptcha(@Param("id") Integer id);
}
