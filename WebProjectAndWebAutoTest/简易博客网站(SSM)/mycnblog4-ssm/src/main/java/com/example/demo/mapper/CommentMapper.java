package com.example.demo.mapper;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.CommentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:03:14 */
@Mapper
public interface CommentMapper {

    // 添加评论 comment;
    int insertCom(@Param("aid") Integer id, @Param("comment") String comment, @Param("uid") Integer uid);

    // 查找评论 commentVOList
    List<Comment> findCom(@Param("aid") Integer id);

    Userinfo selectUser(@Param("uid") Integer uid);

    int comdel(@Param("cid") Integer cid);

    Comment selectCom(@Param("cid") Integer cid);
}
