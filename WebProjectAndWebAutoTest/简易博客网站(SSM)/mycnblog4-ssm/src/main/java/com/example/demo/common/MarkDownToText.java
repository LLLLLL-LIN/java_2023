package com.example.demo.common;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.text.TextContentRenderer;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-09-16 Time:22:46 */

/**
 * markdown 转换成纯文本工具类
 */
public class MarkDownToText {
    public static String convertToPlainText(String markdownText) {
        // 创建Markdown解析器
        Parser parser = Parser.builder().build();

        // 解析Markdown文本
        Node document = parser.parse(markdownText);

        // 创建纯文本渲染器
        TextContentRenderer renderer = TextContentRenderer.builder().build();

        // 测试;
//        System.out.println("调用了 MarkDown 中的 convertoPlainText() 方法");

        // 渲染为纯文本
        return renderer.render(document);
    }
}
