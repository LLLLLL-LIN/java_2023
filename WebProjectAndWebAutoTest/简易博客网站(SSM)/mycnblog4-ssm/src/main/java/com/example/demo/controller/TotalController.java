package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.service.TotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:30 */
@RestController
@RequestMapping("/total")
public class TotalController {
    @Autowired
    private TotalService totalService;
    @RequestMapping("/gettotal")
    public AjaxResult getTotal() {
    System.out.println(AjaxResult.success(totalService.getTotal()));
        totalService.setTotal();
        return AjaxResult.success(totalService.getTotal());
    }
}

