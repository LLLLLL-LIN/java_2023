package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.component.CaptchaUtils;
import com.example.demo.entity.Captcha;
import com.example.demo.mapper.CaptchaMapper;
import com.example.demo.service.CaptchaService;
import jdk.nashorn.internal.ir.CatchNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:01:10 */
@RestController
@RequestMapping("/capt")
public class CaptchaController {
    @Autowired
    private CaptchaService captchaService;


    @RequestMapping("/captcha")
    public AjaxResult generateCaptcha() throws IOException {
        // 得到一个随机的验证码字符串;
        String textString = CaptchaUtils.generateCaptcha(4);
        Captcha captcha = new Captcha();
        captcha.setCaptcha(textString);
        // 生成验证码图片
        BufferedImage image =  CaptchaUtils.createCaptchaImage(130,60,textString);
        // 将验证码图片转换为 Base64 编码的字符串
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "PNG", baos);
        byte[] imageData = baos.toByteArray();
        String base64Image = Base64.getEncoder().encodeToString(imageData);
        captcha.setBase64Image(base64Image);
        Integer id = captchaService.insertAndGetId(captcha);
        captcha.setId(id);
        System.out.println(id);
        return AjaxResult.success(captcha);
    }


}

