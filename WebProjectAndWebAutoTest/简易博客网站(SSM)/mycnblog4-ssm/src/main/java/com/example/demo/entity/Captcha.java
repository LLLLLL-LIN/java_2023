package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-12 Time:13:00 */
@Data
public class Captcha {
    private int id;
    private String captcha;
    private String base64Image;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createtime;
}

