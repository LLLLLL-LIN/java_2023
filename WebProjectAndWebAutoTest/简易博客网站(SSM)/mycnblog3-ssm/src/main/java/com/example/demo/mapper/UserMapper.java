package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    // 注册
    int reg(Userinfo userinfo);

    // 根据用户查询 userinfo 对象
    Userinfo getUserByName(@Param("username") String username);

    // 根据id查找用户
    Userinfo getUserById(@Param("id") Integer id);

    // 修改用户昵称,密码
    int changeNum(Userinfo userinfo);

    Userinfo getbyid(@Param("id") Integer id);

    // 更新登录次数
    int upcounts(@Param("id") Integer id);

    // 设置过多次数登录就设置状态码为 0 ;
    int upUserState(@Param("id") Integer id);

    // state,feezetime,counts 初始化;
    int upFreezeState(@Param("id") Integer id);

    // 更新 userinfo 的 counts 为 0;
    int upZereoCounts(@Param("id") Integer id);
}
