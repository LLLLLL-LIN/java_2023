package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {
    int getArtCountByUid(@Param("uid") Integer uid);

    List<Articleinfo> getMyList(@Param("uid") Integer uid);

    int del(@Param("id") Integer id, @Param("uid") Integer uid);

    Articleinfo getDetail(@Param("id") Integer id);

    int incrRCount(@Param("id") Integer id);

    int add(Articleinfo articleinfo);

    int update(Articleinfo articleinfo);

    // 博客列表页的分页;
    List<Articleinfo> getListByPage(@Param("psize") Integer psize,
                                    @Param("offsize") Integer offsize);

    int getCount();

    // 用户博客列表页的分页;
    List<Articleinfo> getMyListByPage2(
            @Param("id") Integer id,
            @Param("psize") Integer psize,
            @Param("offsize") Integer offsize);

    int getCount2(@Param("id") Integer id);






//    以下是草稿箱博客的操作方法
    int blogSave(Articleinfo articleinfo);

    int getCount3(@Param("id") Integer id);

    List<Articleinfo> getMyListByPage3(@Param("id") Integer id,
                                       @Param("psize") Integer psize,
                                       @Param("offsize") Integer offsize);

    Articleinfo getDetail3(@Param("id") Integer id);

    int incrRCount3(@Param("id") Integer id);

    int update3(Articleinfo articleinfo);

}
