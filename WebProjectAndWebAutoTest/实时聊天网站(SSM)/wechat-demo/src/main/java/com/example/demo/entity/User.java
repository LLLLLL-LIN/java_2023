package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:11:16
 */
@Data
public class User {
    private Integer uid;
    private String netname; // 昵称/用户名;
    private String username; // 账号;
    private String password; // 密码;
    private String email; // 邮箱 (待用);
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime regtime; // 注册时间;
    private LocalDateTime checktime; // 注册时间;
    private MultipartFile avatarUrl; // 头像(待用);
    private Integer status; // 1 为在线,0 为离线;
}
