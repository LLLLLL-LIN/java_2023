package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:15:29
 */
@Mapper
public interface UserMapper {
    // 添加 User;
    Integer userAdd(User user);

    // 根据 username 查找 User;
    User selectByuname(User user);

    // 根据 netname 查找 User;
    List<User> selectBynname(@Param("netname") String netname);

    // 根据 uid 查找 User;
    User selectByUid(@Param("uid") Integer uid);

    Integer checkTime(@Param("uid") Integer uid);

    List<User> selectAll();

    Integer upStatus(@Param("uid") Integer uid);

    Integer upStatus1(@Param("uid") Integer uid);
}
