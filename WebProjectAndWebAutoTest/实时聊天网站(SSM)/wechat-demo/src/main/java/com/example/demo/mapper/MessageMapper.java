package com.example.demo.mapper;

import com.example.demo.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:22:07
 */
@Mapper
public interface MessageMapper {
    List<Message> findMessage(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    Integer msgAdd(Message message);



}
