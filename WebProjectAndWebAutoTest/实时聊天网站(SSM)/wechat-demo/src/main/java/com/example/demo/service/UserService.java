package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaoin
 * Date:2023-11-08
 * Time:15:29
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public Integer addUser(User user){
        return userMapper.userAdd(user);
    }

    public User selectByuname(User user) {
        return userMapper.selectByuname(user);
    }

    public List<User> selectBynname(String netname) {
        return userMapper.selectBynname(netname);
    }

    public User selectByUid(Integer uid) {
        return userMapper.selectByUid(uid);
    }

    public Integer checkTime(Integer uid) {
        return userMapper.checkTime(uid);
    }

    public List<User> selectAll() {
        return userMapper.selectAll();
    }

    public Integer upStatus(Integer uid) {
        return userMapper.upStatus(uid);
    }

    public Integer upStatus1(Integer uid) {
        return userMapper.upStatus1(uid);
    }



}
