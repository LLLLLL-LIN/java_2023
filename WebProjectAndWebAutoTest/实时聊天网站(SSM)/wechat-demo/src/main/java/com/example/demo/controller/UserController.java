package com.example.demo.controller;


import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.component.IsOnline;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-08
 * Time:15:26
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private IsOnline isOnline;

    // 注册模块;
    @RequestMapping("/reg")
    public AjaxResult addUser(User user) {
        // 加盐加密;
        user.setPassword(PasswordUtils.encrypt(user.getPassword()));
        return AjaxResult.success(userService.addUser(user));
    }

    // 登录模块;
    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, User user) {
        User user1 = userService.selectByuname(user);
        if (user1 == null) {
            return AjaxResult.fail(-1, "用户名或密码错误");
        }

        // 这个是封装的验证密码的方法,只需要数据库的加盐后的密码和接口参数的密码;
        // 数据库密码附带盐值,封装了取盐值然后加盐再匹配的方法;
        if (PasswordUtils.check(user.getPassword(), user1.getPassword())) {
            // 密码匹配正确,建立会话;
            HttpSession session = request.getSession();
            session.setAttribute(AppVariable.USER_SESSION_KEY, user1);
//            System.out.println("当前登录用户是: " + session.getAttribute(AppVariable.USER_SESSION_KEY));

            // 设置最近一次在线的时间;
            userService.checkTime(user1.getUid());
            // 主要是登录了就在线状态 status 设置为 1;
            userService.upStatus1(user1.getUid());
            // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
            isOnline.isOnline();

            return AjaxResult.success(200, "密码正确");
        }
        return AjaxResult.success(0, null);
    }

    // 根据 netname 查找 User;
    @RequestMapping("/fnname")
    public AjaxResult findBynname(HttpServletRequest request, String netname) {
        User user1 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 更新当前用户的在线状态 status 为1;
        userService.upStatus1(user1.getUid());
        // 判断所有用户的最近一次的在线时间,超过五分钟,置为离线;
        isOnline.isOnline();

        if (netname == null) {
            return AjaxResult.fail(-5, "传输参数为空");
        }
        // 根据用户名查找用户,采用模糊匹配 like 采用链表;
        List<User> list = userService.selectBynname(netname);
        if (list.size() == 0) {
            return AjaxResult.success(-1,"没有匹配的用户!");
        }
        // 循环置空密码;
        for (User user : list) {
            user.setPassword("");
        }

        return AjaxResult.success(list);
    }

    // 根据 username 查找 User;
    @RequestMapping("/funame")
    public AjaxResult findByuname(HttpServletRequest request, String username) {
        User user3 = UserSessionUtils.getUser(request);
        if(user3 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }
        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user3.getUid());
        // 更新当前用户的在线状态 status 为1;
        userService.upStatus1(user3.getUid());
        // 判断所有用户的最近一次的在线时间,超过五分钟,置为离线;
        isOnline.isOnline();

        // 参数为空,直接返回;
        if (username == null) {
            return AjaxResult.fail(-5, "传输内容为空");
        }
        User user = new User();
        user.setUsername(username);
        // 查找匹配 username 参数的用户;
        User user1 = userService.selectByuname(user);
        if(user1 == null) {
            return AjaxResult.success(-1,"没有该账号!");
        }
        user1.setPassword("");
        return AjaxResult.success(user1);
    }


    @RequestMapping("logout")
    public AjaxResult logout(HttpSession session) {
        // 注销,会话置空;
        session.removeAttribute(AppVariable.USER_SESSION_KEY);
        return AjaxResult.success(1);
    }

    @RequestMapping("check")
    public AjaxResult check(HttpSession session) {
        return AjaxResult.success(1);
    }
}
