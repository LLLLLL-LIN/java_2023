package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.component.IsOnline;
import com.example.demo.entity.Friendship;
import com.example.demo.entity.MsgCount;
import com.example.demo.entity.User;
import com.example.demo.entity.VO.UserVO;
import com.example.demo.service.FriendshipService;
import com.example.demo.service.MsgCountService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:00:32
 */
@RestController
@RequestMapping("/friendship")
public class FriendshipController {
    @Autowired
    private FriendshipService friendshipService;
    @Autowired
    private UserService userService;
    @Autowired
    private MsgCountService msgCountService;
    @Autowired
    private IsOnline isOnline;

    @RequestMapping("/contact")
    public AjaxResult contact(HttpServletRequest request, String username) {
        User user = new User();
        user.setUsername(username);
        User user1 = userService.selectByuname(user);
        User user2 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前添加用户失败!");
        }
        if(user2 == null) {
            return AjaxResult.fail(-6,"获取当前用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user2.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user2.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        List<Friendship> friendshipList = friendshipService.findFriends(user2.getUid());
        for (Friendship friendship : friendshipList) {
            // 拿到非当前登录用户的 uid 去获取用户;
            User user3 = userService.selectByUid(friendship.getU1id().equals(user2.getUid())?
                    friendship.getU2id():friendship.getU1id());
            // 如果二者的 username 一样,说明你已经添加过这个用户了;
            if(user3.getUsername().equals(username)) {
                return AjaxResult.success(-2,"您已经添加过当前用户!");
            }
        }
        // 通过好友关系的 u1id 和 u2id 对未读关系表插入条例;
        Friendship friendship = new Friendship();
        friendship.setU1id(user2.getUid());
        friendship.setU2id(user1.getUid());
        // 简历好友关系就有两张未读消息表,双方针对对方各自有一张;
        msgCountService.countadd(user2.getUid(),user1.getUid());
        msgCountService.countadd(user1.getUid(),user2.getUid());
        return AjaxResult.success(friendshipService.contact(friendship));
    }

    @RequestMapping("/friends")
    public AjaxResult findFriends(HttpServletRequest request) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 根据档期登录用户的 id 拉取好友列表;
        List<Friendship> flist = friendshipService.findFriends(user.getUid());
        if(flist == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        List<User> list = new ArrayList<>(1000);
        for (int i = 0; i < flist.size(); i++) {
            // 针对要查找的用户的 uid 三元运算,取非当前登录用户的 uid;
            User user1 = userService.selectByUid(
                    flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            user1.setPassword("");
            list.add(user1);
        }
        return AjaxResult.success(list);
    }

    @RequestMapping("/fdel")
    public AjaxResult fdel(HttpServletRequest request, String username) {
        if(username == null) {
            return AjaxResult.fail(-5,"传输的参数为空!");
        }
        User user1 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user1.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user1.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 因为按照 username 查询用户只有一个 User 类型参数的方法;
        // 这里就 new 对象,set username;
        User user = new User();
        user.setUsername(username);
        // 获取到要删除的好友用户;
        User user2 = userService.selectByuname(user);
        if(user2 == null) {
            return AjaxResult.fail(-7,"获取用户失败");
        }
        // 删除好友的同时也要删除未读消息的连接;
        msgCountService.delMscount(user1.getUid(), user2.getUid());
        msgCountService.delMscount(user2.getUid(), user1.getUid());
        return AjaxResult.success(friendshipService.fdel(user1.getUid(), user2.getUid()));
    }

    @RequestMapping("/myfriends")
    public AjaxResult myfriends(HttpServletRequest request) {
        User user = UserSessionUtils.getUser(request);
        if(user == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }

        // 将当前用户的最近一次在线时间置为now();
        userService.checkTime(user.getUid());
        // 主要是登录了就在线状态 status 设置为 1;
        userService.upStatus1(user.getUid());
        // 距离最近一次在线时间超过了五分钟就将在线状态 status 设置为 0;
        isOnline.isOnline();

        // 获取用户关系表,取 u1id 和 u2id 作为参数查询未读消息表的未读消息数目;
        List<Friendship> flist = friendshipService.findFriends(user.getUid());
        if(flist == null) {
            return AjaxResult.success(-1,"没有好友");
        }
        List<UserVO> list = new ArrayList<>(1000);
        for (int i = 0; i < flist.size(); i++) {
            User user1 = userService.selectByUid(
                    // 这里数据设计稍微有带你不合适,好友是双方的关系,所以无法确定我们的 uid 就是 u1id 还是 u2id;
                    // 这里进行判断,如果 u2id 是当前用户自己,就用 u1id;
                    flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            // 这个 VO 对象新增了 msgcount 属性, 来记录未读消息数量;
            UserVO userVO = new UserVO();
            // 查找未读消息表;
            // 这里也需要拿出好友的 uid,但是好友关系是不确定 uid 的前后顺序的,所以三元运算得到非当前的登录用户的 id;
            MsgCount msgCount = msgCountService.findCount(user.getUid()
                    ,flist.get(i).getU2id().equals(user.getUid())? flist.get(i).getU1id():flist.get(i).getU2id());
            if(msgCount == null) {
                return null;
            }
            userVO.setMsgcount(msgCount.getMsgcount());
            // 对象深克隆;
            user1.setPassword("");
            BeanUtils.copyProperties(user1, userVO);
            user1.setPassword("");
            list.add(userVO);
        }
        return AjaxResult.success(list);
    }
}
