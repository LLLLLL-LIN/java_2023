package com.example.demo.service;

import com.example.demo.entity.Message;
import com.example.demo.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:22:14
 */
@Service
public class MessageService {
    @Autowired
    private MessageMapper messageMapper;

    public List<Message> findMessage(Integer u1id, Integer u2id) {
        return messageMapper.findMessage(u1id,u2id);
    }

    public int msgAdd(Message message) {
        return messageMapper.msgAdd(message);
    }




}
