package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:15:36
 */
@Data
public class MsgCount {
    private Integer cid;
    private Integer u1id; // receiver;
    private Integer u2id; // sender;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime sendtime; // 发送时间;
    private Integer msgcount; // 未读消息;
}
