package com.example.demo.service;

import com.example.demo.entity.MsgCount;
import com.example.demo.mapper.MsgCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:16:10
 */
@Service
public class MsgCountService {
    @Autowired
    private MsgCountMapper msgCountMapper;

    public MsgCount findCount(Integer u1id,Integer u2id){
        return msgCountMapper.findCount(u1id,u2id);
    }

    public Integer countadd(Integer u1id,Integer u2id){
        return msgCountMapper.countadd(u1id,u2id);
    }

    public int mcountAdd(Integer u1id, Integer u2id) {
        return msgCountMapper.mcountAdd(u1id, u2id);
    }

    public int zeromcount(Integer u1id, Integer u2id) {
        return msgCountMapper.zeromcount(u1id, u2id);
    }

    public int delMscount(Integer u1id, Integer u2id){
        return msgCountMapper.delMscount(u1id,u2id);
    }
}
