package com.example.demo.service;

import com.example.demo.entity.Friendship;
import com.example.demo.mapper.FriendshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:00:33
 */
@Service
public class FriendshipService {
    @Autowired
    private FriendshipMapper friendshipMapper;

    // 添加好友关系;
    public Integer contact(Friendship friendship) {
        return friendshipMapper.contact(friendship);
    }

    // 根据当前登录用户的 uid 作为 uqid 查找用户链表;
    public List<Friendship> findFriends(Integer u1id) {
        return friendshipMapper.findFriends(u1id);
    }

    // 根据 u1id 和 u2id 删除好友关系;
    public Integer fdel(Integer u1id, Integer u2id) {
        return friendshipMapper.fdel(u1id,u2id);
    }
}
