package com.example.demo.mapper;

import com.example.demo.entity.MsgCount;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.annotation.ManagedBean;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:16:07
 */
@Mapper
public interface MsgCountMapper {
    MsgCount findCount(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);

    Integer countadd(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);

    Integer mcountAdd(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    Integer zeromcount(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

    Integer delMscount(@Param("u1id") Integer u1id,@Param("u2id") Integer u2id);
}
