package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.entity.VO.MsgVO;
import com.example.demo.service.MessageService;
import com.example.demo.service.MsgCountService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:22:18
 */
@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @Autowired
    private MsgCountService msgCountService;

    @Autowired
    private UserService userService;

    @RequestMapping("/findmsg")
    public AjaxResult findMessage(HttpServletRequest request, Integer uid) {
        if(uid == null) {
            return AjaxResult.fail(-6,"传输的参数为空!");
        }
        User user1 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-6,"获取当前登录用户失败!");
        }
        // MsgVO 对象多了个聊天记录的对方 netname;
        List<MsgVO> msgVOList = new ArrayList<>(1000);
        // 根据双发的 uid 获取聊天记录;
        List<Message> list = messageService.findMessage(user1.getUid(),uid);
        System.out.println(" 这是uid "+uid);
        for (int i = 0; i < list.size(); i++) {
            MsgVO msgVO = new MsgVO();
            BeanUtils.copyProperties(list.get(i), msgVO);
            // 查找发送信息的用户;
            User suer = userService.selectByUid(list.get(i).getSender());
            // 查找接收信息的用户;
            User ruer = userService.selectByUid(list.get(i).getReceiver());
            if(list.get(i).getSender().equals(user1.getUid())) {
                // 一旦发送方的 id 是当前登录用户,将 netname 改为"我";
                msgVO.setNetname("我");
            }else {
                // 找到聊天信息的对象的信息;
                // 三元运算获取聊天对方的 uid;
                User fuser = userService.selectByUid(suer.getUid().equals(user1.getUid())?ruer.getUid():suer.getUid());
                msgVO.setNetname(fuser.getNetname());
            }
            msgVOList.add(msgVO);
        }
        // 如果拉取了信息,直接将 sender 对 receiver 的未读消息置为 0;
        msgCountService.zeromcount(user1.getUid(),uid);
        return AjaxResult.success(msgVOList);
    }

    @RequestMapping("/send")
    public AjaxResult msgAdd(HttpServletRequest request, Message message) {
        if(message.getReceiver() == null) {
            return AjaxResult.fail(-6,"传输的参数为空!");
        }
        User user1 = UserSessionUtils.getUser(request);
        if(user1 == null) {
            return AjaxResult.fail(-5,"获取当前登录用户失败!");
        }
        message.setSender(user1.getUid());
        // 只要收到了消息,就将 receiver 对 sender 的未读消息 +1;
        System.out.println(msgCountService.mcountAdd(message.getReceiver(), user1.getUid()));
        // 添加消息;
        return AjaxResult.success(messageService.msgAdd(message));
    }
}
