package com.example.demo.mapper;

import com.example.demo.entity.Friendship;
import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-11-09
 * Time:00:27
 */
@Mapper
public interface FriendshipMapper {

    // 添加好友关系
    Integer contact(Friendship friendship);

//    <!--    根据 u1id 查找好友-->
    List<Friendship> findFriends(@Param("u1id") Integer u1id);

    // 根据 u1id 和 u2id 删除好友关系;
    Integer fdel(@Param("u1id") Integer u1id, @Param("u2id") Integer u2id);

}
