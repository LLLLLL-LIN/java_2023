drop database if exists wechat;
create database wechat DEFAULT CHARACTER SET utf8mb4;
use wechat;

DROP TABLE IF EXISTS user;
CREATE TABLE user (
    uid INT PRIMARY KEY auto_increment, -- 用户ID，作为主键
    netname VARCHAR(50), -- 昵称
    username VARCHAR(50) unique not null, -- 用户名/账号
    password VARCHAR(255), -- 密码，建议使用加密存储
    email VARCHAR(100), -- 电子邮件地址
    regtime  DATETIME default null, -- 注册日期
    checktime  DATETIME default null, -- 注册日期
    avatarUrl VARCHAR(255), -- 用户头像URL
    status INT(1) default 0
);

DROP TABLE IF EXISTS friendship;
CREATE TABLE friendship (
    fid INT PRIMARY KEY auto_increment, -- 好友关系ID，作为主键
    u1id INT, -- 用户1的ID
    u2id INT, -- 用户2的ID
    status VARCHAR(20), -- 好友关系状态，如已添加、已删除等
    friendtime DATETIME default null, -- 成为好友的时间
    chattime DATETIME default null -- 更新聊天的时间
--    msgcount1 INT default 0, -- 用户1的未读
--    msgcount2 INT default 0 -- 用户2的未读
);


DROP TABLE IF EXISTS message;
CREATE TABLE message (
    mid INT PRIMARY KEY auto_increment, -- 消息ID，作为主键
    sender INT, -- 发送者的ID
    receiver INT, -- 接收者的ID
    content TEXT, -- 消息内容
    sendtime  DATETIME default null, -- 消息发送时间
    status VARCHAR(20) default 0-- 消息状态，如已发送、已读等
);


DROP TABLE IF EXISTS msgcount;
CREATE TABLE msgcount (
    cid INT PRIMARY KEY auto_increment, -- 消息ID，作为主键
    u1id INT, -- 发送者的ID
    u2id INT, -- 接收者的ID
    sendtime  DATETIME default null, -- 消息发送时间
    msgcount INT default 0
);


