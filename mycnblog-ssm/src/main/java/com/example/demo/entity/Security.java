package com.example.demo.entity;

import lombok.Data;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-15 Time:22:06 */
@Data
public class Security {
    private String securityQuestion1;
    private String securityAnswer1;
    private String securityQuestion2;
    private String securityAnswer2;
    private Integer userid;
}
