package com.example.demo.entity;

import lombok.Data;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:22 */
@Data
public class Total {
    private Integer id;
    private Integer total;
}
