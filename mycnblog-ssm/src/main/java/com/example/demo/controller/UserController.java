package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.component.CaptchaUtils;
import com.example.demo.entity.Captcha;
import com.example.demo.entity.Security;
import com.example.demo.entity.UseridSalt;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CaptchaService;
import com.example.demo.service.UserService;
import com.example.demo.service.SecurityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private CaptchaService captchaService;
    @Autowired
    private SecurityService securityService;


    @RequestMapping("/reg")
    public AjaxResult reg(Userinfo userinfo, Security security,Integer captid, String captcha) {
        // 非空效验和参数有效性效验
        if (userinfo == null || !StringUtils.hasLength(userinfo.getNetname()) ||
                !StringUtils.hasLength(userinfo.getUsername()) ||
                !StringUtils.hasLength(userinfo.getPassword())) {
            return AjaxResult.fail(-1, "非法参数");
        }
        if((userService.getUserByName(userinfo.getUsername())) != null) {
            return AjaxResult.fail(-3,"请修改用户名!");
        }
        if((security.getSecurityAnswer1().length() >=20)
                || (security.getSecurityAnswer2().length() >= 20)) {
            return AjaxResult.success(-2,"密保超过20个字!");
        }
        Captcha captcha1 = captchaService.selectCaptcha(captid);
        if(captcha1 == null) {
            return AjaxResult.success(-1,"验证码获取错误");
        }
        if(!CaptchaUtils.validateCaptcha(captcha,captcha1.getCaptcha())) {
            return AjaxResult.success(-4,"验证码错误!");
        }
        // 就是这里一直没找到多次数据插入的问题,有两个 insertUserSe ;
//        Integer sevo = userinfoSeVOService.insertUserSe(userinfoSeVO);
        // 密码加盐处理
        userinfo.setPassword(PasswordUtils.encrypt(userinfo.getPassword()));
//        int a = userService.reg(userinfo);
//        System.out.println(a);
        Integer userid = userService.reg(userinfo);
        security.setUserid(userid);
        return AjaxResult.success(securityService.insertSecu(security));
    }

    @RequestMapping("/change")
    public AjaxResult change(Userinfo userinfo,HttpServletRequest request) {
        // 非空效验和参数有效性效验
        if (userinfo == null || !StringUtils.hasLength(userinfo.getNetname()) ||
                !StringUtils.hasLength(userinfo.getPassword())) {
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo1 = UserSessionUtils.getUser(request);
        if(userinfo1 == null) {
            return AjaxResult.fail(-1, "未登录!");
        }
        if(userinfo1.getUsername().equals("user")) {
            return AjaxResult.success(-2,"抱歉! 特殊账号! 不提供修改功能!");
        }
        // 密码加盐处理
        userinfo.setPassword(PasswordUtils.encrypt(userinfo.getPassword()));
    //        int a = userService.reg(userinfo);
    //        System.out.println(a);
        System.out.println(userinfo);
        userinfo.setUsername(userinfo1.getUsername());
        return AjaxResult.success(userService.changeNum(userinfo));
    }

    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, String username, String password,Integer captid,String captcha) {
        Captcha captcha1 = captchaService.selectCaptcha(captid);
        if(captcha1 == null) {
            return AjaxResult.success(-1,"验证码获取错误");
        }
        if(!CaptchaUtils.validateCaptcha(captcha,captcha1.getCaptcha())) {
            return AjaxResult.success(-4,"验证码错误!");
        }
        // 1.非空效验
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return AjaxResult.fail(-1, "非法请求");
        }
        // 2.查询数据库
        Userinfo userinfo = userService.getUserByName(username);

        if(userinfo == null){
            return AjaxResult.success(-1,"未登录!");
        }

        // 判断当前的登录账号是否处于冻结状态
        if(userinfo.getState() == 0 && userinfo.getFreezetime() != null && userinfo.getCounts() > 0) {
            // 获得三个时间戳,一个是账号的冻结时间的时间戳,一个是当前系统时间的时间戳,一个是我们把账号冻结的时长的时间戳;
            LocalDateTime freezeTime = userinfo.getFreezetime();
            ZoneId zoneId = ZoneId.systemDefault();
            // 冻结的时间的时间戳;
            long freezeTimestamp = freezeTime.atZone(zoneId).toInstant().toEpochMilli();
            // 获取当前的系统时间的时间戳;
            long currentTime = System.currentTimeMillis();
            // 设置一小时的毫秒数,这是我们需要冻结账号的时长;
            long hourInMillis = 60 * 60 * 1000;
//            long hourInMillis = 60 * 1000;

            // 如果当前时间距离冻结时间戳超过一小时，则解除冻结状态
            if (currentTime - freezeTimestamp >= hourInMillis) {
                // 解除冻结状态,xml 中写值,这里就不用传输需要设置的值了, 传输 userinfo 的 id 就行;
                userService.upFreezeState(userinfo.getId());
            } else {
                // 距离解除冻结状态的剩余时间（以分钟为单位）
                long remainingTime = (freezeTimestamp + hourInMillis - currentTime) / (60 * 1000);
                System.out.println("账号已冻结，请等待 " + remainingTime + " 分钟后再试。");
                return AjaxResult.success(-3,remainingTime);
            }
        }

        // 根据账号查询用户 查到了就需要进入密码的判断
        if (userinfo != null && userinfo.getId() > 0) {   // 有效的用户
            // 前面有状态更新 这里重新获取一下 userinfo;
            Userinfo userinfo4 = userService.getUserByName(username);
            // 状态没更新就直接返回冻结,更新了不进入这个循环,直接到密码认证;
            if(userinfo4.getState() == 0){
                return AjaxResult.success(-2,"账号已冻结!");
            }
            // 两个密码是否相同
//            boolean istrue = PasswordUtils.check(password,userinfo.getPassword());
//            boolean eauqls = password.equals(userinfo.getPassword());
//            if (password.equals(userinfo.getPassword())){
//                // 登录成功
//                // 将用户存储到 session
//                HttpSession session = request.getSession();
//                session.setAttribute(AppVariable.USER_SESSION_KEY, userinfo);
//                userinfo.setPassword(""); // 返回前端之前，隐藏敏感（密码）信息
//                return AjaxResult.success(userinfo);
//            }

            // 采用加盐
            if (PasswordUtils.check(password, userinfo.getPassword())) {
                // 登录成功
                // 将用户存储到 session
                HttpSession session = request.getSession();
                session.setAttribute(AppVariable.USER_SESSION_KEY, userinfo);
                userinfo.setPassword(""); // 返回前端之前，隐藏敏感（密码）信息

                // 只要是登录成功了 之前的 counts就重新设置为 0 了;
                userService.upZereoCounts(userinfo.getId());
                return AjaxResult.success(userinfo);
            } else if (!PasswordUtils.check(password, userinfo.getPassword())){
                userService.upcounts(userinfo.getId());
                if(userinfo.getCounts() == 2) {
                    userService.upUserStata(userinfo.getId());
                }
                Userinfo userinfo3 = userService.getbyid(userinfo.getId());
                userinfo3.setPassword("");
                return AjaxResult.success(200,"密码错误",userinfo3);
            }
        }
        return AjaxResult.success(0, null);
    }

    @RequestMapping("/showinfo")
    public AjaxResult showInfo(HttpServletRequest request) {
        UserinfoVO userinfoVO = new UserinfoVO();
        // 1.得到当前登录用户（从 session 中获取）
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        // Spring 提供的深克隆方法
        BeanUtils.copyProperties(userinfo, userinfoVO);
        // 2.得到用户发表文章的总数
        userinfoVO.setArtCount(articleService.getArtCountByUid(userinfo.getId()));
        return AjaxResult.success(userinfoVO);
    }

    /**
     * 注销（退出登录）
     *
     * @param session
     * @return
     */
    @RequestMapping("/logout")
    public AjaxResult logout(HttpSession session) {
        session.removeAttribute(AppVariable.USER_SESSION_KEY);
        return AjaxResult.success(1);
    }

    @RequestMapping("/getuserbyid")
    public AjaxResult getUserById(Integer id) {
        if (id == null || id <= 0) {
            // 无效参数
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = userService.getUserById(id);
        if (userinfo == null || userinfo.getId() <= 0) {
            // 无效参数
            return AjaxResult.fail(-1, "非法参数");
        }
        // 去除 userinfo 中的敏感数据，ex：密码
        userinfo.setPassword("");
        UserinfoVO userinfoVO = new UserinfoVO();
        BeanUtils.copyProperties(userinfo, userinfoVO);
        // 查询当前用户发表的文章数
        userinfoVO.setArtCount(articleService.getArtCountByUid(id));
        return AjaxResult.success(userinfoVO);
    }

    @RequestMapping("/getbyid")
    public Userinfo getByid(Integer id) {
        return userService.getbyid(id);
    }

    // 找回密码功能的验证当前的用户名是否正确存在;
    @RequestMapping("/findsecu")
    public AjaxResult findsecu(String username, String captcha, Integer captid) {
        Userinfo userinfo = userService.getUserByName(username);
        if(userinfo == null) {
            return AjaxResult.success(-1,"用户名错误!");
        }
        Captcha captcha1 = captchaService.selectCaptcha(captid);
        if(captcha1 == null) {
            return AjaxResult.success(-2,"验证码获取错误!");
        }
        if(!CaptchaUtils.validateCaptcha(captcha,captcha1.getCaptcha())) {
            return AjaxResult.success(-4,"验证码错误!");
        }
        Security security = securityService.getSecuByUserid(userinfo.getId());
        security.setSecurityAnswer1("");
        security.setSecurityAnswer2("");
        return AjaxResult.success(security);
    }


    //
    @RequestMapping("/findpword")
    public AjaxResult findpword(Security security, String captcha, Integer captid, Integer userid) {
        Captcha captcha1 = captchaService.selectCaptcha(captid);
        if(captcha1 == null) {
            return AjaxResult.success(-1,"验证码获取错误!");
        }
        if(!CaptchaUtils.validateCaptcha(captcha,captcha1.getCaptcha())) {
            return AjaxResult.success(-4,"验证码错误!");
        }
        Security security1 = securityService.getSecuByUserid(userid);
        if(security1 == null || security == null) {
            return AjaxResult.success(-2,"密保获取错误!");
        }
        if(!(security1.getSecurityAnswer1().equals(security.getSecurityAnswer1()))
                || !(security1.getSecurityAnswer2().equals(security.getSecurityAnswer2()))) {
            return AjaxResult.success(-3,"密保答案不对!");
        }
        String newsalt = UUID.randomUUID().toString().replace("-", "");
        UseridSalt useridSalt = new UseridSalt();
        useridSalt.setUserid(userid);
        useridSalt.setNewsalt(newsalt);
        userService.upNewSalt(useridSalt);
        Userinfo userinfo = userService.getUserById(userid);
//        userinfo.setPassword("");
        UseridSalt useridSalt1 = new UseridSalt();
        useridSalt1.setUserid(userid);
        useridSalt1.setNewsalt(userinfo.getNewsalt());
        return AjaxResult.success(useridSalt1);
    }

    @RequestMapping("/setpword")
    public AjaxResult setpword(Userinfo userinfo) {
        if(userinfo == null) {
            return AjaxResult.success(-1,"userinfo为空!");
        }
        Userinfo userinfo1 = userService.getUserById(userinfo.getId());
        if(userinfo1 == null) {
            return AjaxResult.success(-2,"没有获取到userinfo对象!");
        }
        if(!(userinfo.getNewsalt().equals(userinfo1.getNewsalt()))) {
            return AjaxResult.success(-3,"重置密钥不匹配!");
        }
        String password = PasswordUtils.encrypt(userinfo.getPassword());
        userinfo.setPassword(password);
        return AjaxResult.success(userService.setPword(userinfo));
    }
}
