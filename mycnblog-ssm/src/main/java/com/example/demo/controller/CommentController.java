package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Comment;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.CommentVO;
import com.example.demo.service.CommentService;
import org.springframework.aop.aspectj.AspectJAfterAdvice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:03:13 */
@RestController
@RequestMapping("/com")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private UserController userController;

    // 添加评论;
    @RequestMapping("/isnertcom")
    public AjaxResult insertCom(Integer id, String comment, HttpServletRequest request) {
        if(comment == null || comment.length() == 0) {
            return AjaxResult.success(-2,"评论为空!");
        }
        System.out.println("str------" + comment.length());
        Userinfo userinfo = UserSessionUtils.getUser(request);
        System.out.println(userinfo);
        if(userinfo == null) {
            return AjaxResult.success(-3,"未登录! 请登录!");
        }
        int uid = userinfo.getId();
        return AjaxResult.success(commentService.insertCom(id,comment,uid));
    }

    // 查找文章下的评论链表;
    @RequestMapping("/findcom")
    public AjaxResult findCom(Integer id,HttpServletRequest request){
        List<CommentVO> commentVOList = new ArrayList<>(10000);
        // CommentVO 对象多一个评论者的 netname;
        CommentVO commentVO = new CommentVO();
        Comment comment = new Comment();
        List<Comment> commentList= commentService.findCom(id);
        if(commentList.size() == 0){
            return AjaxResult.success(null);
        }
        Userinfo userinfo1 = UserSessionUtils.getUser(request);
        // 依次复制到 commentVO 对象上;
        for(int i = 0; i < commentList.size(); i++) {
            // 得到 comment;
            comment = commentList.get(i);
            Userinfo userinfo = userController.getByid(comment.getUserid());
            if(userinfo == null) {
                return AjaxResult.success(-1,"没有找到userinfo");
            }

            // comment 复制到 commentVO;
//            CommentVO commentVO1 = new CommentVO();
            commentVOList.add(new CommentVO());
            BeanUtils.copyProperties(comment, commentVOList.get(i));
            System.out.println(userinfo1!=null && userinfo.getNetname().equals(userinfo1.getNetname()));
            if(userinfo1!=null && userinfo.getNetname().equals(userinfo1.getNetname()) ){
                commentVOList.get(i).setNetname("我");
            }else{
                commentVOList.get(i).setNetname(userinfo.getNetname());
            }
        }
        System.out.println(commentVOList);
        return AjaxResult.success(commentVOList);
    }

    @RequestMapping("/comdel")
    public AjaxResult comdel(Integer cid,HttpServletRequest request) {
        Userinfo userinfo = UserSessionUtils.getUser(request);
        Comment comment = commentService.selectCom(cid);
        if(comment == null) {
            return AjaxResult.success(-1,"没找到评论!");
        }
        Userinfo usereinfo1 = commentService.selectUser(comment.getUserid());
        if(userinfo == null || usereinfo1 == null) {
            return AjaxResult.success(-3,"没有该用户!");
        }
        if(!userinfo.getUsername().equals(usereinfo1.getUsername())) {
            return AjaxResult.success(-2,"抱歉! 非该用户!");
        }
        return AjaxResult.success(commentService.comdel(cid));
    }
}
