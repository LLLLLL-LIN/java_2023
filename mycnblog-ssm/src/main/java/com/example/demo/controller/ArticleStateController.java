package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.MarkDownToText;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Articleinfo;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * 这是草稿箱博客的 controller;
 */

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-10 Time:18:02 */
@RestController
@RequestMapping("/arts")
public class ArticleStateController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @RequestMapping("/blogsave")
    public AjaxResult blogSave(Articleinfo articleinfo, HttpServletRequest request) {
        // 1.非空效验
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle()) ||
                !StringUtils.hasLength(articleinfo.getContent())) {
            // 非法参数
            return AjaxResult.fail(-1, "非法参数");
        }
        // 2.数据库添加操作
        // a.得到当前登录用户的 uid
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null || userinfo.getId() <= 0) {
            // 无效的登录用户
            return AjaxResult.fail(-2, "无效的登录用户");
        }
        articleinfo.setUid(userinfo.getId());
        articleinfo.setState(0);
        // b.添加数据库并返回结果
        return AjaxResult.success(articleService.blogSave(articleinfo));
    }

    @RequestMapping("/artslist")
    public AjaxResult artList(HttpServletRequest request, Integer pindex, Integer psize) {
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        // 1.参数校正
        if (pindex == null || pindex <= 1) {
            pindex = 1;
        }
        if (psize == null || psize <= 1) {
            psize = 2;
        }
        // 分页公式的值 = (当前页码-1)*每页显示条数
        int offset = (pindex - 1) * psize;
        // 文章列表数据
        List<Articleinfo> list = articleService.artList(userinfo.getId(),psize, offset);
        for(int i = 0; i < list.size(); i++) {
            // 去除 markdown 语法;
            String text = MarkDownToText.convertToPlainText(list.get(i).getContent());
            // 判断字符长度
            if(text.length() > 151){
                text = text.substring(0,150) + "......";
            }
            list.get(i).setContent(text);
        }
        // 当前列表总共有多少页
        // a.总共有多少条数据
        int totalCount = articleService.getCount3(userinfo.getId());
        // b.总条数/psize（每页显示条数）
        double pcountdb = totalCount / (psize * 1.0);
        // c.使用进一法得到总页数
        int pcount = (int) Math.ceil(pcountdb);
        HashMap<String, Object> result = new HashMap<>();
        result.put("list", list);
        result.put("pcount", pcount);
        return AjaxResult.success(result);
    }


    @RequestMapping("/detail3")
    public AjaxResult getDetail(Integer id, HttpServletRequest request) {
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if(userinfo == null){
            return AjaxResult.success(-2,null);
        }
        Articleinfo articleinfo = articleService.getDetail3(id);
        if(articleinfo == null) {
            return AjaxResult.success(null);
        }
      if (!articleinfo.getUid().equals(userinfo.getId())) {
            return AjaxResult.success(-3,"非法用户!");
        }
        return AjaxResult.success(articleinfo);
    }


    @RequestMapping("/incr-rcount3")
    public AjaxResult incrRCount3(Integer id) {
        if (id != null && id > 0) {
            int rcount = articleService.incrRCount3(id);
//            logger.error("" + rcount);
            return AjaxResult.success(rcount);
        }
        return AjaxResult.fail(-1, "未知错误");
    }

    @RequestMapping("/adds")
    public AjaxResult add(HttpServletRequest request, Articleinfo articleinfo) {
        // 1.非空效验
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle()) ||
                !StringUtils.hasLength(articleinfo.getContent())) {
            // 非法参数
            return AjaxResult.fail(-1, "非法参数");
        }
        // 2.数据库添加操作
        // a.得到当前登录用户的 uid
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null || userinfo.getId() <= 0) {
            // 无效的登录用户
            return AjaxResult.fail(-2, "无效的登录用户");
        }
        articleinfo.setUid(userinfo.getId());
        articleinfo.setState(1);
        // b.添加数据库并返回结果
        return AjaxResult.success(articleService.add(articleinfo));
    }

    @RequestMapping("/set")
    public AjaxResult update3(Articleinfo articleinfo,HttpServletRequest request) {
        // 非空效验
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle()) ||
                !StringUtils.hasLength(articleinfo.getContent()) ||
                articleinfo.getId() == null) {
            // 非法参数
            return AjaxResult.fail(-1, "非法参数");
        }
        // 得到当前登录用户的 id
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null && userinfo.getId() == null) {
            // 无效用户
            return AjaxResult.fail(-2, "无效用户");
        }

        // 这里的对于文章的归属人的问题,应该是从会话中获取到,不应该由前端来传递参数,有伪造的风险;
        articleinfo.setUid(userinfo.getId());
        articleinfo.setUpdatetime(LocalDateTime.now());
        return AjaxResult.success(articleService.update3(articleinfo));
    }
}
