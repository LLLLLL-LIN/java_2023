package com.example.demo.mapper;

import com.example.demo.entity.Total;
import org.apache.ibatis.annotations.Mapper;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:27 */
@Mapper
public interface TotalMapper {
    // 获取登录页面的访问次数;
    Total getTotal();

    // 更新登录页面的访问次数;
    Integer setTotal();

    // 更新博客列表页的访问次数;
    Integer setTotal2();
}
