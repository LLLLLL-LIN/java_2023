package com.example.demo.mapper;

import com.example.demo.entity.Security;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-14 Time:22:45 */
@Mapper
public interface SecurityMapper {
    int insertSecu(Security security);

    Security getSecuByUserid(@Param("userid") Integer id);
}
