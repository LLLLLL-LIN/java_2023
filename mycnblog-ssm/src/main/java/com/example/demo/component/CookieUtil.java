//package com.example.demo.component;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.Cookie;
//
// // 持久化存储 cookie
///** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-09 Time:17:17 */
//@Component
//public class CookieUtil {
//
//    @Value("${cookie.max-age}")
//    private int cookieMaxAge;
//
//    public Cookie createCookie(String name, String value) {
//        Cookie cookie = new Cookie(name, value);
//        cookie.setMaxAge(cookieMaxAge);
//        return cookie;
//    }
//}
