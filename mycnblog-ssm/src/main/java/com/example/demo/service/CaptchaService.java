package com.example.demo.service;

import com.example.demo.entity.Captcha;
import com.example.demo.mapper.CaptchaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-14 Time:19:44 */
@Service
public class CaptchaService {
    @Autowired
    private CaptchaMapper captchaMapper;

    // 开启添加 Captcha 和获取新添加 Captcha 的 id;
    @Transactional
    public Integer insertAndGetId(Captcha captcha) {
        captchaMapper.insertCapt(captcha);
        return captchaMapper.selectCaptId();
    }

    public Captcha selectCaptcha(Integer id) {
        return captchaMapper.selectCaptcha(id);
    }
}
