package com.example.demo.service;

import com.example.demo.entity.Total;
import com.example.demo.mapper.TotalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/** Created with IntelliJ IDEA Description User:Liaolin Date:2023-10-06 Time:22:28 */
@Service
public class TotalService {
    @Resource
    private TotalMapper totalMapper;

    // 获取登录页面的访问次数;
    public Total getTotal() {
        return totalMapper.getTotal();
    }
    // 更新登录页面的访问次数;
    public int setTotal() {
        return totalMapper.setTotal();
    }


    // 更新博客列表页的访问次数;
    public int setTotal2() {
        return totalMapper.setTotal2();
    }
}
