package com.example.demo.service;

import com.example.demo.entity.Articleinfo;
import com.example.demo.mapper.ArticleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    public int getArtCountByUid(Integer uid) {
        return articleMapper.getArtCountByUid(uid);
    }

    public List<Articleinfo> getMyList(Integer uid) {
        return articleMapper.getMyList(uid);
    }

    public int del(Integer id, Integer uid) {
        return articleMapper.del(id, uid);
    }

    public Articleinfo getDetail(Integer id) {
        return articleMapper.getDetail(id);
    }

    public int incrRCount(Integer id) {
        return articleMapper.incrRCount(id);
    }

    // 添加博客;
    public int add(Articleinfo articleinfo) {
        return articleMapper.add(articleinfo);
    }

    // 修改更新博客;
    public int update(Articleinfo articleinfo) {
        return articleMapper.update(articleinfo);
    }

    // 博客列表页分页;
    public List<Articleinfo> getListByPage(Integer psize, Integer offsize) {
        return articleMapper.getListByPage(psize, offsize);
    }

    // 博客列表页有多少篇博客;
    public int getCount() {
        return articleMapper.getCount();
    }


    //个人博客列表页分页;
    public List<Articleinfo> getMyListByPage2(Integer id,Integer psize, int offset) {
        return articleMapper.getMyListByPage2(id,psize,offset);
    }

   //个人博客列表页有多少篇博客
    public int getCount2(Integer id) {
        return articleMapper.getCount2(id);
    }


    /**
     * 以下是草稿箱博客的操作方法
     * @param articleinfo
     * @return
     */

    // 草稿箱修改更新博客
    public int blogSave(Articleinfo articleinfo) {
        return articleMapper.blogSave(articleinfo);
    }

    public List<Articleinfo> artList(Integer id,Integer psize, int offset) {
        return articleMapper.getMyListByPage3(id,psize,offset);
    }

    public int getCount3(Integer id) {
        return articleMapper.getCount3(id);
    }

    public int incrRCount3(Integer id) {
        return articleMapper.incrRCount(id);
    }

    public Articleinfo getDetail3(Integer id) {
        return articleMapper.getDetail3(id);
    }

    public int update3(Articleinfo articleinfo) {
        return articleMapper.update3(articleinfo);
    }

}
