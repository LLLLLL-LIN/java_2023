--创建数据库
drop database if exists mycnblog;
create database mycnblog DEFAULT CHARACTER SET utf8mb4;
use mycnblog;

--创建 userinfo 表
drop table if exists userinfo;
create table userinfo(
id int primary key auto_increment,
username varchar(100) not null unique,
password varchar(100) not null,
photo varchar(500) default '',
createtime datetime default null,
updatetime datetime default null,
`state` int default 1,
netname varchar(9) default null,
counts int(11) default 0,
freezetime datetime default null,
newsalt varchar(100) default null
) default charset 'utf8mb4';

-- 创建⽂章表  articleinfo
drop table if exists articleinfo;
create table articleinfo(
id int primary key auto_increment,
title varchar(100) not null,
content text not null,
createtime datetime default null,
updatetime datetime default null,
uid int not null,
rcount int not null default 1,
`state` int default 1
)default charset 'utf8mb4';

--创建访问次数表 total
drop table if exists total;
CREATE TABLE total (
  id int(11) primary key auto_increment,
  total int(10) DEFAULT NULL
) default charset 'utf8mb4';

--创建评论表 comment
drop table if exists comment;
create table comment (
id int primary key auto_increment,
content text not null,
createtime datetime default null,
userid int(11),
artid int(11)
)default charset 'utf8mb4';

--创建验证码表
drop table if exists captcha;
create table captcha (
id int primary key auto_increment,
captcha varchar(5),
base64Image BLOB,
createtime datetime default null
)default charset 'utf8mb4';

--创建密保表
drop table if exists security;
create table security(
id int primary key auto_increment,
securityQuestion1 varchar(200),
securityAnswer1 varchar(200),
securityQuestion2 varchar(200),
securityAnswer2 varchar(200),
userid int(11),
createtime datetime default null
)default charset 'utf8mb4';

-- 创建视频表
drop table if exists videoinfo;
create table videoinfo(
vid int primary key,
`title` varchar(250),
`url` varchar(1000),
createtime datetime default null,
updatetime datetime default null,
uid int
)default charset 'utf8mb4';

INSERT INTO `mycnblog`.`userinfo` (`id`, `username`, `password`, `photo`,
`createtime`, `updatetime`, `state`) VALUES
(1, 'admin', 'admin', '', now(), now(), 1)
;