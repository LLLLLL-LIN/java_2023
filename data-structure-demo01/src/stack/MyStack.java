package stack;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:10:05
 */
public class MyStack implements IStack{
    // 定义一个 int 数据来表示栈的大小;
    private int usedSize;
    // 定义一个数组来作为栈的基础;
    int[] elem;
    // 定义一个常数来表示构造方法的栈的大小;
    private static final int DEFAULT_ELEM_SIZE = 10;

    public MyStack() {
        elem = new int[DEFAULT_ELEM_SIZE];
    }

    @Override
    public void push(int x) {
        if (isfull()) {
            // 满了就二倍扩容;
            elem = Arrays.copyOf(elem,elem.length * 2);
        }
        elem[usedSize] = x;
        usedSize++;
    }

    @Override
    public int pop() {
        if (empty()) {
            return -1;
        }
        int backInt = elem[usedSize - 1];
        usedSize--;
        return backInt;
    }

    @Override
    public int peek() {
        if (empty()) {
            return -1;
        }
        return elem[usedSize - 1];
    }

    @Override
    public int size() {
        return usedSize;
    }

    @Override
    public boolean empty() {
        return usedSize == 0;
    }

    @Override
    public boolean isfull() {
        return elem.length == usedSize;
    }
}
