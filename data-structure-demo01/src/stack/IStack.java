package stack;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:10:05
 */
public interface IStack {
    // 栈的添加元素;
    void push(int x);

    // 栈的删除栈顶元素,并返回该元素;
    int pop();

    // 获取栈顶元素,并返回,不删除;
    int peek();

    // 获取栈中有效元素个数;
    int size();

    // 检测栈是否为空;
    boolean empty();

    // 检测栈是否满了;
    boolean isfull();
}
