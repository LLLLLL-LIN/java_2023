/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-15
 * Time:11:02
 */
// 自定义泛型模型;
class MyArray {
    // 自定义数组来存放任意类型数据;
    Object[] myArray = new Object[10];

    // 给定坐标,添加数据;
    public void setValue(int pos, Object toSet) {
        // pos位置,设置value为toSet;
        myArray[pos] = toSet;
    }

    // 给定坐标,拿取数据;
    public Object getValue(int pos) {
        // 返回pos位置的value;
        return myArray[pos];
    }
}

class MyTArray <T> {
    public Object[] myTArray = new Object[10];

    public void setValue(int pos, Object value) {
        myTArray[pos] = value;
    }

    public T getValue(int pos) {
        return (T)myTArray[pos];
    }
}

public class MyArrayDemo01 {
    public static void main(String[] args) {
        // 1.泛型原理;
        MyArray myArray = new MyArray();
        myArray.setValue(0, 1);
        myArray.setValue(1, "world");

        // 虽然往数组存放的时候是int 和 String;
        // 但是取的时候还是不能拿相应的类型来接收,需要转换;
        int pos0 = (int)myArray.getValue(0);
        String str1 = (String)myArray.getValue(1);

        System.out.println(pos0);
        System.out.println(str1);
        
        
        // 2.泛型;
        MyTArray<String> myTArray = new MyTArray<>();
        myTArray.setValue(0, "hello");
        myTArray.setValue(1, "world");
        myTArray.setValue(2, "!");
        
        String strs0 = myTArray.getValue(0);
        String strs1 = myTArray.getValue(1);
        String strs2 = myTArray.getValue(2);
        System.out.println(strs0 + strs1 + strs2);
    }
}
