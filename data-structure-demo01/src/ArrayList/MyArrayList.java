package ArrayList;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-15
 * Time:15:53
 */
public class MyArrayList implements IList {
    // 底层数组,用来存储数据;
    private int[] elem;
    // 数组的已使用大小;
    private int usedSize;

    // 无参构造;
    public MyArrayList() {
        this.elem = new int[10];
    }

    // 参数为数组长度的构造方法;
    public MyArrayList(int size) {
        this.elem = new int[size];
    }

    // 判断数组是否满了;
    @Override
    public boolean isFull() {
        return usedSize == elem.length;
    }

    // 扩容;
    private void notifyCap() {
        elem = Arrays.copyOf(elem, elem.length * 2);
    }

    // 判断数组是否为空;
    @Override
    public boolean isEmpty() {
        // 如果数组的已使用空间为 0,则表示数组为空;
        return usedSize == 0;
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        // 1.判断数组是否满了,满了就扩容;
        if (!isFull()) {
            // 数组满了,调用方法,进行扩容两倍;
            notifyCap();
        }
        // 2.把元素放到末尾,也就是 usedSize 的位置;
        elem[usedSize] = data;
        // 3.usedSize++;
        this.usedSize++;
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        // 1.判断这个 pos 下标是否合理;
        if (pos < 0 || pos > usedSize) {
            System.out.println("给定的 pos 下标不合理,无法进行 add(int pos, int data) 操作!");
            return;
        }
        // 2.判断数组是否满了;
        if (isFull()) {
            // 2.1 数组满了,进行扩容;
            notifyCap();
        }
        // 3.开始赋值并且挪位置;
        for (int i = usedSize; i > pos; i--) {
            this.elem[i] = this.elem[i - 1];
        }
        // 4.pos 位置空了,把 data 放到 pos 位置;
        this.elem[pos] = data;
        // 5.新增元素,则 usedsize++
        this.usedSize++;
    }

    // 判定是否包含某个元素
    public boolean contains(int toFind) {
        // 1.判断数组是否为空;
        if (isEmpty()) {
            // 1.1 数组为空,直接 return;
            System.out.println("数组为空,无法执行 contains() 操作!");
            return false;
        }
        // 2.数组不为空,查找该值;
        for (int i = 0; i < usedSize; i++) {
            // 2.1 遍历判断每个位置的值是否匹配;
            if (this.elem[i] == toFind) {
                // 2.2 匹配到了,返回 true;
                return true;
            }
        }
        // 3. 遍历完数组,没找到这个值,直接 return false;
        return false;
    }

    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        // 1.判断数组是否为空;
        if (isEmpty()) {
            System.out.println("数组为空,无法执行 indexOf() 操作!");
            return -1;
        }
        // 2.数组不为空,遍历数组,进行这个值的匹配;
        for (int i = 0; i < usedSize; i++) {
            // 2.1 对每个位置的值进行匹配;
            if (this.elem[i] == toFind) {
                // 2.2 匹配到了这个值,返回 pos;
                return i;
            }
        }
        // 3.没有匹配到,return -1;
        return -1;
    }

    // 获取 pos 位置的元素
    public int get(int pos) {
        // 1.判断数组是否为空;
        if (isEmpty()) {
            System.out.println("数组为空,无法执行get(int pos) 操作!");
            return -1;
        }
        // 2.判断 pos 位置是符合合理;
        if (pos < 0 || pos >= usedSize) {
            System.out.println("给定的 pos 下标不合理!");
            return -1;
        }
        // 3.返回 toFind 这个位置的值;
        return this.elem[pos];
    }

    // 给 pos 位置的元素设为 value
    public void set(int pos, int value) {
        // 判断 pos 位置是否合理;
        if(pos < 0 || pos >= usedSize) {
            // 相当于更新元素,不同于更新元素,只有在原位置有值的情况下才能更新值,
            // 所以在 pos = usedSize 这个地方,原本值没有值的,就不能更新值,否则空指针异常;
            System.out.println("给定的 pos 下标越界!");
            return;
        }
        this.elem[pos] = value;
    }

    // 删除第一次出现的关键字 key
    public void remove(int toRemove) {
        // 1.数组判空;
        if (isEmpty()) {
            System.out.println("数组为空,无法执行 remove() 操作!");
            return;
        }
        // 2.进行查找关键字匹配的下标 pos;
        int index = indexOf(toRemove);
        // 2.1 如果返回值为 -1,就是没有匹配到,直接 return;
        if (index == -1) {
            return;
        }
        // 3.进行删除操作;
        for (int i = index; i < usedSize; i++) {
            this.elem[i] = this.elem[i + 1];
        }
        // 4.删除数据,把 usedSize 进行 --;
        this.usedSize --;
    }

    // 获取顺序表长度
    public int size() {
        // 返回数组的已使用空间,这个就是顺序表的长度;
        return this.usedSize;
    }

    // 清空顺序表
    public void clear() {
        // 直接把 usedSize 置为 0;
        this.usedSize = 0;
    }

    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display() {
        // 1.数组判空;
        if(isEmpty()) {
            System.out.println("数组为空,无法执行 display() 操作!");
            return;
        }
        // 2.循环遍历 0 ~ usedSize 进行打印;
        for (int i = 0; i < usedSize; i++) {
            System.out.print(this.elem[i] + " ");
        }
        // 格式换行;
        System.out.println();
    }
}
