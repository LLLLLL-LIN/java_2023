package ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-16
 * Time:10:49
 */
// 测试自定义的(顺序表)MyList;
public class TestMyArrayList {
    public static void main(String[] args) {
        MyArrayList myList = new MyArrayList();
        myList.add(0);
        myList.add(33);
        myList.add(2);
        myList.add(33);
        myList.add(4);
        System.out.println("======");
        myList.display();

//        // pos 位置新增元素;
//        myList.add(1, 200);

//        // 匹配关键字 toFind,返回坐标;
//        System.out.println(myList.indexOf(33));

//        // 数组判空;
//        System.out.println(myList.isEmpty());

//        // 数组判满;
//        System.out.println(myList.isFull());

//        // 数组是否包含某个元素;
//        System.out.println(myList.contains(33));

//        // 获取 pos 坐标的值;
//        int value = myList.get(3);
//        System.out.println(value);

//        // 给 pos 位置的值修改成 value;
//        myList.set(3, 311);

//        // 删除第一个匹配关键字 key 的元素;
//        myList.remove(33);

//        // 获取顺序表的长度;
//        System.out.println("顺序表长度" + myList.size());

        // 清空顺序表;
        myList.clear();
        System.out.println("======");
        // 进行打印数组;
        myList.display();
    }

}
