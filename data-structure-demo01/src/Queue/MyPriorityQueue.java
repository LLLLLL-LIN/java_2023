package Queue;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-29
 * Time:16:51
 */
// 自定义优先级队列;
public class MyPriorityQueue {
    public int[] elem;
    public int usedSize;
    MyPriorityQueue() {
        elem = new int[10];
    }

    /**
     * 初始化 elem 数组;
     * @param array
     */
    public void initElem(int[] array) {
        usedSize = array.length;
        this.elem = Arrays.copyOf(array, array.length);
    }

    /**
     * 创建大根堆;
     */
    public int[] createMaxHeap() {
        for (int parent = (usedSize - 1 -1) / 2; parent >= 0 ; parent--) {
            shiftDown(parent, usedSize);
//            shiftDown2(parent, usedSize);
        }
        return this.elem;
    }

    /**
     * 每个节点向下调整(大根堆,大的节点往下调整);
     */
    private void shiftDown(int parent, int len) {
        int child = 2 * parent + 1;
        while (child < len) {
            if (child + 1 < len && elem[child] < elem[child + 1]) {
                child = child + 1;
            }
            // 走到这里说明 child 一定是指向左右孩子节点中最大的那个节点;
            // 所以这里要进行交换的节点也是左右孩子节点中最大的那个孩子节点;
            if (elem[parent] < elem[child]) {
                // 这里说明父节点比最大的子节点还要大,节点的值进行交换;
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                // 这里既然是交换了值,说不定下面的节点会因此发生变换,还需要向下进行排序,
                // 所以这里直接更新 parent 和 child 的下标;
                parent = child;
                child = 2 * parent + 1;
            } else {
                // 说明子树是大根堆,不需要换,直接跳出循环就行;
                break;
            }
        }
    }

    /**
     * 每个节点向下调整(小根堆,小的节点往下调整);
     */
    private void shiftDown2(int parent, int len) {
        int child = 2 * parent + 1;
        while (child < len) {
            if (child + 1 < len && elem[child] > elem[child + 1]) {
                child = child + 1;
            }
            // 走到这里说明 child 一定是指向左右孩子节点中最大的那个节点;
            // 所以这里要进行交换的节点也是左右孩子节点中最大的那个孩子节点;
            if (elem[parent] > elem[child]) {
                // 这里说明父节点比最大的子节点还要大,节点的值进行交换;
                int tmp = elem[child];
                elem[child] = elem[parent];
                elem[parent] = tmp;
                // 这里既然是交换了值,说不定下面的节点会因此发生变换,还需要向下进行排序,
                // 所以这里直接更新 parent 和 child 的下标;
                parent = child;
                child = 2 * parent + 1;
            } else {
                // 说明子树是大根堆,不需要换,直接跳出循环就行;
                break;
            }
        }
    }

    // 数组判满;
    public boolean isFull() {
        return usedSize == elem.length;
    }

    // 新增元素(添加到数组 elem 中);
    public void push(int x) {
        // 1.数组满了;
        if (isFull()) {
            // 1.1 数组进行扩容;
            this.elem = Arrays.copyOf(elem, elem.length * 2);
        }
        // 2.数组新增元素;
        elem[usedSize] = x;
        // 把新增的位置发送过去,重新调整优先级队列;
        shiftUp(usedSize);
        usedSize++;
    }

    // 向上调整,相当于新增元素之后,判断要不要重新排序一下;
    public void shiftUp(int child) {
        int parent = (child - 1) / 2;
        while (child > 0) {
            if (elem[parent] < elem[child]) {
                // 说明父节点的值小于子节点,位置互换;
                // 互换位置;
                int tmp = elem[parent];
                elem[parent] = elem[child];
                elem[child] = tmp;
                // 让下标更新;
                child = parent;
                parent = (child - 1) / 2;
            } else {
                break;
            }
        }
    }

    // 优先级队判空(数组判空);
    public boolean isEmpty() {
        return usedSize == 0;
    }

    // 优先级队列的删除;
    // 实际上是 0 位置和 usedSize 位置交换,调整 0 这一棵树就行;
    public int pop() {
        int parent = 0;
        int child = usedSize - 1;
        if (isEmpty()) {
            return -1;
        }
        // 1.位置互换;
        int oldValue = elem[child];
        elem[child] = elem[parent];
        elem[parent] = oldValue;

        // 删除的话, usedSize - 1;
        usedSize--;
        // 最后一个节点置为 0;
        elem[usedSize] = 0;
        // 开始调整 0 位置这棵树;
        shiftDown(0, usedSize);

        // 返回要删除节点的值;
        return oldValue;
    }

    // 堆排序;
    public void heapSort() {
        int end = usedSize - 1;
        while (end > 0) {
            swap(0,end);
            shiftDown(0, end);
            end--;
        }
    }

    // 数组中指定两个下标的元素交换;
    private void swap(int i, int end) {
        int tmp = this.elem[i];
        this.elem[i] = this.elem[end];
        this.elem[end] = tmp;
    }
}
