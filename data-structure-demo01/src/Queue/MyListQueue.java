package Queue;

import jdk.internal.dynalink.beans.StaticClass;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:17:13
 */
// 链式栈;
public class MyListQueue {
    static class ListNode {
        int val;
        ListNode prev;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }
    private ListNode head;
    private ListNode last;
    private int usedSize;

    public boolean offer(int val) {
        ListNode node = new ListNode(val);
        if (isEmpty()) {
            this.head = node;
            this.last = node;
        } else {
            this.last.next = node;
            node.prev = this.last;
            this.last = this.last.next;
        }
        usedSize++;
        return true;
    }

    public int poll() {
        if (isEmpty()) {
            return -1;
        }
        int val = this.head.val;
        if (this.head.next == null) {
            this.head = null;
            this.last = null;
            return val;
        }
        this.head = this.head.next;
        this.head.prev = null;
        usedSize--;
        return val;
    }

    public int peek() {
        if (isEmpty()) {
            return -1;
        }
        return this.head.val;
    }

    public int size() {
        return usedSize;
    }

    public boolean isEmpty() {
        return usedSize == 0;
    }
}

