package Queue;

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-29
 * Time:16:55
 */
public class TestMyPriorityQueue {
    public static void main(String[] args) {
        int[] array = {27, 15, 19, 18, 28, 34, 65, 49, 25, 37};
        MyPriorityQueue myPriorityQueue = new MyPriorityQueue();
        // 初始化;
        myPriorityQueue.initElem(array);
        // 创建优先级队列;
        int[] newArray =  myPriorityQueue.createMaxHeap();
        for (int num : newArray) {
            System.out.print(num + " ");
        }

////         新增元素;
//        myPriorityQueue.push(90);
//        System.out.println("===");

//        // 删除元素;
//        System.out.println(myPriorityQueue.pop());
//        System.out.println("======");

//        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
//        Integer o1 = 12;
//        Integer o2 = 13;
//        o1.compareTo(o2);

        // 堆排序;
        myPriorityQueue.heapSort();
        System.out.println("===");

    }
}
