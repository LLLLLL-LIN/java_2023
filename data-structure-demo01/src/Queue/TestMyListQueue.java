package Queue;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:17:31
 */
// 链式栈测试;
public class TestMyListQueue {
    public static void main(String[] args) {
        MyListQueue myListQueue = new MyListQueue();
        myListQueue.offer(1);
        myListQueue.offer(2);
        myListQueue.offer(3);
        myListQueue.offer(4);

        System.out.println(myListQueue.peek());
        System.out.println(myListQueue.poll());
        System.out.println(myListQueue.poll());
        System.out.println(myListQueue.poll());

        System.out.println(myListQueue.isEmpty());

        System.out.println(myListQueue.size());
    }
}
