package LinkedList;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-23
 * Time:11:17
 */
// 测试 MyLinkedList;
public class TestMyLInkedList {
    public static void main(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
//        MyLinkedList.ListNode Listnode1 = new MyLinkedList.ListNode(12);
//        MyLinkedList.ListNode Listnode2 = new MyLinkedList.ListNode(12);
//        MyLinkedList.ListNode Listnode3 = new MyLinkedList.ListNode(12);
//        MyLinkedList.ListNode Listnode4 = new MyLinkedList.ListNode(12);
//        MyLinkedList.ListNode Listnode5 = new MyLinkedList.ListNode(12);
//
//        Listnode1.next = Listnode2;
//        Listnode2.next = Listnode3;
//        Listnode3.next = Listnode4;
//        Listnode4.next = Listnode5;
//
//        Listnode5.prev = Listnode4;
//        Listnode4.prev = Listnode3;
//        Listnode3.prev = Listnode2;
//        Listnode2.prev = Listnode1;
//
//        myLinkedList.head = Listnode1;
//        myLinkedList.last = Listnode5;
//
//        myLinkedList.display();
        System.out.println("==================");

//        // 尾插法;
//        myLinkedList.addLast(56);
//        myLinkedList.addLast(66);
//        myLinkedList.addLast(67);

//        // 头插法;
//        myLinkedList.addFirst(10);
//        myLinkedList.addFirst(11);
//        myLinkedList.addFirst(12);

//        // 在 index 位置添加新的节点;
//        myLinkedList.addIndex(1,33);

//        // 删除关键字 key;
//        myLinkedList.remove(12);
//        System.out.println(myLinkedList.head);
//        myLinkedList.remove(34);
//        myLinkedList.addFirst(1);
//        myLinkedList.remove(1);


//        //删除所有的关键字 key;
//        myLinkedList.addFirst(12);
//        myLinkedList.addFirst(12);
//        myLinkedList.addFirst(12);
//        myLinkedList.addFirst(12);
//        myLinkedList.addFirst(13);
//        myLinkedList.removeAllKey(12);

//        // 清空链表;
//        myLinkedList.clear();

        LinkedList<Integer> list = new LinkedList<>();
        list.iterator();
        System.out.println("==================");
        myLinkedList.display();
        System.out.println("head:"+ myLinkedList.head + " // last:" + myLinkedList.last);
    }
}
