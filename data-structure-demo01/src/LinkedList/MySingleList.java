package LinkedList;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-16
 * Time:16:12
 */
// 自定义单向链表(MyLinkedList);
public class MySingleList implements IList {
    // 定义一个类来表示节点;
    static class ListNode {
        // 节点的值;
        public int val;
        // 节点的 next 域;
        public ListNode next;

        // 构造方法(参数为 val);
        public ListNode(int data) {
            this.val = data;
        }

        // 无参的构造方法;
        public ListNode() {

        }

        @Override
        public String toString() {
            return "[" + this.val + "]";
        }
    }

    // 定义一个链表的头节点;
    public ListNode head;

    @Override
    // 头插法
    public void addFirst(int data) {
        // 1.链表判空;
        if (head == null) {
            // 1.1 如果链表为空就让头节点指向当前 new 的节点;
            ListNode node = new ListNode(data);
            this.head = node;
            return;
        }
        // 2.链表不为空的情况;
        // 2.1 new 节点作为头节点;
        ListNode node = new ListNode(data);
        // 新增节点的 next 指向 head;
        node.next = this.head;
        // 更新头节点;
        this.head = node;
    }

    @Override
    // 尾插法;
    public void addLast(int data) {
        // 1.链表判空,单独处理;
        if (this.head == null) {
            // new 节点;
            ListNode node = new ListNode(data);
            this.head = node;
            return;
        }
        // 2.链表不为空;
        ListNode cur = this.head;
        // 2.1 遍历到最后一个节点停止;
        while (cur.next != null) {
            cur = cur.next;
        }
        // 2.2 这个时候 cur 指向最后一个节点;
        ListNode endNode = new ListNode(data);
        // 将当前节点的 next 变成 new 的节点;
        cur.next = endNode;
    }

    // 在 index 位置插入新的节点;
    @Override
    public void addIndex(int index, int data) {
        // 1.判断 index 是否合理;
        if (index < 0 || index > size()) {
            System.out.println("index不合理!");
            return;
        }
        // 2.链表判空;
        if (this.head == null) {
            return;
        }
        // 3.链表不为空,按照 index 分情况添加;
        if (index == 0) {
            // 3.1 如果 index == 0,等于头插法;
            addFirst(data);
        } else if (index == size()) {
            // 3.2 如果 index == size(),相当于尾插法;
            addLast(data);
        } else {
            // 3.3 表示是链表的中间部分的位置插入新的节点;
            // 定义一个节点 prev 去遍历找 index 位置的前置节点;
            ListNode prev = this.head;
            for (int i = 0; i < index - 1; i++) {
                prev = prev.next;
            }
            // 找到了 index 位置的前置节点 prev;
            // new 要新增的节点;
            ListNode toAddNode = new ListNode(data);
            // 表示 index 位置的下一个节点;
            ListNode nextNode = prev.next;
            // 新增节点的 next 是 index 的后一个节点;
            toAddNode.next = nextNode;
            // index 位置的前置节点的 next 更新为新增节点;
            prev.next = toAddNode;
        }
    }

    // 判断链表是否包含某个元素;
    @Override
    public boolean contains(int key) {
        // 1.链表判空;
        if (this.head == null) {
            System.out.println("链表为空,无法执行 contains() 操作!");
            return false;
        }
        // 链表不为空;
        // 2.头节点的 val 和 key 匹配单独处理;
        if (this.head.val == key) {
            return true;
        }
        // 3.new 一个节点表示 this.head;
        ListNode cur = this.head;
        while (cur != null) {
            // 如果当前节点的 val 和 给定的 key 匹配,则返回 true;
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        // 4.执行到链表末尾,没有匹配的节点,则返回 false;
        return false;
    }

    // 删除第一个 val 匹配的节点;
    @Override
    public void remove(int key) {
        // 1.链表判空;
        if (this.head == null) {
            return;
        }
        // 链表不为空;
        // 2.如果头节点的 val 和 给定的 key 匹配;
        if (this.head.val == key) {
            // 2.1 让头节点指向头节点的 next 域;
            // 即使只有头节点这一个节点,也无所谓,指向 null 也是正确的;
            this.head = this.head.next;
            return;
        }
        // 3.头节点的 val 不匹配,再去判断后续的节点;
        ListNode cur = this.head;
        // 这里的判断条件得是 cur.next != 而不是 cur !=,
        // 因为你走到末尾节点的时候,已经在前置节点判断过末尾节点的val是否匹配了;
        while (cur.next != null) {
            if (cur.next.val == key) {
                // 3.1 找到要删除节点;
                ListNode midNode = cur.next;
                // 3.2 要删除节点的前置节点(cur)的 next 变成要删除节点(cur)的 next;
                // 1 -> 2 -> 3 变成 1 -> 3;
                cur.next = midNode.next;
                // 这里记得return出去,否则这里刚好是删除最后一个节点,那么 cur.next 已经是null,
                // 不 return 的话后面的 cur = cur.next 就会报错;
                return;
            }
            cur = cur.next;
        }
    }

    // 删除所有 val 匹配的节点;
    @Override
    public void removeAllKey(int key) {
        // 1.链表判空;
        if (this.head == null) {
            return;
        }
        // 链表不为空;
        // 2.进行删除;
        ListNode prev = this.head;
        // 定义一个先锋节点,去找到 val 和 key 匹配的节点;
        ListNode cur = prev;
        while (cur.next != null) {
            // 首先让 cur 走到下一个节点去;
            cur = cur.next;
            // 判断当前 cur.val 和 key 是否匹配;
            if (cur.val == key) {
                // 匹配的话,prev.next 就需要更新为 cur.next,相当于调过中间的这个要删除的节点;
                prev.next = cur.next;
            } else {
                // 如果先锋节点的val 和 key 不匹配,则让 prev 走到 cur 的位置;
                prev = cur;
            }
        }
        // 3.如果头节点的 val 和 给定的 key 匹配;
        if (this.head.val == key) {
            // 2.1 让头节点指向头节点的 next 域;
            // 即使只有头节点这一个节点,也无所谓,指向 null 也是正确的;
            this.head = this.head.next;
        }
    }

    // 链表的大小;
    @Override
    public int size() {
        // 1.链表判空;
        if (this.head == null) {
            System.out.println("链表为空,无法执行 size() 操作!");
            return -1;
        }
        // 2.new 一个节点的临时变量;
        ListNode cur = this.head;
        // 3.定义一个变量来表示size的返回值;
        int listSize = 0;
        while (cur != null) {
            // 3.遍历链表;
            cur = cur.next;
            // 3.size进行自增;
            listSize++;
        }
        return listSize;
    }

    @Override
    public void clear() {
//        // 1: 直接把 hear 置为 null, 没有引用之后,这个链表就会回收;
//        this.head = null;
        // 2: 每个节点循环置空;
        // 1.链表判空;
        if (this.head == null) {
            return;
        }
        // 2.循环置空;
        ListNode cur = this.head;
        ListNode newCur = cur;
        while (cur != null) {
            newCur = cur;
            cur = cur.next;
            newCur.next = null;
        }
        // 3.到这里,虽然 this.head.next 为 null, 但是头节点的引用还在,所以在打印的时候,这个头节点不是为空,还是能打印
        // 所以这里再将头节点置为空;
        this.head = null;
    }

    @Override
    public void display() {
        // 1.链表判空;
        if (this.head == null) {
            System.out.println("链表为空,无法执行 display() 操作!");
            return;
        }
        // 2.定义一个 head 的临时变量;
        ListNode cur = this.head;
        // 3.循环打印;
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        // 换行;
        System.out.println();
    }

    // 给定节点作为头节点,打印链表;
    public void display(ListNode newHead) {
        // 1.链表判空;
        if (newHead == null) {
            System.out.println("链表为空,无法执行 display() 操作!");
            return;
        }
        // 2.定义一个 head 的临时变量;
        ListNode cur = newHead;
        // 3.循环打印;
        while (cur != null) {
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        // 换行;
        System.out.println();
    }

    @Override
    public String toString() {
        ListNode cur = this.head;
        StringBuilder sbd = new StringBuilder("[");
        while (cur != null) {
            sbd.append(cur.val);
            if (cur.next != null){
                sbd.append(", ");
            }
            cur = cur.next;
        }
        sbd.append("]");
        return sbd.toString();
    }

    // 反转链表;
    public void reverseList() {
        // 1.如果链表为空,或者链表只有头节点这一个节点,直接 return;
        if (this.head == null || this.head.next == null) {
            return;
        }
        // 2.这个时候至少是有两个节点,直接让 cur 指向第二个节点(也就是 head.next);
        ListNode cur = this.head.next;
        // 反转之后头节点就相当于最后一个节点了,直接将头节点的 next 置为空;
        this.head.next = null;
        while (cur != null) {
            ListNode curNext = cur.next;
            cur.next = head;
            head = cur;
            cur = curNext;
        }
    }

    // 返回链表的中间节点;
    public ListNode backMid() {
        // 1.判断链表;
        if (head == null || this.head.next == null) {
            return head;
        }
        // 2.定义快慢指针;
        ListNode quickCur = this.head;
        ListNode slowCur = this.head;
        // 进行判断,因为链表节点数分为偶数个和奇数个;
        while (quickCur != null && quickCur.next != null) {
            // 快慢指针开始往后走;
            slowCur = slowCur.next;
            quickCur = quickCur.next.next;
        }
        // 慢指针的位置就是中间节点,返回慢指针;
        return slowCur;
    }

    // 返回链表的第 K 个节点;
    public ListNode backKthNode(int k) {
        // 1.链表判空 / 参数判断是是否合理;
        if (this.head == null) {
            return null;
        } else if (k > size() || k <= 0) {
            System.out.println("给定的参数 k 不合理!");
            return null;
        }
        // 链表不为空;
        // 2.定义快慢指针;
        ListNode fast = this.head;
        ListNode slow = this.head;
        // 3.快指针走 k - 1;
        while (k - 1 > 0) {
            fast = fast.next;
            k--;
        }
        // 4.快慢指针一起走;
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        // 5.返回这个节点;
        return slow;
    }

    // 链表删除第k个节点;
    public void removeKthNode(int k) {
        // 1.链表判空;
        if (this.head == null) {
            return;
        } else if(k > size() || k <= 0) {
            System.out.println("给定的参数 k 不合理!");
            return;
        }
        // 链表不为空且参数合理;
        // 2.定义快慢指针;
        ListNode fast = this.head;
        ListNode slow = this.head;
        // 3.快指针走 k 步;
        while (k > 0) {
            fast = fast.next;
            k--;
        }
        // 3.1 如果这个时候,fast还是为空,证明要删除的节点是头节点;
        // 3.1.1 不管是链表只有一个节点,fast 走 1 直接为 null;
        // 3.1.2 还是链表有多个节点,但是要走 k 步,本质上是 k = size(),所以 fast 走空;
        // 不管是哪个情况,本质上都是删除头节点的时候 fast 走空;
        // 直接 head 指向下一个节点就行;
        if (fast == null) {
            // 头节点变换位置;
            this.head = head.next;
            // 记得 return 出去;
            return;
        }
        // 4.两个指针一起遍历,slow指针停止的位置就是要删除节点的前置节点;
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        // 5.让 slow 指向下下个节点,相当于删除中间的节点;
        slow.next = slow.next.next;
    }

    // 组合两个升序链表,组合链表也得是升序的;
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        // 1.如果其中任意一个链表为空,直接 return 另一个;
        if (list1 == null) {
            return list2;
        } else if (list2 == null) {
            return list1;
        }
        // 两个链表都不为空,定义新的节点作为头节点,来拼接节点;
        ListNode newHead = new ListNode(-1);
        ListNode newCur = newHead;
        // 定义两个链表的头节点;
        ListNode cur1 = list1;
        ListNode cur2 = list2;
        // 2.开始按顺序拼接,如果其中一个链表为空了,跳出循环;
        while (cur1 != null && cur2 != null) {
            if (cur1.val < cur2.val) {
                newCur.next = cur1;
                newCur = newCur.next;
                cur1 = cur1.next;
            } else {
                newCur.next = cur2;
                newCur = newCur.next;
                cur2 = cur2.next;
            }
        }
        // 3.其中一个链表为空,直接把另外一个有序链表全部拼接过来;
        if (cur1 != null) {
            newCur.next = cur1;
        } else {
            newCur.next = cur2;
        }
        return newHead.next;
    }

    // 判断链表是否是回文的;
    // 给定链表,判断是否是回文的;
    public boolean isPalindrome(ListNode A) {
        // write code here
        // 1.链表判空;
        if (A == null || A.next == null) {
            return true;
        }
        ListNode fast = A;
        ListNode slow = A;
        // 2.找到中间节点;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode cur = slow.next;
        ListNode newCur = cur;
        // 3.从中间节点开始往后改变链表指向(反过来);
        while (cur != null) {
            newCur = cur.next;
            cur.next = slow;
            slow = cur;
            cur = newCur;
        }
        // 4.从两端往中间遍历,判断值是否相等;
        // 这里得采用 slow 而不是 fast,因为链表是偶数的话,fast 就是走空的;
        // 还有就是判断 A.next != slow 而不是 slow.next != A,
        // 因为还是偶数的情况下, -> 的符号会多一个,而 <- 则少一个;
        // slow 就无法进行 .next操作;
        while (A != slow && A.next != slow) {
            if(A.val != slow.val) {
                return false;
            }
            A = A.next;
            slow = slow.next;
        }
        // 5.前面没有 return 出去,说明链表一定是回文结构的,直接 return true;
        return true;
    }

    // 判断两个链表是否是相交链表,返回相交的节点;
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        // 1.判断两个链表是否为空;
        if (headA == null || headB == null) {
            return null;
        }
        if (headA == headB) {
            return headA;
        }
        // 链表不为空;
        int sizeA = 0;
        int sizeB = 0;
        ListNode curA = headA;
        ListNode curB = headB;
        // 2.获取两个链表的大小;
        while(curA != null) {
            curA = curA.next;
            sizeA++;
        }
        while(curB != null) {
            curB = curB.next;
            sizeB++;
        }
        // 3.算出两个链表大小的差值;
        int diff = sizeA - sizeB;
        // 4.让大的链表走差值步;
        // 4.1 首先得让 curA 和 curB 重新回到头节点;
        curA = headA;
        curB = headB;
        if (diff > 0) {
            while (diff > 0) {
                curA = curA.next;
                diff --;
            }
        } else {
            diff = sizeB - sizeA;
            while(diff > 0) {
                curB = curB.next;
                diff --;
            }
        }
        // 5.两个链表同时往后走,如果其中一个节点相同就说明两个链表相交,返回这个节点;
        while (curA != null) {
            if (curA == curB) {
                return curA;
            }
            curA = curA.next;
            curB = curB.next;
        }
        return null;
    }

    // 给定链表,给定 x,按照 x 的大小分割链表;
    public ListNode partition(ListNode pHead, int x) {
        // write code here
        // 1.链表判空;
        if (pHead == null) {
            // 1.1 链表为空,直接 return 出去;
            return null;
        }
        // 链表不为空的情况;
        // 2.定义小于 x 部分的头尾节点和大于 x 部分的头尾节点;
        ListNode bs = null;
        ListNode be = null;
        ListNode as = null;
        ListNode ae = null;
        // 定义节点指向参数中给定的链表;
        ListNode cur = pHead;
        // 3.开始拼接两个链表;
        while (cur != null) {
            // 3.1 小于 x 部分的拼接;
            if (cur.val < x) {
                // 判断是否bs是否为空,为空证明还没有添加第一个节点;
                if (bs == null) {
                    bs = cur;
                    be = cur;
                } else {
                    be.next = cur;
                    be = be.next;
                }

                // 3.2大于 x 部分的拼接;
            } else {
                // 判断 as 是否为空,为空表示还没有添加第一个元素;
                if (as == null) {
                    as = cur;
                    ae = cur;
                } else {
                    ae.next = cur;
                    ae = ae.next;
                }
            }
            cur = cur.next;
        }
        // 4.最后一个节点的 next 置空;
        if (be == null) {
            return as;
        }
        // 5.把两个链表拼接到一起;
        be.next = as;
        // 将尾节点的 next 置为空;
        if (as != null) {
            ae.next = null;
        }
        // 6.返回头节点;
        return bs;
    }

    // 判断链表是否存在环形;
    public boolean hasCycle(ListNode newHead) {
        // 1.链表判空;
        if (newHead == null) {
            return false;
        }
        // 链表不为空;
        // 2.1定义快慢指针;
        ListNode fast = newHead;
        ListNode slow = newHead;
        // 2.2 快慢指针开始进行追击;
        // 链表只要是有环形,就一定能追击到;
        // 如果中间没有 return 出去,说明这个链表一定存在尾节点,直接 return false 就行;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
//            // 2.3 如果中间 fast 为空了,直接 return 出去,防止后续 fast.next的判断出现空指针异常;
//            if (fast == null) {
//                return false;
//            }
//            // 2.4 判断链表是否有环形;
//            if(fast == slow || fast.next == slow) {
//                return true;
//            }

            // 其实两步追击一步,中间不存在跳过的问题,所以也就没有必要针对 fast.next == slow 专门进行判断;
            if (fast == slow) {
                return true;
            }
        }
        // 3.走到这里,说明fast一定是走到空了,所以直接入额 return false;
        return false;
    }

    // 判断链表的环形的第一个节点;
    public ListNode detectCycle(ListNode head) {
        // 1.链表判空;
        if (head == null) {
            return null;
        }
        // 链表非空;
        // 当快慢指针相遇的时候,这个相遇节点到入环的节点和head到入环的节点的距离是一样的;
        // 2.1 定义快慢指针;
        ListNode fast = head;
        ListNode slow = head;
        // 2.2 寻找相遇点;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
            if (fast == slow) {
                break;
            }
        }
        // 3.判断跳出循环的原因;
        // 3.1 判断是不是链表没有环形,导致跳出循环;
        if (fast == null || fast.next == null) {
            return null;
        }
        // fast 不为空,fast 和 slow 相遇;
        // 4.定义节点表示 head, 从 head 以及 fast 的位置开始遍历;
        ListNode cur = head;
        while (cur != fast) {
            cur = cur.next;
            fast = fast.next;
        }
        // 说明 fast 和 cur相遇了,这个节点就是链表的环形的第一个节点;
        // 5.返回这个节点;
        return cur;
    }
}
