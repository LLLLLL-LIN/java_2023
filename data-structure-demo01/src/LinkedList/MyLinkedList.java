package LinkedList;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-23
 * Time:10:21
 */
// 自定义双向链表(MyLinkedList);
public class MyLinkedList implements IList {
    // 定义一个类来表示节点;
    static class ListNode {
        // 节点的 val 域;
        public int val;
        // 节点的 next 域;
        public MyLinkedList.ListNode next;
        // 节点的 prev 域;
        public MyLinkedList.ListNode prev;

        // 构造方法(参数为 val);
        public ListNode(int data) {
            this.val = data;
        }

        // 无参的构造方法;
        public ListNode() {

        }

        // 重写 toString();
        @Override
        public String toString() {
            return "[" + this.val + "]";
        }
    }

    // 定义一个链表的头节点;
    public MyLinkedList.ListNode head;
    // 定义一个链表的尾节点;
    public MyLinkedList.ListNode last;

    // 双向量表的头插法;
    @Override
    public void addFirst(int data) {
        // 1.链表判空;
        if (head == null) {
            ListNode node = new ListNode(data);
            // 没有节点,说明新增节点就是头尾节点;
            head = node;
            last = node;
        } else {
            // 链表不为空;
            ListNode node = new ListNode(data);
            // 2.new 节点之后,开始绑定到链表中;
            node.next = this.head;
            // 3.更新 head 的 prev;
            this.head.prev = node;
            // 4.头节点换位置;
            this.head = node;
        }

    }

    // 双向链表的尾插法;
    @Override
    public void addLast(int data) {
        // 1.链表判空;
        if (head == null) {
            ListNode node = new ListNode(data);
            // 链表为空,新增的节点就是唯一节点,代表头节点和尾节点;
            head = node;
            last = node;
        } else {
            ListNode node = new ListNode(data);
            // 2.node 节点尾插到 last 的后面;
            this.last.next = node;
            // 3.更新 node 的 prev;
            node.prev = this.last;
            // 4.last 更新位置;
            this.last = node;
        }
    }

    // 双向链表的额在 index 位置添加新的节点;
    @Override
    public void addIndex(int index, int data) {
        // 1.判断 index 合不合理;
        if (index < 0 || index > size()) {
            return;
        }
        // index 刚好等于 size(),说明是尾插法,得更新 last;
        if (index == size()) {
            addLast(data);
            return;
        }
        // index 刚好等于 0,说明是头插法,得更新 head;
        if (index == 0) {
            addFirst(data);
            return;
        }
        // 2.让 cur 走到 index 的前一个节点;
        ListNode cur = this.head;
        while (index - 1 > 0) {
            cur = cur.next;
            index--;
        }
        // 定义节点记住 cur.next;
        ListNode curNext = cur.next;
        // 3.在 index 位置插入新的节点;
        ListNode toPut = new ListNode(data);
        cur.next = toPut;
        toPut.next = curNext;
    }

    // 双向链表的判断是否包含某个关键字 key;
    @Override
    public boolean contains(int key) {
        // 1.链表判空;
        if (this.head == null) {
            return false;
        }
        // 链表不为空;
        // 2.循环判断;
        ListNode cur = this.head;
        int size = 0;
        while (cur != null) {
            // 2.1 如果节点的值和 key 相等,直接 return true;
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        // 3.循环走完没有 return 出去,说明不存在这个 key;
        return false;
    }

    //删除第一次出现关键字为key的节点
    @Override
    public void remove(int key) {
        // 1.链表判空;
        if (head == null) {
            return;
        }
        // 链表不为空;
        // 2.遍历链表,查找第一个 val域 匹配的节点;
        ListNode cur = this.head;
        while (cur != null) {
            if (cur.val == key) {
                // 3.头结点单独处理;
                if (this.head == cur) {
                    //3.1 更新 head 的位置;
                    this.head = this.head.next;
                    // 如果只有一个节点的时候,head.next 为空了,更新位置的 head 将无法执行.prev操作;
                    // 这里执行以下对更新位置的 head 进行非空判断;
                    if (this.head == null) {
                        // 执行到这里,说明整个链表就只有一个节点,这个节点要被删了,就需要把 last 也置为 null;
                        this.last = null;
                        return;
                    }
                    // 将新的头节点的 prev域 置空;
                    this.head.prev = null;
                } else {
                    // 4.不是头节点;说明是中间节点或者,尾节点;
                    // 按照顺序得先删中间节点,所以尾节点不能在此之前单独拎出来特殊处理;
                    cur.prev.next = cur.next;
                    // 4.1 如果是要删除的是尾节点;
                    if (cur == this.last) {
                        // 更新尾节点的位置;
                        this.last = cur.prev;
                        // 新的尾节点的 next 置为 null;
                        this.last.next = null;
                    } else {
                        // 4.2 不是尾节点;
                        cur.next.prev = cur.prev;
                    }
                }
                return;
            } else {
                // 所以这里和删除节点是二选一,选择 if else 语句;
                cur = cur.next;
            }
        }
    }

    // 双向链表的删除所有关键字;
    @Override
    public void removeAllKey(int key) {
        // 1.链表判空;
        if (head == null) {
            return;
        }
        // 链表不为空;
        // 2.开始循环判断;
        ListNode cur = this.head;
        while (cur != null) {
            if (cur.val == key) {
                // 3.头结点单独处理;
                if (this.head == cur) {
                    //3.1 更新 head 的位置;
                    this.head = this.head.next;
                    // 如果只有一个节点的时候,head.next 为空了,更新位置的 head 将无法执行.prev操作;
                    // 这里执行以下对更新位置的 head 进行非空判断;
                    if (this.head == null) {
                        // 执行到这里,说明整个链表就只有一个节点,这个节点要被删了,就需要把 last 也置为 null;
                        this.last = null;
                        return;
                    }
                    // 将新的头节点的 prev域 置空;
                    this.head.prev = null;
                } else {
                    // 4.不是头节点;说明是中间节点或者,尾节点;
                    // 按照顺序得先删中间节点,所以尾节点不能在此之前单独拎出来特殊处理;
                    cur.prev.next = cur.next;
                    // 4.1 如果是要删除的是尾节点;
                    if (cur == this.last) {
                        // 更新尾节点的位置;
                        this.last = cur.prev;
                        // 新的尾节点的 next 置为 null;
                        this.last.next = null;
                    } else {
                        // 4.2 不是尾节点;
                        cur.next.prev = cur.prev;
                    }
                }
            }
            cur = cur.next;
        }
    }

    // 双向链表的大小;
    @Override
    public int size() {
        // 1.链表判空;
        if (this.head == null) {
            return 0;
        }
        // 链表不为空;
        // 2.遍历链表;
        ListNode cur = this.head;
        int size = 0;
        while (cur != null) {
            // 链表大小自增;
            size++;
            cur = cur.next;
        }
        // 3.返回链表大小;
        return size;
    }

    // 双向链表的清空;
    @Override
    public void clear() {
        // 1.链表判空;
        if (this.head == null) {
            return;
        }
        // 链表不为空;
        ListNode cur = this.head;
        // 2.循环置空,每个节点的 next 和 prev 都置为空;
        while (cur != null) {
            ListNode node = cur;
            cur = cur.next;
            node.prev = null;
            node.next = null;
        }
        this.head = null;
        this.last = null;
    }

    // 双向链表的打印;
    @Override
    public void display() {
        // 1.链表判空;
        if (this.head == null) {
            return;
        }
        // 链表不为空;
        // 2.循环打印;
        ListNode cur = this.head;
        while (cur != null) {
            // 打印节点的 val;
            System.out.print(cur.val + " ");
            cur = cur.next;
        }
        // 换行;
        System.out.println();
    }
}
