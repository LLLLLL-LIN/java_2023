package LinkedList;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-16
 * Time:16:14
 */
public class TestMySingleList {
    public static void main(String[] args) {
        MySingleList mySingleList1 = new MySingleList();
        MySingleList.ListNode Listnode1 = new MySingleList.ListNode(12);
        MySingleList.ListNode Listnode2 = new MySingleList.ListNode(23);
        MySingleList.ListNode Listnode3 = new MySingleList.ListNode(34);
        MySingleList.ListNode Listnode4 = new MySingleList.ListNode(23);
        MySingleList.ListNode Listnode5 = new MySingleList.ListNode(12);

        Listnode1.next = Listnode2;
        Listnode2.next = Listnode3;
        Listnode3.next = Listnode4;
        Listnode4.next = Listnode5;

        mySingleList1.head = Listnode1;

        mySingleList1.display();
        System.out.println("==============================");

//        // 头插法;
//        mySingleList1.addLast(1);
//        mySingleList1.addLast(2);
//        mySingleList1.addLast(3);

//        // 尾插法;
//        mySingleList1.addFirst(1);
//        mySingleList1.addFirst(2);
//        mySingleList1.addFirst(3);

//        // 指定位置 index,插入新的节点;
//        mySingleList1.addIndex(1, 11);
//        mySingleList1.addIndex(0, 100);
//        mySingleList1.addIndex(7, 108);

//        // 判断链表是否包含某个元素(val);
//        System.out.println(mySingleList1.contains(12));
//        System.out.println(mySingleList1.contains(34));
//        System.out.println(mySingleList1.contains(33));
//        System.out.println(mySingleList1.contains(56));

//        // 删除第一个 val = key 的节点;
//        mySingleList1.addFirst(56);
//        mySingleList1.remove(12);
//        mySingleList1.remove(34);
//        mySingleList1.remove(56);

//        // 打印链表的大小;
//        System.out.println(mySingleList1.size());
//        mySingleList1.addFirst(21);
//        System.out.println(mySingleList1.size());
//        mySingleList1.addLast(66);
//        System.out.println(mySingleList1.size());

//        // 链表清空;
//        mySingleList1.clear();

//        // 删除所有 key == val 的节点;
//        mySingleList1.addFirst(34);
//        mySingleList1.addIndex(2,34);
//        mySingleList1.addIndex(3,34);
//        mySingleList1.addLast(34);
//        mySingleList1.display();
//        mySingleList1.removeAllKey(34);

//        // 反转链表;
//        mySingleList1.reverseList();

//        // 链表的中间节点;
//        mySingleList1.addFirst(10);
//        mySingleList1.addFirst(11);
//        mySingleList1.addLast(66);
//        mySingleList1.addLast(67);
//        System.out.println(mySingleList1.backMid());

//        // 返回第k个节点;
//        System.out.println(mySingleList1.backKthNode(3));

//        // 删除第k个节点;
//        mySingleList1.removeKthNode(5);

//        // 判断链表是否是回文结构;
//        System.out.println(mySingleList1.isPalindrome(mySingleList1.head));

//        // 分割链表;
//        MySingleList.ListNode xListNode = mySingleList1.partition(mySingleList1.head,22);
//        mySingleList1.display(xListNode);

        // 检查工具类的 toString 方法;
        System.out.println(mySingleList1.toString());

        mySingleList1.display();

    }
}
