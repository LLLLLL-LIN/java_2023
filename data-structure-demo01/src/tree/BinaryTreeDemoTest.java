package tree;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-25
 * Time:16:26
 */
public class BinaryTreeDemoTest {
    public static void main(String[] args) {
        BinaryTreeDemo binaryTree = new BinaryTreeDemo();
        BinaryTreeDemo.TreeNode root = binaryTree.createTree();
//        // 二叉树的前序遍历;
//        binaryTree.preOrder(root);
//        System.out.println();
//        // 二叉树的中序遍历;
//        binaryTree.inOrder(root);
//        System.out.println();
//        // 二叉树的后序遍历;
//        binaryTree.postOrder(root);
//        System.out.println();

//        // 二叉树前序遍历的 List 结果;
//        List<Character> characterList = binaryTree.preorderTraversal(root);
//        List<Character> list = new LinkedList<>();
//        List<Character> characterList = binaryTree.preorderTraversal(root, list);
//        System.out.println(characterList);

//        // 二叉树大小;
//        System.out.println(binaryTree.size(root));
//        System.out.println(binaryTree.size(root));
//
//        System.out.println(binaryTree.size2(root));
//        System.out.println(binaryTree.size2(root));
//
//        // 获取二叉树叶子节点的个数;
//        System.out.println("叶子节点的个数: "binaryTree.getLeafTreeNodeCount(root));

//        // 获取第 k 层的节点个数;
//        System.out.println("第 k 层的节点个数: " + binaryTree.getKLevelTreeNodeCount(root, 1));

//        // 获取数的高度;
//        System.out.println("树的高度: " + binaryTree.getHeight(root));
        
//        // 判断树中是否存在 val = key 的节点;
//        System.out.println(binaryTree.find(root, 'D').val);
//        System.out.println(binaryTree.find(root, 'K').val);

//        // 树的翻转;
//        binaryTree.preOrder(root);
//        binaryTree.invertTree(root);
//        System.out.println("换行");
//        binaryTree.preOrder(root);

//        // 输入前序遍历字符串构造二叉树(空为 # 如: 输入: abc##de#g##f### 输出: c b e g d f a );
//        Scanner in = new Scanner(System.in);
//        // 循环读取字符串;
//        while (in.hasNextLine()) {
//            String str = in.nextLine();
//            BinaryTreeDemo.TreeNode newRoot = BinaryTreeDemo.createTree(str);
//            binaryTree.inOrder(newRoot);
//        }

//        // 层序打印;
//        System.out.print("层序遍历1: ");
//        binaryTree.levelOrder(root);
//        // 换行;
//        System.out.println();
//        System.out.print("层序遍历2: ");
//        List<List<Character>> ret = binaryTree.levelOrder2(root);
//        System.out.println(ret);

//        // 判断是否是完全二叉树;
//        System.out.println(binaryTree.isCompleteTree(root));

//        // 二叉树非递归的前序遍历;
//        binaryTree.preOrder(root);
//        System.out.println();
//        System.out.println(binaryTree.preorderTraversal(root));
//        binaryTree.preorderNot(root);

//        // 二叉树非递归的前序遍历;
//        binaryTree.inOrder(root);
//        System.out.println();
//        System.out.println(binaryTree.inorderTraversal(root));
//        binaryTree.inorderNot(root);

        // 二叉树非递归的前序遍历;
        binaryTree.postOrder(root);
        System.out.println();
        System.out.println(binaryTree.postorderTraversal(root));
        binaryTree.postorderNot(root);
    }
}
