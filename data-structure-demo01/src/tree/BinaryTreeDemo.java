package tree;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-25
 * Time:16:18
 */
// 自定义二叉树;
public class BinaryTreeDemo {
    static class TreeNode {
        char val;
        TreeNode left;
        TreeNode right;

        public TreeNode(char val) {
            this.val = val;
        }
    }

    // 创建二叉树;
    public TreeNode createTree() {
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');

        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;

        return A;
    }

    // 前序遍历;
    public void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    // 中序遍历;
    public void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.val + " ");
        inOrder(root.right);
    }

    // 后序遍历;
    public void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.val + " ");
    }

    /**
     * leecode
     * 144. 二叉树的前序遍历;
     * 给你二叉树的根节点 root ，返回它节点值的 前序 遍历。
     * 示例 1:
     * 输入：root = [1,null,2,3]
     * 输出：[1,2,3]
     */
    // 1 采用拼接;
    public List<Character> preorderTraversal(TreeNode root) {
        List<Character> list = new LinkedList<>();
        if (root == null) {
            return list;
        }
        list.add(root.val);
        List<Character> leftList = preorderTraversal(root.left);
        list.addAll(leftList);
        List<Character> rightList = preorderTraversal(root.right);
        list.addAll(rightList);
        return list;
    }
//    // 2.因为递归,变量一直是一个方法一个变量,每个 list 都是存储一个节点的 val,所以无法统一到一起,直接让 list 跟着走;
//    public List<Character> preorderTraversal(TreeNode root, List<Character> list) {
//        if (root == null) {
//            return list;
//        }
//        list.add(root.val);
//        list = preorderTraversal(root.left, list);
//        list = preorderTraversal(root.right, list);
//        return list;
//    }
//    // 3.直接定义全局变量,所有递归都是存储在一个 list;
//    List<Character> list = new LinkedList<>();
//    public List<Character> preorderTraversal(TreeNode root) {
//        if (root == null) {
//            return list;
//        }
//        newPreOrder(root);
//        return list;
//    }
//    // 前序遍历;
//    public void newPreOrder(TreeNode root) {
//        if (root == null) {
//            return;
//        }
//        list.add(root.val);
//        newPreOrder(root.left);
//        newPreOrder(root.right);
//    }

    /**
     * leecode
     * 94. 二叉树的中序遍历;
     * 给定一个二叉树的根节点 root ，返回 它的 中序 遍历 。
     * 示例 1：
     * 输入：root = [1,null,2,3]
     * 输出：[1,3,2]
     * 示例 2：
     * 输入：root = []
     * 输出：[]
     * 示例 3：
     * 输入：root = [1]
     * 输出：[1]
     */
    public List<Character> inorderTraversal(TreeNode root) {
        List<Character> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        List<Character> leftList = inorderTraversal(root.left);
        list.addAll(leftList);
        list.add(root.val);
        List<Character> rightList = inorderTraversal(root.right);
        list.addAll(rightList);
        return list;
    }


    /**
     * leecode
     * 145. 二叉树的后序遍历;
     * 给你一棵二叉树的根节点 root ，返回其节点值的 后序遍历;
     * 示例 1:
     * 输入：root = [1,null,2,3]
     * 输出：[3,2,1]
     * 示例 2：
     * 输入：root = []
     * 输出：[]
     * 示例 3：
     * 输入：root = [1]
     * 输出：[1]
     */
    public List<Character> postorderTraversal(TreeNode root) {
        List<Character> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        List<Character> leftList = postorderTraversal(root.left);
        list.addAll(leftList);
        List<Character> rightList = postorderTraversal(root.right);
        list.addAll(rightList);
        list.add(root.val);
        return list;
    }

    /**
     * newcode;
     * KY11 二叉树遍历;
     * 描述:
     * 编一个程序，读入用户输入的一串先序遍历字符串，根据此字符串建立一个二叉树（以指针方式存储）。
     * 例如如下的先序遍历字符串： ABC##DE#G##F### 其中“#”表示的是空格，空格字符代表空树。建立起此二叉树以后，再对二叉树进行中序遍历，输出遍历结果。
     * 输入描述：
     * 输入包括1行字符串，长度不超过100。
     * 输出描述：
     * 可能有多组测试数据，对于每组数据， 输出将输入字符串建立二叉树后中序遍历的序列，每个字符后面都有一个空格。 每个输出结果占一行。
     * <p>
     * 示例 1:
     * 输入：abc##de#g##f###
     * 输出：c b e g d f a
     */
    // 根据前序遍历字符串来构造二叉树;
    static int i;

    public static TreeNode createTree(String str) {
        TreeNode root = null;
        // 1.判断是否是 #,不同处理;
        if (str.charAt(i) == '#') {
            i++;
        } else {
            root = new TreeNode(str.charAt(i));
            i++;
            root.left = createTree(str);
            root.right = createTree(str);
        }
        // 返回构建好的二叉树,用来在 main 方法中调用遍历打印的方法;
        return root;
    }

    /**
     * leecode
     * 110. 平衡二叉树;
     * 给定一个二叉树，判断它是否是平衡二叉树;
     * <p>
     * 示例 1:
     * 输入：root = [3,9,20,null,null,15,7]
     * 输出：true
     */
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        return maxDepth(root) >= 1;
    }

    int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftHigh = maxDepth(root.left);
        int rightHigh = maxDepth(root.right);
        if (leftHigh >= 0 && rightHigh >= 0 && Math.abs(leftHigh - rightHigh) <= 1) {
            return Math.max(leftHigh, rightHigh) + 1;
        } else {
            return -1;
        }
    }

    /**
     * leecode
     * 100. 相同的树;
     * 给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。
     * 如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
     * 示例 1:
     * 输入：p = [1,2,3], q = [1,2,3]
     * 输出：true
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        // 1.两个树都为空的情况下,为相同;
        if (p == null && q == null) {
            return true;
        }
        // 2.走到这里说明至少有一个不为空,再次删除掉其中一个树为空的情况;
        if (p == null || q == null) {
            return false;
        }
        // 3.如果树的节点位置都能匹配上,那么就看节点的值是否相等;
        if (p.val != q.val) {
            return false;
        }
        // 4.开始递归,因为两个语句返回值都是 boolean,直接并就行,只要在其中一个环节是 false,返回的就是 false;
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    /**
     * leecode
     * 572. 另一棵树的子树;
     * 给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。
     * 如果存在，返回 true ；否则，返回 false 。
     * 二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。
     * 示例 1:
     * 输入：root = [3,4,5,1,2], subRoot = [4,1,2]
     * 输出：true
     */
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        // 1.两个树都为空的情况下,为相同;
        if (root == null) {
            return false;
        }
        // 调用 isSameTree()来递归判断是不是子树;
        if (isSameTree(root, subRoot)) {
            return true;
        }
        // 这个直接让 root 递归往后走,每个 root 的节点都和 subRoot 的节点判断一下;
        return isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    /**
     * leecode
     * 101. 对称二叉树;
     * 给你一个二叉树的根节点 root ， 检查它是否轴对称。
     * 示例 1:
     * 输入：root = [1,2,2,3,4,4,3]
     * 输出：true
     */
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSameTree2(root.left, root.right);
    }

    boolean isSameTree2(TreeNode p, TreeNode q) {
        // 1.两个树都为空的情况下,为相同;
        if (p == null && q == null) {
            return true;
        }
        // 2.走到这里说明至少有一个不为空,再次删除掉其中一个树为空的情况;
        if (p == null || q == null) {
            return false;
        }
        // 3.如果树的节点位置都能匹配上,那么就看节点的值是否相等;
        if (p.val != q.val) {
            return false;
        }
        // 4.开始递归,因为两个语句返回值都是 boolean,直接并就行,只要在其中一个环节是 false,返回的就是 false;
        // 和前面那个 isSameTree() 稍微区别,这里问的是不是对称数,所有判断左右节点是反过来的;
        return isSameTree2(p.left, q.right) && isSameTree2(p.right, q.left);
    }

    /**
     * leecode
     * 226. 翻转二叉树;
     * 给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。
     * 示例 1:
     * 输入：root = [4,2,7,1,3,6,9]
     * 输出：[4,7,2,9,6,3,1]
     */
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        // 相当于前序遍历改打印为交换节点的 left 和 right;
        // 一定得是先序遍历;得先把左右子树换了之后,才能让原本在左边的元素到右边,让链表反转;
        // 否则左边的子树的翻转总是在左边反转,不能翻转到右边;
        TreeNode leftRoot = root.left;
        root.left = root.right;
        root.right = leftRoot;
        invertTree(root.left);
        invertTree(root.right);
        // 返回翻转树的 root;
        return root;
    }

    /**
     * leecode
     * 获取树中节点的个数
     */
    int nodeSize;

    int size(TreeNode root) {
        if (root == null) {
            return 0;
        }
        nodeSize++;
        size(root.left);
        size(root.right);
        return nodeSize;
    }

    /**
     * leecode
     * 获取树中节点的个数
     */
    int size2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        // +1相当于保证左右节点没有的情况下,至少本身一个
        return size2(root.left) + size2(root.right) + 1;
    }

    /**
     * leecode
     * 获取叶子节点的个数
     */
    int getLeafTreeNodeCount(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        // 递归,每次递归都是判断某一个节点是否有叶子节点;
        return getLeafTreeNodeCount(root.left) + getLeafTreeNodeCount(root.right);
    }

    /**
     * leecode
     * 子问题思路-求叶子结点个数
     * 获取第K层节点的个数
     */
    int getKLevelTreeNodeCount(TreeNode root, int k) {
        if (root == null) {
            return -1;
        }
        // 第一层只要是 root 非空,那就是一个节点,这里特殊处理;
        if (k == 1) {
            return 1;
        }
//        // 找到 k - 1 这个层次,判断左右节点是否存在,返回不同的个数;
//        if (k == 2) {
//            if (root.left == null && root.right == null) {
//                return 0;
//            } else if (root.left == null || root.right == null) {
//                return 1;
//            } else {
//                return 2;
//            }
//        }
//        // 第一层只要是 root 非空,那就是一个节点,这里特殊处理;
//        if (k == 1) {
//            return 1;
//        }
        return getKLevelTreeNodeCount(root.left, k - 1) + getKLevelTreeNodeCount(root.right, k - 1);
    }


    /**
     * leecode
     * 获取二叉树的高度
     */
    int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
//        return (Math.max(getHeight(root.left), getHeight(root.right))) + 1;
        return ((getHeight(root.left) > getHeight(root.right)) ? (getHeight(root.left) + 1) : (getHeight(root.right) + 1));
    }

    /**
     * leecode
     * 检测值为 value 的元素是否存在
     */
    TreeNode find(TreeNode root, char val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        TreeNode leftNode = find(root.left, val);
        if (leftNode != null) {
            return leftNode;
        }
        TreeNode rightNode = find(root.right, val);
        if (rightNode != null) {
            return rightNode;
        }
        return null;
    }

    /**
     * 层序遍历;
     */
    public void levelOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            System.out.print(cur.val + " ");
            if (cur.left != null) {
                queue.offer(cur.left);
            }
            if (cur.right != null) {
                queue.offer(cur.right);
            }
        }
    }

    /**
     * 102. 二叉树的层序遍历
     * 层序遍历 2;
     * 返回值不一样;
     * 给你二叉树的根节点 root ，返回其节点值的 层序遍历 。 （即逐层地，从左到右访问所有节点）。
     * 实例 1:
     * 输入：root = [3,9,20,null,null,15,7]
     * 输出：[[3],[9,20],[15,7]]
     */
    public List<List<Character>> levelOrder2(TreeNode root) {
        List<List<Character>> ret = new LinkedList<>();
        if (root == null) {
            return ret;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Character> list = new LinkedList<>();
            while (size > 0) {
                TreeNode cur = queue.poll();
                if (cur == null) {
                    return null;
                }
                list.add(cur.val);
                if (cur.left != null) {
                    queue.offer(cur.left);
                }
                if (cur.right != null) {
                    queue.offer(cur.right);
                }
                size--;
            }
            ret.add(list);
        }
        return ret;
    }

    /**
     * 判断一棵树是不是完全二叉树
     */
    boolean isCompleteTree(TreeNode root) {
        if (root == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode newRoot = queue.poll();
            if (newRoot == null) {
                break;
            }
            queue.offer(newRoot.left);
            queue.offer(newRoot.right);
        }
        while (!queue.isEmpty()) {
            TreeNode cur = queue.peek();
            if (cur == null) {
                queue.poll();
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * leecode
     * 236. 二叉树的最近公共祖先;
     * 给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。
     * 示例 1:
     * 输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
     * 输出：3
     * 解释：节点 5 和节点 1 的最近公共祖先是节点 3 。
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return null;
        }
        if (root == p || root == q) {
            return root;
        }
        TreeNode pNode = lowestCommonAncestor(root.left, p, q);
        TreeNode qNode = lowestCommonAncestor(root.right, p, q);
        if (pNode != null && qNode != null) {
            return root;
        } else if (pNode != null && qNode == null) {
            return pNode;
        } else {
            return qNode;
        }
    }

    public TreeNode lowestCommonAncestor2(TreeNode root, TreeNode p, TreeNode q) {
        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        getPath(root, stack1, p);
        getPath(root, stack2, q);
        int sizep = stack1.size();
        int sizeq = stack2.size();
        int len = 0;
        if (sizep > sizeq) {
            len = sizep - sizeq;
            while (len > 0) {
                stack1.pop();
                len--;
            }
        } else {
            len = sizeq - sizep;
            while (len > 0) {
                stack2.pop();
                len--;
            }
        }
        while (!stack1.isEmpty() && !stack2.isEmpty()) {
            if (stack1.peek() == stack2.peek()) {
                return stack1.peek();
            } else {
                stack1.pop();
                stack2.pop();
            }
        }
        return null;
    }

    boolean getPath(TreeNode root, Stack<TreeNode> stack, TreeNode toFind) {
        if (root == null || toFind == null) {
            return false;
        }
        stack.push(root);
        if (root == toFind) {
            return true;
        }
        boolean flag1 = getPath(root.left, stack, toFind);
        if (flag1) {
            return true;
        }
        boolean flag2 = getPath(root.right, stack, toFind);
        if (flag2) {
            return true;
        }
        stack.pop();
        return false;
    }

    /**
     * leecode;
     * 606. 根据二叉树创建字符串;
     * 给你二叉树的根节点 root ，请你采用前序遍历的方式，将二叉树转化为一个由括号和整数组成的字符串，返回构造出的字符串。
     * 空节点使用一对空括号对 "()" 表示，转化后需要省略所有不影响字符串与原始二叉树之间的一对一映射关系的空括号对。
     * 实例 1:
     * 输入：root = [1,2,3,4]
     * 输出："1(2(4))(3)"
     * 解释：初步转化后得到 "1(2(4)())(3()())" ，但省略所有不必要的空括号对后，字符串应该是"1(2(4))(3)" 。
     */
    String str = "";

    public String tree2str(TreeNode root) {
        if (root == null) {
            return null;
        }
        methond(root);
        return str;
    }

    private void methond(TreeNode root) {
        if (root == null) {
            return;
        }
        str += root.val;
        if (root.left != null) {
            str += "(";
            methond(root.left);
            str += ")";
        } else {
            if (root.right == null) {
                return;
            } else {
                str += "()";
            }
        }
        if (root.right != null) {
            str += "(";
            methond(root.right);
            str += ")";
        } else {
            return;
        }
    }

    /**
     * 非递归的前序遍历;
     */
    public void preorderNot(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                System.out.print(cur.val + " ");
                // 这里一直让 cur 往左子树走,一旦跳出循环,说明是,左子树为空了,需要后退一步,走柚子树;
                // 这就是为什么需要一个 Stack 来存储;
                cur = cur.left;
            }
            // 一旦跳出前面的循环了,说明一定是 cur 为空了,这个时候让 cur 回溯一步,让 cur 走右边;
            TreeNode top = stack.pop();
            cur = top.right;
        }
    }

    /**
     * 非递归的中遍历;
     */
    public void inorderNot(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                // 这里一直让 cur 往左子树走,一旦跳出循环,说明是,左子树为空了,需要后退一步,走柚子树;
                // 这就是为什么需要一个 Stack 来存储;
                cur = cur.left;
            }
            // 一旦跳出前面的循环了,说明一定是 cur 为空了,这个时候让 cur 回溯一步,让 cur 走右边;
            TreeNode top = stack.pop();
            // 这里打印的时间需要变换一下,这里是在前面跳出循环走到这一步的,说明,左子树已经完成了,现在栈顶的元素就是中,直接打印;
            System.out.print(top.val + " ");
            cur = top.right;
        }
    }

    /**
     * 非递归的后序遍历;
     */
    public void postorderNot(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        TreeNode prev = null;
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }
            TreeNode top = stack.peek();
            if (top.right == null || top.right == prev) {
                System.out.print(top.val + " ");
                stack.pop();
                prev = top;
            } else {
                cur = top.right;
            }
        }
    }
}
