package testdemo;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-15
 * Time:16:04
 */
public class TestDemo {
    static Object locker = new Object();

    static Object locker1 = new Object();
    public static void main(String[] args) throws InterruptedException {
        TestDemo testDemo1 = new TestDemo();
        TestDemo testDemo2 = new TestDemo();
        TestDemo testDemo3 = new TestDemo();
        Thread t1 = new Thread(() ->{
            try {
                System.out.println("t1 线程");
                testDemo1.method1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() ->{
            try {
                System.out.println("t2 线程");
                testDemo2.method2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(() ->{
            try {
                System.out.println("t3 线程");
                testDemo3.method2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
    public static synchronized void method1() throws InterruptedException {
            System.out.println("111");
            Thread.sleep(5000);
//            locker1.wait();
//            locker.wait();
    }

    public static synchronized void method2() throws InterruptedException {
        System.out.println("222");
        Thread.sleep(5000);
//            locker1.wait();
//            locker.wait();
    }
}
