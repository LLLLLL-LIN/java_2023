package testdemo; /**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-05
 * Time:17:36
 */

import java.util.*;

/**
 * 测试类;
 * < ? extends E> 和 <E>有什么区别呢 不都是能够接收E和E的子类吗?
 */
public class TestDemo03 {

//    public static void main(String[] args) {
//        List<Number> list1 = new ArrayList<>();
//        List<? extends  Number> list2 = new ArrayList<>();
//        Double dou1 = 22.2;
//        Integer int1 = 12;
//        list1.add(dou1);
//        list1.add(int1);
//        System.out.println(list1);
//
////        list2.add(dou1);
////        list2.add(int1);
//        System.out.println(list2);
//    }

    // 找到字符串中第一个不重复的字符的下标;
    public int firstUniqChar(String s) {
        if(s.length() == 0) {
            return -1;
        }
        Map<Character, Integer> hashMap = new HashMap<>();
        for(int i = 0; i < s.length(); i++) {
            hashMap.put(s.charAt(i), hashMap.getOrDefault(s.charAt(i), 0) + 1);
        }
        for (int i = 0; i < s.length(); i++) {
            if (hashMap.get(s.charAt(i)) == 1) {
                return i;
            }
        }
        return -1;
    }



    public static void main(String[] args) {
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("张三", 3);
        hashMap.put("李四", 4);
        hashMap.put("王五", 5);
        Set<Map.Entry<String, Integer>> set =  hashMap.entrySet();
    }
}
