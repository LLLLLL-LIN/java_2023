package testdemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-24
 * Time:11:08
 */

public class TestDemo01 {
    public static void main(String[] args) {
//        Stack<Integer> stack = new Stack<>();
//        System.out.println(stack.peek());

//        List<Number> num = new ArrayList<>();
//        Integer integer1 = 1;
//        int count = 2;
//        num.add(integer1);
//        num.add(count);
//        System.out.println(num);

//        // 数据超出范围;
//        int count = 1;
//        for(int i = 1; i <= 100; i++) {
//            count = count * i;
//        }
//        System.out.println("result: " + count);

        // 在 -127 ~ 128 的数据(Integer类型)都是放在一个长为 256 的数组中;
        // 由 x + (-(-127)) 来作为 index 存储 x;
        // 所以为什么在这个范围(-127 ~ 128)的数据返回的是 true 呢? 因为他们都是指向同一个数组;
        int s1 = 12;
        int s2 = 12;
        int s5 = 139;
        System.out.println("s1(12) == s2(12) : " + (s1 == s2));
        System.out.println("s1(12) == s5(13) : " + (s1 == s5));
        int s3 = 129;
        int s4 = 129;
        // int 类型也就是基础类型的 == 比较的话比较的是值;
        // 所以值相同就是 true;
        System.out.println("s3(129) == s4(129) : " + (s3 == s4));

        // Integer类型/引用类型,虽然在 -127~128 这个范围是有缓存数组,
        // 但是这个每个下标的数据类型还是 Integer,所以这里的 == 比较的还是数据的内存地址,而不是数组的内存地址;
        // 所以值即使在这个范围,数值不同的情况下,他的 == 比较的返回结果还是 false;
        Integer in1 = 12;
        Integer in2 = 12;
        Integer in5 = 13;
        System.out.println("in1(12) == in2(12) : " + (in1 == in2));
        System.out.println("in1(12) == in5(13) : " + (in1 == in5));
        Integer in3 = 129;
        Integer in4 = 129;
        // 这里 sout 调用的 toString() 实际上是将 Integer 类型的 s3 转换成 String 了;
        // 在调用 String 的 valueof() 的时候,再往底层走,实际上找的是 s3 的内存地址;
        System.out.println("in3(129) == in4(129) : " + (in3 == in4));
    }
}
