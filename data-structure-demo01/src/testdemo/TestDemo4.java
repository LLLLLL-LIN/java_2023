package testdemo;

import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-10
 * Time:17:28
 */
public class TestDemo4 {
    static int x = 100;

    public static void main(String[] args) throws InterruptedException {
//        TestDemo4 testDemo4 = new TestDemo4();
//        TestDemo4 testDemo5 = new TestDemo4();
//        testDemo4.x++;
//        testDemo5.x++;
//        System.out.println(x);

//        Integer i01=59;
//        int i02=59;
//        Integer i03=Integer.valueOf(59);
//        Integer i04=new Integer(59);
//        System.out.println(i02 == i04);

//        int x = -5;
//        int y = -12;
//        System.out.println(y % x);
//        Callable<String> callable = new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return "null";
//            }
//        };

//        // 线程工厂创建线程;
//        ThreadFactory threadFactory = new ThreadFactory() {
//            @Override
//            public Thread newThread(Runnable r) {
//                Thread t = new Thread(r);
//                return t;
//            }
//        };
//        Thread t = threadFactory.newThread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("hello world!");
//            }
//        });
//        t.start();
//
////        ExecutorService excutor = Executors.newCachedThreadPool();


//        // Future 和 FutureTask
//        FutureTask<String> strTask = new FutureTask<>(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return "hello world!";
//            }
//        });
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        Future<?> str = executorService.submit(strTask);
//        Thread.sleep(3000);
//        System.out.println(executorService.isTerminated());
//        try {
//            // 获取任务执行结果，此处会阻塞直到任务完成
//            String result = strTask.get();
//            System.out.println("Task result: " + result);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } finally {
//            // 关闭线程池
//            executorService.shutdown();
//        }

        // 继承的测试;
        Animal dog = new Dog("newDog", 10);
        dog.eat();
    }
}
class Animal {
    String name = "animal";
    int age = 12;
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void eat() {
        System.out.println("Animal eats");
    }
    public void move() {
        System.out.println("Animal move");
    }
}
class Dog extends Animal {
    String name = "dog";
    int age = 13;
    public Dog(String name, int age) {
        super(name, age);
        this.age = age;
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println("dog eat");
    }
    public void method1() {
        super.move();
    }
}

