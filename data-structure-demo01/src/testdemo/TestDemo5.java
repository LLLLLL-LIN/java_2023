package testdemo;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-15
 * Time:13:58
 */

import sun.reflect.generics.tree.Tree;

import java.util.*;

/**
 * 单例模式的 demo;
 */
public class TestDemo5 {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getSingleton();
    }
}

/**
 * 1:方法加锁;
 */
class SingletonClass {
    private static SingletonClass singletonClass = null;

    private SingletonClass() {
    }
    public static synchronized SingletonClass getSingletonClass() {
        if (singletonClass == null) {
            singletonClass = new SingletonClass();
        }
        return singletonClass;
    }
}

/**
 * 双重检测,为对象加锁,减少加锁的频率;
 */
class Singleton {
    // 采用 volatile 保证内存可见性;
    private static volatile Singleton singleton = null;
    private Singleton() {

    }
    public static Singleton getSingleton() {
        // 判断实例对象是否为空;
        if (singleton == null) {
            synchronized (Singleton.class) {
                // 怕在此期间这个 singleton 被修改了;
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }

}
