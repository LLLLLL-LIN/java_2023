package testdemo;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-03-30
 * Time:14:49
 */
// PriorityQueue 的测试;
class Student implements Comparable<Student>{
    int age;
    String name;

    public Student(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    // 调用 Ingeger 的 比较方法;
    public int compareTo(Student s2) {
        return Integer.compare(this.age, s2.age);
//        return (this.age < s2.age) ? -1 : ((this.age == s2.age) ? 0 : 1);
//        return Integer.valueOf(this.age).compareTo(Integer.valueOf(s2.age));
    }
}
public class TestDemo02 {
    public static void main(String[] args) {
        PriorityQueue<Student> priorityQueue = new PriorityQueue<>();
        Student s1 = new Student(12, "张三");
        // 添加第二个的 student,如果 student 没有实现 Comparable 接口则会报错;
        Student s2 = new Student(13, "李四");
        priorityQueue.offer(s1);
        priorityQueue.offer(s2);
        for (Student s : priorityQueue) {
            System.out.println(s.age + " / " + s.name);
        }
    }
}
