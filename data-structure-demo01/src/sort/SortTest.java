package sort;

import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-04
 * Time:16:49
 */
public class SortTest {
    public static void main(String[] args) {
        SortDemo01 sortDemo01 = new SortDemo01();
        sortDemo01.print();
        System.out.println();
//        sortDemo01.insert();
//        sortDemo01.shellSort();
//        sortDemo01.selectSort();
//        sortDemo01.bubbleSort();
//        sortDemo01.heapSort();
//        sortDemo01.selectSort2();
        sortDemo01.quickSort(0, sortDemo01.array.length - 1);
        sortDemo01.print();
    }
}
