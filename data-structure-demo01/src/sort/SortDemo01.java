package sort;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2024-04-04
 * Time:16:49
 */

import java.util.Arrays;

/**
 * 排序类;
 */
public class SortDemo01 {
    public final int[] array = {3, 6, 4, 5, 2, 1, 9, 8, 7, 10};

    /**
     * 直接插入排序;
     * 时间复杂度:
     * 最慢: O(N^2);
     * 最快: O(N);
     * 空间复杂度: O(1);
     * 稳定性:稳定的:
     */
    public void insert() {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    /**
     * 希尔排序;
     * 时间复杂度:
     * 空间复杂度: O(1);
     * 不稳定的;
     */
    public void shellSort() {
        int gap = array.length;
        while (gap > 1) {
            gap = gap / 2;
            shell(array, gap);
        }
    }

    private void shell(int[] array, int gap) {
        for (int i = gap; i < array.length; i++) {
            int tmp = array[i];
            int j = i - gap;
            for (; j >= 0; j -= gap) {
                if (array[j] < tmp) {
                    break;
                }
                array[j + gap] = array[j];
            }
            array[j + gap] = tmp;
        }
    }

    /**
     * 选择排序(单向的);
     * 时间复杂度: O(N^2);
     * 空间复杂度: O(1);
     */
    public void selectSort() {
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int tmp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = tmp;
        }
    }

    /**
     * 双向的选择排序;
     * 时间复杂度:
     * 空间复杂度:
     */
    public void selectSort2() {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int minIndex = left;
            int maxIndex = right;
            for (int j = left; j <= right; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
                if (array[j] > array[maxIndex]) {
                    maxIndex = j;
                }
            }
            swap(left, minIndex);
            // 这里防止第一个是最大值,导致 maxindex 一直不更新,还是在 left 的位置,然后在在执行完最小值的交换之后,
            // 后续再次执行 swap 操作,就会让最小值换到末尾去了;
            // 这里就让 maxindex 更新位置就行;
            if (maxIndex == left) {
                maxIndex = minIndex;
            }
            swap(right, maxIndex);
            left++;
            right--;
        }
    }

    /**
     * 冒泡排序;
     * 时间复杂度: O(N^2);
     * 在优化之后,最快情况下: O(N);
     * 空间复杂度: O(1);
     */
    public void bubbleSort() {
        // 优化操作;
        boolean flg = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    swap(j, j + 1);
                    flg = true;
                }
            }
            if (!flg) {
                break;
            }
        }
    }

    /**
     * 堆排序;
     * 时间复杂度: O(N * logN);
     * 空间复杂度: O();
     * 不稳定的排序;
     */
    public void heapSort() {
        for (int parent = (array.length - 1 - 1) / 2; parent >= 0; parent--) {
            shiftDown(parent);
        }
    }

    public void shiftDown(int parent) {
        int child = parent * 2 + 1;
        while (child < array.length) {
            if (child + 1 < array.length && array[child] > array[child + 1]) {
                child = child + 1;
            }
            if (array[parent] > array[child]) {
                int num = array[parent];
                array[parent] = array[child];
                array[child] = num;
                parent = child;
                child = 2 * parent + 1;
            } else {
                break;
            }
        }
    }

    /**
     * 快速排序(Hoare版);
     * 时间复杂度:
     *      最快: O(N * logN);
     *      最慢: O(N^2); 单边树;
     * 空间复杂度:
     *      最小: O(logN); 递归是先递归左边,在递归右边,同时开辟的空间就是数的高度;
     *      最大: O(N); 也就是单边树;
     * 不稳定的排序方法;
     */
    public void quickSort(int start, int end) {
        if (start >= end) {
            return;
        }
//        int pivot = pratitionHoare(start, end);
        int pivot = pratitionHole(start, end);
        quickSort(start, pivot - 1);
        quickSort(pivot + 1, end);
    }

    // Hoare法;
    private int pratitionHoare(int left, int right) {
        int tmp = array[left];
        int i = left;
        while (left < right) {
            // 这里很奇怪,得先写 right-- 这个循环,入股先写 left++ 这个循环就不行;
            // 原因:
            // 以为 left 停止的位置永远是比基准值大的位置,如果这个时候 left 停止,但是 right 没有停止,
            // 一直走到 left == right 才停止,这个时候停止的位置就是比基准值大的位置,这个位置就会和基准值的位置进行交换,
            // 这个较大的值就会交换到前面去了,所以得让 right 先走,right 停止了,让 left 去找 right;
            while (left < right && array[right] >= tmp) {
                // 判断条件得是 array[right] >= tmp 这个 " = " 号必须要有,否则会死循环;
                // 因为头尾的值相等的话,会 right 的值交换到 left,然后又交换到 right;
                right--;
            }
            while (left < right && array[left] <= tmp) {
                left++;
            }
            // Hoare法是等左右两边都走到比基准值大或者小的值的时候都停下来,交换值;
            swap(left, right);
        }
        swap(i, left);
        return left;
    }

    // 挖坑法;
    private int pratitionHole(int left, int right) {
        int tmp = array[left];
        int i = left;
        while (left < right) {
            while (left < right && array[right] >= tmp) {
                right--;
            }
            // 挖坑法是只要比基准值小/大就交换;而不是等到另一边找到一个比基准值大/小的值在交换;
            array[left] = array[right];
            while (left < right && array[left] <= tmp) {
                left++;
            }
            array[right] = array[left];
        }
        // 填坑,把基准值放到坑中;
        array[left] = tmp;
        return left;
    }

    /**
     * 数组的打印;
     */
    public void print() {
        System.out.println(Arrays.toString(array));
//        for (int j : array) {
//            System.out.print(j + " ");
//        }
    }

    /**
     * 交换数组中的任意两个位置的值;
     *
     * @param index1
     * @param index2
     */
    public void swap(int index1, int index2) {
        int tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;
    }
}
