package com.example.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:
 * Date:-09-12
 * Time:12:27
 */
@Aspect
@Component
public class UserAspect {

    // 切点 设置拦截的规则, 这里拦截的规则是 UserController 中的所有类, 也可以将括号中的值改一下, 匹配到方法;
    @Pointcut("execution(* com.example.demo.controller.UserController.*(..))")
    public void pointcut() {

    }

    @Before("pointcut()")
    public void beforeAdvice() {
        System.out.println("执行了前置通知!");
    }

    @After("pointcut()")
    public void afterAdivice() {
        System.out.println("执行了后置通知!");
    }

    @Around("pointcut()")
   public Object aroundAdivice(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("进入环绕通知!");
        Object obj = null;
        obj = joinPoint.proceed();
        System.out.println("退出环绕通知!");
        return obj;
    }
}
