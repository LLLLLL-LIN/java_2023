package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-12
 * Time:12:30
 */
@RestController
@RequestMapping("testaop1")
public class ArtController {

    @RequestMapping("/hi")
    public String sayHi(String name) {
        System.out.println("art的 sayHi()");
        return "hi artcontroller " + name;
    }

    @RequestMapping("/hello")
    public String sayHello() {
        System.out.println("art的 sayHello()");
        return "hello world! artcontroller";
    }
}
