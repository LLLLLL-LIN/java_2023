package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA
 * Description
 * User:Liaolin
 * Date:2023-09-12
 * Time:12:25
 */
@RestController
@RequestMapping("/testaop")
public class UserController {

    @RequestMapping("/hi")
    public String sayHi(String name) {
        System.out.println("user的 sayHi()");
        return "hi usercontroller " + name;
    }

    @RequestMapping("/hello")
    public String sayHello() {
        System.out.println("user的 sayHello()");
        return "hello world! uesrcontroller ";
    }
}
